
function dayNumToName(theDay) {
	switch(theDay)
	
	{
	case 0: return "Sunday";
	case 1: return "Monday";
	case 2: return "Tuesday";
	case 3: return "Wednesday";
	case 4: return "Thursday";
	case 5: return "Friday";
	case 6: return "Saturday";
	}
}

function dayNumToShortName(theDay) {
	text = dayNumToName(theDay);
	return text.substr(0, 3);
}

function loadWeatherFromCoordinates(lat, lon) {
	logDebug("loadWeatherFromCoordinates(" + lat + ", " + lon);
	var pointasstring=lat+","+lon;
	logDebug("pointasstring = " + pointasstring);
	geocoder.getLocations(pointasstring, setWeatherFromAddress);
}

function setWeatherFromAddress(response) {
	var texttodisplay = "";
	if (!response || response.Status.code != 200)
	{
		texttodisplay="No Country";
	}
	else
	{
		place = response.Placemark[0];
		logDebug(response + " - " + response.Placemark + " - " + place + " - " + place.address + " - " + place.AddressDetails.Country.AdministrativeArea + " - " + place.AddressDetails.Country.CountryName);
		logDebug(place.address);
	}
	//url = "googleWeatherWs.php?cityID="+ place.address + "&lang=" + lang;
	url = "/yrWeatherWs.php?cityID="+ place.address + "&lang=" + lang;
	logDebug("url = " + url);
	var ai = AJAXInteraction(url, null, setWeather, true);
	ai.doGet();
}


function setWeather(responseXML) {
	logDebug("setWeather(" + responseXML);
	xmlDoc=responseXML; 
	root=xmlDoc.documentElement;
	//logDebug("root = " + root + " - " + root.hasChildNodes);
	
	today = new Date();
	
	x=xmlDoc.getElementsByTagName("location")[0].childNodes;
	y=xmlDoc.getElementsByTagName("location")[0].firstChild;
	locationName = "";
	for (i=0;i<x.length;i++)
	{
		//logDebug(i + " - " + y.nodeName + "");
		if (y.nodeType==1)
		{
			if (y.nodeName == "name") {
				locationName = y.firstChild.nodeValue;
				break;
			}
		}
		y=y.nextSibling;
	}
	
	//document.getElementById("weatherLocationDiv").innerHTML += " - " + locationName;
		
	dayNum = 0;
	x=xmlDoc.getElementsByTagName("tabular")[0].childNodes;
	y=xmlDoc.getElementsByTagName("tabular")[0].firstChild;
	
	str = "<table cellspacing=0 cellpadding=0 width=90% border=0>";
	str += "<tr><td></td><td colspan=6><img src='/images/transp.gif' height=2 width=5></td></tr>";	
	str += "<tr><td></td><td colspan=6><b>" + locationName + "</b></td></tr>";	
	
	foundNow = false;
	period = 0;
					
	for (i=0;i<x.length;i++)
	{
		if (y.nodeType==1)
		{//Process only element nodes (type 1)
			//logDebug(i + " - " + y.nodeName + "");
			if (y.nodeName == "time") {
				fromDate = new Date(y.getAttribute("from"));
				toDate = new Date(y.getAttribute("to"));
				period = y.getAttribute("period");
				logDebug(dayNum + " - fromDate = " + fromDate);
				//logDebug(" - " + y.getAttribute("from") + " - " + fromDate + " - " + fromDate.getUTCHours());
				z=y.childNodes;
				for (j=0;j<z.length;j++) {
					if (z[j].nodeType == 1) {
						//logDebug(j + " - " + z[j].nodeName + " - " + z[j].nodeType);
						if (z[j].nodeName == "symbol") {
							//logDebug(" -- " + z[j].nodeName + " = " + z[j].getAttribute("number") + " - " + z[j].getAttribute("name"));
							symbolNumber = z[j].getAttribute("number");
							symbolName = z[j].getAttribute("name");
						}	
						else if (z[j].nodeName == "temperature") {
							//logDebug(" -- " + z[j].nodeName + " = " + z[j].getAttribute("unit") + " - " + z[j].getAttribute("value"));		
							temperatureValue = z[j].getAttribute("value");
						}
						else if (z[j].nodeName == "windDirection") {
							//logDebug(" -- " + z[j].nodeName + " = " + z[j].getAttribute("code") + " - " + z[j].getAttribute("name"));			
							windDirectionCode = z[j].getAttribute("code");
						}
						else if (z[j].nodeName == "windSpeed") {
							//logDebug(" -- " + z[j].nodeName + " = " + z[j].getAttribute("mps") + " - " + z[j].getAttribute("name"));		
							windSpeedMps = z[j].getAttribute("mps");
							windSpeedKmh = Math.round(windSpeedMps * 3.6);
						}
						
					}
				}
				//logDebug(dayNum + " < 4 && (" + fromDate.getDate() + " == " + today.getDate() + " || " + fromDate.getUTCHours() + " >= 11 || " + fromDate.getUTCHours() + " <= 14)");
				//logDebug(dayNum < 4 + " && (" + fromDate.getDate() == today.getDate() + " || " + fromDate.getUTCHours() >= 11 + " || " + fromDate.getUTCHours() <= 14);
				logDebug(dayNum + " - " + fromDate.getDate() + " == " + today.getDate() + " -> " + (fromDate.getDate() == today.getDate()) + " - " + period);
				if (dayNum < 4 && ((fromDate.getDate() == today.getDate()) || (period == 2))) {
					isToday = false;
					logDebug("foundNow = " + foundNow);
					if (!foundNow && fromDate.getDate() == today.getDate()) {
						day = "Today<br><font size=-2>" + formatNumber(fromDate.getUTCHours()) + "h00</font><!--" + foundNow + "-->";
						foundNow = true;						
						isToday = true;
					}
					else
						day = dayNumToShortName(fromDate.getDay());
						
					logDebug("isToday = " + isToday + " - " + fromDate.getDate() + " != " + today.getDate() + " -> " + (fromDate.getDate() != today.getDate()));
					if (isToday || (fromDate.getDate() != today.getDate())) {
						logDebug("New day");
						str += "<tr>";
						str += "<td><img src='/images/transp.gif' height=1 width=5></td>";
						str += "<td alt='" + fromDate + "'>" + day + "</td><td><img src='/images/weather/yr/" + formatNumber(symbolNumber) + "d.png' alt='" + symbolName + "' title='" + symbolName + "' width=38 height=38></td>";
						str += "<td><b>" + temperatureValue+ "&deg;C</b></td>";
						str += "<td><img src='/images/transp.gif' height=1 width=5></td>";
						str += "<td><img src='/images/wind/" + windDirectionCode + ".gif' alt='" + windSpeedMps + " m/s - " + windSpeedKmh + " km/h' title='" + windSpeedMps + " m/s - " + windSpeedKmh + " km/h' width=16 height=16></td>"
						str += "<td align=right>" + windSpeedKmh + " km/h</td>";
						str += "</tr>";
						
						dayNum++;
					}
				}
				
			}
		}
		y=y.nextSibling;
	}
	str += "</table>";
	document.getElementById("weatherdivInner").innerHTML = str;
}

function initWeatherWin() {
	logDebug("-> initWeatherWin()");
	logDebug("cityID = " + cityID);
	loadWeatherFromID(cityID)
}

function loadWeatherFromID(weatherID) {
	document.getElementById("weatherdivInner").innerHTML = "<table width=100% height=100%><tr><td align=center><img src='/images/ajaxLoader.gif'><p><?= getLangProp('wind.loading') ?><p>&nbsp;</td></tr></table>";
	if (weatherID != "" && weatherID != null && weatherID.charAt(0) != '+') {	
		//url = "googleWeatherWs.php?cityID="+cityID + "&lang=" + lang;
		url = "/yrWeatherWs.php?cityID="+weatherID + "&lang=" + lang;
		logDebug("url = " + url);
		var ai = AJAXInteraction(url, null, setWeather, true);
		ai.doGet();
	}
	else {
		logDebug("userHomeLat = " + userHomeLat + " - userHomeLng = " + userHomeLng);
		//loadWeather(userHomeLat, userHomeLng);
	}
}

function loadWeather(weatherLat, weatherLon) {
	logDebug("-> loadWeather(" + weatherLat + ", " + weatherLon);
	//url = "googleWeatherWs.php?lat="+ weatherLat + "&lon=" + weatherLon + "&lang=" + lang;
	url = "/yrWeatherWs.php?lat="+ weatherLat + "&lon=" + weatherLon + "&lang=" + lang;
	logDebug("url = " + url);
	var ai = AJAXInteraction(url, null, setWeather, true);
	ai.doGet();
}