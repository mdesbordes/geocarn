<?php include 'header.php' ?>
<?php include 'inc/country.php' ?>
<?php
if (isset($_GET['activityId']))
	$activityId = $_GET['activityId'];
else
	$activityId = -1;
?>

<script>
lang = "fr";

activityId = <?= $activityId; ?>;
if (activityId == null || activityId == "")
	activityId = -1;
var user = "<?= $userSession; ?>";

var map;
var openskimap;
var trackIdArray = [];
var tourLat = 0;
var tourLon = 0;
var trackData;
var weekData;

var markerArray = [];

var currentDate = new Date();
var season;
var year = currentDate.getFullYear();
var month = 1 + currentDate.getMonth();
var isInitDone = false;

var MA_CLE= "rhfiamuq2cejl2h4xgcmwpge";

google.load('visualization', '1', {packages: ['corechart', 'line']});
//google.setOnLoadCallback(init());

window.onresize = function() {
	logDebug("onresize");
    resizeDiv();
};

function init() {
	logDebug("-> init()");
	logDebug("year = " + year);
	resizeDiv();

	logDebug("activityId = " + activityId);
	typeFormSel = document.getElementById('typeFormMySelectOption');
	typeFormSel.options[0].selected;
	timeTypeFormSel = document.getElementById('timeTypeFormMySelectOption');
	timeTypeFormSel.options[0].selected;


	$.getJSON("activityStatWS.php?user=" + user + "&action=season&green=1&year=" + year + "&month=" + month + "&activity=" + activityId , function(data) {
		activityData = data.activities;
		setTitle(activityData);

		drawTable(activityData);
		isInitDone = true;
	});

	$.getJSON("tracksWS.php?track=season&green=1&year=" + year + "&month=" + month + "&limit=500&user=" + user + "&activity=" + activityId , function(data) {
		trackData = data.tracks;

		drawTrackTable(trackData);
		drawGraph("Duration", "Day");
	});

	$.getJSON("activityStatWS.php?user=" + user + "&year=" + year + "&action=week&green=1&activity=" + activityId , function(data) {
		weekData = data.activities;
	});

}

function resizeDiv() {
	logDebug("-> resizeDiv()");
	var width = window.innerWidth
		|| document.documentElement.clientWidth
		|| document.body.clientWidth;

	var height = window.innerHeight
		|| document.documentElement.clientHeight
		|| document.body.clientHeight;

	logDebug("width = " + width + " - height = " + height);
	//document.getElementById("maiwindow").style.height = (height - 340) + "px";
	//document.getElementById("maiwindow").style.width = width - 5 + "px";
	//document.getElementById("mapWin").style.height = (height - 340) + "px";
	document.getElementById("tableWin").style.height = (height - 340) + "px";
	document.getElementById("summaryWin").style.height = (height - 340) + "px";
	document.getElementById("graphWin").style.height =  "200px";
}

function setTitle(data) {
	logDebug("-> setTitle(" + data);
	var currentDate = new Date();
	//if (month >= 8)
	//	theYear = 1 + year;
	//else
		theYear = year;
	season = theYear;
	if (activityId == 1 || activityId == 2 || activityId == 6)
		season = (theYear-1) + " - " + theYear;

	if (activityId == -1)
		activityIcon = "all";
	else
		activityIcon = activityId;

	if (activityId == -1)
		seasonStr = "Year " + season;
	else
		seasonStr = "Season " + season;

	title = "<table><tr valign=middle>";
	title += "<td width=50><img src='/images/" + activityIcon + ".png' width=45 height=45></td>";
	title += "<td width=260 class='title' valign=middle>" + seasonStr + "</td>";

	title = title + "<form id=\"seasonForm\"><td align=right valign=middle>";
	title = title + "<select id=seasonFormMySelectOption onChange=\"e = document.getElementById('seasonFormMySelectOption');year=e.options[e.selectedIndex].value;month=1;init();\">";
	var currentDate = new Date();
	for (theYear = currentDate.getFullYear(); theYear >= 2005 ; theYear--) {
		if (theYear == year)
			selected = " selected";
		else
			selected = "";
		title = title + "<option value=" + theYear + selected + ">" + theYear + "</option>";
	}
	title = title + "</select></td></form>";

	title += "<td width=15>&nbsp;</td>";
	title = title + "<td>";
	title = title + "<a href=# onClick='handleClickCategory(-1); return false;'><img src='/images/all.png' width=45 height=45></a>&nbsp;&nbsp;";
	for (i = 0; i <= 16; i++)
		title = title + "<a href=# onClick='handleClickCategory(" + i + "); return false;'><img src='/images/" + i + ".png' width=45 height=45></a>&nbsp;&nbsp;";
	title = title + "</td>";

	title = title + "</tr></table>";
	$("#titleDiv").html(title);
	/*
	title = "<img src='/images/" + data[0].activityId + ".png' width=45 height=45>&nbsp;&nbsp;&nbsp;" + data[0].tourName;
	title = title + "<div class='toolbar' style='position: absolute; right: 0px; display: inline;'>";
	title = title + " &nbsp;&nbsp; <a href=# onClick='initGeoPortail()'>[GEO]</a>";
	title = title + " &nbsp;&nbsp; <a href=# onClick='initGoogleMaps()'>[GOOGLE]</a>";
	title = title + "</div>";
	$("#titleDiv").html(title);
	*/
	$("#subTitleDiv").html("");
}

function drawTable(activityData) {
	$("#summaryTable").html("");
	if (activityData != null && activityData.length > 0) {
		activitySum = 0;
		summin = 0;
		activityDist = 0;
		activityAltDiff = 0;

		for (var i = 0; i < activityData.length; i++) {
			rowData = activityData[i];
			activitySum += new Number(rowData.activitySum);
			summin += new Number(rowData.summin);
			activityDist += new Number(rowData.activityDist);
			activityAltDiff += new Number(rowData.activityAltDiff);
		}
		var c = [];
		timeSpent = secondsToHms(summin *60);
		co2 = Math.round(activityDist * CO2_PER_KM); // 200 g/CO2/km
		price = Math.round(activityDist * EURO_PER_KM); // 0.5€/km
		c.push("<tr height=100><td align=center valign=middle width=64><img src='/images/CO2.png' width=64></td><td>&nbsp;</td><td align=center class='dataHuge' valign=middle>" + activitySum + "<div class='bigunit'> Kg CO2</div></td></tr>");
		c.push("<tr height=100><td align=center valign=middle width=64><img src='/images/bicycle.png' width=64></td><td>&nbsp;</td><td align=center class='dataHuge' valign=middle>" + Math.round(activityDist) + "<div class='bigunit'> km</div></td></tr>");
		c.push("<tr height=100><td align=center valign=middle width=64><img src='/images/coin.png' width=64></td><td>&nbsp;</td><td align=center class='dataHuge' valign=middle>" + price + "<div class='bigunit'> €</div></td></tr>");
		
		$("#summaryTable").html(c.join(""));
	}
	else {
		$("#summaryTable").html("<tr><td class='title' align=center>No tracks for this category during the season " + season + "</td></tr>");
	}
}

function drawRow(rowId, rowData) {
	var c = [];
	timeSpent = secondsToHms(rowData.summin *60);
	/*
	if (timeSpent < 60)
		timeSpent = timeSpent + " min";
	else
		timeSpent = Math.round(timeSpent / 60) + " h";
		*/
	//var row = $("<tr />");
    //$("#tracksTable").append(row); //this will append tr element to table... keep its reference for a while since we will add cels into it
    c.push("<tr height=60><td align=center class='dataBig' valign=middle>" + rowData.activitySum + "<div class='unit'> tracks</div></td>");
	c.push("<td align=center class='dataBig' valign=middle>" + timeSpent + "</td>");
	c.push("<td align=center class='dataBig' valign=middle>" + Math.round(rowData.activityDist) + "<div class='unit'> km</div></td>");
	if (activityId != 2 && activityId != 4 && activityId != 8)
		c.push("<td align=center class='dataBig' valign=middle>" + rowData.activityAltDiff + "<div class='unit'> m</div></td>");
	c.push("</tr>");

	$("#summaryTable").html(c.join(""));
}

function drawTrackTable(data) {
	logDebug("drawTrackTable(" + data);
	$("#tracksTable").html("");
	if (data != null && data.length > 0) {
		var row = $("<tr width=100% />")
		$("#tracksTable").append(row);
		row.append($("<td width=20><img src=images/transp.gif width=20 height=1></td><td width=85><img src=images/transp.gif width=85 height=1></td><td width=60%></td><td width=25><img src=images/transp.gif width=25 height=1></td><td width=65><img src=images/transp.gif width=65 height=1></td><td width=50><img src=images/transp.gif width=50 height=1></td><td width=50><img src=images/transp.gif width=50 height=1></td>"));
		for (var i = 0; i < data.length; i++) {
			drawTrackRow(i, data[i]);
		}

		$("#tracksTable td").addClass("tracksTableIndex");
		$("#tracksTable > tbody > tr:odd").addClass("rowOdd");
		$("#tracksTable > tbody > tr:not(.odd)").addClass("rowEven");
	}
}

function drawTrackRow(rowId, rowData) {
	logDebug("drawTrackRow(" + rowId);
	var row = $("<tr />")
    $("#tracksTable").append(row); //this will append tr element to table... keep its reference for a while since we will add cels into it
    row.append($("<td align=center><a href='activity.php?activityId=" + rowData.activityId + "'><img src='/images/" + rowData.activityId + ".png' width=20 height=20></a></td>"));

	if (rowData.activityId > 0)
		row.append($("<td align=center><a href='track.php?trackId=" + rowData.trackId + "'>" + shortDate(rowData.trackDate, 10) + "</a></td>"));
	else
		row.append($("<td align=center>" + shortDate(rowData.trackDate, 10) + "</td>"));

	if (rowData.trackName != "")
		trackName = rowData.trackName;
	else
		trackName = "";
	if (rowData.activityId > 0) {
		if (trackName != "")
			row.append($("<td><a href='tour.php?tourId=" + rowData.tourId + "'>"  + rowData.tourName + "</a> / <a href='track.php?trackId=" + rowData.trackId + "'>" + trackName + "</a></td>"));
		else
			row.append($("<td><a href='tour.php?tourId=" + rowData.tourId + "'>"  + rowData.tourName + "</a></td>"));
	}
	else {
		if (trackName != "")
			row.append($("<td>" + rowData.tourName + " / " + trackName + "</td>"));
		else
			row.append($("<td>" + rowData.tourName + "</td>"));
	}

	row.append($("<td width=85 align=center><img src='/images/flags/24/" + (rowData.country).toLowerCase() + ".png'></td>"));

	row.append($("<td align=center>" + rowData.trackDuration + "</td>"));

	if (rowData.activityId > 0)
		row.append($("<td align=center>" + rowData.trackDistance + "</td>"));
	else
		row.append($("<td></td>"));

	if (activityId != 2 && activityId != 4 && activityId != 8)
		row.append($("<td align=center>" + rowData.trackAltGain + "</td>"));
	else
		row.append($("<td></td>"));
}

function handleClickCategory(categoryId) {
	activityId = categoryId;
	init();
}

function drawGraph(type, timeType) {
	logDebug("-> drawGraph(" + type + ", " + timeType);

	$("#graphWinInner").html("");

	var gdata = new google.visualization.DataTable();

	if (timeType == "Day" || timeType == "Total") {
		graphData = trackData;
		gdata.addColumn('date', 'Date');
	}
	else {
		graphData = weekData;
		gdata.addColumn('number', 'Week');
	}
	gdata.addColumn('number', type);
	gdata.addColumn({type: 'string', role: 'tooltip', 'p': {'html': true}});

	if (graphData != null && graphData.length > 0) {

		dataIndex = 0;
		totalValue = 0;
		for (var i = (graphData.length - 1); i >= 0; i--) {
			rowData = graphData[i];
			if (timeType == "Day" || timeType == "Total") {
				trackIdArray[i] = rowData.trackId;

				datetime = rowData.trackDate;

				trackAverageSpeed = Math.round(rowData.trackAverageSpeed * 100) / 100;
				if (trackAverageSpeed == 0) trackAverageSpeed = "";
				trackAverageUpSpeed = rowData.trackAverageUpSpeed;
				if (trackAverageUpSpeed == 0) trackAverageUpSpeed = "";
				trackDistance = rowData.trackDistance;
				if (trackDistance == 0) trackDistance = "";
				trackAltDiff = rowData.trackAltGain;
				if (trackAltDiff == 0) trackAltDiff = "";
				trackDuration = rowData.trackDuration;
				trackDuration = Math.round(hmsToSecondsOnly(trackDuration) / 36) / 100;

				name = rowData.tourName;
				if (rowData.trackName != null && rowData.trackName != "")
					name = name + " / " + rowData.trackName;

				formatStr = 'dd/MM';
				theDate = new Date(shortDate(datetime, 10));
			} else {
				trackDuration = Math.round((rowData.summin / 60) * 100) / 100;
				trackDistance = Math.round(rowData.sumDistance * 100) / 100;
				trackAltDiff = rowData.sumAltGain;

				name = "";

				formatStr = '#';
				theDate = parseFloat(rowData.week);
			}

			if (type == "Duration") {
				value = trackDuration;
				valueToolTip = secondsToHms(trackDuration * 3600);
				unit = "";
				legend = "Duration (h)";
			}
			else if (type == "Distance") {
				value = trackDistance;
				valueToolTip = value;
				unit = "km";
				legend = "Distance (" + unit + ")";
			}
			else if (type == "Altitude gain") {
				value = trackAltDiff;
				valueToolTip = value;
				unit = "m";
				legend = "Altitude gain (" + unit + ")";
			}

			if (value != "" && !isNaN(value)) {

				totalValue += parseFloat(value);
				//alert();
				if (name != "")
					tooltip = $.datepicker.formatDate( "dd-mm-yy",new Date(theDate)) + "<br>" + name + "<br><b>" + valueToolTip + "</b> " + unit;
				else
					tooltip = "Week " + theDate + "<br><b>" + valueToolTip + "</b> " + unit;

				if (timeType == "Total")
					value = totalValue;

				console.log("-> " + dataIndex + ", " + theDate + ", " + parseFloat(value));
				gdata.addRow([theDate, parseFloat(value), tooltip]);
				dataIndex++;
			}
		}

		var options = {
			bar: {groupWidth: 10},
			hAxis: {
			  title: '',
			  format: formatStr
			},
			vAxis: {
			  minValue: 0,
			  title: legend
			},
			backgroundColor: '#FFFFFF',
			tooltip: { isHtml: true },
			animation:{
				duration: 1000,
				easing: 'out',
			}
		};

		if (timeType == "Day" || timeType == "Week")
			chart = new google.visualization.ColumnChart(document.getElementById('graphWinInner'));
		else
			chart = new google.visualization.LineChart(document.getElementById('graphWinInner'));
		chart.draw(gdata, options);
	  }
}


</script>

<body onLoad="init();">

<?php include 'bodyHeader.php' ?>

<div id="titleDiv" class="title" style="width: 100%, height: 45px; position: relative; top: 45px;"></div>
<div id="titleDivToolBar" class="toolbar" style="width: 100px; height: 45px; position: absolute; top: 45px; right: 0px;"></div>

<div id="maiwindow" class="maiwindow" style="position: relative; top: 45px; left: 0px; height: 60%">
<table width=100% height=100%>
	<tr height=70%>
		<td width=50% height=100%>
			<DIV id="summaryWin" class="window" style="width: 100%; height: 100%; z-index : 3; overflow:hidden;">
				<div id="" class="windowtopbargreen" style="position: relative; top: 0px; left: 0px; height=15px; width: 100%">
				<table width=100% height=100% cellpadding=0 cellspacing=0 border=0><tr>
					<td class="windowtopbargreen" width=35%>Summary</td>
					<td class="windowtopbargreen" width=1><img src="images/transp.gif" height=16 width=1></td>
					</tr>
				</table>
				</div>
				<div id="mainWinInner" class="innerwindow" style="position: relative; top: 0px; width: 100%; height: 100%; overflow:hidden;">
					<table width=100% height=100%>
						<tr>
							<td width=240 align=center valign=middle>
								<img src=/images/recycling.png width=240>
							</td>
							<td width=50% align=center valign=middle>
								<table width=100% height=100% id="summaryTable" class="summaryTable">
								  <tr>
									<td align=center><img src='images/ajaxLoader.gif'> <p>
									  <p>&nbsp;</td>
								  </tr>
								</table>
							</td>
						</tr>
					</table>
				  </div>
				</div>
			</div>
		</td>

		<td width=5><img src="images/transp.gif" height=5 width=1></td>

		<td width=50% height=100%>			
			<DIV id="tableWin" class="window" style="width: 100%; height: 100%; z-index : 3; overflow:hidden;">
			<div id="" class="windowtopbargreen" style="position: relative; top: 0px; left: 0px; height=15px; width: 100%">
			<table width=100% cellpadding=0 cellspacing=0 border=0><tr>
				<td class="windowtopbargreen" width=35%>Tracks</td>
				<td class="windowtopbargreen" width=1><img src="images/transp.gif" height=16 width=1></td>
				<td class="windowtopbargreen" width=65% align=right>
				<img src="images/help.gif" width=15 height=15 border=0 title="Main window" alt="Main window">
				</td></tr>
			</table>
			</div>
			<div id="mainWinInner" class="innerwindow" style="position: relative; top: 0px; width: 100%; height: 100%; overflow:scroll;">
				<table width=100% id="tracksTable" class="tracksTable">
				  <tr>
					<td align=center><img src='images/ajaxLoader.gif'> <p>
					  <p>&nbsp;</td>
				  </tr>
				</table>
			  </div>
			</div>
		</td>

	</tr>

	<tr height=30%>
		<td width=100% height=200 colspan=3>

			<DIV id="graphWin" class="window" style="width: 100%; height: 100%; z-index : 3; overflow:hidden;">
				<div id="" class="windowtopbargreen" style="position: relative; top: 0px; left: 0px; height=15px; width: 100%">
				<table width=100% cellpadding=0 cellspacing=0 border=0><tr>
					<td class="windowtopbargreen" width=35%>Graph</td>
					<td class="windowtopbargreen" width=1><img src="images/transp.gif" height=16 width=1></td>
					<form id="typeForm"><td class="windowtopbargreen" width=65% align=right valign=middle>
						<select id=typeFormMySelectOption onChange="typeFormSel = document.getElementById('typeFormMySelectOption'); timeTypeFormSel = document.getElementById('timeTypeFormMySelectOption'); drawGraph(typeFormSel.options[typeFormSel.selectedIndex].value , timeTypeFormSel.options[timeTypeFormSel.selectedIndex].value);">
							<option value="Duration" selected>Duration</option>
							<option value="Distance">Distance</option>
							<option value="Altitude gain">Altitude gain</option>
						</select>
						<select id=timeTypeFormMySelectOption onChange="typeFormSel = document.getElementById('typeFormMySelectOption'); timeTypeFormSel = document.getElementById('timeTypeFormMySelectOption'); drawGraph(typeFormSel.options[typeFormSel.selectedIndex].value , timeTypeFormSel.options[timeTypeFormSel.selectedIndex].value);">
							<option value="Day" selected>by day</option>
							<option value="Week">by week</option>
							<option value="Total">total</option>
						</select>
					</td></form>
					<td class="windowtopbargreen" align=right width=15>
					<img src="images/help.gif" width=15 height=15 border=0 title="Main window" alt="Main window">
					</td></tr>
				</table>
				</div>
				<div id="graphWinInner" class="innerwindow" style="position: relative; top: 0px; width: 100%; height: 180; overflow:hidden;">
					<table width=100% class="tracksTable">
					  <tr>
						<td align=center><img src='images/ajaxLoader.gif'> <p>
						  <p>&nbsp;</td>
					  </tr>
					</table>
				</div>
			</div>

		</td>
	</tr>

</table>
</div>

<?php include 'bodyFooter.php' ?>

</body>
</html>
