<?php include 'inc/logger.php' ?>
<?php
require_once "xmlparser.php";

session_start();

//require_once "ws/earthToolsWS.php";

// http://www.earthtools.org/height/45.234646055167545/5.803313255310059
$server = $_SERVER['SERVER_NAME'];
$DOMAIN = "http://" . $server;

function parseGpx($uploadfile) {
	logDebug("-> parseGpx(" . $uploadfile);
	
	// /var/lib/openshift/543693d05973ca1e3c00024c/app-root/data/gpx/
	//echo "<br>elevationws = $elevationws<p>";
					
	global $DOMAIN;
					
	//$fileUrl = "$DOMAIN/$uploadfile";
	$fileUrl = $uploadfile;
	//echo "Parsing file $fileUrl...<p>";
	//$xmldata = file_get_contents($fileUrl);
	$fh = fopen($fileUrl, 'r+');
	$xmldata = fread($fh, filesize($fileUrl));
	fclose($fh); 
	$parser = new XMLParser($xmldata);
	$parser->Parse();
	$parserDoc = $parser->document;
	
	$latArray;
	$lonArray;
	$elevationArray;
	$timeArray;
	$index = 0;

	//Echo the plot of the first <movie>
	foreach($parser->document->tagChildren as $child) {
		$tag = $child->tagData;
		$tagNameStr = $child->tagName;
		
		//echo "<p>- TAG = $tagNameStr = $tag <p>";
		foreach($child->tagChildren as $subchild) {
			if(is_object($subchild)) {
				$suboutName = $subchild->tagName;
				$subout = $subchild->tagData;
				//echo "<br>1 -&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;- $suboutName = $subout";
				
				foreach($subchild->tagChildren as $subchild2) {
					$latitude = 0;
					$longitude = 0;
					$elevation = 0;
					$time = 0;
			
					if(is_object($subchild2)) {
						$suboutName = $subchild2->tagName;
						$subout = $subchild2->tagData;
						//echo "<br>2 -&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;- $suboutName = $subout";
						if ($suboutName == 'trkpt') {
							$latitude = $subchild2->tagAttrs['lat'];
							$longitude = $subchild2->tagAttrs['lon'];
							//echo "<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;- latitude = $latitude";
							//echo "<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;- latitude = $longitude";						
						}
						foreach($subchild2->tagChildren as $subchild3) {
							if(is_object($subchild3)) {
								$suboutName = $subchild3->tagName;
								$subout = $subchild3->tagData;
								//echo "<br>3 -&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;- $suboutName = $subout";
								if ($suboutName == 'time')
									$time = $subout;
								else if ($suboutName == 'ele')
									$elevation = $subout;
							}
						}
					}
					//echo "<br>&nbsp;Point";
					//echo "<br>&nbsp;- time = $time";
					//echo "<br>&nbsp;- latitude = $latitude";
					//echo "<br>&nbsp;- latitude = $longitude\n\n";	
					//echo "<br>&nbsp;- elevation = $elevation\n\n";	
					if ($latitude != 0 && $longitude != 0) {
						$latArray[$index] = $latitude;
						$lonArray[$index] = $longitude;
						$elevationArray[$index] = $elevation;
						$timeArray[$index] = getDateTime($time); 
						//echo "$latArray<br>\n";
						$index++;
					}
				}			
			}
		}	
	}
	
	//if ($elevationws == 1)
	//	$elevationArray = getElevation($latArray, $lonArray);
	
	$distArray = calcDistance($latArray, $lonArray);
	
	$_SESSION['latArray'] = $latArray;
	$_SESSION['lonArray'] = $lonArray;
	$_SESSION['timeArray'] = $timeArray;
	$_SESSION['elevationArray'] = $elevationArray;
	$_SESSION['distArray'] = $distArray;
	
	//showPointsOnMap($latArray, $lonArray, $timeArray, $elevationArray, $distArray);
	return getPointJSON($latArray, $lonArray, $timeArray, $elevationArray, $distArray);	
}

// 2007-03-10T15:54:00Z
function getDateTime($timeStr) {
	$year = substr($timeStr, 0, 4);
	$month = substr($timeStr, 5, 2);
	$day = substr($timeStr, 8, 2);
	$hour = substr($timeStr, 11, 2);
	$min = substr($timeStr, 14, 2);
	$sec = substr($timeStr, 17, 2);
	//date_default_timezone_set('UTC');	
	if ($year > 0 && $month > 0 && $day > 0 && $hour >= 0 && $min >= 0 && $sec >=0)
		$time = mktime($hour, $min, $sec, $month, $day, $year);
	else
		$time = 0;
	return $time;
}

function getElevation($latArray, $lonArray) {
	$elevationArray = array();
	$elevation = 0;
	$index = 0;
	for ($i = 0; $i < count($latArray); $i++) {		
		//echo "<hr>$i - $index\n";
		if ($index == 0) {
			$retry = 0;
			$elevation = 0;
			echo "<br>$i - $elevation - $retry\n";
			while ($elevation == 0 && $retry < 3) {
				echo "<br>-> getElevationWS($latArray[$i], $lonArray[$i])\n";
				$elevation = getElevationWS($latArray[$i], $lonArray[$i]);
				echo "<br>-> $elevation ($retry)\n";
				$retry++;
				// Wait 1,5 sec
				usleep(1500000);
			}
			echo "<p>$i --> $elevation<p>\n";		
			
		}
		$elevationArray[$i] = $elevation;
		$index++;
		if ($index > 10)
			$index = 0;
	}
	return $elevationArray;
}

function calcDistance($latArray, $lonArray) {
	$count = count($latArray);
	$distArray = array();
	$dist = 0;
	$lat1 = $latArray[0];
	$lon1 = $lonArray[0];
	$distArray[0] = 0;
	for ($i = 1; $i < $count; $i++) {
		$lat2 = $latArray[$i];
		$lon2 = $lonArray[$i];
		if ($lat1 != $lat2 || $lon1 != $lon2) {
	    	$x = (sin($lat1/57.2958) * sin($lat2/57.2958)) + (cos($lat1/57.2958) * cos($lat2/57.2958) * cos($lon2/57.2958 - $lon1/57.2958));
			$thisDist = 6378.7 * atan(sqrt(1-$x*$x)/$x);
		}
		else {
			$thisDist = 0;
		}
		$distArray[$i] = $thisDist;
		$dist = $dist + $thisDist;
		$lat1 = $lat2;
		$lon1 = $lon2;	
	}
	
	return $distArray;
}

function showPointsOnMap($latArray, $lonArray, $timeArray, $elevArray, $distArray) {
	$count = count($latArray);
	echo "<script>\n";
	echo "// $count points\n";
	echo "var lat = new Array(); \n";
	echo "var lon = new Array(); \n";
	echo "var time = new Array(); \n";
	echo "var ele = new Array(); \n";
	echo "var dist = new Array(); \n";
	for ($i = 0; $i < $count; $i++) {
		$lat = $latArray[$i];
		$lon = $lonArray[$i];
		$time = $timeArray[$i];
		$elev = $elevArray[$i];
		$dist = $distArray[$i];
		echo "lat[$i] = $lat;\n";
		echo "lon[$i] = $lon;\n";	
		echo "time[$i] = $time;\n";	
		echo "ele[$i] = $elev;\n";		
		echo "dist[$i] = $dist;\n";
	}
	echo "window.parent.showPoints(lat, lon, time, ele, dist);\n";
	echo "</script>\n";
}

function getPointJSON($latArray, $lonArray, $timeArray, $elevArray, $distArray) {
	$count = count($latArray);
	$str = "{ \"track\":  [";
	for ($i = 0; $i < $count; $i++) {
		if ($i > 0)
			$str .= ",";
		$lat = $latArray[$i];
		$lon = $lonArray[$i];
		$time = $timeArray[$i];
		$elev = $elevArray[$i];
		$dist = $distArray[$i];
		$str .= " {";
		$str .=  "\"lat\" : \"$lat\",\n";
		$str .=  "\"lon\" : \"$lon\",\n";	
		$str .=  "\"time\" : \"$time\",\n";	
		$str .=  "\"ele\" : \"$elev\",\n";		
		$str .=  "\"dist\" : \"$dist\"\n";
		$str .= "}";
	}
	$str .= "] }";
	echo $str;
}


header('Content-type: text/xml;  application/json', true);

if (isset($_REQUEST['fileId']) && $_REQUEST['fileId'] != '') {
	$fileId = $_REQUEST['fileId'];
}
else {
	$fileId = $_SESSION['uploadfile'];
}
logDebug("fileId = " . $fileId);
parseGpx($fileId);

//echo "$latArray<p>";
//echo $_SESSION['uploadfile'];

$latArray2 = $_SESSION['latArray'];

exit;

//echo "$latArray2<p>";

?>