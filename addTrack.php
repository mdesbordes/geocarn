<?php include 'header.php' ?>
<?php include 'inc/language.php' ?>
<?php include 'inc/country.php' ?>
<?php include 'inc/strava.php' ?>
<?php
if (isset($_GET['action']))
	$action = $_GET['action'];
else
	$action = null;

if (isset($_GET['fileId']))
	$fileId = $_GET['fileId'];
else
	$fileId = null;

if (isset($_SESSION['uploadfile']))
	$fileName = $_SESSION['uploadfile'];
else
	$fileName = null;

$currentDate = date('Y/m/d', time());
?>

<script>
var map = null;
var planned = false;
var stravaActivities = null;
</script>

<script src=addTourInc.js LANGUAGE="JavaScript"></script>

<script>
lang = "fr";

var tourId = -1;
var action = "<?= $action; ?>";
var fileId = "<?= $fileId; ?>";
var fileName = "<?= $fileName ?>";
var user = "<?= $_SESSION['user'] ?>";
var map = null;
var mapSize = "NORMAL";

function init() {
	console.log("-> init( ) ");
	console.log("action = " + action);
	if (action != "manual" && action != "plan" && action != "strava") {
		uploadDone("<?= $fileId; ?>");
	}
	if (action == "plan")
		planned = true;
	if (action == "strava") {
		initStravaActivities();		
	}
	
	this.document.tourform.planned.value = planned;

	console.log("before");
	initGoogleMaps();
	console.log("after");
	console.log("<- init()");
}

function resizeMap() {
	var width = window.innerWidth
		|| document.documentElement.clientWidth
		|| document.body.clientWidth;
	width = width - 40;

	var height = window.innerHeight
		|| document.documentElement.clientHeight
		|| document.body.clientHeight;

	document.getElementById("mainWinMap").style.height =  (height-30) + "px";
	document.getElementById("viewerDiv").style.height = (height-30) + "px";
	google.maps.event.trigger(map, 'resize');
	document.getElementById("mainWinTable").style.height = (height-30) + "px";
	
	document.getElementById("mainWinMap").style.width =  (width - 15 - 350 - 520) + "px";
	document.getElementById("viewerDiv").style.width = "100%";
	google.maps.event.trigger(map, 'resize');
	
	console.log("mainWinTable = " + document.getElementById("mainWinTable").style.height);
	mapSize = "NORMAL";
}

function maxMinMap() {
	var width = window.innerWidth
		|| document.documentElement.clientWidth
		|| document.body.clientWidth;
	width = width - 40;

	var height = window.innerHeight
		|| document.documentElement.clientHeight
		|| document.body.clientHeight;

	if (mapSize == "NORMAL") {
		document.getElementById("mainWinMap").style.width = (width - 520 - 40) + "px";
		document.getElementById("viewerDiv").style.width = "100%";
		google.maps.event.trigger(map, 'resize');
		document.getElementById("mainWinTable").style.width = "20px";
		mapSize = "MAX";
	}
	else {
		document.getElementById("mainWinMap").style.width =  (width - 15 - 350 - 520) + "px";
		document.getElementById("viewerDiv").style.width = "100%";
		google.maps.event.trigger(map, 'resize');
		document.getElementById("mainWinTable").style.width = "602px";
		mapSize = "NORMAL";
	}
}

function initStravaActivities() {
	console.log("-> initStravaActivities()");
	$.getJSON("stravaWs.php?action=activities", function(data) {
		if (data != null) {
			stravaActivities = data;
			var $select = $('#stravaActivityList');
			$select.empty();
			$select.append('<option value=-1>Select an activity</option>');			
			console.log(data.length);
			for (i = 0; i < data.length; i++) {
				console.log(data[i].name);
				$select.append('<option value=' + i + '>' + dateToYmd(data[i].start_date) + " - " + shortText(data[i].name, 30, null) + '</option>');			
			}
		}
	});
}

function selectStravaActivity() {
	var stravaActivityList = document.getElementById("stravaActivityList");
	var selectedValue = stravaActivityList.options[stravaActivityList.selectedIndex].value;
	console.log("selectedValue = " + selectedValue);
	if (selectedValue > -1) {
		console.log(stravaActivities[selectedValue].id);
		console.log(stravaActivities[selectedValue].start_date);
		console.log(stravaActivities[selectedValue].name);
		console.log(stravaActivities[selectedValue].distance);
		console.log(stravaActivities[selectedValue].moving_time);
		console.log(stravaActivities[selectedValue].total_elevation_gain);
		console.log(stravaActivities[selectedValue].average_speed);
		console.log(stravaActivities[selectedValue].max_speed);
		
		$('#stravaid').val(stravaActivities[selectedValue].id);
		$('#trackname').val(stravaActivities[selectedValue].name);
		/*
		$('#distance').val(roundNumber((stravaActivities[selectedValue].distance / 1000), 100));		
		$('#timediff').val(secondsToHms(stravaActivities[selectedValue].moving_time));
		$('#altgain').val(stravaActivities[selectedValue].total_elevation_gain);
		$('#averagespeed').val(roundNumber(stravaActivities[selectedValue].average_speed*mSecToKmH, 100));
		$('#altmin').val(stravaActivities[selectedValue].elev_low);
		$('#altmax').val(stravaActivities[selectedValue].elev_high);	
		*/
		$('#date').val(dateToYmd(stravaActivities[selectedValue].start_date));					
		$.getJSON("stravaWs.php?action=points&activity=" + stravaActivities[selectedValue].id, function(data) {
			if (data != null) {
				stravaActivitySelected(data);
			}
		});
	}
}

// Definition url des services Geoportail
function geoportailLayer(name, key, layer, options)
{ var l= new google.maps.ImageMapType
  ({ getTileUrl: function (coord, zoom)
      {  return "https://wxs.ign.fr/" + key + "/geoportail/wmts?LAYER=" + layer
          + "&EXCEPTIONS=text/xml"
          + "&FORMAT="+(options.format?options.format:"image/jpeg")
          + "&SERVICE=WMTS&VERSION=1.0.0&REQUEST=GetTile"
          + "&STYLE="+(options.style?options.style:"normal")+"&TILEMATRIXSET=PM"
          + "&TILEMATRIX=" + zoom
          + "&TILECOL=" + coord.x + "&TILEROW=" + coord.y;
      },
    tileSize: new google.maps.Size(256,256),
    name: name,
    minZoom: (options.minZoom ? options.minZoom:0),
    maxZoom: (options.maxZoom ? options.maxZoom:18)
  });
  l.attribution = ' &copy; <a href="http://www.ign.fr/">IGN-France</a>';
  return l;
}
// Ajout de l'attribution Geoportail a la carte
function geoportailSetAttribution (map, attributionDiv)
{ if (map.mapTypes.get(map.getMapTypeId()) && map.mapTypes.get(map.getMapTypeId()).attribution)
  {  attributionDiv.style.display = 'block';
    attributionDiv.innerHTML = map.mapTypes.get(map.getMapTypeId()).name
      +map.mapTypes.get(map.getMapTypeId()).attribution;
  }
  else attributionDiv.style.display = 'none';
}


function initGoogleMaps() {
	console.log("-> initGoogleMaps(");
	var mapOptions = {
		mapTypeId: 'OSM',
	  center: { lat: 45, lng: 6},
	  zoom: 8,
		mapTypeControlOptions: {
			mapTypeIds: ['carte', google.maps.MapTypeId.TERRAIN, google.maps.MapTypeId.SATELLITE, 'OSM', 'OCM', 'Nautical SWE', 'OpenSeaMap'],
			style:google.maps.MapTypeControlStyle.DROPDOWN_MENU
		},
	};
	map = new google.maps.Map(document.getElementById('viewerDiv'), mapOptions);
	console.log("map1 = " + map);

	/** Definition des couches  */
  // Carte IGN
  map.mapTypes.set('carte', geoportailLayer("IGN", MA_CLE, "GEOGRAPHICALGRIDSYSTEMS.MAPS", { maxZoom:18 }));
  // Ajouter un control pour l'attribution
  var attributionDiv = document.createElement('div');
  attributionDiv.className = "attribution";
  geoportailSetAttribution(map, attributionDiv);
  map.controls[google.maps.ControlPosition.BOTTOM_RIGHT].push(attributionDiv);
  // Afficher / masquer le copyright en fonction de la couche
  google.maps.event.addListener(map, 'maptypeid_changed',
    function()
    {  geoportailSetAttribution(this, attributionDiv);
    });

  //Define OSM map type pointing at the OpenStreetMap tile server
	map.mapTypes.set("OSM", new google.maps.ImageMapType({
		getTileUrl: function(coord, zoom) {
			return "https://tile.openstreetmap.org/" + zoom + "/" + coord.x + "/" + coord.y + ".png";
		},
		tileSize: new google.maps.Size(256, 256),
		name: "OSM",
		maxZoom: 18
	}));

	//Define OSM map type pointing at the OpenStreetMap tile server
		map.mapTypes.set("OCM", new google.maps.ImageMapType({
			getTileUrl: function(coord, zoom) {
				return "https://tile.thunderforest.com/cycle/" + zoom + "/" + coord.x + "/" + coord.y + ".png";
			},
			tileSize: new google.maps.Size(256, 256),
			name: "OpenCycleMap",
			maxZoom: 18
		}));
		/*
		var openskimap = new google.maps.ImageMapType({
				getTileUrl: function(ll, z) {
					var X = ll.x % (1 << z);
					return "//skimap.org/OpenSkiMaps/tile/openskimap/" + z + "/" + X + "/" + ll.y + ".png";
				},
				tileSize: new google.maps.Size(256, 256),
				isPng: true,
				maxZoom: 18,
				name: "OpenSkiMap",
				alt: "OpenSkiMap"
			});
		*/

		map.mapTypes.set("Nautical SWE", new google.maps.ImageMapType({
			getTileUrl: function(coord, zoom) {
				return "https://map.eniro.com/geowebcache/service/tms1.0.0/nautical/"+zoom+"/"+ coord.x +"/"+((1 << zoom) - 1 - coord.y) + ".png";
				//return "http://map.eniro.com/geowebcache/service/gmaps?layers=nautical&zoom=" + zoom + "&x=" + coord.x + "&y=" + coord.y + "&format=image/png";
			},
			tileSize: new google.maps.Size(256, 256),
			name: "Nautical SWE",
			maxZoom: 18
		}));

		openseamap = new google.maps.ImageMapType({
			getTileUrl: function(coord, zoom) {
				return "https://tiles.openseamap.org/seamark/" + zoom + "/" + coord.x + "/" + coord.y + ".png";
			},
			tileSize: new google.maps.Size(256, 256),
			name: "OpenSeaMap",
			maxZoom: 18
		});

		//Define OSM map type pointing at the OpenSnowMap tile server
		openskimap = new google.maps.ImageMapType({
				getTileUrl: function(coord, zoom) {
				return "https://www.opensnowmap.org/opensnowmap-overlay/" + zoom + "/" + coord.x + "/" + coord.y + ".png";
			},
			tileSize: new google.maps.Size(256, 256),
			name: "OpenSkiMap",
			maxZoom: 18
			});


	/*
	var drawingManager = new google.maps.drawing.DrawingManager({
		drawingMode: google.maps.drawing.OverlayType.POLYLINE,
		drawingControl: true,
		drawingControlOptions: {
		  position: google.maps.ControlPosition.TOP_RIGHT,
		  drawingModes: [
			google.maps.drawing.OverlayType.POLYLINE
		  ]
		},
		markerOptions: {icon: 'images/beachflag.png'},
		circleOptions: {
		  fillColor: '#ffff00',
		  fillOpacity: 1,
		  strokeWeight: 5,
		  clickable: false,
		  editable: true,
		  zIndex: 1
		}
	});
	console.log("drawingManager = " + drawingManager);

	drawingManager.setMap(map);

	google.maps.event.addListener(drawingManager, 'polylinecomplete', function (event) {
		console.log(event.getPath().getArray());
		latlonArray = event.getPath().getArray();
		drawDone(event.getPath());
	});

	*/

	polyline = new google.maps.Polyline({
        strokeColor: 'red',
        strokeWeight: 1,
        map: map
    });

    google.maps.event.addListener(map, 'click', function (event) {
			console.log("click");
			  x = 0
        update_timeout = setTimeout(function(){
            if (x == 0) {
                addPoint(event.latLng);
            };
        }, 200);
    });

		google.maps.event.addListener(map, 'dblclick', function (event) {
			console.log("dblclick");
			x = 1;
      drawDone(polyline.getPath());
			return false;
    });

	//map.addListener('click', function(e) { drawpoint(e.latLng); });

	console.log("map2 = " + map);
	//var bikeLayer = new google.maps.BicyclingLayer();
	//  bikeLayer.setMap(map);
	console.log("<- initGoogleMaps()");
}

function chooseActivityCategory(activityId) {
	logDebug("-> chooseActivityCategory(" + activityId);
	$.getJSON("toursWS.php?activity=" + activityId + "&user=" + user + "&limit=500" , function(data) {
		logDebug("data = " + data.length);
		var $select = $('#tour');
		$select.empty();
		var tours = data.tours;
		$select.append("<option value=''>...</option>");
		for (i = 0; i < tours.length; i++)
		{
			$select.append('<option value=' + tours[i].tourId + '>' + tours[i].tourName + '</option>');
		};
	});
	document.getElementById("formValues").style.visibility="visible";
}

function chooseTour(theform) {
	theform.tourname.disabled=true;

	// TODO Load latest track from tour
}

function loadTourData() {
	logDebug("-> loadTourData(");
	i = tourform.tour.selectedIndex;
	logDebug(i + " - " + tourform.tour.options[i].value);

	tourId = tourform.tour.options[i].value;
	$.getJSON("toursWS.php?tour=" + tourId, function(data) {
		logDebug("data = " + data.length);
		var tracks = data.tracks;
		logDebug("tracks = " + tracks.length);
		if (tracks.length > 0) {
			logDebug("data[0] = " + tracks[0].trackDate + " - " +  tracks[0].tourName);
			tourform.timediff.value = tracks[0].trackDuration;
			tourform.distance.value = tracks[0].trackDistance;
		}
		resizeMap();
	});

}

$(function() {
	$( "#date" ).datepicker({
		showOn: "button",
		buttonImage: "/images/calendar.gif",
		buttonImageOnly: true,
		buttonText: "Select date",
		currentText: "Now"
	});
	$("#date").datepicker( "setDate", new Date() );
	
	$("#date").datepicker( "option", "dateFormat", "yy/mm/dd" );
	$("#date").datepicker( $.datepicker.regional[ "fr" ] );
	$("#date").datepicker( "option", "defaultDate", new Date() );
	$("#date").datepicker( "option", "duration", "slow" );
	$("#date").datepicker( "option", "firstDay", 1 );
	$("#date").datepicker( "option", "dayNamesShort", [ "Dim", "Lun", "Mar", "Mer", "Jeu", "Ven", "Sam" ] );
});
                //--><!]]>

</script>

<body onLoad="init();">

<?php include 'bodyHeader.php' ?>


<div id="maiwindow" class="maiwindow" style="position: relative; top: 45px; left: 0px; right: 0px; bottom: 0px;">
<table width=100% height=90%>
	<tr height=100%>
		<td width=365 height=100%>
			<DIV id="mainWin" class="window" style="width: 100%; height: 100%; z-index : 3; overflow:hidden;">
				<div id="" class="windowtopbar" style="position: relative; top: 0px; left: 0px; height=15px; width: 100%">
					<table width=100% cellpadding=0 cellspacing=0 border=0><tr>
						<td class="windowtopbar" width=35%>Track Details</td>
						<td class="windowtopbar" width=1><img src="images/transp.gif" height=16 width=1></td>
						<td class="windowtopbar" width=65% align=right>
						<img src="images/help.gif" width=15 height=15 border=0 title="Main window" alt="Main window">
						</td></tr>
					</table>
				</div>

				<div id="formDiv" class="innerwindow" style="position: relative; top: 0px; width: 100%; height: 100% overflow:scroll;">

					<?php
					if ($action != "manual" && $action != "plan" && $action != "strava") {
					?>
					&nbsp;Track successfully uploaded from <?= substr (strrchr ( $_SESSION['uploadfile'], '_'), 1) ?>
					<?php
					}
					else
					{
					?>
					<?php
					}
					?>
					<table width=100%>
						<form name=tourform id=tourform onSubmit="createTour();return false;">
						<tr>
							<td width=100% valign=top class=box>
								<table class="addTrackTable" width=100%>
									<tr>
										<td align=center>Choose a sport category :</td>
									</tr>
									<tr>
										<td valign=middle align=center>
											<input type=radio name=activity value="0" onClick="chooseActivityCategory(0)"><img src="images/0.png" width=30 height=30 border=0>&nbsp;&nbsp;
											<input type=radio name=activity value="1" onClick="chooseActivityCategory(1)"><img src="images/1.png" width=30 height=30 border=0>&nbsp;&nbsp;
											<input type=radio name=activity value="2" onClick="chooseActivityCategory(2)"><img src="images/2.png" width=30 height=30 border=0>&nbsp;&nbsp;
											<input type=radio name=activity value="3" onClick="chooseActivityCategory(3)"><img src="images/3.png" width=30 height=30 border=0>&nbsp;&nbsp;
											<input type=radio name=activity value="4" onClick="chooseActivityCategory(4)"><img src="images/4.png" width=30 height=30 border=0>&nbsp;&nbsp;
											<input type=radio name=activity value="5" onClick="chooseActivityCategory(5)"><img src="images/5.png" width=30 height=30 border=0>
											<br>
											<input type=radio name=activity value="6" onClick="chooseActivityCategory(6)"><img src="images/6.png" width=30 height=30 border=0>&nbsp;&nbsp;
											<input type=radio name=activity value="7" onClick="chooseActivityCategory(7)"><img src="images/7.png" width=30 height=30 border=0>&nbsp;&nbsp;
											<input type=radio name=activity value="8" onClick="chooseActivityCategory(8)"><img src="images/8.png" =30 height=30 border=0>&nbsp;&nbsp;
											<input type=radio name=activity value="9" onClick="chooseActivityCategory(9)"><img src="images/9.png" width=30 height=30 border=0>&nbsp;&nbsp;
											<input type=radio name=activity value="10" onClick="chooseActivityCategory(10)"><img src="images/10.png" width=30 height=30 border=0>&nbsp;&nbsp;
											<input type=radio name=activity value="11" onClick="chooseActivityCategory(11)"><img src="images/11.png" width=30 height=30 border=0>
											<br>
											<input type=radio name=activity value="12" onClick="chooseActivityCategory(12)"><img src="images/12.png" width=30 height=30 border=0>&nbsp;&nbsp;
											<input type=radio name=activity value="13" onClick="chooseActivityCategory(13)"><img src="images/13.png" width=30 height=30 border=0>&nbsp;&nbsp;
											<input type=radio name=activity value="14" onClick="chooseActivityCategory(14)"><img src="images/14.png" width=30 height=30 border=0>&nbsp;&nbsp;
											<input type=radio name=activity value="15" onClick="chooseActivityCategory(15)"><img src="images/15.png" width=30 height=30 border=0>&nbsp;&nbsp;
											<input type=radio name=activity value="16" onClick="chooseActivityCategory(16)"><img src="images/16.png" width=30 height=30 border=0>&nbsp;&nbsp;
											<input type=radio name=activity value="17" onClick="chooseActivityCategory(17)"><img src="images/17.png" width=30 height=30 border=0>
										</td>
									</tr>
								</table>

								<div id="formValues" style="visibility: hidden;">

									<p>

									<?php
									if ($action != "manual" && $action != "plan" && $action != "strava") {
									?>
									<?php
									}
									else if ($action == "strava")
									{
									?>
									<table width=100% class="addTrackTable">
										<tr>
											<td>Choose a Strava activity :</td>
											<td>
												<select name=stravaActivityList id=stravaActivityList  onchange="selectStravaActivity(value);">
													<option value="-1">Click on Strava's logo to login</option>
												</select>
											</td>
											<td>
												<?php
												if (!$loggedInStrava) {
												?>
													<a href=# onClick="window.open('https://www.strava.com/oauth/authorize?client_id=29307&redirect_uri=http://geocarn-env.avzztenr5u.eu-west-3.elasticbeanstalk.com/strava.php&response_type=code&approval_prompt=auto&scope=view_private,write', '_blank', 'toolbar=no,scrollbars=yes,resizable=yes,top=100,left=150,width=600,height=600'); "><img src="/images/strava.png" width=25 border=0></a>
												<?php
												} else {
												?>
													<img src="<?= $_SESSION['strava_athlete_photo'] ?>" width=30 alt="<?= $_SESSION['strava_athlete_username'] ?>" title="<?= $_SESSION['strava_athlete_username'] ?>">
												<?php
												}
												?>												
											</td>
										</tr>
									</table>
									<p>
									<?php
									}
									else
									{
									?>
									<table width=100% class="addTrackTable">
										<tr>
											<td height=25 align=center>Start drawing a path on the map... and double click when done.</td>
										</tr>
									</table>									
									<p>
									<?php
									}
									?>

									<table width=100% class="addTrackTable">
										<tr>
											<td height=5><img src="/images/transp.gif" width=50 height=5></td>
										</tr>
										<tr>
											<td>Choose a tour :</td>
											<td>
											<select id="tour" name="tour" onChange="chooseTour(this.form);">
											</select>
											&nbsp;&nbsp;&nbsp;
											<a href=# onClick="loadTourData();">Import</a>
											</td>
										</tr>
										<tr>
											<td>...or create a new one :</td>
											<td><input name="tourname" id="tourname" size="30" type="text" /></td>
										</tr>
										<tr>
											<td>Track name :</td>
											<td><input name="trackname" id="trackname" size="30" type="text" /></td>
										</tr>

										<?php
										if ($action != "plan") {
										?>
										<tr>
											<td>Date: </td>
											<td><input type=text name=date id="date" size=10 value="<?= $currentDate ?>">		</td>
										</tr>
										<?php
										}
										?>

										<tr>
											<td>Country : </td>
											<td>
												<select name=country>
												<option value="">Select a country</option>
												<?= getOptionList("", $_SESSION['country'], 15) ?>
												</select>
											</td>
										</tr>
										<tr>
											<td>Planned : </td>
											<td>
												<input type="checkbox" name="planned" value="false">
											</td>
										</tr>
										<tr>
											<td height=5><img src="/images/transp.gif" width=50 height=5></td>
										</tr>
									</table>

										<p>

									<table width=100%>
										<tr>
											<td height=15><img src="/images/transp.gif" width=180 height=15></td>
											<td height=15><img src="/images/transp.gif" width=5 height=15></td>
											<td height=15><img src="/images/transp.gif" width=180 height=15></td>
										</tr>
										<tr valign=top>
											<td width=180>
												<table class="subAddTrackTable" width=100%>
													<tr><td height=5><img src="/images/transp.gif" width=50 height=5></td></tr>
													<tr><td>Duration: </td><td><input type=text name=timediff id=timediff size=6 value="00:00:00"></td></tr>
													<tr><td>Distance: </td><td><input type=text name=distance id=distance size=6 value="0.00"> km	</td></tr>
													<tr><td></td><td>&nbsp;</td></tr>
													<tr><td>Average: </td><td></td></tr>
													<tr><td>Speed: </td><td><input type=text name=averagespeed id=averagespeed size=6> km/h</td></tr>
													<tr><td>Up speed: </td><td><input type=text name=averageupspeed size=6> m/h</td></tr>
													<tr><td></td><td><a href=# onClick="calculateAverageSpeed();return false;" class="formbutton">Calculate avg</a></td></tr>
													<tr><td><img src="/images/transp.gif" width=1 height=15></td></tr>
												</table>
											</td>
											<td width=5><img src="/images/transp.gif" width=5 height=1></td>
											<td width=180>
												<table class="subAddTrackTable" width=100%>
													<tr><td>Altitude: </td><td></td></tr>
													<tr><td>Gain: </td><td><input type=text name=altgain id=altgain size=6> m</td></tr>
													<tr><td>Difference: </td><td><input type=text name=altdiff size=6> m</td></tr>
													<tr><td>Top: </td><td><input type=text name=altmax id=altmax size=6> m</td></tr>
													<tr><td>Bottom: </td><td><input type=text name=altmin id=altmin size=6> m</td></tr>
													<tr><td>Start: </td><td><input type=text name=altstart size=6> m</td></tr>
													<tr><td>Duration up: </td><td><input type=text name=timeup size=6 value="00:00:00"></td></tr>
												</table>
											</td>
										</tr>
										<tr>
											<td height=5><img src="/images/transp.gif" width=50 height=5></td>
										</tr>
									</table>

									<table width=100%>
										<tr>
											<td width=100% height=25 align=center>
											<input type=hidden id=stravaid value="">
											<a href=# onClick="createTour();return false;" class="formbuttonbig">Create track</a>
											</td>
										</tr>
									</table>
									<!--input type=submit onClick="document.getElementById('loaderWin').style.visibility='visible'; return true;"-->

								</div>

							</td>
						</tr>
						</form>
					</table>
				</div>
			</div>
			<br>
			<img src="images/transp.gif" width=520 height=1>
		</td>

		<td width=5><img src="images/transp.gif" height=650 width=1></td>

		<td width=50% height=100% valign=top>
			<DIV id="mainWinMap" class="window" style="width: 100%; height: 100%; z-index : 3; overflow:hidden;">
				<div id="" class="windowtopbar" style="position: relative; top: 0px; left: 0px; height=15px; width: 100%">
					<table width=100% cellpadding=0 cellspacing=0 border=0><tr>
						<td class="windowtopbar" width=35%>Track Map</td>
						<td class="windowtopbar" width=1><img src="images/transp.gif" height=16 width=1></td>
						<td class="windowtopbar" width=65% align=right>
						<a href=# onClick="maxMinMap();"><img src="images/help.gif" width=15 height=15 border=0 title="Main window" alt="Main window"></a>
						</td></tr>
					</table>
				</div>
				<div id="viewerDiv" class="innerwindow" style="position: relative; top: 0px; width: 100%; height: 100%; overflow:hidden;">
					<?php
					if ($action != "manual" && $action != "plan") {
					?>
					<table width=100% id="tracksTable" class="window">
					  <tr>
						<td align=center><img src='images/ajaxLoader.gif'> <p>
						  <p>&nbsp;</td>
					  </tr>
					</table>
					<?php
					}
					?>
				</div>

			</div>
		</td>

		<td width=5><img src="images/transp.gif" height=680 width=1></td>

		<td width=180 height=100% valign=top>
			<DIV id="mainWinTable" class="window" style="width: 100%; height: 100%; z-index : 3; overflow:hidden;">
				<div id="" class="windowtopbar" style="position: relative; top: 0px; left: 0px; height=15px; width: 100%">
					<table width=100% cellpadding=0 cellspacing=0 border=0><tr>
						<td class="windowtopbar" width=35%>Track Points</td>
						<td class="windowtopbar" width=1><img src="images/transp.gif" height=16 width=1></td>
						<td class="windowtopbar" width=65% align=right>
						<img src="images/help.gif" width=15 height=15 border=0 title="Main window" alt="Main window">
						</td></tr>
					</table>
				</div>
				<div id="pointlist" class="innerwindow" style="position: relative; top: 0px; width: 100%; height: 100%; overflow:scroll;">
					<?php
					if ($action != "manual" && $action != "plan") {
					?>
					<table width=100% id="tracksTable" class="window">
						<tr>
						<td align=center><img src='images/ajaxLoader.gif'> <p>
							<p>&nbsp;</td>
						</tr>
					</table>
					<?php
					}
					?>
					<br>
					<img src="images/transp.gif" height=1 width=5>

				</div>
			</div>
		</td>

	</tr>
</table>
</div>


<?php include 'bodyFooter.php' ?>

<div id="loaderDiv" class="innerwindow" style="position: relative; top: 20px; width: 100px; height: 100px overflow:scroll;"> </div>
<div id="loaderMsgDiv" class="innerwindow" style="position: relative; top: 20px; width: 100px; height: 100px overflow:scroll;"> </div>
<div id="statusDiv" class="innerwindow" style="position: relative; top: 20px; width: 100px; height: 100px overflow:scroll;"> </div>
<div id="statusCloseButton" class="innerwindow" style="position: relative; top: 20px; width: 100px; height: 100px overflow:scroll;"> </div>


</body>
</html>
