<?php include 'header.php' ?>
<?php
$view = $_GET['view'];
?>

<script>
lang = "fr";
mapType = "<?php if (isset($_GET['mapType'])) echo $_GET['mapType']; ?>";
view = "<?= $view ?>";
var isPlanned = false;
category = -1;
var requestUser = user;

var map = null;
var openskimap;
var markerArray = [];
var currentPosition = null;

var jsonData = null;

function init() {
	$("#titleDiv").html("");
	$("#subTitleDiv").html("");

	setTitle();

	if (view == "plan") {
		view = "list";
		isPlanned = true;
	}

	if (view == "map")
		showMap();
	else
		showList();
}

function showList() {
	document.getElementById("viewerDiv").innerHTML="<img src=/images/ajaxLoader.gif>";
	$.getJSON("tracksWS.php?track=latest&limit=500&user=" + requestUser + "&activity=" + category + "&planned=" + isPlanned , function(data) {
		drawTable(data.tracks);
	});
	document.getElementById("listTabDiv").className ="toplinkboxSel";
	document.getElementById("mapTabDiv").className ="toplinkbox";
}

function showMap() {
	document.getElementById("viewerDiv").innerHTML="<img src=/images/ajaxLoader.gif>";
	$.getJSON("tracksWS.php?track=latest&limit=50&user=" + requestUser + "&activity=" + category + "&planned=" + isPlanned , function(data) {
		jsonData = data;
		logDebug("-> " + data);
		/*
		if (mapType == "GEO")
			initGeoPortail();
		else
			initGoogleMaps(data.tracks);
		*/
		initMap(data.tracks);
	});
	document.getElementById("listTabDiv").className ="toplinkbox";
	document.getElementById("mapTabDiv").className ="toplinkboxSel";
	//document.getElementById("agendaTabDiv").className ="toplinkbox";
}

function showAgenda() {
	document.getElementById("listTabDiv").className ="toplinkbox";
	document.getElementById("mapTabDiv").className ="toplinkbox";
	//document.getElementById("agendaTabDiv").className ="toplinkboxSel";
}

function setTitle() {
	title = "<a href=# onClick='handleClickCategory(-1); return false;'><img src='/images/all.png' width=45 height=45></a>&nbsp;&nbsp;";
	for (i = 0; i <= ACTIVITY_NUM; i++)
		title = title + "<a href=# onClick='handleClickCategory(" + i + "); return false;'><img src='/images/" + i + ".png' width=45 height=45></a>&nbsp;&nbsp;";
	$("#titleDiv").html(title);
}

function drawTable(data) {
	document.getElementById("viewerDiv").innerHTML="<table width=100% id='tracksTable' class='tracksTableIndex'></table>";
	drawHeader();
    for (var i = 0; i < data.length; i++) {
        drawRow(i, data[i]);
    }
	$("#tracksTable td").addClass("tracksTableIndex");
	$("#tracksTable > tbody > tr:odd").addClass("rowOdd");
    $("#tracksTable > tbody > tr:not(.odd)").addClass("rowEven");
}

function drawRow(rowId, rowData) {
	var row = $("<tr />")
    $("#tracksTable").append(row); //this will append tr element to table... keep its reference for a while since we will add cels into it
    row.append($("<td width=30 align=center><a href='activity.php?activityId=" + rowData.activityId + "'><img src='/images/" + rowData.activityId + ".png' width=30 height=30></a></td>"));
	if (!isPlanned) {
		if (rowData.activityId > 0)
			row.append($("<td width=85 align=center><a href='track.php?trackId=" + rowData.trackId + "'>" + shortDate(rowData.trackDate, 10) + "</a></td>"));
		else
			row.append($("<td align=center>" + shortDate(rowData.trackDate, 10) + "</td>"));
	}
	if (rowData.activityId > 0)
		row.append($("<td><a href='tour.php?tourId=" + rowData.tourId + "'>"  + rowData.tourName + "</a></td>"));
	else
		row.append($("<td align=left>"  + rowData.tourName + "</td>"));
	if (rowData.trackName != "")
		trackName = rowData.trackName;
	else
		trackName = shortDate(rowData.trackDate, 10);
	if (rowData.activityId > 0)
		row.append($("<td align=left><a href='track.php?trackId=" + rowData.trackId + "'>" + trackName + "</a></td>"));
	else
		row.append($("<td align=left>" + trackName + "</td>"));
	row.append($("<td width=85 align=center><img src='/images/flags/24/" + (rowData.country).toLowerCase() + ".png'></td>"));
	row.append($("<td width=85 align=center><a href='user.php?user=" + rowData.user + "'>" + rowData.user + "</a></td>"));
	if (!isPlanned)
		row.append($("<td width=60 align=center>" + rowData.trackDuration + "</td>"));
	if (rowData.activityId > 0)
		row.append($("<td width=60 align=center>" + rowData.trackDistance + "</td>"));
	else
		row.append($("<td></td>"));
	if (rowData.activityId > 0)
		row.append($("<td width=60 align=center>" + rowData.trackAltGain + "</td>"));
	else
		row.append($("<td></td>"));

	if (rowData.trackLength > 30) {
		row.append($("<td align=center><a href='/gpx.php?tourId=" + rowData.tourId + "&trackId=" + rowData.trackId + "'><img src='/images/gpx.gif' width=20 border=0></a>&nbsp;<a href='/kml.php?tourId=" + rowData.tourId + "&trackId=" + rowData.trackId + "'><img src='/images/googleearth.gif' width=20 border=0></a>"));
	}
	else
		row.append($("<td></td>"));

}

function drawHeader() {
	$("#tracksTable").html("");
	var header = $('<tr/>');
	$("#tracksTable").append(header);
	header.append($("<td class='indextab' width=60 align=center>Activity</td>"));
	if (!isPlanned)
		header.append($("<td class='indextab' align=center>Date</td>"));
	header.append($("<td class='indextab'>Tour Name</td>"));
	header.append($("<td class='indextab'>Track Name</td>"));
	header.append($("<td class='indextab' align=center>Country</td>"));
	header.append($("<td class='indextab' align=center>User</td>"));
	if (!isPlanned)
		header.append($("<td class='indextab' align=center>Duration</td>"));
	header.append($("<td class='indextab' align=center>Distance</td>"));
	header.append($("<td class='indextab' align=center>Altitude Gain</td>"));
	header.append($("<td class='indextab' align=center>Track</td>"));
}

function handleClickMyTracks(checkBoxValue) {
	if (checkBoxValue)
		requestUser = user;
	else requestUser = '';
	if (view == "map")
		showMap();
	else
		showList();
}

function handleClickCategory(categoryId) {
	category = categoryId;

	if (view == "map")
		showMap();
	else
		showList();
}

function getLocation() {
	logDebug("-> getLocation(");
  if (currentPosition == null)
	navigator.geolocation.getCurrentPosition(centerMap);
  else
	centerMap(currentPosition);
}

function centerMap(position) {
logDebug("-> centerMap(" + position);
	currentPosition = position;
	var latitude = position.coords.latitude;
	var longitude = position.coords.longitude;
	logDebug("position = " + latitude + ", " + longitude);
	map.setCenter(new google.maps.LatLng(latitude, longitude));

	mapBounds = map.getBounds();
	updateTourList(map, mapBounds.getSouthWest().lat(), mapBounds.getSouthWest().lng(), mapBounds.getNorthEast().lat(), mapBounds.getNorthEast().lng());
}

        <!--//--><![CDATA[//><!--
            	var iv= null;
				var viewer=null;

                function initGeoPortail() {

                        iv= Geoportal.load(
                                 // div's ID:
                                 'viewerDiv',
                                 // API's keys:
                                 ['rhfiamuq2cejl2h4xgcmwpge'],
                                 {
                                 },
                                 //zoom level
                                 15,
                                 //options
                                 {
									layers:['GEOGRAPHICALGRIDSYSTEMS.MAPS','ORTHOIMAGERY.ORTHOPHOTOS','CADASTRALPARCELS.PARCELS'],

									layersOptions:{'GEOGRAPHICALGRIDSYSTEMS.MAPS':{visibility:true,opacity:1,  minZoomLevel:1,maxZoomLevel:18},
												'ORTHOIMAGERY.ORTHOPHOTOS':{visibility:false,opacity:1, minZoomLevel:1, maxZoomLevel:18},
												'CADASTRALPARCELS.PARCELS':{visibility:false,opacity:1, minZoomLevel:12,maxZoomLevel:18}},
									onView: function() {
										viewer=this.getViewer();
										for (i = 0; i < trackIdArray.length; i++) {
											/* style de la trace */
											var styleTrace = new OpenLayers.StyleMap({
											"default": new OpenLayers.Style({

											strokeColor: colorTrackArray[i],
											strokeOpacity: 0.8,
											strokeWidth:4

											}),
											"select": new OpenLayers.Style({
											strokeColor: '#FF0000',
											})
											});

											/* ajout du fichier gpx   */
											gpxLayer = viewer.getMap().addLayer(
											"GPX",
											"trace",
											"http://geocarn-marcdesbordes.rhcloud.com/gpx.php?tourId=" + tourId + "&trackId=" + trackIdArray[i],{
												visibility: true,
												opacity:0.8,
												styleMap: styleTrace,
												eventListeners:{
													'loadend':function(){
														if (this.maxExtent) {
															this.map.zoomToExtent(this.maxExtent);
															this.setVisibility(true);
															}
														}
													}
												}
											);
										}
									}
							}
                        );



                };

		function initGoogleMaps(data) {
			logDebug("-> initGoogleMaps(" + data);
			var mapOptions = {
			  zoom: 8,
			  mapTypeId: google.maps.MapTypeId.TERRAIN
			};
			map = new google.maps.Map(document.getElementById('viewerDiv'),
				mapOptions);

			var bikeLayer = new google.maps.BicyclingLayer();
			  bikeLayer.setMap(map);

			logDebug("data = " + data);
			logDebug("data.length = " + data.length);

			bounds = displayMarkers(map, data);
			map.fitBounds(bounds);

				  google.maps.event.addListener(map, 'dragend', function() {
					logDebug("dragend");
					center = map.getCenter();
					currentCenter = center;
					mapBounds = map.getBounds();
					updateTourList(map, mapBounds.getSouthWest().lat(), mapBounds.getSouthWest().lng(), mapBounds.getNorthEast().lat(), mapBounds.getNorthEast().lng());

				  });


			}

// Definition url des services Geoportail
function geoportailLayer(name, key, layer, options)
{
	logDebug("-> geoportailLayer(" + name + ", " + key + ", " + layer  + ", " + options);
	var l= new google.maps.ImageMapType
  ({ getTileUrl: function (coord, zoom)
      {  return "http://wxs.ign.fr/" + key + "/geoportail/wmts?LAYER=" + layer
          + "&EXCEPTIONS=text/xml"
          + "&FORMAT="+(options.format?options.format:"image/jpeg")
          + "&SERVICE=WMTS&VERSION=1.0.0&REQUEST=GetTile"
          + "&STYLE="+(options.style?options.style:"normal")+"&TILEMATRIXSET=PM"
          + "&TILEMATRIX=" + zoom
          + "&TILECOL=" + coord.x + "&TILEROW=" + coord.y;
      },
    tileSize: new google.maps.Size(256,256),
    name: name,
    minZoom: (options.minZoom ? options.minZoom:0),
    maxZoom: (options.maxZoom ? options.maxZoom:18)
  });
  l.attribution = ' &copy; <a href="http://www.ign.fr/">IGN-France</a>';
  return l;
}
// Ajout de l'attribution Geoportail a la carte
function geoportailSetAttribution (map, attributionDiv)
{
	logDebug("-> geoportailSetAttribution(" + map + ", " + attributionDiv);
	if (map.mapTypes.get(map.getMapTypeId()) && map.mapTypes.get(map.getMapTypeId()).attribution)
  {  attributionDiv.style.display = 'block';
    attributionDiv.innerHTML = map.mapTypes.get(map.getMapTypeId()).name
      +map.mapTypes.get(map.getMapTypeId()).attribution;
  }
  else attributionDiv.style.display = 'none';
}

var map;
// Initialisation de la carte
function initMap(data)
{ // La carte Google
  map = new google.maps.Map( document.getElementById('viewerDiv'),
  {  mapTypeId: google.maps.MapTypeId.TERRAIN,
    streetViewControl: false,
    mapTypeControlOptions: { mapTypeIds: ['carte', google.maps.MapTypeId.TERRAIN, google.maps.MapTypeId.SATELLITE, 'OSM', 'OTM', 'OCM', "Outdoors", "SwissTopo", 'Nautical SWE', 'marine'], style:google.maps.MapTypeControlStyle.DROPDOWN_MENU },
    zoom: 12
  });

  /** Definition des couches  */

   //Define OSM map type pointing at the OpenStreetMap tile server
	map.mapTypes.set("OSM", new google.maps.ImageMapType({
		getTileUrl: function(coord, zoom) {
			return "http://tile.openstreetmap.org/" + zoom + "/" + coord.x + "/" + coord.y + ".png";
		},
		tileSize: new google.maps.Size(256, 256),
		name: "OSM",
		maxZoom: 18
	}));

  //Define OTM map type pointing at the OpenStreetMap tile server
	map.mapTypes.set("OTM", new google.maps.ImageMapType({
		getTileUrl: function(coord, zoom) {
			return "https://a.tile.opentopomap.org/" + zoom + "/" + coord.x + "/" + coord.y + ".png";
		},
		tileSize: new google.maps.Size(256, 256),
		name: "OTM",
		maxZoom: 18
	}));
	
	//Define OCM map type pointing at the Open Cycle Map tile server
	map.mapTypes.set("OCM", new google.maps.ImageMapType({
		getTileUrl: function(coord, zoom) {
			return "http://tile.thunderforest.com/cycle/" + zoom + "/" + coord.x + "/" + coord.y + ".png?apikey=" + OCM_KEY;
		},
		tileSize: new google.maps.Size(256, 256),
		name: "OpenCycleMap",
		maxZoom: 18
	}));
	
  //Define Outdoors map type pointing at the OpenStreetMap tile server
	map.mapTypes.set("Outdoors", new google.maps.ImageMapType({
		getTileUrl: function(coord, zoom) {
			return "http://tile.thunderforest.com/outdoors/" + zoom + "/" + coord.x + "/" + coord.y + ".png?apikey=" + OCM_KEY;
		},
		tileSize: new google.maps.Size(256, 256),
		name: "Outdoors",
		maxZoom: 18
	}));	

	//Define OSM map type pointing at the SwissTopo tile server	
	var BASE_URL = 'https://wmts10.geo.admin.ch';
    var layer = 'ch.swisstopo.pixelkarte-farbe';
    var timestamp = 20140520;
    var format = 'jpeg';
    var url = BASE_URL + '/1.0.0/'+layer+'/default/'+timestamp.toString()+'/3857/{z}/{x}/{y}.' + format;

	map.mapTypes.set("SwissTopo", new google.maps.ImageMapType({
			maxZoom: 20,
			minZoom: 7,
			name: "SwissTopo",
			tileSize: new google.maps.Size(256, 256),
			credit: 'swisstopo',
			getTileUrl: function(coord, zoom) {
						return BASE_URL +  '/1.0.0/'+layer+'/default/'+timestamp.toString()+'/3857/'+ zoom + "/" + coord.x + "/" + coord.y + ".jpeg";
					}
		}));

	// Eniro sjökort
	map.mapTypes.set("Nautical SWE", new google.maps.ImageMapType({
		getTileUrl: function(coord, zoom) {
			return "http://map.eniro.com/geowebcache/service/tms1.0.0/nautical/"+zoom+"/"+ coord.x +"/"+((1 << zoom) - 1 - coord.y) + ".png";
			//return "http://map.eniro.com/geowebcache/service/gmaps?layers=nautical&zoom=" + zoom + "&x=" + coord.x + "&y=" + coord.y + "&format=image/png";
		},
		tileSize: new google.maps.Size(256, 256),
		name: "Nautical SWE",
		maxZoom: 18
	}));


  //Define OSM map type pointing at the OpenSnowMap tile server
	openskimap = new google.maps.ImageMapType({
			getTileUrl: function(coord, zoom) {
			return "http://www.opensnowmap.org/opensnowmap-overlay/" + zoom + "/" + coord.x + "/" + coord.y + ".png";
		},
		tileSize: new google.maps.Size(256, 256),
		name: "OpenSkiMap",
		maxZoom: 18
		});

  // Carte IGN
  map.mapTypes.set('carte', geoportailLayer("IGN", MA_CLE, "GEOGRAPHICALGRIDSYSTEMS.MAPS", { maxZoom:18 }));
   map.mapTypes.set('marine', geoportailLayer("marine", MA_CLE, "GEOGRAPHICALGRIDSYSTEMS.COASTALMAPS", { maxZoom:18 }));


  // Ajouter un control pour l'attribution
  var attributionDiv = document.createElement('div');
  attributionDiv.className = "attribution";
  geoportailSetAttribution(map, attributionDiv);
  map.controls[google.maps.ControlPosition.BOTTOM_RIGHT].push(attributionDiv);
  // Afficher / masquer le copyright en fonction de la couche
  google.maps.event.addListener(map, 'maptypeid_changed',
    function()
    {  geoportailSetAttribution(this, attributionDiv);
    });

	bounds = displayMarkers(map, data);
	map.fitBounds(bounds);

	  google.maps.event.addListener(map, 'dragend', function() {
		logDebug("dragend");
		center = map.getCenter();
		currentCenter = center;
		mapBounds = map.getBounds();
		updateTourList(map, mapBounds.getSouthWest().lat(), mapBounds.getSouthWest().lng(), mapBounds.getNorthEast().lat(), mapBounds.getNorthEast().lng());

	  });
}

function updateTourList(map, minLat, minLong, maxLat, maxLong) {
	logDebug("-> updateTourList(" + map + ", " + minLat + ", " + minLong + ", " + maxLat + ", " + maxLong);
	$.getJSON("tracksWS.php?track=map&minLat=" + minLat + "&minLong=" + minLong+ "&maxLat=" + maxLat + "&maxLong=" + maxLong + "&limit=50&user=" + requestUser  + "&activity=" + category + "&planned=" + isPlanned , function(data) {
		jsonData = data;
		logDebug("-> " + data);
		for (i = 0; i < markerArray.length; i++) {
			if (markerArray[i] != null)
				markerArray[i].setMap(null);
		}
		markerArray = [];
		if (mapType != "GEO")
			displayMarkers(map, data.tracks);
	});
}

function displayMarkers(map, data) {
	logDebug("-> displayMarkers(" + map + ", " + data);
	var bounds = new google.maps.LatLngBounds ();

			for (var i = 0; i < data.length; i++) {
				if (data[i].activityId > 0) {
					tourId = data[i].tourId;
					trackId =  data[i].trackId;
					if (data[i].startlatitude != "" && data[i].startlatitude !=  0)
						lat = data[i].startlatitude;
					else
						lat = data[i].latitude;
					if (data[i].startlongitude != "" && data[i].startlongitude !=  0)
						lon = data[i].startlongitude;
					else
						lon = data[i].longitude;

					var p = new google.maps.LatLng(lat,lon);
					bounds.extend(p);

					var image = {
						url: 'images/' + data[i].activityId + '.png',
						// This marker is 20 pixels wide by 32 pixels tall.
						size: new google.maps.Size(60, 60),
						// The origin for this image is 0,0.
						origin: new google.maps.Point(0,0),
						// The anchor for this image is the base of the flagpole at 0,32.
						anchor: new google.maps.Point(0, 10),
						scaledSize: new google.maps.Size(30, 30)
					  };


					/*
					var marker = new google.maps.Marker({
						  position: p,
						  map: map,
						  title: data[i].tourName + " / " + data[i].trackName,
						  icon: image
					  });
					*/

					marker = createMarker(p, map, data, image, i, tourId, trackId);
					markerArray[i] = marker;

					// fit bounds to track
					//map.fitBounds(bounds);
					}
					else {
						markerArray[i] = null;
					}
				  }
				 return bounds;
}
                //--><!]]>

function createMarker(p, map, data, image, i, tourId, trackId) {
  var marker = new google.maps.Marker({
					  position: p,
					  map: map,
					  title: data[i].tourName + " / " + data[i].trackName + " / " + data[i].user,
					  icon: image
				  });
	google.maps.event.addListener(marker, "click", function() {
		//alert(number + " - " + marker.getLatLng() + " - " + marker.getTitle());
		window.location.href = "/track.php?tourId=" + tourId + "&trackId=" + trackId;
	  });
  return marker;
}

</script>

<body onLoad="init();">

<?php include 'bodyHeader.php' ?>

<div id="tabDiv" class="title" style="height: 45px; position: relative; top: 45px; left:0px; width: 240px;">
	<?php
	if ($view != "plan") {
	?>
	<div id="listTabDiv" class="toplinkbox" style="display: inline;top: 0px; left:0px; align: center;"><a href=# onClick="showList()">List</a></div>
	<div id="mapTabDiv" class="toplinkbox" style="display: inline;top: 0px; left:80px; align: center;"><a href=# onClick="showMap()">Map</a></div>
	<?php
	}
	?>
</div>
<div id="titleDiv" class="title" style="height: 45px; position: absolute; top: 52px; right: 0px; left: 280px;"></div>
<div id="toolDiv" class="toolbar" style="width: 250px; height: 45px; position: absolute; top: 52px; right: 0px;">
	<table>
		<tr>
			<?php if ($_SESSION['user'] != null && $_SESSION['user'] != "") { ?><td>My tracks only</td><?php } ?>
			<td>&nbsp;&nbsp;&nbsp;</td>
			<td><a href=# onClick="getLocation()">Center Map</a></td>
		</tr>
		<tr>
			<?php if ($_SESSION['user'] != null && $_SESSION['user'] != "") { ?><form id="showTracks"><td align=center><input type=checkbox name="mytracks" checked onClick="handleClickMyTracks(this.checked);"></td></form><?php } ?>
			<td></td>
			<td></td>
		</tr>
	</table>
</div>

<div id="maiwindow" class="mainwindow" style="position: absolute; top: 100px; left: 0px; bottom: 5px; right: 5px; ">

			<DIV id="mainWin" class="window" style="position: absolute; left: 0px; top: 0px; bottom: 5px; right: 5px; z-index : 3; overflow:hidden;">
			<div id="" class="windowtopbar" style="position: relative; top: 0px; left: 0px; height=15px; width: 100%">
			<table width=100% cellpadding=0 cellspacing=0 border=0><tr>
				<td class="windowtopbar" width=35%>Main</td>
				<td class="windowtopbar" width=1><img src="images/transp.gif" height=16 width=1></td>
				<td class="windowtopbar" width=65% align=right>
				<img src="images/help.gif" width=15 height=15 border=0 title="Main window" alt="Main window">
				</td></tr>
			</table>
			</div>
			<div id="viewerDiv" class="innerwindow" style="position: relative; top: 0px; left: 0px; bottom: 5px; right: 5px; overflow:scroll;">
			<table width=100% height=100% id="tracksTable" class="tracksTableIndex">
			  <tr>
				<td align=center><img src='images/ajaxLoader.gif'> <p>
				  <p>&nbsp;</td>
			  </tr>
			</table>
		  </div>
		</div>


</div>

<div id="bottombar" class="bottombar">
&nbsp;
</div>

<?php include 'bodyFooter.php' ?>

</body>
</html>
