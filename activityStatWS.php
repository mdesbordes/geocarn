<?php include 'inc/connectDb.php' ?>
<?php include 'inc/logger.php' ?>
<?php

function getTimeStat($db, $user) {
	logDebug('-> getTimeStat(' . $user);
	global $ACTIVITY;

	$request = "SELECT SEC_TO_TIME(SUM(TIME_TO_SEC(duration))) sumtime, round(SUM(TIME_TO_SEC(duration))/60) summin FROM TRACKER_TRACK join TRACKER_TOUR on TRACKER_TRACK.TOUR_ID=TRACKER_TOUR.ID where week(TRACKER_TRACK.datetime) = week(now()) and year(TRACKER_TRACK.datetime) = year(now()) and TRACKER_TOUR.ACTIVITY_ID != 15";
	if ($user != null && $user != "")
		$request .= " AND TRACKER_TOUR.USER_ID = '$user' ";
	$request .= " AND TRACKER_TRACK.PLANNED_ONLY = 0 ";
	
	logDebug($request);
	$result = mysqli_query($db, $request);

	$rows = array();
	while($r = mysqli_fetch_assoc($result)) {
		$rows['activities'][] = $r;
	}

	return json_encode($rows);
}

function getActivityStatByWeek($db, $user, $activity, $year, $green) {
	logDebug('-> getActivityStatByWeek(' . $user . ", " . $activity . ", " . $year . ', ' . $green);
	global $ACTIVITY;

	$request = "SELECT SEC_TO_TIME(SUM(TIME_TO_SEC(duration))) sumtime, round(SUM(TIME_TO_SEC(duration))/60) summin, SUM(DURATION) sumDuration, SUM(DISTANCE) sumDistance, SUM(ALT_GAIN) sumAltGain, week(TRACKER_TRACK.datetime) week FROM TRACKER_TRACK join TRACKER_TOUR on TRACKER_TRACK.TOUR_ID=TRACKER_TOUR.ID where year(TRACKER_TRACK.datetime) = year(now()) ";
	if ($activity != null && $activity != "" && $activity != -1)
		$request .= " and TRACKER_TOUR.ACTIVITY_ID = '$activity' ";
	if ($user != null && $user != "")
		$request .= " AND TRACKER_TOUR.USER_ID = '$user' ";
	if ($green == 1)
		$request .= " AND TRANSPORT = 1";
	$request .= " AND TRACKER_TRACK.PLANNED_ONLY = 0 ";	
	$request .= " group by week(TRACKER_TRACK.datetime)";
	logDebug($request);
	$result = mysqli_query($db, $request);

	$rows = array();
	while($r = mysqli_fetch_assoc($result)) {
		$rows['activities'][] = $r;
	}

	return json_encode($rows);
}


function getActivityStat($db, $user, $days) {
	logDebug('-> getActivityStat(' . $user. ", " . $days);
	global $ACTIVITY;

	$request = "SELECT TRACKER_TOUR.ACTIVITY_ID activityId, SUM(1) activitySum, round(SUM(TIME_TO_SEC(duration))/60) summin FROM TRACKER_TRACK join TRACKER_TOUR on TRACKER_TRACK.TOUR_ID=TRACKER_TOUR.ID  WHERE datediff( curdate( ) , TRACKER_TRACK.DATETIME ) <=" . $days ." ";
	if ($user != null && $user != "")
		$request .= " AND TRACKER_TOUR.USER_ID = '$user' ";
	$request .= " AND TRACKER_TRACK.PLANNED_ONLY = 0 ";
	$request .= " group by TRACKER_TOUR.ACTIVITY_ID  ";
	$request .= " order by activitySum desc";
	logDebug($request);
	$result = mysqli_query($db, $request);

	$rows = array();
	while($r = mysqli_fetch_assoc($result)) {
		$rows['activities'][] = $r;
	}

	return json_encode($rows);
}

function getActivitySeasonStatRequest($user, $year, $month, $activity, $type, $green) {
	if ($type == "SUMMER") {
		$startDate = $year . "-01-01";
		$endDate = $year . "-12-31";
		$condition = " (TRACKER_TOUR.ACTIVITY_ID != 1 AND TRACKER_TOUR.ACTIVITY_ID != 2 AND TRACKER_TOUR.ACTIVITY_ID != 6) ";
	}
	else {
		if ($month < 8)
			$year = $year -1;
		$startDate = $year . "-08-01";
		$endDate = ($year+1) . "-07-31";
		$condition = " (TRACKER_TOUR.ACTIVITY_ID = 1 OR TRACKER_TOUR.ACTIVITY_ID = 2 OR TRACKER_TOUR.ACTIVITY_ID = 6) ";
	}

	$request = "SELECT TRACKER_TOUR.ACTIVITY_ID activityId, SUM(1) activitySum, SUM(DISTANCE) activityDist, SUM(ALT_DIFF) activityAltDiff, round(SUM(TIME_TO_SEC(duration))/60) summin FROM TRACKER_TRACK join TRACKER_TOUR on TRACKER_TRACK.TOUR_ID=TRACKER_TOUR.ID where TRACKER_TRACK.DATETIME >= '$startDate' and TRACKER_TRACK.DATETIME <= '$endDate'";
	$request .= " AND $condition";
	if ($user != null && $user != "")
		$request .= " AND TRACKER_TOUR.USER_ID = '$user'";
	if ($activity > -1)
		$request .= " AND TRACKER_TOUR.ACTIVITY_ID = $activity";
	if ($green == 1)
		$request .= " AND TRANSPORT = 1";
	$request .= " AND TRACKER_TRACK.PLANNED_ONLY = 0 ";
	$request .= " group by TRACKER_TOUR.ACTIVITY_ID ";
	$request .= " order by summin desc";
	logDebug($request);
	return $request;
}

function getAllActivitySeasonStatRequest($db, $user, $year, $month, $green) {
	logDebug('-> getAllActivitySeasonStatRequest(' . $user . ', ' . $year . ', ' . $month . ', ' . $green);
	$request = getActivitySeasonStatRequest($user, $year, $month, -1, "SUMMER", $green);
	$resultSummer = mysqli_query($db, $request);

	$request = getActivitySeasonStatRequest($user, $year, $month, -1, "WINTER", $green);
	$resultWinter = mysqli_query($db, $request);

	$rows = array();

	while($r = mysqli_fetch_assoc($resultWinter)) {
		$rows['activities'][] = $r;
	}
	while($r = mysqli_fetch_assoc($resultSummer)) {
		$rows['activities'][] = $r;
	}

	return json_encode($rows);
}

function getActivitySeasonStat($db, $user, $year, $month, $activity, $green) {
	logDebug('-> getActivitySeasonStat(' . $user . ', ' . $year . ', ' . $month . ', ' . $activity . ', ' . $green);
	global $ACTIVITY;

	$request = getActivitySeasonStatRequest($user, $year, $month, $activity, "SUMMER", $green);
	$resultSummer = mysqli_query($db, $request);

	$request = getActivitySeasonStatRequest($user, $year, $month, $activity, "WINTER", $green);
	$resultWinter = mysqli_query($db, $request);

	$rows = array();

	while($r = mysqli_fetch_assoc($resultWinter)) {
		$rows['activities'][] = $r;
	}
	while($r = mysqli_fetch_assoc($resultSummer)) {
		$rows['activities'][] = $r;
	}

	return json_encode($rows);
}

// --- Begin --- 

if (isset($_GET['user']))
	$user = $_GET['user'];
else
	$user = null;
if (isset($_GET['action']))
	$action = $_GET['action'];
else 
	$action = null;
if (isset($_GET['year']))
	$year = $_GET['year'];
else
	$year = null;
if (isset($_GET['month']))
	$month = $_GET['month'];
else
	$month = null;
if (isset($_GET['activity']))
	$activity = $_GET['activity'];
else
	$activity = null;
if (isset($_GET['days']))
	$days = $_GET['days'];
else
	$days = null;
if (isset($_GET['green']))
	$green = $_GET['green'];
else
	$green = 0;

if ($action == 'season' && $activity > -1)
	$activityStat = getActivitySeasonStat($db, $user, $year, $month, $activity, $green);
else if ($action == 'season')
	$activityStat = getAllActivitySeasonStatRequest($db, $user, $year, $month, $green);
else if ($action == 'time')
	$activityStat = getTimeStat($db, $user);
else if ($action == 'week')
	$activityStat = getActivityStatByWeek($db, $user, $activity, $year, $green);
else
	$activityStat = getActivityStat($db, $user, $days);

header('Content-type: text/xml;  application/json', true);
?><?= $activityStat ?><?php exit; ?>
