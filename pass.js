function setPass(data) {
	logDebug("-> setPass(");
	logDebug(data.length);
	$("#passTable").html("");
	for (i = 0; i < data.length; i++) {
		var row = $("<tr align=left valign=middle />");
		logDebug(data[i].Name + " - " + data[i].State + " - " + data[i].IsMountainPass);
		$("#passTable").append(row);
		if (data[i].State == 0)
			color = "grey";
		else if (data[i].State == 1)
			color = "green";
		else if (data[i].State == 2)
			color = "red";
		else
			color = "orange";
		// orange or grey
		
		row.append($("<td align=center>" + data[i].Name + "</td><td><a href='http://www.google.fr/maps/place/" + data[i].Coordinates.Latitude + " " + data[i].Coordinates.Longitude + "' target=_new><img src=/images/" + color + ".gif width=10 height=10 border=0></a></td>"));
	}
	
	$("#passTable > tbody > tr:odd").addClass("rowOdd");
    $("#passTable > tbody > tr:not(.odd)").addClass("rowEven"); 
}

function initPassWin() {
	var ai = AJAXInteraction("/passWS.php", null, setPass);
	ai.doGet();
}

function loadPassWin() {
	logDebug("-> loadPassWin(");
	$.getJSON("/passWS.php" , function(data) {
		logDebug(data.Data);
		logDebug(data.Data.Closures);
		logDebug(data.Data.Closures.length);
		setPass(data.Data.Closures);
	});
}
