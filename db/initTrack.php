<?php include '../inc/connectDb.php' ?>
<?php

$request = "CREATE TABLE IF NOT EXISTS `TRACKER_TRACK` (
  `ID` int(8) NOT NULL AUTO_INCREMENT,
  `TOUR_ID` varchar(256) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `UPDATED` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `DATETIME` datetime NOT NULL,
  `NAME` varchar(128) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `DESCRIPTION` varchar(4000) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `DURATION` time NOT NULL,
  `DURATION_UP` time NOT NULL,
  `DISTANCE` float NOT NULL,
  `ALT_DIFF` int(11) NOT NULL,
  `TRACK` text COLLATE latin1_general_ci NOT NULL,
  `ALT_GAIN` int(11) NOT NULL,
  `ALT_MIN` int(11) NOT NULL,
  `ALT_MAX` int(11) NOT NULL,
  `ALT_START` int(11) NOT NULL,
  `AVERAGE_SPEED` double NOT NULL,
  `AVERAGE_UP_SPEED` double NOT NULL,
  `START_LATITUDE` float NOT NULL,
  `START_LONGITUDE` float NOT NULL,
  `PLANNED_ONLY` tinyint(1) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `ID` (`ID`,`TOUR_ID`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=1410 ;";
//$result = mysql_query($request,$db);
if (mysqli_query($db,$request)) {
	echo "Table created successfully<br>";
 } else {
	echo "Table creating database: " . mysqli_error($conn) . "<br>";
 }

/*
$request = "ALTER TABLE `TRACKER_TRACK` ADD TRANSPORT tinyint(1) NOT NULL DEFAULT 0;";
if (mysqli_query($db,$request)) {
	echo "Table altered successfully<br>";
 } else {
	echo "Error inserting data: " . mysqli_error($db) . "<br>";
 }
*/

$request = "UPDATE `TRACKER_TRACK` set TRANSPORT=1 where TOUR_ID=438 OR TOUR_ID=440 OR TOUR_ID=240 OR TOUR_ID=232 OR TOUR_ID=287 OR TOUR_ID=231";
if (mysqli_query($db,$request)) {
	echo "Data deleted successfully<br>";
 } else {
	echo "Error inserting data: " . mysqli_error($db) . "<br>";
 }
 
/*
$url = "http://geocarn-env.avzztenr5u.eu-west-3.elasticbeanstalk.com/db/initTrackSql.php";
$commands = file_get_contents($url);
$splittedstring=explode("INSERT",$commands);
foreach ($splittedstring as $key => $value) {
	//echo "splittedstring[".$key."] = ".$value."<br>";
	$request = "INSERT " . $value;
	if (mysqli_query($db,$request)) {
		echo "Data inserted successfully<br>";
	} else {
		echo "Error inserting data: " . mysqli_error($db) . "<br>";
	}
}
*/

/*
$request = "DELETE FROM `TRACKER_TRACK` where ID=1456";
if (mysqli_query($db,$request)) {
	echo "Data deleted successfully<br>";
 } else {
	echo "Error inserting data: " . mysqli_error($db) . "<br>";
 }
*/

$request = "SELECT * FROM TRACKER_TRACK ORDER BY ID";
	//$result = mysql_query($request,$db);
	$result = mysqli_query($db,$request);
	$index = 0;
	//while ($meta = mysql_fetch_object($result))
	while ($meta = mysqli_fetch_assoc($result))
	{
		echo "TOUR_ID: " . $meta["TOUR_ID"]. "<br>";
		echo "- " . $meta["ID"]. " - " . $meta["TOUR_ID"]. " - " . $meta["NAME"]. " - " . $meta["PLANNED_ONLY"]. " - " . $meta["TRANSPORT"]. "<br>";
	}

mysqli_free_result($result); 