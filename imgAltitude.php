<?php include 'inc/connectDb.php' ?><?php

$WIDTH = 600;
$HEIGHT = 250;

// http://localhost/altitude.php?rel=0&num=9&e0=1230&t0=100&e1=1289&t1=300&e2=1358&t2=500&e3=1362&t3=600&e4=1492&t4=800&e5=1582&t5=1000&e6=1732&t6=1100&e7=1632&t7=1200&e8=1932&t8=140
if (isset($_GET['debug'])) {
	$debug = true;
}
else {
	$debug = false;
	header("Content-type: image/png");
}

$trackId = $_GET['trackId'];
$tourId = $_GET['tourId'];
if (isset($_GET['activityId']))
	$activityId = $_GET['activityId'];
else
	$activityId = -1;
if (isset($_GET['width']))
	$width = $_GET['width'];
else
	$width = $WIDTH;
if (isset($_GET['height']))
	$height = $_GET['height'];
else	
	$height = $HEIGHT;

if (isset($_GET['rel']))
	$relativeElevation = $_GET['rel'];
else
	$relativeElevation = -1;
if ($relativeElevation == '0')
	$relativeElevation = FALSE;
else 
	$relativeElevation = TRUE;

$request = "select * from TRACKER_TRACK where ID = $trackId AND TOUR_ID = $tourId" ;
$result = mysqli_query($db,$request);
while ($row = mysqli_fetch_array($result)) {	
	//echo $row["URL"]." - ".$row["LATITUDE"]." - ".$row["LONGITUDE"]."<br>";
	$trackname = $row["NAME"];
	$datetime = $row["DATETIME"];
	$description = $row["DESCRIPTION"];
	$track = $row["TRACK"];
	// 45.1094;5.8767;1751;1247497|45.1095;5.8769;1751
}

$trackExplode = explode('|', $track);
for ($i = 0; $i < (count($trackExplode)-1); $i++) {
	$point = $trackExplode[$i];
	$points[$i] = explode(';', $point);	
}
	

$im = imagecreatetruecolor($width, $height);
$red = imagecolorallocate($im, 255, 0, 0);
$black = imagecolorallocate($im, 0, 0, 0);

$colorBlue = imagecolorallocate($im, 0x6A, 0x8F, 0xBD);
$colorGray = imagecolorallocate($im, 0x88, 0x88, 0x88);
$colorDDDDDD = imagecolorallocate($im, 0xDD, 0xDD, 0xDD);
$colorAAAAAA = imagecolorallocate($im, 0xAA, 0xAA, 0xAA);

$colors[0] = imagecolorallocate($im, 0x00, 0xFF, 0x00);  // 888888
$colors[1] = imagecolorallocate($im, 0x00, 0xFF, 0x00);  // 00FF00
$colors[2] = imagecolorallocate($im, 0x33, 0xFF, 0x00);  // 33FF00
$colors[3] = imagecolorallocate($im, 0x66, 0xFF, 0x00);  // 66FF00
$colors[4] = imagecolorallocate($im, 0x99, 0xFF, 0x00);  // 99FF00
$colors[5] = imagecolorallocate($im, 0xCC, 0xFF, 0x00);  // CCFF00
$colors[6] = imagecolorallocate($im, 0xFF, 0xFF, 0x00);  // FFFF00
$colors[7] = imagecolorallocate($im, 0xFF, 0xCC, 0x00);  // FFCC00
$colors[8] = imagecolorallocate($im, 0xFF, 0x99, 0x00);  // FF9900
$colors[9] = imagecolorallocate($im, 0xFF, 0x66, 0x00);  // FF6600
$colors[10] = imagecolorallocate($im, 0xFF, 0x33, 0x00);  // FF3300
$colors[11] = imagecolorallocate($im, 0xFF, 0x00, 0x00); // FF0000
$colors[12] = imagecolorallocate($im, 0xCC, 0x00, 0x00); // CC0000
$colors[13] = imagecolorallocate($im, 0x99, 0x00, 0x00); // 990000
$colors[14] = imagecolorallocate($im, 0x66, 0x00, 0x00); // 660000
$colors[15] = imagecolorallocate($im, 0x33, 0x00, 0x00); // 330000
$colors[16] = imagecolorallocate($im, 0xFF, 0x00, 0x00); // 000000

$font = "arial.ttf";
$size = 3;
// Make the background transparent
imagecolortransparent($im, $black);

$totalDistance = 0;
$eleMin = 100000;
$eleMax = 0;
$timeMin =1000000;
$timeMax = 0;
$speedMax = 0;
$prevSpeed = 0;
if ($debug == "true")
	echo "<table><tr><td>i</td><td>speed</td><td>speed</td><td>distance</td><td>datetime</td><td>datetime - prevdatetime</td></tr>";
if (count($points) > 1) {
	for ($i = 0; $i < (count($points)-1); $i++) {
		$lat = $points[$i][0];
		$lon = $points[$i][1];
		$elevation = $points[$i][2];
		$datetime = $points[$i][3];
		$timestamp = $i;
		if ($i == 0) {
			$prevdatetime = 0;
		}
		if ($i > 0) {
			$distance = getDistance($lat, $lon, $prevlat, $prevlon);
			$totalDistance += $distance;		
			//echo $totalDistance . "<br>";
			$speed = getSpeed($lat, $lon, $prevlat, $prevlon, $datetime, $prevdatetime);
			if ($i > 1)
				$prevSpeed = getSpeed($points[$i-1][0], $points[$i-1][1], $points[$i-2][0], $points[$i-2][1], $points[$i-1][3], $points[$i-2][3]);
			else
				$prevSpeed = 0;
			if ($i < (count($points)-1))
				$nextSpeed = getSpeed($points[$i+1][0], $points[$i+1][1], $points[$i][0], $points[$i][1], $points[$i+1][3], $points[$i][3]);
			else
				$nextSpeed = 0;
			if ($debug == "true")
				echo "<tr><td>" . $i . "</td><td>" . $speed . "</td><td>" . $speed * 22482 . "</td><td>" . $distance . "</td><td>" . $datetime . "</td><td>" . ($datetime - $prevdatetime) . "</td></tr>";
			if ($speed > $speedMax) {
				$variation = 0;
				if ($debug == "true")
					echo "<tr><td colspan=6>speed = " . $speed  . " - prevSpeed = " . $prevSpeed  . " - nextSpeed = " . $nextSpeed . " - speedMax = " . $speedMax . "</td></tr>";
				$avgSpeed = ($prevSpeed + $nextSpeed) / 2;
				if ($avgSpeed < $speed) {
					if  ($avgSpeed != 0)
						$variation = ($speed - $avgSpeed) / $avgSpeed;
				}
				else if ($speed != 0)
					$variation = ($avgSpeed - $speed) / $speed;
				if ($debug == "true")
					echo "<tr><td colspan=6>Variation = " . $variation  . "</td></tr>";
				if (abs($variation) < 1) {
					$speedMax = $speed;
					if ($debug == "true")
						echo "<tr><td colspan=6> -> speedMax = " . $speedMax . "</td></tr>";
				}
			}
			$prevSpeed = $speed;
		}
		//echo "'" . $elevation . "'<br>";
		if ($elevation != "N/A") {
			if ($elevation > $eleMax)
				$eleMax = $elevation;
			if ($elevation < $eleMin)
				$eleMin = $elevation;
			if ($timestamp > $timeMax)
				$timeMax = $timestamp;
			if ($timestamp < $timeMin)
				$timeMin = $timestamp;
		}
		$prevlat = $lat;
		$prevlon = $lon;
		$prevdatetime = $datetime;
	}
	if ($debug == "true")
		echo "</table>";
		
	if ($relativeElevation) {
		$ratio = $eleMax - $eleMin;
		$delta = $eleMin;
	}
	else {
		$ratio = $eleMax;
		$delta = 0;
	}
	$xRatio = $width / $totalDistance;
	if ($speedMax != 0)
		$speedRatio = $height / $speedMax;
	else 
		$speedRatio = 0;
	
	if ($debug == "true") {
		echo $totalDistance. "<br>";
		echo "speedMax = " . $speedMax . "<br>";
		echo "speedMax = " . $speedMax * 22482 . "<br>";
		echo "speedRatio = " . $speedRatio . "<br>";

		echo "<p>---<p>";

		echo $timeMin . " " . $timeMax . " " . $eleMin . " " . $eleMax . "<p>";
	}

	$xPrev = 0;
	$yPrev = 0;
	$prevlat = $points[0][0];
	$prevlon = $points[0][1];
	$prevSpeed = 0;
	$distance = 0;
	$color = $colorGray;

	// Elevation
	if ($activityId != 4 && $activityId != 2) {
		for ($i = 0; $i < count($points); $i++) {
			$lat = $points[$i][0];
			$lon = $points[$i][1];
			$elevation = $points[$i][2];
			$datetime = $points[$i][3];
			$timestamp = $i;
			//echo $timestamp . " " . $elevation . "<br>";
			
			$deltaDist = getDistance($lat, $lon, $prevlat, $prevlon);
			$distance += $deltaDist;
			if ($elevation != "N/A") {
				$x = $distance * $xRatio;
				$y = $height - ($height * ($elevation - $delta)) / $ratio;
				//echo $x . " " . $y . "<p>";
				if ($i > 0) {
					if ($i > 0) {
						$rateSum = 0;
						$rateCount = 0;
						if ($i > 2) {
							$rateSum += getRate($points[$i-1][0], $points[$i-1][1], $points[$i-2][0], $points[$i-2][1], $points[$i-1][2], $points[$i-2][2]);
							$rateCount++;
						}
						if ($i > 1) {
							$rateSum += getRate($points[$i][0], $points[$i][1], $points[$i-1][0], $points[$i-1][1], $points[$i][2], $points[$i-1][2]);
							$rateCount++;
						}
						if ($i < (count($points)-1)) {
							$rateSum += getRate($points[$i+1][0], $points[$i+1][1], $points[$i][0], $points[$i][1], $points[$i+1][2], $points[$i][2]);
							$rateCount++;
						}
						if ($i < (count($points)-2)) {
							$rateSum += getRate($points[$i+2][0], $points[$i+2][1], $points[$i+1][0], $points[$i+1][1], $points[$i+2][2], $points[$i+1][2]);
							$rateCount++;
						}
						$rate = $rateSum / $rateCount;
						//echo $lat . ", " . $lon . " - " . $points[$i-5][0] . ", " . $points[$i-5][1] . " (" . $rateSum . ") -> " . $rate . "<br>";
						if (round(abs($rate/2)) <= 16)
							$color = $colors[round(abs($rate/2))];
						else
							$color = $colors[16];
						
					}
					
					//imageline ($im,$xPrev,$yPrev,$x,$y,$colorBlue); 
					imagefilledpolygon($im, array(
						$xPrev, $yPrev,
						$xPrev, $height,
						$x, $height,
						$x, $y
					),
					4,
					$color);
				}
				$xPrev = $x;
				$yPrev = $y;
				$prevele = $elevation;
			}
			$prevlat = $lat;
			$prevlon = $lon;
		}
	}

	if ($debug == "true") 
		echo "<p>";
	
	// Speed
	if ($debug == "true") 
		echo "<table><tr><td>i</td><td>speed</td><td>distance</td><td>datetime</td><td>datetime - prevdatetime</td></tr>";
	
	$xPrev = 0;
	$yPrev = 0;
	$prevlat = $points[0][0];
	$prevlon = $points[0][1];
	$distance = 0;
	$speedAvg = 0;
	$speedArray = array();
	for ($i = 0; $i < count($points); $i++) {
		$lat = $points[$i][0];
		$lon = $points[$i][1];
		$elevation = $points[$i][2];
		$datetime = $points[$i][3];
		$timestamp = $i;
		//echo $timestamp . " " . $elevation . "<br>";
		
		$deltaDist = getDistance($lat, $lon, $prevlat, $prevlon);
		$distance += $deltaDist;
		$x = $distance * $xRatio;
		if ($i == 0) {
			$prevdatetime = 0;
			$speedArray[$i] = 0;
		}
		else {
			if (($datetime - $prevdatetime) != 0)
				$speed = 3600 * $speedRatio * ($deltaDist / ($datetime - $prevdatetime));
			else
				$speed = 0;
			
			$speedArray[$i] = $speed;
			if ($i > 2 && isset($speedArray[$i-3])) 
				$speedAvg = ($speedArray[$i-3] + $speedArray[$i-2] + $speedArray[$i-1] + $speedArray[$i]) / 4; 
			if ($i > 1 && isset($speedArray[$i-2])) 
				$speedAvg = ($speedArray[$i-2] + $speedArray[$i-1] + $speedArray[$i]) / 3; 
			if ($i > 0 && isset($speedArray[$i-1])) 
				$speedAvg = ($speedArray[$i-1] + $speedArray[$i]) / 2; 
			if ($debug == "true") {
				echo "<tr><td>" . $i . "</td><td>" . $speed . "</td><td>" . $deltaDist . "</td><td>" . $datetime . "</td><td>" . ($datetime - $prevdatetime) . "</td></tr>";
				echo "<tr><td>" . $i . "</td><td colspan=5>" . $xPrev . " - " . $x . " , " . ($height - $prevSpeed). " - " . ($height - $speedAvg) . "</td></tr>";
			}
			imageline ($im,$xPrev,($height - $prevSpeed),$x,($height - $speedAvg),$colorBlue); 	
		}
		$xPrev = $x;
		$prevlat = $lat;
		$prevlon = $lon;
		$prevSpeed = $speedAvg;
		$prevdatetime = $datetime;
	}
	if ($debug == "true")
		echo "</table>";

	// Elevation scale
	if ($activityId != 4 && $activityId != 2) {
		$eleDelta = 100;
		if ($ratio > 1500)
			$eleDelta = 200;
		if ($ratio > 3000)
			$eleDelta = 500;
		//imagestring($im, $size, 200, 100 - 15, $ratio, $colorGray);
		for ($i = 0; $i < 100; $i++) {
			$elevation = $i * $eleDelta;
			$y = $height - ($height * ($elevation - $delta)) / $ratio + 1;
			//imageline ($im,0,$y,$width,$y,$colorGray); 
			if ($y > 0) {
				imageline ($im,0,$y-1,$width,$y-1,$colorDDDDDD); 	
				if (round($elevation / 500) == ($elevation /500))
					imageline ($im,0,$y,$width,$y,$colorAAAAAA); 
				if ($y > 10)
					imagestring($im, $size, 0, $y - 15, $elevation . "m", $colorGray);
			}
		}
	}

	// Speed scale
	$speedDelta = 10;
	/*
	if ($speedRatio > 1500)
		$speedDelta = 10;
	if ($speedRatio > 3000)
		$speedDelta = 10;
	*/
	if ($debug == "true") 
			echo "<p>speedRatio = " . $speedRatio;
	
	for ($i = 1; $i < 10; $i++) {
		$speed = $i * $speedDelta;
		$y = $height - ($speed * $speedRatio) + 1;
		//imageline ($im,0,$y,$width,$y,$colorGray); 
		if ($debug == "true") 
			echo "<p>" . $y . " - " . $height . " - " . $speed;
		if ($y > 0) {
			imageline ($im,0,$y-1,$width,$y-1,$colorBlue); 	
			if ($y > 10)
				imagestring($im, $size, $width - 50, $y - 15, $speed . "km/h", $colorBlue);
		}
	}

	$dist = 1;
	$x = 0;
	//echo $xRatio . "<br>";
	if ($xRatio > 25)
		$xDelta = 1;
	else 
		$xDelta = 10;
	$index = 0;
	//echo $xDelta . " - " . $xRatio . "<br>";
	while ($x < $width && $index < 100) {
		$x = $dist * $xRatio;
		//echo $x . "<br>";
		imageline ($im,$x,0,$x,$height,$colorDDDDDD); 
		$dist += $xDelta;
			$index++;
	}
}
else {
	imagestring($im, $size, 50, 50, "No data!", $colorBlue);
}	
// Save the image
imagepng($im);
imagedestroy($im);

function getColor($colorIndex, $colors) {
	while ($colorIndex >= count($colors))
		$colorIndex = $colorIndex - count($colors);
	return $colors[$colorIndex];
}

function getRate($latitudeFrom, $longitudeFrom,
    $latituteTo, $longitudeTo, $elevationFrom, $elevationTo) {
    $distance = getDistance($latitudeFrom, $longitudeFrom, $latituteTo, $longitudeTo);
//echo $elevationTo . " - " . $elevationFrom . " -> eleDiff = " . ($elevationTo - $elevationFrom) . "<br>";
if ($distance != 0)
    $rate = 100 * ($elevationTo - $elevationFrom) / ($distance * 1000);
else
    $rate = 0;
    return $rate;
}

function getDistance($latitudeFrom, $longitudeFrom,
    $latituteTo, $longitudeTo)
{
    //echo "<br>->getDistance($latitudeFrom, $longitudeFrom, $latituteTo, $longitudeTo)<br>";
    // 1 degree equals 0.017453292519943 radius
    $degreeRadius = deg2rad(1);
 
    // convert longitude and latitude values
    // to radians before calculation
    $latitudeFrom  *= $degreeRadius;
    $longitudeFrom *= $degreeRadius;
    $latituteTo    *= $degreeRadius;
    $longitudeTo   *= $degreeRadius;
 
    // apply the Great Circle Distance Formula
    $d = sin($latitudeFrom) * sin($latituteTo) + cos($latitudeFrom)
       * cos($latituteTo) * cos($longitudeFrom - $longitudeTo);
    $distance = (6371.0 * acos($d));
    //echo "distance = " . $distance . "<br>";
    if (is_nan($distance)) {
        $distance = 0;
        //echo "-> distance = " . $distance . "<br>";
    }
    return $distance;
}

function getSpeed($lat, $lon, $prevlat, $prevlon, $datetime, $prevdatetime) {
	$distance = getDistance($lat, $lon, $prevlat, $prevlon);
	if (($datetime - $prevdatetime) != 0)
		$speed = 3600 * $distance / ($datetime - $prevdatetime);
	else
		$speed = 0;
	if ($speed < 0)
		$speed = 0;
	return $speed;
}
?>