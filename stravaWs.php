<?php include 'inc/logger.php' ?>
<?php
if(!isset($_SESSION)) 
{ 
	session_set_cookie_params(680400);
	session_start(); 
} 

function postRequest($url, $data) {
	logDebug("-> postRequest(".$url);
	logDebug("client_id = ".$data['client_id']);
	logDebug("code = ".$data['code']);

	// use key 'http' even if you send the request to https://...
	$options = array(
		'http' => array(
			'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
			'method'  => 'POST',
			'content' => http_build_query($data)
		)
	);
	$context  = stream_context_create($options);
	$result = file_get_contents($url, false, $context);
	return $result;
}

function getRequest($url) {
	logDebug("-> getRequest(".$url);
	$result = file_get_contents($url);
	return $result;
}

function getActivityPoints($strava_activity_id, $strava_access_token) {
	logDebug("-> getActivityPoints(".$strava_activity_id.", ".$strava_access_token );
	
	$pointRatio = 3; 
	
	$url = "https://www.strava.com/api/v3/activities/" . $strava_activity_id . "/streams?keys=distance,time,latlng,altitude&key_by_type=true&access_token=" . $strava_access_token;
	$resultPoints = getRequest($url);
	if ($resultPoints === FALSE) { 
		logError("Error!");
		logDebug("result = ".$resultPoints);	
		return 0;	
	}
	else {
		logDebug("resultPoints = ".$resultPoints);
		$objPoints = json_decode($resultPoints);
		/*
		logDebug("objPoints = " . count($objPoints));		
		for ($tableIndex = 0; $tableIndex<count($objPoints); $tableIndex++) {			
			$table = $objPoints[$tableIndex];
			logDebug("table->type = " . $table->type);
			if ($table->type == "latlng")
				$objLatLngs = $table;
			else if ($table->type == "time")
				$objTimes = $table;
			else if ($table->type == "altitude")
				$objAltitude = $table;
			else if ($table->type == "distance")
				$objDistance = $table;
		}
		*/
		
		$objLatLngs = $objPoints->latlng;
		$objTimes = $objPoints->time;
		$objAltitude = $objPoints->altitude;
		$objDistance = $objPoints->distance;
		
		// distance
		// altitude
		$index = 0;
		$resultIndex = 0;
		logDebug("Points = " . count($objLatLngs->data));
		$resultArray = array();
		foreach($objLatLngs->data as $latlng) {
			if (round($index / $pointRatio) == ($index / $pointRatio)) {
				//logDebug("- " . $latlng[0] . " - " . $latlng[1] . " - " . $objTimes->data[$index] . " - " . $objAltitude->data[$index]);
				$resultArray[$resultIndex] = ['lat' => $latlng[0], 'lon' => $latlng[1], 'time' => $objTimes->data[$index], 'ele' => $objAltitude->data[$index], 'dist' => $objDistance->data[$index]];	
				$resultIndex++;
			}
			$index++;
		}
		logDebug("resultArray = " . count($resultArray));
		return json_encode($resultArray);
	}
}

function getActivities($strava_athlete_id, $strava_access_token) {
	logDebug("-> getActivities(".$strava_athlete_id.", ".$strava_access_token );
	$before = time();
	$after = time() - (30 * 24 * 60 * 60);
	$url = "https://www.strava.com/api/v3/athletes/" . $strava_athlete_id . "/activities?before=" . $before . "after=" . $after . "&per_page=80&page=1&access_token=" . $strava_access_token;
	$resultRoutes = getRequest($url);
	
	if ($resultRoutes === FALSE) { 
		logError("Error!");
		logDebug("result = ".$resultRoutes);	
		return 0;	
	}
	else {
		logDebug("resultRoutes = ".$resultRoutes);
		$objRoutes = json_decode($resultRoutes);
		foreach($objRoutes as $route) {
			logDebug("- " . $route->id . " - " . $route->name . " - " . $route->type);
		}
		return $resultRoutes;
	}
}

// --- Begin ---

logDebug("-> stravaWS.php");


if (isset($_GET['action']))
	$action = $_GET['action'];
else
	$action = null;
if (isset($_GET['activity']))
	$strava_activity_id = $_GET['activity'];
else	
	$strava_activity_id = null;

logDebug("action = ".$action);

$strava_access_token = $_SESSION['strava_access_token'];
$strava_athlete_id = $_SESSION['strava_athlete_id'];


if ($action == "activities") {
	$resultArray = getActivities($strava_athlete_id, $strava_access_token);
}
else if ($action == "points") {
	$resultArray = getActivityPoints($strava_activity_id, $strava_access_token);
}

header('Content-type: text/xml;  application/json', true);
?><?= $resultArray ?><?php exit; ?>
