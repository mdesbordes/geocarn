<?php
if (isset($_SESSION['user']))
	$userSession = $_SESSION['user'];
else
	$userSession = null;
?>

<script>
function shortString(text, maxLength) {
	if (text.length > maxLength)
	 	newtext = text.substr(0, (maxLength-3)) + "...";
	else
        newtext = text;
	return newtext;
}

function continuousSearch(searchField) {
	logDebug("-> continuousSearch(" + searchField.value);
	
	document.getElementById("searchResultDiv").style.visibility = 'visible';
	
	if (searchField.value.length > 1) {
		$.getJSON("searchWS.php?search=" + searchField.value, function(data) {
			searchResponse(data.tracks);
		});
	}
	else {
		$("#searchResultTable").html("");
		document.getElementById("searchResultDiv").style.visibility = 'hidden';
	}
}

function searchResponse(data) {		
	logDebug("-> searchResponse(" + data);
	$("#searchResultTable").html("");
	
	for (var i = 0; i < data.length; i++) {
        var row = $("<tr />");
		$("#searchResultTable").append(row);
		name = data[i].tourName;
		if (data[i].trackName != null && data[i].trackName != "")
			name += " / " + data[i].trackName;
		row.append("<td><img src='/images/" + data[i].activityId + ".png' width=20></td><td><a href='track.php?trackId=" + data[i].trackId + "' style='height : 20px; width: 370px; background : #FFF; color : #000;'>" + data[i].trackDate.substring(0,10) + " | " + shortText(name, 40, null) + "</a></td>");		
    }
}

function stopSearch(searchField) {
	document.getElementById("searchResultDiv").style.visibility = 'hidden';
}

</script>

<div id="topbar" class="topbar">

<ul id="menu">

		<li>
		<font size=+18><b>&nbsp;*&nbsp;</b></font>
		</li>

		<li>
                <a href="/">Dashboard</a>
        </li>

        <li>
			<?php
			if ($userSession != null && $userSession != "") {
			?>
                <a href="/map.php">Tracks</a>
				<ul>
                        <li><a href="/map.php?view=list">List</a></li>
                        <li><a href="/map.php?view=map">Map</a></li>
                </ul>
			<?php
			}
			?>
        </li>




		<li>
                <ul>

                </ul>
        </li>

        <li>
			<?php
			if ($userSession != null && $userSession != "") {
			?>
                <a href="#"><?= $userSession ?></a>
                <ul>
                        <li><a href="/activity.php">Activities</a></li>
                        <li><a href="/agenda.php">Agenda</a></li>
						<li><a href="/heatMap.php?activityId=0">Heat map</a></li>
						<li><a href="/green.php">Eco-friendly</a></li>
                        <li><a href="/map.php?view=plan">Planned routes</a></li>
                        <li><a href="/weight.php">Weight</a></li>					
                        <li><a href="/profile.php">Profile</a></li>
                        <li><a href="/logout.php">Logout</a></li>
                </ul>
			<?php
			}
			else
			{
			?>
				<a href="#" onClick="document.getElementById('loginWin').style.visibility = 'visible'; return false;">Login</a>

			<?php
			}
			?>
        </li>

		 <li>
                <ul>

                </ul>
        </li>

		<?php
		if ($userSession != null && $userSession != "") {
		?>
        <li>
                <a href="#">+</a>
				<ul>
                        <li><a href="#" onClick="document.getElementById('uploadTrackWin').style.visibility = 'visible'; return false;">Import gpx</a></li>							
						<li><a href="/addTrack.php?action=strava">From Strava</a></li>
						<li><a href="/addTrack.php?action=manual">Manual</a></li>
						<li><a href="/addTrack.php?action=plan">Plan a route</a></li>

                </ul>
        </li>
		<li style="width: 400px; text-align: left;">
                <input type=text name="searchField" id="searchField" onchange="continuousSearch(this);" onkeypress="this.onchange(this);" oninput="this.onchange(this);" onClick="stopSearch(this);" style="width: 250px;">	
				<div id=searchResultDiv style="background-color: #FFFFFF; border-style: solid; border-color: #EEEEEE; visibility:'hidden';">
                    <table id="searchResultTable">						
					</table>
                </div>
        </li>
		<?php
		}
		?>


</ul>
</div>

<script>
document.getElementById("searchResultDiv").style.visibility = 'hidden';
</script>
