<?php include 'header.php' ?>

<script>
lang = "fr";

var normalmap;
var bigmap;
var map;

var photoNum = 0;
var openskimap;
var trackId = <?= $_GET['trackId']; ?>;
var categoryId = -1;
var tourId = -1;
var tourLat;
var tourLon;
var timestamp;
var trackPointsLength = -1;
var mapSize = "NORMAL";

var trackNum = 0;

//var MA_CLE= "rhfiamuq2cejl2h4xgcmwpge";

var colors = [
  "#888888",
  "#00FF00",
  "#33FF00",
  "#66FF00",
  "#99FF00",
  "#CCFF00",
  "#FFFF00",
  "#FFCC00",
  "#FF9900",
  "#FF6600",
  "#FF3300",
  "#FF0000",
  "#CC0000",
  "#990000",
  "#660000",
  "#330000",
  "#000000"
];

window.onresize = function(event) {
    resizeDiv();
};

function init() {
	resizeDiv();
	
	$.getJSON("tracksWS.php?track=<?= $_GET['trackId']; ?>" , function(data) {
		tourId = data.tracks[0].tourId;
		tourLat = data.tracks[0].latitude;
		tourLon = data.tracks[0].longitude;
		categoryId = data.tracks[0].activityId;
		timestamp = new Number(data.tracks[0].trackTimestamp) + 43200;
		trackPointsLength = data.tracks[0].trackPoints;
		setTitle(data.tracks);
		drawTable(data.tracks);
		setImg(data.tracks);
		setDesc(data.tracks);
		//initGeoPortail();
		normalmap = initMap("map");
		//bigmap = initMap("bigMap");
		initTrackMap(normalmap);
		//initTrackMap(bigmap);
		initWeather();
		//populateTrackList(categoryId);
	});

	/*
	$.getJSON("tracksWS.php?action=photo&track=<?= $_GET['trackId']; ?>" , function(photoData) {
		initPhotos(photoData.photos);
	});
	*/
}

function initWeather() {
	$.getJSON("darkSkyWS.php?lat=" + tourLat + "&lon=" + tourLon + "&timestamp=" + timestamp , function(weatherData) {
		showWeather(weatherData);
	});
}

function showWeather(weatherData) {
	logDebug("-> showWeather(" + weatherData);
	logDebug("temp = " + weatherData.currently.temperature);
	logDebug("wind speed = " + weatherData.currently.windSpeed);
	logDebug("icon = " + weatherData.currently.icon);
	$("#weatherDiv").html("<table width=100% class='window'><tr><td>&nbsp;</td><td><img src='/images/weather/" + weatherData.currently.icon + ".png' width=38 height=38></td><td>Temperature : " + Math.round(weatherData.currently.temperature) + "&deg;C</td><td>Humidity : " + Math.round(weatherData.currently.humidity * 100) + "%</td><td>Wind  : " + Math.round(weatherData.currently.windSpeed) + " Km/h</td><td><img src='/images/wind/" + getWindIcon(weatherData.currently.windBearing) + ".gif' width=16 height=16 title='" + weatherData.currently.windBearing + "&deg'></td><td>&nbsp;</td></tr></table>");
}

function updateTables() {
	$.getJSON("tracksWS.php?track=<?= $_GET['trackId']; ?>" , function(data) {
		setTitle(data.tracks);
		drawTable(data.tracks);
		setDesc(data.tracks);
	});
}

function resizeDiv() {
	logDebug("-> resizeDiv()");
	var width = window.innerWidth
		|| document.documentElement.clientWidth
		|| document.body.clientWidth;

	var height = window.innerHeight
		|| document.documentElement.clientHeight
		|| document.body.clientHeight;

	/*
	if (photoNum > 0) {
		imageDivHeight = 80;
	}
	else {
		imageDivHeight = 0;
	}
	*/
	logDebug("photoNum = "+photoNum);
	heightShift = 120;
	document.getElementById("mainwindow").style.height = (height - heightShift) + "px";
	document.getElementById("mainTable").style.height = (height - heightShift) + "px";
	document.getElementById("trTop").style.height = (height - heightShift) + "px";
	//document.getElementById("trBottom").style.height = imageDivHeight + "px";
	document.getElementById("mainWinMap").style.height = (height - heightShift) + "px";
	document.getElementById("mainWinTable").style.height = (height - heightShift) + "px";

	google.maps.event.trigger(normalmap, 'resize');
}

function maxMinMap() {
	var width = window.innerWidth
		|| document.documentElement.clientWidth
		|| document.body.clientWidth;
	width = width - 40;

	var height = window.innerHeight
		|| document.documentElement.clientHeight
		|| document.body.clientHeight;

	if (mapSize == "NORMAL") {
		document.getElementById("mainWinMap").style.width = (width - 40) + "px";
		document.getElementById("map").style.width = "100%";
		google.maps.event.trigger(normalmap, 'resize');
		document.getElementById("mainWinTable").style.width = "20px";
		mapSize = "MAX";
	}
	else {
		document.getElementById("mainWinMap").style.width =  (width - 15 - 602) + "px";
		document.getElementById("map").style.width = "100%";
		google.maps.event.trigger(normalmap, 'resize');
		document.getElementById("mainWinTable").style.width = "602px";
		mapSize = "NORMAL";
	}
}

function setTitle(data) {
	title = "<table height=45><tr valign=middle>";
  title += "<td><a href='activity.php?activityId=" + data[0].activityId + "'><img src='/images/" + data[0].activityId + ".png' width=45 height=45 border=0></a></td>";
	title += "<td>&nbsp;</td>";
	title += "<td class='title' valign=middle><a href=/tour.php?tourId=" + data[0].tourId + ">" + data[0].tourName + "</a></td>";
	//$("#titleDiv").html(title);

	//title = "<img src='/images/" + data[0].activityId + ".png'>";
	if (data[0].trackName != "")
		title = title + "<td class='subtitle' valign=middle onMouseOut=\"hideField(this);\" onMouseOver=\"showField(this);\" ondblclick=\"updateField('NAME', '" + data[0].trackName + "');\">&nbsp;&nbsp;/&nbsp;&nbsp;" + data[0].trackName + "</td>";
	else
		title = title + "<td class='subtitle' valign=middle onMouseOut=\"hideField(this);\" onMouseOver=\"showField(this);\" ondblclick=\"updateField('NAME', '');\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";
	title = title + "</td></tr></table>";
	$("#titleDiv").html(title);

	titleToolbar = "<table height=45>";
	titleToolbar = titleToolbar + "<tr valign=middle>";
	if (data[0].transport == 1)
		titleToolbar = titleToolbar + "<td rowspan=2 valign=middle onMouseOut=\"hideField(this);\" onMouseOver=\"showField(this);\" ondblclick=\"updateField('TRANSPORT', '1');\"><img src=/images/ecocar.png width=24></td>";
	else 
		titleToolbar = titleToolbar + "<td rowspan=2 valign=middle onMouseOut=\"hideField(this);\" onMouseOver=\"showField(this);\" ondblclick=\"updateField('TRANSPORT', '0');\"><img src=/images/transp.gif width=24></td>";
	titleToolbar = titleToolbar + "<td class='titleDate' align=center onMouseOut=\"hideField(this);\" onMouseOver=\"showField(this);\" ondblclick=\"updateField('DATETIME', '" + data[0].trackDate + "');\">" + shortDate(data[0].trackDate, 10) + "</td>";
	titleToolbar = titleToolbar + "</tr>";
	if (trackPointsLength > 0) {
		titleToolbar = titleToolbar + "<tr valign=middle>";
		titleToolbar = titleToolbar + "<td align=center><a href='/gpx.php?tourId=" + tourId + "&trackId=" + trackId + "'><img src='/images/gpx.gif' width=20 border=0></a>";
		titleToolbar = titleToolbar + "<a href='/kml.php?tourId=" + tourId + "&trackId=" + trackId + "'><img src='/images/googleearth.gif' width=20 border=0></a></td>";
		titleToolbar = titleToolbar + "</td>";
		titleToolbar = titleToolbar + "</tr>";
	}
	titleToolbar = titleToolbar + "</table>";
	$("#titleDivToolBar").html(titleToolbar);
}

function setImg(data) {
	logDebug("setImg("+data);
	logDebug("data[0].trackPoints = "+data[0].trackPoints);
	if (data[0].trackPoints > 0) {
		img = "<img src='imgAltitude.php?tourId=" + tourId + "&trackId=" + trackId + "&rel=1' width=600 height=250>";
		colorScale = "<table width=100%><tr><td>Grade (%)</td>";
		for (i = 0; i < colors.length; i++)
			colorScale += "<td bgcolor='" + colors[i] + "' align=center width=5%>" + ((i*2) + 1) + "</td>";
		colorScale += "</tr></table>";
		$("#altitudeWinInner").html(img + colorScale);
	}
	else {
		$("#altitudeWinInner").height(0);
	}
}

function setDesc(data) {
	$("#descriptionDiv").html("<table width=100%><tr><td width=100% onMouseOut=\"hideField(this);\" onMouseOver=\"showField(this);\" ondblclick=\"updateField('DESCRIPTION', '" + data[0].description + "');\">" + data[0].description + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td></tr></table>");
}

function drawTable(data) {
	for (var i = 0; i < data.length; i++) {
        drawRow(i, data[i]);
    }
}

function drawRow(rowId, rowData) {
	var c = [];

	if (rowData.activityId == 4) {
		distance = Math.round(rowData.trackDistance * 100 / KMtoNM) / 100 + " NM";
		avgSpeed = Math.round(rowData.trackAverageSpeed * 100 / KMtoNM) / 100 + " K";
	}
	else {
		distance = rowData.trackDistance + " Km";
		avgSpeed = rowData.trackAverageSpeed + " Km/h";
	}
	//var row = $("<tr />");
    //$("#tracksTable").append(row); //this will append tr element to table... keep its reference for a while since we will add cels into it
    c.push("<tr><td align=center colspan=2>Distance</td></tr>");
	c.push("<tr><td align=center class='dataBig' colspan=2 onMouseOut=\"hideField(this);\" onMouseOver=\"showField(this);\" ondblclick=\"updateField('DISTANCE', '" + rowData.trackDistance + "');\">" + distance + "</td></tr>");
	if (rowData.planned == 0) {
		c.push("<tr><td align=center>Average speed</td><td align=center>Duration</td></tr>");
		c.push("<tr><td align=center class='dataBig' onMouseOut=\"hideField(this);\" onMouseOver=\"showField(this);\" ondblclick=\"updateField('AVERAGE_SPEED', '" + rowData.trackAverageSpeed + "');\">" + avgSpeed + "</td><td align=center class='dataBig' onMouseOut=\"hideField(this);\" onMouseOver=\"showField(this);\" ondblclick=\"updateField('DURATION', '" + rowData.trackDuration + "');\">" + rowData.trackDuration + "</td></tr>");
	}
	if (rowData.activityId != 2 && rowData.activityId != 4 && rowData.activityId != 8) {
		if (rowData.planned == 0) {
			c.push("<tr><td align=center>Average up speed</td><td align=center>Duration up</td></tr>");
			c.push("<tr><td align=center class='data' onMouseOut=\"hideField(this);\" onMouseOver=\"showField(this);\" ondblclick=\"updateField('AVERAGE_UP_SPEED', '" + rowData.trackAverageUpSpeed + "');\">" + rowData.trackAverageUpSpeed + " m/h</td><td align=center class='data' onMouseOut=\"hideField(this);\" onMouseOver=\"showField(this);\" ondblclick=\"updateField('DURATION_UP', '" + rowData.trackDurationUp + "');\">" + rowData.trackDurationUp + "</td></tr>");
		}
		c.push("<tr><td colspan=2 width=100%><table width=100%><tr><td align=center width=33%>Altitude difference</td><td align=center width=33%>Altitude min</td><td align=center width=33%>Altitude max</td></tr></table></td></tr>");
		c.push("<tr><td colspan=2 width=100%><table width=100%><tr><td align=center width=33% class='data' onMouseOut=\"hideField(this);\" onMouseOver=\"showField(this);\" ondblclick=\"updateField('ALT_DIFF', '" + rowData.trackAltDiff + "');\">" + rowData.trackAltDiff + " m</td><td align=center width=33% class='data' onMouseOut=\"hideField(this);\" onMouseOver=\"showField(this);\" ondblclick=\"updateField('ALT_MIN', '" + rowData.trackAltMin + "');\">" + rowData.trackAltMin + " m</td><td align=center width=33% class='data' onMouseOut=\"hideField(this);\" onMouseOver=\"showField(this);\" ondblclick=\"updateField('ALT_MAX', '" + rowData.trackAltMax + "');\">" + rowData.trackAltMax + " m</td></tr></table></td></tr>");
	}
	$("#tracksTable").html(c.join(""));
}

function updateField(fieldName, fieldValue) {
	var newValue = prompt("Enter new value for " + fieldName + " : ", fieldValue);
	if (newValue != null && newValue.length > 0)
		updateFieldWSCall(fieldName, newValue);
}

function updateFieldWSCall(fieldName, fieldValue) {
	$("#tracksTable").html("<tr><td align=center><img src='images/ajaxLoader.gif'> <p><p>&nbsp;</td></tr>");
	$.getJSON(encodeURI("tracksWS.php?action=UPDATE&track=" + trackId + "&fieldname=" + fieldName + "&fieldvalue=" + fieldValue + "&user=" + user) , function(data) {
		logDebug("tracksWS -> "+data);
		updateTables();
	});
}

function displayPhoto(url, width, height) {
	logDebug("-> displayPhoto(" + url + ", " + width + ", " + height);
	if (height > width)
		imgHeight = "height=" + document.getElementById("mainWinMap").style.height;
	else
		imgHeight = "";
	logDebug("imgHeight = " + imgHeight);
	logDebug("<img src='" + url + "' " + imgHeight + ">");
	$("#map").html("<table width=100% height=100% bgcolor=#FFFFFF><tr><td align=center valign=middle><img src='" + url + "' " + imgHeight + "></td></tr></table>");
}

function initPhotos(photos) {
	$("#photoTable").html("");
	var c = [];
	if (photos != null) {
		photoNum = photos.length;
		for (var i = 0; i < photos.length; i++) {
			url = photos[i].url;
			urlparts = url.split("/");
			urlbase = "";
			for (var j = 0; j < (urlparts.length -1); j++) {
				urlbase += urlparts[j] + "/";
			}
			imgName = urlparts[urlparts.length -1];
			//echo $urlBase . " - " . $urlExplode[count($urlExplode)-1] ."<br>";
			urlPhoto = urlbase + "s800/" + imgName;
			urlThumb = urlbase + "s64-c/" + imgName;
			c.push("<td align=center><a href=# onClick=\"displayPhoto('" + urlPhoto + "', " + photos[i].width + ", " + photos[i].height + ");\"><img src='" + urlThumb + "' width=64 height=64 border=0></a></td>");
		}
	}
	$("#photoTable").html(c.join(""));

	resizeDiv();
}

function showField(span) {
	span.style.backgroundColor = "#6a8fbd";
	span.style.borderColor = "#6a8fbd";
}

function hideField(span) {
	span.style.backgroundColor = "#FFFFFF";
	span.style.borderColor = "#FFFFFF";
}

function chooseCategory(theform) {
	logDebug("-> chooseCategory(" + theform);
	logDebug(theform.category.options[category.selectedIndex].value);
	theCategory = theform.category.options[category.selectedIndex].value;
	$.getJSON("tracksWS.php?track=latest&activity=" + theCategory + "&user=" + user + "&limit=500&planned=false" , function(data) {
		logDebug("data = " + data.length);
		var $select = $('#tour');
		$select.empty();
		var tracks = data.tracks;
		$select.append("<option value=''>...</option>");
		for (i = 0; i < tracks.length; i++)
		{
			displayName = shortDate(tracks[i].trackDate, 10) + " / " + tracks[i].tourName;
			if (tracks[i].trackName != "")
				displayName += " / " + tracks[i].trackName;
			$select.append('<option value=' + "tourId=" + tracks[i].tourId + "&trackId=" + tracks[i].trackId + '>' + displayName + '</option>');
		};
	});
}

function chooseTrack(theform) {
	logDebug(theform.tour.options[tour.selectedIndex].value);

	req = theform.tour.options[tour.selectedIndex].value;
	trackNum++;
	
	$.ajax({
	  type: "GET",
	  url: "/gpx.php?" +req,
	  dataType: "xml",
	  success: function(xml) {
		var points = [];
		$(xml).find("trkpt").each(function() {
		  var lat = $(this).attr("lat");
		  var lon = $(this).attr("lon");
		  var p = new google.maps.LatLng(lat, lon);
		  points.push(p);		  
		});

		var poly = new google.maps.Polyline({
		  // use your own style here
		  path: points,
		  strokeColor: colorTrackArray[trackNum],
		  strokeOpacity: .8,
		  strokeWeight: 4,
		});
		logDebug("poly = " + poly);
		logDebug("normalmap = " + normalmap);
		poly.setMap(normalmap);
	  }
	});

}

var iv= null;
var viewer=null;

// Definition url des services Geoportail
function geoportailLayer(name, key, layer, options)
{ var l= new google.maps.ImageMapType
  ({ getTileUrl: function (coord, zoom)
      {  return "https://wxs.ign.fr/" + key + "/geoportail/wmts?LAYER=" + layer
          + "&EXCEPTIONS=text/xml"
          + "&FORMAT="+(options.format?options.format:"image/jpeg")
          + "&SERVICE=WMTS&VERSION=1.0.0&REQUEST=GetTile"
          + "&STYLE="+(options.style?options.style:"normal")+"&TILEMATRIXSET=PM"
          + "&TILEMATRIX=" + zoom
          + "&TILECOL=" + coord.x + "&TILEROW=" + coord.y;
      },
    tileSize: new google.maps.Size(256,256),
    name: name,
    minZoom: (options.minZoom ? options.minZoom:0),
    maxZoom: (options.maxZoom ? options.maxZoom:18)
  });
  l.attribution = ' &copy; <a href="http://www.ign.fr/">IGN-France</a>';
  return l;
}
// Ajout de l'attribution Geoportail a la carte
function geoportailSetAttribution (map, attributionDiv)
{ if (map.mapTypes.get(map.getMapTypeId()) && map.mapTypes.get(map.getMapTypeId()).attribution)
  {  attributionDiv.style.display = 'block';
    attributionDiv.innerHTML = map.mapTypes.get(map.getMapTypeId()).name
      +map.mapTypes.get(map.getMapTypeId()).attribution;
  }
  else attributionDiv.style.display = 'none';
}

// Initialisation de la carte
function initMap(mapDiv)
{

// La carte Google
  map = new google.maps.Map( document.getElementById(mapDiv),
  {
    mapTypeId: google.maps.MapTypeId.SATELLITE,
	panControl: true,
	zoomControl: true,
	mapTypeControl: true,
	rotateControl: true,
	scaleControl: true,
	streetViewControl: true,
	overviewMapControl: true,
    mapTypeControlOptions: {
		mapTypeIds: ['carte', 'IGN Pentes', 'carte-litto', google.maps.MapTypeId.TERRAIN, google.maps.MapTypeId.SATELLITE, 'OSM', 'OTM', 'OCM', 'Outdoors', 'Nautical SWE', 'OpenSeaMap'],
		style:google.maps.MapTypeControlStyle.DROPDOWN_MENU
	},
    center: new google.maps.LatLng(tourLat, tourLon),
    zoom: 12,

  });
  map.setTilt(45);

  /** Definition des couches  */
  // Carte IGN
  map.mapTypes.set('carte', geoportailLayer("IGN", MA_CLE, "GEOGRAPHICALGRIDSYSTEMS.MAPS", { maxZoom:18 }));
  // Ajouter un control pour l'attribution
  var attributionDiv = document.createElement('div');
  attributionDiv.className = "attribution";
  geoportailSetAttribution(map, attributionDiv);
  map.controls[google.maps.ControlPosition.BOTTOM_RIGHT].push(attributionDiv);
  // Afficher / masquer le copyright en fonction de la couche
  google.maps.event.addListener(map, 'maptypeid_changed',
    function()
    {  geoportailSetAttribution(this, attributionDiv);
    });

	//SCANLITTO_PYR-JPEG_WLD_WM
	map.mapTypes.set('carte-litto', geoportailLayer("IGN Littoral", MA_CLE, "SCANLITTO_PYR-JPEG_WLD_WM", { maxZoom:18 }));
  // Ajouter un control pour l'attribution
  var attributionDiv = document.createElement('div');
  attributionDiv.className = "attribution";
  geoportailSetAttribution(map, attributionDiv);
  map.controls[google.maps.ControlPosition.BOTTOM_RIGHT].push(attributionDiv);
  // Afficher / masquer le copyright en fonction de la couche
  google.maps.event.addListener(map, 'maptypeid_changed',
    function()
    {  geoportailSetAttribution(this, attributionDiv);
    });
	
  //Define OSM map type pointing at the OpenStreetMap tile server
	map.mapTypes.set("OSM", new google.maps.ImageMapType({
		getTileUrl: function(coord, zoom) {
			return "https://tile.openstreetmap.org/" + zoom + "/" + coord.x + "/" + coord.y + ".png";
		},
		tileSize: new google.maps.Size(256, 256),
		name: "OSM",
		maxZoom: 18
	}));

  //Define OTM map type pointing at the OpenStreetMap tile server
	map.mapTypes.set("OTM", new google.maps.ImageMapType({
		getTileUrl: function(coord, zoom) {
			return "https://a.tile.opentopomap.org/" + zoom + "/" + coord.x + "/" + coord.y + ".png";
		},
		tileSize: new google.maps.Size(256, 256),
		name: "OTM",
		maxZoom: 18
	}));

	//Define OCM map type pointing at the Open Cycle Map tile server
	map.mapTypes.set("OCM", new google.maps.ImageMapType({
		getTileUrl: function(coord, zoom) {
			return "http://tile.thunderforest.com/cycle/" + zoom + "/" + coord.x + "/" + coord.y + ".png?apikey=" + OCM_KEY;
		},
		tileSize: new google.maps.Size(256, 256),
		name: "OpenCycleMap",
		maxZoom: 18
	}));
	
  //Define Outdoors map type pointing at the OpenStreetMap tile server
	map.mapTypes.set("Outdoors", new google.maps.ImageMapType({
		getTileUrl: function(coord, zoom) {
			return "http://tile.thunderforest.com/outdoors/" + zoom + "/" + coord.x + "/" + coord.y + ".png?apikey=" + OCM_KEY;
		},
		tileSize: new google.maps.Size(256, 256),
		name: "Outdoors",
		maxZoom: 18
	}));


		/*
		var openskimap = new google.maps.ImageMapType({
				getTileUrl: function(ll, z) {
					var X = ll.x % (1 << z);
					return "//skimap.org/OpenSkiMaps/tile/openskimap/" + z + "/" + X + "/" + ll.y + ".png";
				},
				tileSize: new google.maps.Size(256, 256),
				isPng: true,
				maxZoom: 18,
				name: "OpenSkiMap",
				alt: "OpenSkiMap"
			});
		*/

		map.mapTypes.set("Nautical SWE", new google.maps.ImageMapType({
			getTileUrl: function(coord, zoom) {
				return "https://map.eniro.com/geowebcache/service/tms1.0.0/nautical/"+zoom+"/"+ coord.x +"/"+((1 << zoom) - 1 - coord.y) + ".png";
				//return "http://map.eniro.com/geowebcache/service/gmaps?layers=nautical&zoom=" + zoom + "&x=" + coord.x + "&y=" + coord.y + "&format=image/png";
			},
			tileSize: new google.maps.Size(256, 256),
			name: "Nautical SWE",
			maxZoom: 18
		}));

		openseamap = new google.maps.ImageMapType({
			getTileUrl: function(coord, zoom) {
				return "https://tiles.openseamap.org/seamark/" + zoom + "/" + coord.x + "/" + coord.y + ".png";
			},
			tileSize: new google.maps.Size(256, 256),
			name: "OpenSeaMap",
			maxZoom: 18
		});

		//Define OSM map type pointing at the OpenSnowMap tile server
		openskimap = new google.maps.ImageMapType({
				getTileUrl: function(coord, zoom) {
				return "https://www.opensnowmap.org/opensnowmap-overlay/" + zoom + "/" + coord.x + "/" + coord.y + ".png";
			},
			tileSize: new google.maps.Size(256, 256),
			name: "OpenSkiMap",
			maxZoom: 18
			});
			
				//Define OSM map type pointing at the OpenSnowMap tile server
		mountainslopes = new google.maps.ImageMapType({
				getTileUrl: function(coord, zoom) {
				return "http://wxs.ign.fr/an7nvfzojv5wa96dsga5nk8w/geoportail/wmts?layer=GEOGRAPHICALGRIDSYSTEMS.SLOPES.MOUNTAIN&style=normal&tilematrixset=PM&Service=WMTS&Request=GetTile&Version=1.0.0&Format=image/png&TileMatrix="+zoom+"&TileCol="+coord.x+"&TileRow="+coord.y;
			},
			tileSize: new google.maps.Size(256, 256),
			name: "IGN Pentes",
			maxZoom: 18
			});

			// Carte IGN SLOPES
			/*
  map.mapTypes.set('carte-pentes', geoportailLayer("IGN Pentes", MA_CLE, "GEOGRAPHICALGRIDSYSTEMS.SLOPES.MOUNTAIN", { maxZoom:18 }));
  // Ajouter un control pour l'attribution
  var attributionDiv = document.createElement('div');
  attributionDiv.className = "attribution";
  geoportailSetAttribution(map, attributionDiv);
  map.controls[google.maps.ControlPosition.BOTTOM_RIGHT].push(attributionDiv);
  // Afficher / masquer le copyright en fonction de la couche
  google.maps.event.addListener(map, 'maptypeid_changed',
    function()
    {  geoportailSetAttribution(this, attributionDiv);
    });
	*/


		//map.overlayMapTypes.push(null); // create empty overlay entry
        //map.overlayMapTypes.setAt("1",openskimap);
    return map;
}
//google.maps.event.addDomListener(window, 'load', initMap);

function initTrackMap(themap) {
	var bikeLayer = new google.maps.BicyclingLayer();
	  bikeLayer.setMap(themap);

	if (categoryId == 4)
		themap.setMapTypeId("Nautical SWE");
	else if (categoryId == 7)
		themap.setMapTypeId("OCM");
	themap.setCenter(new google.maps.LatLng(tourLat, tourLon));

	if (trackPointsLength > 0) {
		$.ajax({
		  type: "GET",
		  url: "/gpx.php?tourId=" + tourId + "&trackId=" + trackId,
		  dataType: "xml",
		  success: function(xml) {
			var points = [];
			var bounds = new google.maps.LatLngBounds ();
			$(xml).find("trkpt").each(function() {
			  var lat = $(this).attr("lat");
			  var lon = $(this).attr("lon");
			  var p = new google.maps.LatLng(lat, lon);
			  points.push(p);
			  bounds.extend(p);
			});

			var poly = new google.maps.Polyline({
			  // use your own style here
			  path: points,
			  strokeColor: colorTrackArray[0],
			  strokeOpacity: .8,
			  strokeWeight: 4,
			});
			logDebug(poly);

			poly.setMap(themap);

			// fit bounds to track
			themap.fitBounds(bounds);
		  }
		});
	}
}

function handleClickShowSlopes(checkBoxValue) {
	logDebug("-> handleClickShowSlopes(" + checkBoxValue);
	if (checkBoxValue) {
		logDebug("-> true - " + mountainslopes);
		map.overlayMapTypes.push(null); // create empty overlay entry
        map.overlayMapTypes.setAt("1",mountainslopes);
		//map.overlayMapTypes.insertAt(1, openskimap);
	}
	else {
		logDebug("-> false");
		map.overlayMapTypes.clear();
	}
}

function handleClickShowSkiSlopes(checkBoxValue) {
	logDebug("-> handleClickShowSkiSlopes(" + checkBoxValue);
	if (checkBoxValue) {
		logDebug("-> true - " + openskimap);
		map.overlayMapTypes.push(null); // create empty overlay entry
        map.overlayMapTypes.setAt("1",openskimap);
		//map.overlayMapTypes.insertAt(1, openskimap);
	}
	else {
		logDebug("-> false");
		map.overlayMapTypes.clear();
	}
}

function handleClickShowSea(checkBoxValue) {
	logDebug("-> handleClickShowSea(" + checkBoxValue);
	if (checkBoxValue) {
		logDebug("-> true - " + openseamap);
		map.overlayMapTypes.push(null); // create empty overlay entry
        map.overlayMapTypes.setAt("1",openseamap);
		//map.overlayMapTypes.insertAt(1, openskimap);
	}
	else {
		logDebug("-> false");
		map.overlayMapTypes.clear();
	}
}

function handleClickTilt(checkBoxValue) {
	logDebug("-> handleClickTilt(" + checkBoxValue);
	if (checkBoxValue) {
		logDebug("-> true - " + openseamap);
		map.setTilt(45);
		logDebug("-> tilt = " + map.getTilt());
		//map.overlayMapTypes.insertAt(1, openskimap);
	}
	else {
		logDebug("-> false");
		map.setTilt(0);
		logDebug("-> tilt = " + map.getTilt());
	}
}

</script>

<body onLoad="init();" onResize="resizeDiv();">

<?php include 'bodyHeader.php' ?>

<div id="titleDiv" class="title" style="height: 45px; position: relative; left: 0px; right: 140px; top: 45px;"></div>
<div id="titleDivToolBar" class="toolbar" style="width: 140px; height: 45px; position: absolute; top: 45px; right: 0px;"></div>

<div id="mainwindow" class="maiwindow" style="position: relative; top: 45px; left: 0px; height: 400px">
<table width=100% height=400 border=0 id="mainTable">
	<tr height=400 id="trTop">
		<td width=65% height=100% valign=top>
			<DIV id="mainWinMap" class="window" style="width: 100%; height: 100%; z-index : 3; overflow:hidden;">
				<div id="" class="windowtopbar" style="position: relative; top: 0px; left: 0px; height=15px; width: 100%">
				<table width=100% cellpadding=0 cellspacing=0 border=0><tr>
					<td class="windowtopbar" width=35%>Map</td>
					<td class="windowtopbar" width=1 valign=middle><img src="/images/transp.gif" height=16 width=1></td>
					<form id="showSlopes"><td class="windowtopbar" width=65% align=right valign=middle>
					<a href=# onClick="initMap();initTrackMap();">Map</a>
					&nbsp;&nbsp;
					<a href="track3D.php?trackId=<?= $_GET['trackId']; ?>">3D Map</a>
					&nbsp;&nbsp;
					<input type=checkbox name="tilt" onClick="handleClickTilt(this.checked);"> 45&deg;
					<input type=checkbox name="showSlopes" onClick="handleClickShowSkiSlopes(this.checked);"> Ski pistes
					<input type=checkbox name="showSlopes" onClick="handleClickShowSlopes(this.checked);"> Mountain slopes
					<input type=checkbox name="showSlopes" onClick="handleClickShowSea(this.checked);"> Sea marks
					&nbsp;&nbsp;
					<a href=# onClick="maxMinMap();"><img src="images/max-min-win.png" width=15 height=15 border=0 title="Main window" alt="Main window"></a>
					</td></form></tr>
				</table>
				</div>
				<div id="map" class="innerwindow" style="position: relative; top: 0px; width: 100%; height: 100% overflow:scroll;">

				</div>
			</div>
		</td>

		<td width=5><img src="images/transp.gif" height=200 width=1></td>

		<td width=602 height=100% valign=top>
			<DIV id="mainWinTable" class="window" style="width: 100%; height: 100%; z-index : 3; overflow:scroll;">
				<div id="" class="windowtopbar" style="position: relative; top: 0px; left: 0px; height=15px; width: 100%">
				<table width=100% cellpadding=0 cellspacing=0 border=0><tr>
					<td class="windowtopbar" width=35%>Track</td>
					<td class="windowtopbar" width=1><img src="/images/transp.gif" height=16 width=1></td>
					<td class="windowtopbar" width=65% align=right>
					<img src="images/help.gif" width=15 height=15 border=0 title="Main window" alt="Main window">
					</td></tr>
				</table>
				</div>
				<div id="mainWinInner" class="innerwindow" style="position: relative; top: 0px; width: 100%; height: 100% overflow:scroll;">
					<div id="subTitleDiv" style="width: 100%, height: 50px"></div>

					<table width=100% id="tracksTable" class="window">
					  <tr>
						<td align=center><img src='images/ajaxLoader.gif'> <p>
						  <p>&nbsp;</td>
					  </tr>
					</table>

					<div id="altitudeWinInner" style="position: relative; top: 10px; width: 100%, height: 220px"></div>

					<div id="descriptionDiv" style="position: relative; top: 10px; width: 100%, height: 100px" class="trackDesc"></div>

					<div id="weatherDiv" style="position: relative; top: 10px; width: 100%, height: 50px">Loading weather</div>
					
					<div style="position: relative; top: 20px; width: 100%, height: 50px">
						<form name=trackform >
						Add track on map : <br>
						Choose a category ; 
						<select id='category' name='category' onChange='chooseCategory(this.form);'>
						<script>
							for (i = 0; i <= 15; i++)
								document.write("<option value='" + i + "'>" + categoryArray[i] + "</option>");
						</script>
						</select>
						<br>
						Choose a track : <select id='tour' name='tour' onChange='chooseTrack(this.form);'></select>
						</form>
					</div>
				</div>
			</div>
		</td>
	</tr>
	<!--tr height=64 id="trBottom">
		<td colspan=3 width=100% height=80 valign=top>
			<div id="imageDiv" style="width: 100%; height: 80px; overflow-x:scroll; overflow-y: hidden;" class="trackDesc">
				<table id="photoTable" class="window">
				  <tr>
					<td align=center><img src='/images/ajaxLoader.gif'> <p>
					  <p>&nbsp;</td>
				  </tr>
				</table>
			</div>
		</td>
	</tr-->
</table>
</div>

<?php include 'bodyFooter.php' ?>

<!--div id="bigMap" style="height: 100%; width: 100%; position: absolute; left: 0px; top: 0px; z-index: -1;"></div-->

</body>

</html>
