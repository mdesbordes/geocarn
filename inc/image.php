<?php 
session_start();

$relative = $_GET['relative'];

header("Content-type: image/png");
$im = @imagecreate(538, 180)
    or die("Cannot Initialize new GD image stream");
$background_color = imagecolorallocate($im, 240, 240, 240);
$text_color = imagecolorallocate($im, 118, 134, 255);
$line_chart_color = imagecolorallocate($im, 255, 49, 49);
$line_vertical_color = imagecolorallocate($im, 255, 152, 152);
$line_100_color = imagecolorallocate($im, 195, 195, 195);
$line_1000_color = imagecolorallocate($im, 154, 154, 154);
imagestring($im, 3, 200, 8,  "Elevation chart", $text_color);
imageline($im, 8, 172, 530, 172, $text_color);
imageline($im, 8, 8, 8, 172, $text_color);

$distArray = $_SESSION['distArray'];
$latArray = $_SESSION['latArray'];
if ($distArray != null)
	$distArrayCount = count($distArray);
else
	$distArrayCount = "No array in Session";

$elevationArray = $_SESSION['elevationArray'];
$maxElevation = 0;
$minElevation = 9999999;

if ($elevationArray == null)
	$elevationArray = array(1300, 1400, 1500, 1550, 1600, 1900, 2100, 2125);

for ($i = 0; $i <  count($elevationArray); $i++) {
	if ($elevationArray[$i] > $maxElevation)
		$maxElevation = $elevationArray[$i];
	if ($elevationArray[$i] != 0 && $elevationArray[$i] < $minElevation)
		$minElevation = $elevationArray[$i];
}
if ($maxElevation != 0) {
	if ($relative == 1)
		$yscale = (150.0 / ($maxElevation - $minElevation));
	else
		$yscale = (150.0 / $maxElevation);
}
else
	$yscale = 1.0;
$xscale = 530.0 / count($elevationArray);
$prevX = 10;
$prevY = 170;
for ($i = 0; $i < count($elevationArray); $i++) {
	if ($elevationArray[$i] != 0) {
		$x = $i * $xscale + 10;
		if ($relative == 1)
			$y = (172-(($elevationArray[$i] - $minElevation) * $yscale));
		else
			$y = (172-($elevationArray[$i] * $yscale));
		if ($i > 0) {
			imageline($im, $prevX, $prevY, $x, $y, $line_chart_color);
		}
		//echo "$y<p>";
		//imageline($im, $x, 170, $x, $y, $line_vertical_color);
		$prevX = $x;
		$prevY = $y;
	}
}


for ($i = 0; $i < 5; $i++) {
	for ($j = 0; $j < 10 ;  $j++) {
		$elevation = $i * 1000 + $j * 100;
		if ($relative == 1)
			$y = (172-(($elevation - $minElevation) * $yscale));
		else
			$y = (172-($elevation * $yscale));
		imageline($im, 10, $y, 530, $y, $line_100_color);
	}
	$elevation = $i * 1000;
	if ($relative == 1)
		$y = (172-(($elevation - $minElevation) * $yscale));
	else
		$y = (172-($elevation * $yscale));
	imageline($im, 10, $y, 530, $y, $line_1000_color);
}

imagepng($im);
imagedestroy($im);
?>