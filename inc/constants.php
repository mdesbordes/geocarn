<?php

$ACTIVITY = array();
$ACTIVITY[0] = "na";
$ACTIVITY[1] = "skiing";
$ACTIVITY[2] = "skating";
$ACTIVITY[3] = "hiking";
$ACTIVITY[4] = "sailing";
$ACTIVITY[5] = "cycling";
$ACTIVITY[6] = "paragliding";

$TRACK_COLOR = array();
$TRACK_COLOR[0] = "#ff0000";
$TRACK_COLOR[1] = "#0000ff";
$TRACK_COLOR[2] = "#00ff00";
$TRACK_COLOR[3] = "#000099";
$TRACK_COLOR[4] = "#009900";
$TRACK_COLOR[5] = "#009999";
$TRACK_COLOR[6] = "#990000";
$TRACK_COLOR[7] = "#990099";

$HOME_DIR = "/home/a3953673/public_html/";
?>