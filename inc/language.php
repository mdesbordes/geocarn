<?php
require_once('logger.php');
$lang = "";
logInfo("0-lang = " . $_SESSION['lang']);

if ($_SESSION['lang'] == '1') {
	$lang = '';
	$_SESSION['lang'] = '';
}
if (isset($_REQUEST['lang']) && $_REQUEST['lang'] != '') {
	$lang = $_REQUEST['lang'];
	logInfo("1-lang = " . $lang);
}
else if ($_SESSION['lang'] == '') {
	$languages = strtolower( $_SERVER["HTTP_ACCEPT_LANGUAGE"] );
	$languages = explode( ",", $languages );
	logInfo("languages = " . count($languages) . " " . $languages[0] . " " . $languages[1] . " " . $languages[2] . " " . $languages[3]);
	$lang = substr($languages[0], 0, 2);
	logInfo("2-lang = " . $lang);
}
else if ($_SESSION['lang'] != '') {
	$lang = $_SESSION['lang'];
	logInfo("3-lang = " . $lang);
}
if ($lang == "")
	$lang = "en";

logDebug("4*lang = '" . $lang . "'");
	
if ($lang == "fr" || $lang == "sv" || $lang == "en") {
	logDebug("-> ");
}
else {
	logDebug("-> en ");
	$lang = "en";
}
$_SESSION['lang'] = $lang;
logDebug("5-lang = " . $_SESSION['lang']);
$langfile = "lang_$lang.ini";
logDebug("file = " . $langfile);
$ini_array = parse_ini_file($langfile);
//logDebug("ini_array = " . $ini_array);

function getLangProp($key) {
	global $ini_array;
	if (isset($ini_array[$key]) && $ini_array[$key] != null && $ini_array[$key] != '')
		return $ini_array[$key];
	else
		return $key;
}
?>