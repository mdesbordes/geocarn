<?php
if(!isset($_SESSION)) 
{ 
	session_set_cookie_params(680400);
	session_start(); 
} 
	
if (isset($_SESSION['user']) && $_SESSION['user'] != null && $_SESSION['user'] != "") {
	$IS_LOGGED_IN = true;
	$USERNAME = $_SESSION['user']; // TODO Get from DB
	$ADDRESS = $_SESSION['address'];
} 

$server = $_SERVER['SERVER_NAME'];

if ($server == "geocarn.free.fr") {
	$host="sql.free.fr"; 
	$userdb="geocarn"; 
	$base="geocarn"; 
	$passwddb="marco99"; 
}
else if ($server == "geocarn.olympe-network.com") {
	$host="sql.olympe-network.com"; 
	$userdb="geocarn"; 
	$base="geocarn"; 
	$passwddb="geo007"; 
}
else if ($server == "geocarn.host56.com") {
	$host="mysql9.000webhost.com"; 
	$userdb="a3953673_geocarn"; 
	$base="a3953673_geocarn"; 
	$passwddb="geo007"; 
}
else if ($server == "geocarn.horizon-host.com") {
	$host="mysql.horizon-host.com"; 
	$userdb="u148545268_geoc"; 
	$base="u148545268_geocarn"; 
	$passwddb="geo007"; 
}
else if ($server == "geocarn.freecluster.eu") {
	$host="sql201.epizy.com"; 
	$userdb="epiz_20753070"; 
	$base="epiz_20753070_geocarn"; 
	$passwddb="marco99"; 
}
else if ($server == "geocarn-env.avzztenr5u.eu-west-3.elasticbeanstalk.com") {
	//echo("RDS_HOSTNAME = " . getenv('RDS_HOSTNAME'));
	//echo("RDS_HOSTNAME = " . getenv('RDS_PASSWORD'));
	$host=getenv('RDS_HOSTNAME'); 
	$userdb=getenv('RDS_USERNAME');
	$base=getenv('RDS_DB_NAME'); 
	$passwddb=getenv('RDS_PASSWORD');  
}
else if ($server == "geocarn-marcdesbordes.rhcloud.com") {
	define('DB_HOST', getenv('OPENSHIFT_MYSQL_DB_HOST'));
	define('DB_PORT', getenv('OPENSHIFT_MYSQL_DB_PORT'));
	define('DB_USER', getenv('OPENSHIFT_MYSQL_DB_USERNAME'));
	define('DB_PASS', getenv('OPENSHIFT_MYSQL_DB_PASSWORD'));
	define('DB_NAME', getenv('OPENSHIFT_GEAR_NAME'));
	$host=constant("DB_HOST"); 
	$userdb=constant("DB_USER");
	$base=constant("DB_NAME"); 
	$passwddb=constant("DB_PASS"); 
}
else {
	$host="localhost"; 
	$userdb="root"; 
	$base="tourtracker"; 
	$passwddb="mysql"; 
}


/*
//on effectue la connexion
$db=@mysql_connect("$host","$userdb","$passwddb");
//Connexion to the database
$success=mysql_select_db("$base",$db);
//Si la connexion echoue
 if (!$success) {
 	echo ("Erreur de connexion � la BD!");
	exit;
 }
 */
 
 // New PHP 7
$db = new mysqli("$host","$userdb","$passwddb","$base");
if ($db -> connect_errno)
{
	printf("DB Connect error : %s\n", $db->connect_error);
	exit();
}
 
?>