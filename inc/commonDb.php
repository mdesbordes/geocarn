<?php include 'connectDb.php' ?><?php

ini_set('display_errors','On');

function getUserId($username, $db) {
	//echo ("getUserId($username, $db)");
	$requete = "SELECT USER_ID FROM USERS where USERNAME='$username'";
	$result = mysql_query($requete,$db);
	$item = mysql_fetch_object($result);
	//echo ("$item->USER_ID");
	return $item->USER_ID;
}
function getUsername($sessionId, $db) {
	//echo ("getUsername($sessionId, $db)");
	$requete = "SELECT USER_ID FROM SESSIONS where SESSION_ID='$sessionId'";
	//echo("$requete");
	$result = mysql_query($requete,$db);
	$item = mysql_fetch_object($result);
	//echo ("$item->USER_ID");
	$userId = $item->USER_ID;
	$requete = "SELECT USERNAME FROM USERS where USER_ID=$userId";
	$result = mysql_query($requete,$db);
	$item = mysql_fetch_object($result);
	//echo ("$item->USER_ID");
	return $item->USERNAME;
}
function getUserModified($userId, $db) {
	$requete = "SELECT MODIFIED FROM USERS where USER_ID='$userId'";
	$result = mysql_query($requete,$db);
	$item = mysql_fetch_object($result);
	return $item->MODIFIED;
}


function getDropListAlbums($db, $userId, $language, $sessionId) {
	$tableStr = "";
	$requete = "SELECT ID, TIMESTAMP, TITLE, TITLE_SE FROM ALBUMS ORDER BY TIMESTAMP DESC";
	//echo("$requete");
	$result = mysql_query($requete,$db);

	$numAlbums = 0;
	while ($meta =mysql_fetch_object($result))
	{
		if ($language == 'se')
			$title = $meta->TITLE_SE;
		else
			$title = $meta->TITLE;
		$albumDate = date("d/m/Y", $meta->TIMESTAMP);
		$tableStr .= "<option value='$meta->ID'>$albumDate | $title</option>";
		$numAlbums++;
	}
	if ($numAlbums == 0) {
		$tableStr .= "<option value='0'>No album available</option>";
	}
	return $tableStr;
}

function getLastestAlbum($db, $userId, $language, $sessionId) {
	$tableStr = "";
	$requete = "SELECT ID, TIMESTAMP, TITLE, DESCRIPTION, TITLE_SE, DESCRIPTION_SE, USER_ID FROM ALBUMS ORDER BY TIMESTAMP DESC";
	//echo("$requete");
	$result = mysql_query($requete,$db);

	$meta =mysql_fetch_object($result);

	$albumDate = date("d/m/Y", $meta->TIMESTAMP);

	$requete2 = "SELECT USERNAME FROM USERS WHERE USER_ID=$meta->USER_ID";
	$result2 = mysql_query($requete2,$db);
	$item =mysql_fetch_object($result2);

	$requete3 = "SELECT * FROM PHOTOS WHERE ALBUM_ID=$meta->ID";
	$result3 = mysql_query($requete3,$db);
	$photoCount = 0;
	$filethumb = '';
	while ($meta3 = mysql_fetch_object($result3)) {
		$photoFile[$photoCount] = $meta3->FILE_PATH;
		$photoCount++;
		$meta4 = $meta3;
	}
	if ($photoCount == 0)
		$filethumb = 'images/transp.gif';
	else {
		$photoNum = rand(0, $photoCount-1);
		//$file = $meta4->FILE_PATH;
		$file = $photoFile[$photoNum];
		$filethumb = "$file";
		$filethumb .= "_thumb.jpg";
		createThumb($file, $filethumb, 120, 120, false);
	}

	if ($language == 'se') {
		$title = $meta->TITLE_SE;
		$desc = $meta->DESCRIPTION_SE;
	}
	else {
		$title = $meta->TITLE;
		$desc = $meta->DESCRIPTION;
	}

	$tableStr .= "<td width=122 height=122 bgcolor=white align=center>";
	$tableStr .= "<a href=album.php?albumId=$meta->ID title='$title'><img src='$filethumb' border=0></a>";
	$tableStr .= "</td><td width=20><img src=images/transp.gif width=20 height=1></td><td width=400>";
	$tableStr .= "<a href=album.php?albumId=$meta->ID title='$desc'><span class=newstitle><b>$title</b></span></a>";
	$tableStr .= "&nbsp;&nbsp;&nbsp;&nbsp;$albumDate&nbsp;&nbsp;<p>";
	$tableStr .= "<b>$photoCount photos</b><p>";
	$tableStr .= "$desc";
	if (isAlbumMapped($db, $userId, $sessionId, $meta->ID) > 0) {
		$tableStr .= "<p><a href=map.php?albumId=$meta->ID><img src=images/map.gif border= 0></a>&nbsp;&nbsp;";
		$tableStr .= "<a href=kml.php?albumId=$meta->ID><img src=images/kmz.gif border= 0></a>";
	}
	$tableStr .= "</td>";
	return $tableStr;
}

function getAllAlbums($db, $userId, $language, $sessionId, $maxNum) {
	$tableStr = "";
	$requete = "SELECT ID, TIMESTAMP, TITLE, DESCRIPTION, TITLE_SE, DESCRIPTION_SE, USER_ID FROM ALBUMS ORDER BY TIMESTAMP DESC";
	//echo("$requete");
	$result = mysql_query($requete,$db);

	$numAlbums = 0;
	while ($meta =mysql_fetch_object($result))
	{
		if ($numAlbums < ($maxNum+1) && ($maxNum == 9999 || $numAlbums > 0)) {
			$albumDate = date("d/m/Y", $meta->TIMESTAMP);

			$requete2 = "SELECT USERNAME FROM USERS WHERE USER_ID=$meta->USER_ID";
			$result2 = mysql_query($requete2,$db);
			$item =mysql_fetch_object($result2);

			$requete3 = "SELECT * FROM PHOTOS WHERE ALBUM_ID=$meta->ID";
			$result3 = mysql_query($requete3,$db);
			$photoCount = 0;
			while ($meta3 =mysql_fetch_object($result3)) {
				if ($photoCount == 0) {
					$file = $meta3->FILE_PATH;
					$filethumb = "$file";
					$filethumb .= "_little.jpg";
					createThumb($file, $filethumb, 80, 80, true);
				}
				$photoCount++;
			}

			if ($language == 'se') {
				$title = $meta->TITLE_SE;
				$desc = $meta->DESCRIPTION_SE;
			}
			else {
				$title = $meta->TITLE;
				$desc = $meta->DESCRIPTION;
			}

			$tableStr .= "<tr valign=top height=10><td width=20><img src=images/transp.gif height=10 width=20></td>";
			$tableStr .= "<td width=82><img src=images/transp.gif height=10 width=82></td>";
			$tableStr .= "<td width=10><img src=images/transp.gif height=10 width=10></td>";
			$tableStr .= "<td width=288><img src=images/transp.gif height=10 width=288></td>";
			$tableStr .= "<td width=10><img src=images/transp.gif height=10 width=10></td>";
			$tableStr .= "<td width=100><img src=images/transp.gif height=10 width=100></td>";
			$tableStr .= "<td width=20><img src=images/transp.gif height=10 width=20></td>";
			$tableStr .= "</tr>\n";
			$tableStr .= "<tr valign=top height=10><td width=20>&nbsp;</td>";
			$tableStr .= "<td height=82 bgcolor=white valign=middle align=center><a href=album.php?albumId=$meta->ID title='$title'><img src='$filethumb' border=0></a></td>";
			$tableStr .= "<td width=10>&nbsp;</td>";
			$tableStr .= "<td height=10 valign=middle>";
			$tableStr .= "<a href=album.php?albumId=$meta->ID><span class=newstitle><b>$title</b></span></a><p>$desc";
			$tableStr .= "</td><td align=right valign=middle>";
			if (isAlbumMapped($db, $userId, $sessionId, $meta->ID) > 0) {
				$tableStr .= "<a href=map.php?albumId=$meta->ID><img src=images/map.gif border= 0></a>&nbsp;&nbsp;";
				$tableStr .= "<a href=kml.php?albumId=$meta->ID><img src=images/kmz.gif border= 0></a>";
			}
			$tableStr .= "</td><td align=right valign=middle>$albumDate<p><b>$photoCount photos</b>";
			$tableStr .= "</td><td></td></tr>\n";
		}
		$numAlbums++;
	}
	if ($numAlbums == 0) {
		$tableStr .= "<tr valign=top height=10><td width=10>&nbsp;</td><td width=180>Pas d'albums.</td></tr>\n";
	}
	else if ($numAlbums > $maxNum) {
		$tableStr .= "<tr valign=top height=10><td><img src=images/transp.gif height=5 width=1></td><td></td></tr>\n";
		$tableStr .= "<tr valign=top height=10><td><img src=images/transp.gif height=10 width=1></td><td></td><td></td>";
		$tableStr .= "<td align=right><a href=albumList.php><span class=newstitle><b>... see all $numAlbums albums</b></a></td></tr>\n";
	}
	return $tableStr;
}

function getAlbumList($db, $userId, $language, $sessionId) {
	$tableStr = "";
	$requete = "SELECT ID, TIMESTAMP, TITLE, TITLE_SE, DESCRIPTION, DESCRIPTION_SE, USER_ID FROM ALBUMS ORDER BY TIMESTAMP DESC";
	//echo("$requete");
	$result = mysql_query($requete,$db);

	$numAlbums = 0;
	while ($meta =mysql_fetch_object($result))
	{
		if ($language == 'se') {
			$title = $meta->TITLE_SE;
			$desc = $meta->DESCRIPTION_SE;
		}
		else {
			$title = $meta->TITLE;
			$desc = $meta->DESCRIPTION;
		}
		getShortString($title, 19);
		$tableStr .= "<tr><td><img src=images/transp.gif width=10 height=10></td>";
		$tableStr .= "<td width=100%><a href=album.php?albumId=$meta->ID title='$desc'>$title</a></td>";
		$tableStr .= "<td><img src=images/transp.gif width=10 height=10></td></tr>";
		$tableStr .= "<tr><td><img src=images/transp.gif width=10 height=5></td></tr>";
		$numAlbums++;
	}
	if ($numAlbums == 0) {
		$tableStr .= "<tr valign=top height=10><td width=10><td width=190>Pas d'albums.</td></tr>";
	}
	return $tableStr;
}

function getAlbumTitle($db, $userId, $language, $sessionId, $albumId) {
	$title = "";
	if ($albumId > 0) {
		$requete = "SELECT * FROM ALBUMS WHERE ID=$albumId";
		//echo("$requete");
		$result = mysql_query($requete,$db);
		$numAlbums = 0;
		while ($meta =mysql_fetch_object($result)) {
			if ($language == 'se') {
				$title = $meta->TITLE_SE;
			}
			else {
				$title = $meta->TITLE;
			}
			$albumUserId = "$meta->USER_ID";
		}
	}
	return $title;
}

function getAlbumUrl($db, $sessionId, $albumId) {
	$url = "";
	if ($albumId > 0) {
		$requete = "SELECT URL FROM ALBUMS WHERE ID=$albumId";
		//echo("$requete");
		$result = mysql_query($requete,$db);
		$numAlbums = 0;
		while ($meta =mysql_fetch_object($result)) {
			$url = "$meta->URL";
		}
	}
	if ($url != "")
		$url = "<a href='http://desbordes.family.free.fr/gallery/$url/' class=white>/gallery/$url/</a>";
	return $url;
}


function getPhotos($db, $userId, $language, $sessionId, $albumId) {
	$tableStr = "";

	if ($albumId > 0) {
		$requete = "SELECT * FROM ALBUMS WHERE ID=$albumId";
		//echo("$requete");
		$result = mysql_query($requete,$db);
		$numAlbums = 0;
		while ($meta =mysql_fetch_object($result)) {
			$albumUserId = "$meta->USER_ID";
				if ($language == 'se') {
				$title = $meta->TITLE_SE;
				$desc = $meta->DESCRIPTION_SE;
			}
			else {
				$title = $meta->TITLE;
				$desc = $meta->DESCRIPTION;
			}
			$tableStr .= "<tr><td></td><td colspan=10><span class=bigtext><b>$title</b></span></td></tr>";
			$tableStr .= "<tr><td></td><td colspan=10><img src=images/transp.gif height=5 width=1></td></tr>";
			$tableStr .= "<tr><td></td><td colspan=10>$desc</td></tr>";
			$tableStr .= "<tr><td width=10><img src=images/transp.gif height=20 width=10></td><td width=122><img src=images/transp.gif height=1 width=122></td>";
			$tableStr .= "<td width=10><img src=images/transp.gif height=1 width=10></td><td width=122><img src=images/transp.gif height=1 width=122></td>";
			$tableStr .= "<td width=10><img src=images/transp.gif height=1 width=10></td><td width=122><img src=images/transp.gif height=1 width=122></td>";
			$tableStr .= "<td width=10><img src=images/transp.gif height=1 width=10></td><td width=122><img src=images/transp.gif height=1 width=122></td></tr>";
		}

		$requete = "SELECT ID, TIMESTAMP, FILE_PATH, DESCRIPTION FROM PHOTOS WHERE ALBUM_ID=$albumId ORDER BY TIMESTAMP DESC";
		//echo("$requete");
		$result = mysql_query($requete,$db);
		$numPhotos = 0;
		while ($meta =mysql_fetch_object($result))
		{
			//if ($numPhotos == 0 || $numPhotos == 4 || $numPhotos == 8 || $numPhotos == 12 || $numPhotos == 16 || $numPhotos == 20 || $numPhotos == 24 || $numPhotos == 28 || $numPhotos == 32 || $numPhotos == 36 || $numPhotos == 40 || $numPhotos == 44 || $numPhotos == 48 || $numPhotos == 52 || $numPhotos == 56 || $numPhotos == 60 || $numPhotos == 64 || $numPhotos == 68 || $numPhotos == 72 || $numPhotos == 76 || $numPhotos == 80 || $numPhotos == 84 || $numPhotos == 88 || $numPhotos == 92 || $numPhotos == 96 || $numPhotos == 100)
			if (($numPhotos / 4) == (round($numPhotos / 4)))
				$tableStr .= "<tr valign=middle>";
			if ($meta->FILE_PATH != '') {
				$file = $meta->FILE_PATH;
				$filethumb = "$file";
				$filethumb .= "_thumb.jpg";
				$photoId = $numPhotos;
				if ($photoId < 10)
					$photoId = '0' + $photoId;
				createThumb($file, $filethumb, 120, 120, false);
				$photoId = $meta->ID;
				$description = $meta->DESCRIPTION;
				$description .= " | ";
				$description .= date("d/m/Y H:i", $meta->TIMESTAMP);

				$tableStr .= "<td width=10><img src=images/transp.gif width=10 height=140></td>";
				$tableStr .= "<td align=center>";
				$tableStr .= "<table cellpadding=5 cellspacing=5><tr><td align=center bgcolor=#FFFFFF>";
				$tableStr .= "<a href='photo.php?albumId=$albumId&photoId=$photoId' title=\"$description\"><img src='$filethumb' border=0 alt=\"$description\"></a>";
				$tableStr .= "</td></tr></table>";
				$tableStr .= "</td>";
			}
			//if ($numPhotos == 3 || $numPhotos == 7 || $numPhotos == 11 || $numPhotos == 15 || $numPhotos == 19 || $numPhotos == 23 || $numPhotos == 27 || $numPhotos == 31 || $numPhotos == 35 || $numPhotos == 39 || $numPhotos == 43 || $numPhotos == 47 || $numPhotos == 51 || $numPhotos == 55 || $numPhotos == 59 || $numPhotos == 63 || $numPhotos == 67 || $numPhotos == 71 || $numPhotos == 75 || $numPhotos == 79 || $numPhotos == 83 || $numPhotos == 87 || $numPhotos == 91 || $numPhotos == 95 || $numPhotos == 99 || $numPhotos == 103)
			if ((($numPhotos + 1)/ 4) == (round(($numPhotos + 1) / 4)))
				$tableStr .= "</tr><tr><td height=10 colspan=10><img src=images/transp.gif width=10 height=10></td></tr>";
			$numPhotos++;
		}
	}
	return $tableStr;
}

function getPhoto($db, $userId, $sessionId, $albumId, $photoId) {
	$tableStr = "";
	$topTableStr = "";

	if ($albumId > 0) {
		$requete = "SELECT ID, TIMESTAMP, FILE_PATH, DESCRIPTION FROM PHOTOS WHERE ALBUM_ID=$albumId AND ID=$photoId ORDER BY ID DESC";
		//echo("$requete");
		$result = mysql_query($requete,$db);
		while ($meta =mysql_fetch_object($result))
		{
			if ($meta->FILE_PATH != '') {
				$file = $meta->FILE_PATH;
				$description = $meta->DESCRIPTION;
				$description .= " | ";
				$description .= date("d/m/Y H:i", $meta->TIMESTAMP);
				$tableStr .= "<img src='$file' border=0 alt=\"$description\" title=\"$description\">";
			}
		}
	}
	return "$tableStr";
}

function getPhotoDescription($db, $userId, $language, $sessionId, $albumId, $photoId) {
	$tableStr = "";
	$topTableStr = "";
	$firstId = -1;
	$previousId = -1;
	$nextId = -1;
	$thisId = -1;

	if ($albumId > 0) {
		$requete = "SELECT ID, TIMESTAMP, FILE_PATH, DESCRIPTION, DESCRIPTION_SE FROM PHOTOS WHERE ALBUM_ID=$albumId ORDER BY ID DESC";
		$result = mysql_query($requete,$db);
		while ($meta =mysql_fetch_object($result))
		{
			$currentId = $meta->ID;
			if ($firstId == -1)
				$firstId = $currentId;
			if ($currentId == $photoId) {
				$thisId = $currentId;
				if ($meta->FILE_PATH != '') {
					if ($language == 'se') {
						$desc = $meta->DESCRIPTION_SE;
					}
					else {
						$desc = $meta->DESCRIPTION;
					}
					$tableStr .= "$desc";
				}
			}
			if ($thisId == -1)
				$previousId = $currentId;
			if ($thisId != -1 && $currentId != $photoId && $nextId == -1)
				$nextId = $currentId;
		}
		if ($previousId == -1)
			$previousId = $currentId;
		if ($nextId == -1)
			$nextId = $firstId;
	}
	$toolBox = getPhotoToolBox($db, $userId, $language, $sessionId, $albumId, $photoId);
	$linkPrevious = "<a href='photo.php?albumId=$albumId&photoId=$previousId' class=white title='Previous photo'><<</a>";
	$linkNext = "<a href='photo.php?albumId=$albumId&photoId=$nextId' class=white title='Next photo'>>></a>";
	return "<tr><td>&nbsp;&nbsp;</td><td width=300 align=left>$tableStr</td><td>&nbsp;&nbsp;</td><td width=72 align=right>$linkPrevious&nbsp;$toolBox&nbsp;$linkNext</td></tr>";
}

function getAlbumOwner($albumId, $db) {
	$requete = "SELECT USER_ID FROM ALBUMS WHERE ID=$albumId";

	$result = mysql_query($requete,$db);
	$item =mysql_fetch_object($result);

	return $item->USER_ID;
}

function getNumAlbums($db) {
	$requete = "SELECT * FROM ALBUMS";
	$result = mysql_query($requete,$db);

	$numAlbums = 0;
	while ($meta =mysql_fetch_object($result))
	{
		$numAlbums++;
	}
	return $numAlbums;
}

function getNumPhotos($db) {
	$requete = "SELECT * FROM PHOTOS";
	$result = mysql_query($requete,$db);

	$numPhotos = 0;
	while ($meta =mysql_fetch_object($result))
	{
		$numPhotos++;
	}
	return $numPhotos;
}

function getLastLogin($db, $userId) {
	$requete = "SELECT LAST_LOGIN2 FROM USERS where USER_ID='$userId'";
	$result = mysql_query($requete,$db);
	$item = mysql_fetch_object($result);
	$last_login = $item->LAST_LOGIN2;
	return $last_login;
}

function getLastLoginDate($db, $userId) {
	return date("d/m/Y", getLastLogin($db, $userId));
}

function getNewPhotos($db, $userId, $language, $sessionId) {
	$last_login = getLastLogin($db, $userId);

	//$requete = "SELECT * FROM t1.ALBUMS, t2.PHOTOS WHERE t2.TIMESTAMP > $last_login";
	$requete = "SELECT * FROM PHOTOS WHERE TIMESTAMP > $last_login ORDER BY ID DESC, ALBUM_ID DESC";
	$result = mysql_query($requete,$db);
	$tableStr = "";
	$thisAlbumId = -1;
	$previousAlbumId = -1;
	$photoCount = -1;
	while ($meta =mysql_fetch_object($result))
	{
		if ($thisAlbumId == -1 || $thisAlbumId != $meta->ALBUM_ID) {
			$thisAlbumId = $meta->ALBUM_ID;
			if ($previousAlbumId != -1) {
				$title = getAlbumTitle($db, $userId, $language, $sessionId, $previousAlbumId);
				$title = getShortString($title, 19);
				$tableStr .= "<tr><td></td><td><a href=album.php?albumId=$previousAlbumId><span class=newstitle>$title</span></a>";
				$tableStr .= "<br>$photoCount new photos!";
				$tableStr .= "</td></tr>";
				$tableStr .= "<tr><td>&nbsp;</td></tr>";
			}
			$previousAlbumId = $thisAlbumId;
			$photoCount = 0;
		}
		$photoCount++;
	}
	if ($photoCount > 0) {
		if ($previousAlbumId != -1) {
			$title = getAlbumTitle($db, $userId, $language, $sessionId, $previousAlbumId);
			$title = getShortString($title, 19);
			$tableStr .= "<tr><td></td><td><a href=album.php?albumId=$previousAlbumId><span class=newstitle>$title</span></a>";
			$tableStr .= "<br>$photoCount new photos!";
			$tableStr .= "</td></tr>";
			$tableStr .= "<tr><td>&nbsp;</td></tr>";
		}
	}
	else
		$tableStr .= "<tr><td></td><td>No new photo!</td></tr>";
	return $tableStr;
}

function getNewAlbums($db, $userId, $sessionId) {
	$last_login = getLastLogin($db, $userId);

	$requete = "SELECT * FROM ALBUMS WHERE TIMESTAMP > $last_login";
	$result = mysql_query($requete,$db);
	$albumLink = "";

	while ($meta =mysql_fetch_object($result))
	{
		$albumLink .= "<tr><td>&nbsp;&nbsp;<a href=albumDetail.php?albumId=$meta->ID title='$meta->DESCRIPTION'><span class=orangelittletext>$meta->TITLE >></span></a></td></tr>";
	}
	if ($albumLink != "")
		$albumLink .= "<tr><td>&nbsp;</td></tr>";
	return $albumLink;
}

function getPhotoLocations($db, $userId, $sessionId, $albumId) {
	$rtn = "";
	if ($albumId > 0) {
		$index = 0;
		$requete = "SELECT * FROM PHOTOS WHERE ALBUM_ID=$albumId";
		$result = mysql_query($requete,$db);
		while ($meta =mysql_fetch_object($result)) {
			$photoId = $meta->ID;
			if ($photoId > 0) {
				$rtn .= getPhotoLocation($db, $userId, $sessionId, $albumId, $photoId, $index);
				$index = $index + 1;
			}
		}
	}
	return $rtn;
}

function getPhotoLocation($db, $userId, $sessionId, $albumId, $photoId, $index) {
	echo("// photoId = $photoId\n\n");
	if (!$photoId > 0)
		return getPhotoLocations($db, $userId, $sessionId, $albumId);
	$latitude = "";
	$longitude = "";
	if ($albumId > 0) {
		$requete = "SELECT PHOTO_ID, LATITUDE, LONGITUDE FROM COORDINATE WHERE PHOTO_ID=$photoId";
		$result = mysql_query($requete,$db);
		if ($meta =mysql_fetch_object($result)) {
			$latitude = $meta->LATITUDE;
			$longitude = $meta->LONGITUDE;
		}
	}
	if ($latitude != "")
		return "photoIdArray[$index] = $meta->PHOTO_ID;\nlatitudeArray[$index] = $latitude;\nlongitudeArray[$index] = $longitude;\n";
	else
		return "";
}

function isAlbumMapped($db, $userId, $sessionId, $albumId) {
	if ($albumId > 0) {
		$requete = "SELECT * FROM COORDINATE WHERE PHOTO_ID IN (SELECT ID FROM `PHOTOS` WHERE ALBUM_ID=$albumId)";
		$result = mysql_query($requete,$db);
		$index = 0;
		while ($meta =mysql_fetch_object($result)) {
			$index++;
		}
	}
	return $index;
}

function getKmlFileForAlbum($db, $userId, $sessionId, $albumId) {
	$tableStr = "<Document>\n";
	$DOMAIN = "desbordes.family.free.fr";
	if ($albumId > 0) {
		$requete = "SELECT * FROM ALBUMS WHERE ID=$albumId";
		//echo("$requete");
		$result = mysql_query($requete,$db);
		$numAlbums = 0;
		while ($meta =mysql_fetch_object($result)) {
			$albumUserId = "$meta->USER_ID";
				if ($language == 'se') {
				$title = $meta->TITLE_SE;
				$desc = $meta->DESCRIPTION_SE;
			}
			else {
				$title = $meta->TITLE;
				$desc = $meta->DESCRIPTION;
			}
			$tableStr .= "<name>$title</name>\n";
			$tableStr .= "<open>1</open>\n";
			$tableStr .= "<description><![CDATA[$desc]]></description>\n";
			$tableStr .= "<Style id=\"sn_camera\">";
			$tableStr .= "<IconStyle>\n";
			$tableStr .= "<scale>1.2</scale>\n";
			$tableStr .= "<Icon>\n";
			$tableStr .= "<href>http://maps.google.com/mapfiles/kml/shapes/camera.png</href>\n";
			$tableStr .= "</Icon>\n";
			$tableStr .= "<hotSpot x=\"0.5\" y=\"0\" xunits=\"fraction\" yunits=\"fraction\"/>\n";
			$tableStr .= "</IconStyle>\n";
			$tableStr .= "<ListStyle>\n";
			$tableStr .= "</ListStyle>\n";
			$tableStr .= "</Style>\n";
		}

		$requete = "SELECT ID, TIMESTAMP, FILE_PATH, DESCRIPTION FROM PHOTOS WHERE ALBUM_ID=$albumId ORDER BY ID DESC";
		//echo("$requete");
		$result = mysql_query($requete,$db);
		$numPhotos = 0;
		while ($meta =mysql_fetch_object($result))
		{
			if ($meta->FILE_PATH != '') {
				$file = $meta->FILE_PATH;
				$filethumb = "$file";
				$filethumb .= "_thumb.jpg";
				$photoId = $numPhotos;
				if ($photoId < 10)
					$photoId = '0' + $photoId;
				createThumb($file, $filethumb, 120, 120, false);
				$photoId = $meta->ID;

				$requeteCoord = "SELECT PHOTO_ID, LATITUDE, LONGITUDE FROM COORDINATE WHERE PHOTO_ID=$photoId";
				$resultCoord = mysql_query($requeteCoord,$db);
				$latitude = "";
				if ($metaCoord =mysql_fetch_object($resultCoord)) {
					$latitude = $metaCoord->LATITUDE;
					$longitude = $metaCoord->LONGITUDE;
				}
				if ($latitude != "") {
					$tableStr .= "<Placemark>\n";
					$tableStr .= "<name>$meta->FILE_PATH</name>\n";
					$tableStr .= "<description><![CDATA[\n";
					$tableStr .= "<a href='http://$DOMAIN/gallery/photo.php?albumId=$albumId&photoId=$photoId' title=\"$meta->DESCRIPTION\"><img src='http://$DOMAIN/gallery/$filethumb' border=0 alt=\"$meta->DESCRIPTION\"></a>\n";
					$tableStr .= "]]></description>";
					$tableStr .= "<styleUrl>#sn_camera</styleUrl>";
					$tableStr .= "<Point>\n";
					$tableStr .= "<coordinates>$metaCoord->LONGITUDE,$metaCoord->LATITUDE,0</coordinates>\n";
					$tableStr .= "</Point>\n";
					$tableStr .= "</Placemark>\n";
				}
			}
			$numPhotos++;
		}
	}
	$tableStr .= "</Document>\n";
	$tableStr .= "</kml>\n";
	return $tableStr;

}

function getShortString($str, $len) {
	if (strlen($str) > $len) {
		$str = substr($str, 0, ($len-3));
		$str .= "...";
	}
	return $str;
}

function getTracksArray($result) {
	logDebug('-> getTracksArray(' . $result);

	global $ACTIVITY;

	$tracksArray = array();
	$str = "";
	$index = 0;
	$ids = array();
	$names = array();
	$descriptions = array();
	while ($meta = mysql_fetch_object($result))
	{
		$id = $meta->ID;
		$name = $meta->NAME;
		$description = $meta->DESCRIPTION;
		$date = $meta->DATETIME;
		$track = $meta->TRACK;
		$distance = $meta->DISTANCE;
		$altDiff = $meta->ALT_DIFF;
		$altgain = $meta->ALT_GAIN;
		$altmin = $meta->ALT_MIN;
		$altmax = $meta->ALT_MAX;
		$altstart = $meta->ALT_START;
		$duration = $meta->DURATION;
		$durationup = $meta->DURATION_UP;
		$averagespeed= $meta->AVERAGE_SPEED;
		$averagespeedup= $meta->AVERAGE_UP_SPEED;

		$ids[$index] = $id;
		$names[$index] = $name;
		$descriptions[$index] = $description;
		$dates[$index] = $date;
		$tracks[$index] = $track;
		$distances[$index] = $distance;
		$altDiffs[$index] = $altDiff;
		$altgains[$index] = $altgain;
		$altmins[$index] = $altmin;
		$altmaxs[$index] = $altmax;
		$altstarts[$index] = $altstart;
		$durations[$index] = $duration;
		$durationsup[$index] = $durationup;
		$averagespeeds[$index] = $averagespeed;
		$averagespeedsup[$index] = $averagespeedup;
		$index++;
	}
	$tracksArray['id'] = $ids;
	$tracksArray['name'] = $names;
	$tracksArray['description'] = $descriptions;
	$tracksArray['date'] = $dates;
	$tracksArray['track'] = $tracks;
	$tracksArray['distance'] = $distances;
	$tracksArray['altDiff'] = $altDiffs;
	$tracksArray['altgain'] = $altgains;
	$tracksArray['altmin'] = $altmins;
	$tracksArray['altmax'] = $altmaxs;
	$tracksArray['altstart'] = $altstarts;
	$tracksArray['duration'] = $durations;
	$tracksArray['durationup'] = $durationsup;
	$tracksArray['averagespeed'] = $averagespeeds;
	$tracksArray['averagespeedup'] = $averagespeedsup;

	// $altdiff, $altgain, $altmin, $altmax, $altstart, $timediff, $distance
	return $tracksArray;
}

function getTracks($db, $tourId) {
	global $ACTIVITY;

	$request = "";
	$request = "SELECT * FROM TRACKER_TRACK WHERE TOUR_ID='$tourId' AND PLANNED_ONLY = FALSE ORDER BY DATETIME DESC";
	$result = mysql_query($request,$db);

	return getTracksArray($result);
}


function getTour($db, $tourId) {
	global $ACTIVITY;

	$tour = array();

	$request = "SELECT * FROM TRACKER_TOUR WHERE ID=$tourId";
	$result = mysql_query($request,$db);
	while ($meta = mysql_fetch_object($result))
	{
		$activityName = $ACTIVITY[$meta->ACTIVITY_ID];
		$id = $meta->ID;
		$tour['name'] = $meta->NAME;
		$tour['date'] = $meta->DATETIME;
		$tour['userId'] = $meta->USER_ID;
		$tour['description'] = $meta->DESCRIPTION;
		$tour['country'] = $meta->COUNTRY;
		$tour['latitude'] = $meta->LATITUDE;
		$tour['longitude'] = $meta->LONGITUDE;
		$tour['country'] = $meta->COUNTRY;
		$tour['activityId'] = $meta->ACTIVITY_ID;
		$tour['planned'] = $meta->PLANNED_ONLY;
		$tour['activityName'] = $activityName;
		$photoId = $meta->PHOTO_ID;
		if ($photoId != '') {
			$photoReq = "SELECT * FROM TRACKER_IMAGE WHERE ID='$photoId'";
			$photoResult = mysql_query($photoReq,$db);
			while ($photoMeta = mysql_fetch_object($photoResult)) {
				$tour['photoUrl'] = $photoMeta->URL;
			}
		}
	}
	return $tour;
}

function getToursArray($result, $db) {
	logDebug("getToursArray(" . $result);
	global $ACTIVITY;

	$str = "";
	$index = 0;
	$ids = array();
	$names = array();
	$descriptions = array();
	$countries = array();
	$activities = array();
	$latitudes = array();
	$longitudes = array();
	$distances = array();
	$users = array();
	$photos = array();
	while ($meta = mysql_fetch_object($result))
	{
		$activity = $meta->ACTIVITY_ID;
		$activityName = $ACTIVITY[$activity];
		$id = $meta->ID;
		$date = $meta->DATETIME;
		$name = $meta->NAME;
		$description = $meta->DESCRIPTION;
		$country = $meta->COUNTRY;
		$latitude = $meta->LATITUDE;
		$longitude = $meta->LONGITUDE;
		$user = $meta->USER_ID;
		$photoId = $meta->PHOTO_ID;

		$ids[$index] = $id;
		$dates[$index] = $date;
		$names[$index] = $name;
		$descriptions[$index] = $description;
		$countries[$index] = $country;
		$activitiesId[$index] = $activity;
		$activities[$index] = $activityName;
		$latitudes[$index] = $latitude;
		$longitudes[$index] = $longitude;
		$users[$index] = $user;
		//$str .= "<br><img src='images/$activityNum.gif'>&nbsp;<a href='tour.php?id=$id'>$name, $country</a>";

		if ($photoId != null && $photoId != "") {
			$sql = "select * from TRACKER_IMAGE where ID = '$photoId'" ;

			$resultImg = mysql_query($sql, $db);
			while ($metaImg = mysql_fetch_object($resultImg))
			{
				$id = $metaImg->ID;
				$date = $metaImg->DATETIME;
				$url = $metaImg->URL;
			}

			$photos[$index] = $url;
		}
		$index++;
	}
	$tours['id'] = $ids;
	$tours['date'] = $dates;
	$tours['name'] = $names;
	$tours['description'] = $descriptions;
	$tours['country'] = $countries;
	$tours['activity'] = $activities;
	$tours['activityId'] = $activitiesId;
	$tours['latitude'] = $latitudes;
	$tours['longitude'] = $longitudes;
	$tours['user'] = $users;
	$tours['photos'] = $photos;
	return $tours;
}

function getToursTracksArray($result, $db, $startIndex, $dataLength) {
	logDebug("getToursTracksArray(" . $result);
	global $ACTIVITY;
	$dataNum = 0;

	$str = "";
	$index = 0;
	$tourIds = array();
	$tourNames = array();
	$descriptions = array();
	$tourCountries = array();
	$tourActivitiesId = array();
	$tourActivities = array();
	$latitudes = array();
	$longitudes = array();
	$trackDates = array();
	$trackIds = array();
	$trackNames = array();
	$trackDistances = array();
	$imageURLs = array();
	$users = array();
	$photos = array();
	// tour.ID tourID, tour.NAME tourNAME, tour.ACTIVITY_ID tourACTIVITY_ID, track.DATETIME trackDATETIME, track.DISTANCE trackDISTANCE
	while ($meta = mysql_fetch_object($result))
	{
		if ($index >= $startIndex && $dataNum < $dataLength) {
			$tourActivity = $meta->tourACTIVITY_ID;
			$tourActivityName = $ACTIVITY[$activity];
			$tourId = $meta->tourID;
			$tourName = $meta->tourNAME;
			//$description = $meta->DESCRIPTION;
			$tourCountry = $meta->tourCOUNTRY;
			$latitude = $meta->LATITUDE;
			$longitude = $meta->LONGITUDE;
			$user = $meta->USER_ID;
			$photoId = $meta->PHOTO_ID;
			$trackDate = $meta->trackDATETIME;
			$trackId = $meta->trackID;
			$trackName = $meta->trackNAME;
			$trackDistance = $meta->trackDISTANCE;
			$imageURL = $meta->imageURL;

			$tourIds[$dataNum] = $tourId;
			$tourNames[$dataNum] = $tourName;
			$tourCountries[$dataNum] = $tourCountry;
			$tourActivitiesId[$dataNum] = $tourActivity;
			$tourActivities[$dataNum] = $tourActivityName;
			$trackDates[$dataNum] = $trackDate;
			$trackIds[$dataNum] = $trackId;
			$trackNames[$dataNum] = $trackName;
			$trackDistances[$dataNum] = $trackDistance;
			$imageURLs[$dataNum] = $imageURL;
			$dataNum++;
			/*
			if ($photoId != null && $photoId != "") {
				$sql = "select * from TRACKER_IMAGE where ID = '$photoId'" ;

				$resultImg = mysql_query($sql, $db);
				while ($metaImg = mysql_fetch_object($resultImg))
				{
					$id = $metaImg->ID;
					$date = $metaImg->DATETIME;
					$url = $metaImg->URL;
				}

				$photos[$index] = $url;
			}
			*/
			$index++;
		}
	}
	logDebug("count(tourId) = " . count($tourIds));
	$tracks['startIndex'] = $startIndex;
	$tracks['dataLength'] = $dataLength;
	$tracks['tourIds'] = $tourIds;
	$tracks['tourNames'] = $tourNames;
	$tracks['tourCountries'] = $tourCountries;
	$tracks['tourActivitiesId'] = $tourActivitiesId;
	$tracks['tourActivities'] = $tourActivities;
	$tracks['trackDate'] = $trackDates;
	$tracks['trackIds'] = $trackIds;
	$tracks['trackName'] = $trackNames;
	$tracks['trackDistance'] = $trackDistances;
	$tracks['imageURL'] = $imageURLs;
	logDebug("count(tracks) = " . count($tracks));
	return $tracks;
}

function getUsersTours($db, $userId, $activityId, $season) {
	logDebug("getUsersTours(" . $userId . ", " . $activityId . ", " . $season);
	global $ACTIVITY;

	if ($season == 0) {
		$startDate = null;
	}
	else {
		if ($season == -1) {
			if ($activityId == 1 || $activityId == 2) {
				logDebug("date('M') < 9 : " . date("M") . " -> " . (date("M") < 9));
				if (date("M") < 9) {
					$startDate = (date("Y")-1) . "-09-01";
				}
				else
					$startDate = date("Y") . "-09-01";
				$endDate = date("Y") . "-12-31";
			}
			else {
				$startDate = date("Y"). "-01-01";
				$endDate = date("Y"). "-12-31";
			}
		}
		else {
			if ($activityId == 1 || $activityId == 2) {
				$startDate = ($season-1) . "-09-01";
				$endDate = $season. "-08-31";
			}
			else {
				$startDate = $season. "-01-01";
				$endDate = $season. "-12-31";
			}
		}
	}
	logDebug($startDate . " - " . $endDate);

	$tours = array();

	$request = "SELECT * FROM TRACKER_TOUR WHERE USER_ID='$userId' ";
	if ($activityId != -1)
		$request .= "AND ACTIVITY_ID=$activityId ";
	$request .= "AND ID IN (SELECT TOUR_ID FROM TRACKER_TRACK ";
	if ($startDate != null)
		$request .= "WHERE DATETIME >= '$startDate' AND DATETIME <= '$endDate'";
	$request .= ") AND PLANNED_ONLY = FALSE ORDER BY DATETIME DESC";
	logDebug($request);
	$result = mysql_query($request,$db);

	return getToursArray($result, $db);
}

function getUsersTrackList($db, $userId, $startIndex, $dataLength, $activityId) {
	logDebug("getUsersTrackList(" . $userId . ", " . $activityId);
	global $ACTIVITY;

	$tours = array();

	$request = "SELECT tour.ID tourID, tour.NAME tourNAME, tour.COUNTRY tourCOUNTRY, tour.ACTIVITY_ID tourACTIVITY_ID, track.DATETIME trackDATETIME, track.ID trackID, track.NAME trackNAME, track.DISTANCE trackDISTANCE, image.URL imageURL ";
	$request .= " FROM TRACKER_TOUR tour, TRACKER_TRACK track, TRACKER_IMAGE image WHERE tour.USER_ID =  '$userId' AND track.TOUR_ID = tour.ID AND image.ID = tour.PHOTO_ID";
	if ($activityId != -1)
		$request .= "AND tour.ACTIVITY_ID=$activityId AND track.PLANNED_ONLY = FALSE";
	$request .= " ORDER BY track.DATETIME DESC";
	logDebug($request);
	$result = mysql_query($request,$db);

	return getToursTracksArray($result, $db, $startIndex, $dataLength);
}

function getUsersTourList($db, $userId, $activityId) {
	logDebug("getUsersTours(" . $userId . ", " . $activityId);
	global $ACTIVITY;

	$tours = array();

	$request = "SELECT * FROM TRACKER_TOUR WHERE USER_ID='$userId' ";
	if ($activityId != -1)
		$request .= "AND ACTIVITY_ID=$activityId ";
	$request .= "AND ID IN (SELECT TOUR_ID FROM TRACKER_TRACK WHERE PLANNED_ONLY = FALSE";
	$request .= ") ORDER BY NAME ASC";
	logDebug($request);
	$result = mysql_query($request,$db);

	return getToursArray($result, $db);
}

function getLatestTours($db, $activityId, $country) {
	global $ACTIVITY;

	$tours = array();

	$request = "";
	$where = "";
	if ($activityId != "" && $activityId != -1)
		$where .= "ACTIVITY_ID=$activityId ";
	if ($country != "")
		$where .= "COUNTRY='$country' ";
	if ($where != "")
		$where = " WHERE " . $where;
		$request = "SELECT * FROM TRACKER_TOUR $where ORDER BY UPDATED DESC";
	$result = mysql_query($request,$db);

	return getToursArray($result, $db);
}

function getToursInArea($activityId, $minLat, $minLong, $maxLat, $maxLong, $username) {
	logDebug("getToursInArea(" . $activityId . ", " . $minLat . ", " . $minLong . ", " . $maxLat . ", " . $maxLong . ", " . $username);
	global $db;
	global $ACTIVITY;

	$tours = array();

	$request = "";
	$where = "";
	if ($activityId != "" && $activityId != -1)
		$where =  " ACTIVITY_ID=$activityId AND ";
	if ($username != null && $username != "")
		$where .= " USER_ID='$username' AND ";
	$where .= " LATITUDE > $minLat AND LATITUDE < $maxLat AND LONGITUDE > $minLong AND LONGITUDE < $maxLong";
	$request = "SELECT * FROM TRACKER_TOUR WHERE $where ORDER BY UPDATED DESC";
	logDebug($request);
	$result = mysql_query($request,$db);

	return getToursArray($result, $db);
}


function createAlbum($tourId, $trackId, $datetimeArray, $urlArray, $latArray, $lonArray, $widthArray, $heightArray, $num) {
	logDebug("createAlbum(" . $tourId. ", " . $trackId. ", " . $datetimeArray. ", " . $urlArray. ", " . $latArray. ", " . $lonArray);
	global $db;

	$rtn = array();
	$finalresult = 1;

	$user = $_SESSION['user'];

	$request = "";
	for ($i = 0; $i < $num; $i++) {
		$request = "INSERT INTO TRACKER_IMAGE (TOUR_ID,TRACK_ID ,USER_ID, DATETIME ,URL ,LATITUDE ,LONGITUDE ,WIDTH ,HEIGHT) VALUES('" . $tourId . "', '" . $trackId . "', '" . $user . "' , '" . $datetimeArray[$i] . "', '" . $urlArray[$i] . "', '" . $latArray[$i] . "', '" . $lonArray[$i] . "', '" . $widthArray[$i] . "', '" . $heightArray[$i] . "');";
		logDebug($request);
		$result = mysql_query($request,$db);
		logDebug($result);
		if ($result != 1) {
			$finalresult = $result;
		}
	}

	if ($finalresult != 1) {
		$rtn['error'] = "Creating album!";
	}
	else {
		$rtn['error'] = "";
		$rtn['user'] = $user;
		$rtn['tourId'] = $tourId;
		$rtn['trackId'] = $trackId;
	}
	return $rtn;
}

function getLatestPhotos($trackId, $tourId) {
	global $db;

	$sql = "select * from TRACKER_IMAGE where TRACK_ID = $trackId AND TOUR_ID = $tourId" ;

	$result = mysql_query($sql, $db);

	while ($meta = mysql_fetch_object($result))
	{
		$id = $meta->ID;
		$date = $meta->DATETIME;
	}
}

function getPhotosForTour($tourId) {
	global $db;

	$rtn = array();

	$sql = "select * from TRACKER_IMAGE where TOUR_ID = $tourId" ;

	$result = mysql_query($sql, $db);
	$index = 0;

	$ids = array();
	$urls = array();
	while ($meta = mysql_fetch_object($result))
	{
		$ids[$index] = $meta->ID;
		$date = $meta->DATETIME;
		$url = $meta->URL;
		$urls[$index] = $url;
		$index++;
	}
	$rtn['id'] = $ids;
	$rtn['url'] = $urls;
	return $rtn;
}

function setPhotoForTour($tourId, $photoId) {
	global $db;

	$error = "";
	$request = "UPDATE TRACKER_TOUR SET PHOTO_ID='$photoId' WHERE ID='$tourId'";
	$result = mysql_query($request,$db);
	if ($result == 1) {
	}
	else {
		$error = "Error saving photo.";
	}
	return $error;
}

function getUsersPhotosCount($userId) {
	global $db;
	$sql = "select count(*) from TRACKER_IMAGE where USER_ID = '$userId'" ;

	$result = mysql_query($sql, $db);
	$row = mysql_fetch_row($result);
	return $row[0];
}

function getUsersTracksCount($userId) {
	global $db;
	$sql = "select count(*) from TRACKER_TRACK where TOUR_ID IN (select ID from TRACKER_TOUR where USER_ID = '$userId') AND PLANNED_ONLY = FALSE" ;

	$result = mysql_query($sql, $db);
	$row = mysql_fetch_row($result);
	return $row[0];
}

function getUsersToursCount($userId) {
	global $db;
	$sql = "select count(*) from TRACKER_TOUR where USER_ID = '$userId'" ;

	$result = mysql_query($sql, $db);
	$row = mysql_fetch_row($result);
	return $row[0];
}

function getActivityInfoForUser($db, $userId, $activityId, $season) {
	logDebug("getActivityInfoForUser(" . $userId . ", " . $activityId . ", " . $season);
	global $ACTIVITY;

	if ($season != -1) {
		if ($activityId == 1 || $activityId == 2) {
			$startDate = ($season-1) . "-09-01";
			$endDate = $season. "-08-31";
		}
		else {
			$startDate = $season. "-01-01";
			$endDate = $season. "-12-31";
		}
	}
	else {
		if ($activityId == 1 || $activityId == 2) {
			logDebug("date('M') < 9 : " . date("M") . " -> " . (date("M") < 9));
			if (date("M") < 9) {
				$startDate = (date("Y")-1) . "-09-01";
			}
			else
				$startDate = date("Y") . "-09-01";
			$endDate = date("Y") . "-12-31";
		}
		else {
			$startDate = date("Y"). "-01-01";
			$endDate = date("Y"). "-12-31";
		}
	}
	logDebug($startDate . " - " . $endDate);

	logDebug($startDate . " - " . $endDate);
	$track= array();

	$request = "SELECT * FROM TRACKER_TRACK WHERE TOUR_ID IN (SELECT ID FROM TRACKER_TOUR WHERE USER_ID='$userId' AND ACTIVITY_ID=$activityId) AND DATETIME >= '$startDate' AND DATETIME <= '$endDate' AND PLANNED_ONLY = FALSE";
//echo $request;
	$result = mysql_query($request,$db);
	$index = 0;
	while ($meta = mysql_fetch_object($result))
	{
		$trackId = $meta->ID;
		$tourId = $meta->TOUR_ID;
		$track[$index]['trackId'] = $trackId;
		$track[$index]['tourId'] = $tourId;
		$track[$index]['name'] = $meta->NAME;
		$track[$index]['date'] = $meta->DATETIME;
		$track[$index]['distance'] = $meta->DISTANCE;
		$track[$index]['duration'] = $meta->DURATION;
		$track[$index]['altDiff'] = $meta->ALT_DIFF;
		$tourRequest = "SELECT * FROM TRACKER_TOUR WHERE ID='" . $tourId . "'";
		$tourResult = mysql_query($tourRequest ,$db);
		while ($tourMeta = mysql_fetch_object($tourResult))
		{
			$track[$index]['tourName'] = $tourMeta->NAME;
			$track[$index]['latitude'] = $meta->LATITUDE;
			$track[$index]['longitude'] = $meta->LONGITUDE;
		}
		$index++;
	}
	return $track;
}

function isUsernameAvailable($db, $username) {
	$request = "SELECT * FROM USER WHERE USERNAME = '$username'";
	$result = mysql_query($request,$db);
	while ($meta = mysql_fetch_object($result))
	{
		return FALSE;
	}
	return TRUE;
}

function registerUser($db, $username, $email, $password, $key) {
	$password = md5($password);
	$error = "";
	$request = "INSERT INTO USER (USERNAME,EMAIL,PASSWORD,REGKEY) VALUES ('$username', '$email', '$password', '$key')";
	$result = mysql_query($request,$db);
	if ($result == 1) {
	}
	else {
		$error = "Error registering user.";
	}
	return $error;
}

function loginUser($db, $username, $password) {
	logDebug("->loginUser( ".$db. ", ".$username);
	$password = md5($password);
	$user = array();
	$request = "SELECT * FROM USER WHERE USERNAME = '$username' AND PASSWORD = '$password'";
	//$result = mysql_query($request,$db);
	$result = $mysqli->query($request);
	logDebug("result = ".$result);
	$index = 0;
	//while ($meta = mysql_fetch_object($result))
	while ($meta = $result->fetch_object())
	{
		if ($meta->ACTIVE == 1) {
			$user['userid'] = $meta->ID;
			$user['countryCode'] = "FR";
			$user['country'] = "France";
			$user['address'] = "les adrets";
			$user['email'] = $meta->EMAIL;
			$user['lang'] = $meta->LANG;
			$user['weather'] = $meta->WEATHER;
			$user['albumuser'] = $meta->ALBUM_USER;
			$user['snow'] = $meta->SNOW;
			$user['wind'] = $meta->WIND;
			$user['description'] = $meta->DESCRIPTION;
			$user['userlat'] = $meta->LATITUDE;
			$user['userlon'] = $meta->LONGITUDE;
			$user['error'] = null;
		}
		else {
			$user['error'] = "Registration not confirmed";
		}
		$index++;
	}
	if ($index == 0)
		$user['error'] = "Wrong username or password";
	return $user;
}

function confirmRegistration($db, $email, $key) {
	$error = "";
	$request = "SELECT ID FROM USER WHERE EMAIL = '$email' AND REGKEY = '$key'";
	echo($request."<br>");
	$result = mysql_query($request,$db);
	echo($result ."<br>");
	$index = 0;
	while ($meta = mysql_fetch_object($result))
	{
		$index++;
	}
	echo($index."<br>");
	if ($index > 0) {
		$request = "UPDATE USER SET ACTIVE = 1 WHERE EMAIL = '$email' AND REGKEY = '$key'";
	echo($request."<br>");
		$result = mysql_query($request,$db);
	echo($result ."<br>");
		if ($result == 1) {
		}
		else {
			$error = "Error confirming registration.";
		}
	}
	else {
		$error = "Wrong email or key.";
	}
	return $error;
}

function addBeacon($db, $beaconId, $username) {
	$error = "";
	$request = "UPDATE USER SET WIND='$beaconId' WHERE USERNAME='$username'";
	$result = mysql_query($request,$db);
	if ($result == 1) {
	}
	else {
		$error = "Error saving beacon.";
	}
	return $error;
}


function getLatestTracks($db, $limit) {
	logDebug('-> getLatestTracks(' . $db);
	global $ACTIVITY;

	$request = "";
	$request = "SELECT TRACKER_TOUR.ACTIVITY_ID activityId, TRACKER_TRACK.DATETIME trackDate, TRACKER_TRACK.ID trackId, TRACKER_TRACK.TOUR_ID tourId, TRACKER_TOUR.NAME tourName, TRACKER_TRACK.NAME trackName, TRACKER_TRACK.DURATION trackDuration, TRACKER_TRACK.DISTANCE trackDistance, TRACKER_TRACK.ALT_GAIN trackAltGain FROM TRACKER_TRACK join TRACKER_TOUR on  TRACKER_TRACK.TOUR_ID=TRACKER_TOUR.ID order by TRACKER_TRACK.DATETIME DESC LIMIT " . $limit;
	logDebug($request);
	$result = mysql_query($request,$db);

	$rows = array();
	while($r = mysql_fetch_assoc($result)) {
		$rows['tracks'][] = $r;
	}

	return json_encode($rows);
}



?>
