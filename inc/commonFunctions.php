<?php

function strtolower_iso8859_1($s){
    $i = strlen($s);
    while ($i > 0) {
        --$i;
        $c =ord($s[$i]);
        if (($c & 0xC0) == 0xC0) {
            // two most significante bits on
            if (($c != 215) and ($c != 223)){ // two chars OK as is
                // to get lowercase set 3. most significante bit if needed:
                $s[$i] = chr($c | 0x20);
            }
        }
    }
	$s = strtolower($s);
	$firstChar = substr($s, 0, 1);
	$firstChar = strtoupper($firstChar);
	$ret = $firstChar . substr($s, 1);
    return $ret;
}    

function displayHelp($helpText, $helpDiv) {
	return " onMouseOver=\"displayHelp('$helpText', '$helpDiv');\" onMouseOut=\"hideHelp();\" style=\"margin:0; padding:0 display:inline\"";
}

function shortText($text, $maxLength) {
	
	if (strlen($text) > $maxLength) {
		$newtext = "<div style='display: inline;' title='$text' alt='$text'>" . substr($text, 0, $maxLength) . "...</div>";
	} else {
		$newtext = "<div style='display: inline;' title='$text' alt='$text'>" . $text . "</div>";
	}
	return $newtext;
}

function shortTextFormat($text, $maxLength, $tag) {
	
	if (strlen($text) > $maxLength) {
		$newtext = "<div style='display: inline;' title='$text' alt='$text'>" . "<" . $tag . ">" . substr($text, 0, $maxLength) . "..."  . "</" . $tag . ">" . "</div>";
	} else {
		$newtext = "<div style='display: inline;' title='$text' alt='$text'>" . "<" . $tag . ">" . $text . "</" . $tag . ">" . "</div>";
	}
	return $newtext;
}

function getMonthName($monthNum) {
	switch ($monthNum) {
    case 1:
        return "january"; break;
    case 2:
        return "february"; break;
	case 3:
        return "march"; break;
	case 4:
        return "april"; break;
	case 5:
        return "may"; break;
	case 6:
        return "june"; break;
	case 7:
        return "july"; break;
	case 8:
        return "august"; break;
	case 9:
        return "september"; break;
	case 10:
        return "october"; break;
	case 11:
        return "november"; break;
	case 12:
        return "december"; break;

	}
	return $monthNum;
}

function getPicasaImageUrl($url) {
	$urlExplode = explode('/', $url);
	$urlBase = "";
	for ($j = 0; $j < (count($urlExplode)-1); $j++) {
		$urlBase .= $urlExplode[$j] . "/";
	}
	$imgName = $urlExplode[count($urlExplode)-1];
	//echo $urlBase . " - " . $urlExplode[count($urlExplode)-1] ."<br>";
	$url = $urlBase . "s800/" . $imgName;
	return $url;
}

function getPicasaImageThumbUrl($url) {
	$urlExplode = explode('/', $url);
	$urlBase = "";
	for ($j = 0; $j < (count($urlExplode)-1); $j++) {
		$urlBase .= $urlExplode[$j] . "/";
	}
	$imgName = $urlExplode[count($urlExplode)-1];
	//echo $urlBase . " - " . $urlExplode[count($urlExplode)-1] ."<br>";
	$urlThumb = $urlBase . "s64-c/" . $imgName;
	return $urlThumb;
}
?>