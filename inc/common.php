<?php

if(!isset($_SESSION)) 
    { 
        session_set_cookie_params(680400);
		session_start(); 
    } 

include 'inc/logger.php';
include 'inc/constants.php';
include 'inc/language.php';
include 'inc/commonDb.php';
include 'inc/commonFunctions.php';
include 'inc/urlLoader.php';
include 'xmlparser.php';
include 'ipToLocationWS.php';

$server = $_SERVER['SERVER_NAME'];
$DOMAIN = "http://" . $server;

$IS_LOGGED_IN = false;
$USERNAME = "";

if (isset($_SESSION['user']) && $_SESSION['user'] != null && $_SESSION['user'] != "") {
	$IS_LOGGED_IN = true;
	$USERNAME = $_SESSION['user']; // TODO Get from DB
	$ADDRESS = $_SESSION['address'];
} 
else {
	$_SESSION['user'] = "";
	if (!isset($_SESSION['countryCode']) || ($_SESSION['countryCode'] == "" && $_SESSION['country'] == "")) {
		$ip = $_SERVER['REMOTE_ADDR'];

		$location = getLocationFromIpWS($ip);
		list($address, $countryCode, $countryName) = explode('|', $location);
		$_SESSION['country'] = $countryName;
		$_SESSION['countryCode'] = $countryCode;
/*
		if ($address != "*")
			$ADDRESS = $address;
		else
			$ADDRESS = "";
		$_SESSION['address'] = $ADDRESS;
*/
	}
}

// Activity
$activityId = null;
if (isset($_GET['activity']))
	$activityId = $_GET['activity'];
if ($activityId != null && activityId != "") {
	$_SESSION['sActivity'] = $activityId;
}
else if ($_SESSION['sActivity'] > 0) {
}
else {
	$_SESSION['sActivity'] = 0;
}
$activityId = $_SESSION['sActivity'];
$activityName = $ACTIVITY[$activityId];
$activityKey = "menu.activity.$activityName"; ?>