<?php

function searchString($input, $begins, $end) {
	$result = null;
	//displayCode("-> searchString(" . $begins[0] . ")");	
	for ($i = 0; $i < count($begins); $i++) {
		$start = strpos($input, $begins[$i]);
		//echo "<p>";
		//displayCode($begins[$i]);
		//displayCode("start = " . $start . " - " . $begins[$i] . "<br>");
		if ($start > -1) {
			$input = substr($input, $start + strlen($begins[$i]));
		}
		else {
			return null;
		}
	}
	$stop = strpos($input, $end);
	//echo "stop = " . $stop . "<br>";
	if ($stop > -1)
		$result = substr($input, 0, $stop);
	if ($result != null)
		$result = trim($result);
	//echo "<- result = " . $result . "<p>";
	return $result;
}

function searchSimpleString($input, $begin, $end) {
	$result = null;
	//displayCode("-> searchString(" . $begins[0] . ")");	
	$start = strpos($input, $begin);
	//echo "<p>";
	//displayCode($begins[$i]);
	//displayCode("start = " . $start . " - " . $begins[$i] . "<br>");
	if ($start > -1) {
		$input = substr($input, $start);
	}
	else {
		return null;
	}
	
	$stop = strpos($input, $end);
	//echo "stop = " . $stop . "<br>";
	if ($stop > -1)
		$result = substr($input, 0, $stop);
	if ($result != null)
		$result = trim($result);
	//echo "<- result = " . $result . "<p>";
	return $result;
}


function displayCode($mycode) {
	$stvarno = array ("<", ">");

	$zamjenjeno = array ("&lt;","&gt;");

	$code = str_replace($stvarno, $zamjenjeno, $mycode);
	
	echo $code . "<br>";
}
?>