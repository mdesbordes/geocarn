<?php
$loggedInStrava = FALSE;

function getRequest($url) {
	logDebug("-> getRequest(".$url);
	$result = file_get_contents($url);
	return $result;
}

function postRequest($url, $data) {
	logDebug("-> postRequest(".$url);
	
	// use key 'http' even if you send the request to https://...
	$options = array(
		'http' => array(
			'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
			'method'  => 'POST',
			'content' => http_build_query($data)
		)
	);
	$context  = stream_context_create($options);
	$result = file_get_contents($url, false, $context);
	return $result;
}

if (isset($_GET['strava_action']))
	$strava_action = $_GET['strava_action'];
else
	$strava_action = null;
	logDebug("strava_action = ".$strava_action);

	if (isset($_SESSION['strava_access_token'])) {
		$strava_access_token = $_SESSION['strava_access_token'];
		$url = "https://www.strava.com/api/v3/athlete?access_token=" . $strava_access_token;
		$resultAthlete = getRequest($url);
		$objAthlete = null;

		if ($resultAthlete === FALSE) { 
			logError("Error!");
			logDebug("result = ".$resultAthlete);		
		}
		else {
			logDebug("resultAthlete = ".$resultAthlete);
			$objAthlete = json_decode($resultAthlete);
			logDebug("- " . $objAthlete->id . " - " . $objAthlete->username);
			$loggedInStrava = TRUE;
		}
	}
	
?>