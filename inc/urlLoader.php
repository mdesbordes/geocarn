<?php include 'inc/constants.php' ?><?php
ini_set('display_errors','Off'); 

define('DATA_DIR', getenv('OPENSHIFT_DATA_DIR'));
$dataDir=constant("DATA_DIR"); 

function cleanString($content) {
	//bon caracteres
	$GoodCharaters = array ("�","�","�","�","�","�","�","�","�","�","�","�","*","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�");  
	 
	//Mauvais caract�res
	$BadCharacters = array ("¡","¢","£","¤","¥","¦","§","¨","©","ª","«","¬","�*","®","¯","°","±","²","³","´","µ","¶","·","¸","¹","º","»","¼","½","¾","¿","×","÷","À","Á","Â","Ã","Ä","Å","Æ","Ç","È","É","Ê","Ë","Ì","Í","Î","Ï","Ð","Ñ","Ò","Ó","Ô","Õ","Ö","Ø","Ù","Ú","Û","Ü","Ý","Þ","ß","� ","á","â","ã","ä","å","æ","ç","è","é","ê","ë","ì","�*","î","ï","ð","ñ","ò","ó","ô","õ","ö","ø","ù","ú","û","ü","ý","þ","ÿ");  
	
	return str_replace($BadCharacters ,$GoodCharaters ,$content);
}


function readUrlContent($url) {
	logDebug( '-> readUrlContent(' . $url . ')');
	$retry = 0;
	logDebug( $url );
	
	$content = "";
	while ($content == '' && $retry < 3) {
		ini_set("user_agent", 'Mozilla/5.0 (Windows NT 5.1) AppleWebKit/535.1 (KHTML, like Gecko) Chrome/14.0.835.202 Safari/535.1');
		$content=file_get_contents($url);
		//$content=get_html($url);
		
		if ($content == '') {
			sleep (5 * $retry + 1);
			$retry++;			
		}
	}
	if ($content != null && $content != '') {
		$content=cleanString($content);
	}
	return $content;
}

function readUrlFromCache($url, $time, $duration) {
	logDebug('-> readUrlFromCache(' . $url . ', '. $time . ', '. $duration . ')');
	//$CACHE_PATH = getenv('OPENSHIFT_DATA_DIR');
	$CACHE_PATH = "/files";
	
	$fichier_cache = $CACHE_PATH . '/' . md5($url) . '.cache';
	logDebug( $fichier_cache );
	
	$fichier_cache_existe = ( file_exists($fichier_cache) ) ? filemtime($fichier_cache) : 0;
	logDebug( $fichier_cache_existe . ' > ' . ( time() - $duration ));
	
	if ($fichier_cache_existe > time() - $duration ) {
		logDebug('readfile(' . $fichier_cache);
		$pointeur = fopen($fichier_cache, 'r');
		$content = '';
		while (!feof($pointeur)) {
            $content .= fread($pointeur, 4096);
        }
		fclose($pointeur);
	}
	else {
		logDebug('readUrlContent(' . $url);
		$content = readUrlContent($url);
		if ($content != null && $content != '') {
			logDebug('fopen(' . $fichier_cache);
			$pointeur = fopen($fichier_cache, 'w');
			logDebug($pointeur);
			fwrite($pointeur, $content);
			fclose($pointeur);
		}
	}
	return $content;
}

function getUrlContent($url, $time, $duration) {
	logDebug('getUrlContent(' . $url . ', '. $time . ', '. $duration . ')');
	//$content = readUrlFromCache($url, $time, $duration);
	$content = readUrlContent($url);
	return $content;
}

?>