<?php include "constants.php" ?><?php

$INFO =  "Info ";
$DEBUG = "Debug";
$ERROR = "Error";
$PERF = "Perf";

define('LOG_DIR', getenv('OPENSHIFT_DATA_DIR'));
$logDir=constant("LOG_DIR"); 

function initLog($type) {
	global $PERF;
	global $ERROR;

	//$logDir="logs";
	$logDir=$_SERVER['DOCUMENT_ROOT'] . "/logs/";
	$filename = $logDir;
	
	if ($type == $PERF)
		$filename .= "perf";
	else if ($type == $ERROR)
		$filename .= "error";
	else
		$filename .= "log";
		
	$filename .= date("Ymd", time()) . ".log";
	
	//$filename = "/var/log/httpd/error_log";
	
	$handle = fopen($filename, "a");
	return $handle;
}

function writelog($msg, $type) {
	$handle = initLog($type);
	$contents = date("H:i:s", time()) . " | " . $type . " | " . $_SERVER["SCRIPT_NAME"] . " | " . $msg . "\r\n";
	//echo($contents);
	fwrite($handle, $contents);
	fclose($handle);
}

function logDebug($msg) {
	global $DEBUG;
	writelog($msg, $DEBUG);
}

function logInfo($msg) {
	global $INFO;
	writelog($msg, $INFO);
}

function logError($msg) {
	global $ERROR;
	writelog($msg, $ERROR);
}

function logPerf($msg, $time) {
	global $PERF;
	
	$time = round($time * 10000) / 10000;
	$msg = $msg . " : " . $time;
	writelog($msg, $PERF);
}

function microtime_float()
{
    list($usec, $sec) = explode(" ", microtime());
    return ((float)$usec + (float)$sec);
}


?>