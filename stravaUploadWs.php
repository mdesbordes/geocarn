<?php include 'inc/logger.php' ?>
<?php 
if(!isset($_SESSION)) 
{ 
	session_set_cookie_params(680400);
	session_start(); 
} 

function postRequest($url, $data) {
	logDebug("-> postRequest(".$url);
	logDebug("strava_access_token = ".$_SESSION['strava_access_token']);
	
	// use key 'http' even if you send the request to https://...
	$options = array(
		'http' => array(
			'header'  => "Content-type: application/x-www-form-urlencoded\r\nAuthorization: Bearer " . $_SESSION['strava_access_token'] . "\r\n" ,
			'method'  => 'POST',
			'content' => http_build_query($data)
		)
	);
	logDebug("header = " .  $options['http']['header']);
	$context  = stream_context_create($options);
	$result = file_get_contents($url, false, $context);
	return $result;
}

function uploadActivity($file, $activity_type, $name, $description, $private, $trainer, $commute, $data_type, $external_id)
{
	logDebug("-> uploadActivity(".$file);
	$path = 'https://www.strava.com/api/v3/uploads';
	//$parameters['query'] = [
	$parameters = [
		'activity_type' => $activity_type,
		'name' => $name,
		'description' => $description,
		'private' => $private,
		'trainer' => $_SESSION['strava_athlete_id'],
		'commute' => $commute,
		'data_type' => $data_type,
		'external_id' => $external_id,
		'file' => $file
	];
	return postRequest($path, $parameters);        
}
	
$file = "/var/www/html/gpx/1539352055_Montbonnot_Gieres_Villard_Bonnot.gpx";
$name = "Test track";
$description = "description";
$private = 1;
$trainer = "";
$commute = "";
$activity_type = "";
$data_type = "gpx";
$external_id = "123";
	
$result = uploadActivity($file, $activity_type, $name, $description, $private, $trainer, $commute, $data_type, $external_id);	

logDebug("result = ".$result);

header('Content-type: text/xml;  application/json', true);
?><?= $result ?><?php exit; ?>