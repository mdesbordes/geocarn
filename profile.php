<?php include 'header.php' ?>
<?php include 'inc/strava.php' ?>

<body onLoad="init();">

<?php include 'bodyHeader.php' ?>

<div id="titleDiv" class="title" style="width: 100%, height: 45px; position: relative; top: 45px;"></div>
<div id="titleDivToolBar" class="toolbar" style="width: 100px; height: 45px; position: absolute; top: 45px; right: 0px;"></div>

<div id="maiwindow" class="maiwindow" style="position: relative; top: 45px; left: 0px; height: 80%">
<table width=100% height=100%>
	<tr height=100%>
		<td width=50%>
			<DIV id="mapWin" class="window" style="width: 100%; height: 100%; z-index : 3; overflow:hidden;">
				<div id="" class="windowtopbar" style="position: relative; top: 0px; left: 0px; height=15px; width: 100%">
				<table width=100% height=100% cellpadding=0 cellspacing=0 border=0><tr>
					<td class="windowtopbar" width=35%>Profile</td>
					<td class="windowtopbar" width=1><img src="images/transp.gif" height=16 width=1></td>
					<td class="windowtopbar" width=65% align=right>
					<a href=# onClick="document.getElementById('maiwindow').style.bottom='0px';document.getElementById('maiwindow').style.height='600px';"><img src="images/help.gif" width=15 height=15 border=0 title="Main window" alt="Main window"></a>
					</td></tr>
				</table>
				</div>
				<div id="profileWinInner" class="innerwindow" style="position: relative; top: 0px; width: 100%; height: 100%; overflow:hidden;"> 
					<p>
					<?= $_SESSION['user'] ?>
					
				</div>	
			</div>
		</td>
	
		<td width=5><img src="images/transp.gif" height=5 width=1></td>
	
		<td width=50%> 
		<DIV id="tableWin" class="window" style="width: 100%; height: 100%; z-index : 3; overflow:hidden;">
			<div id="" class="windowtopbar" style="position: relative; top: 0px; left: 0px; height=15px; width: 100%">
			<table width=100% cellpadding=0 cellspacing=0 border=0><tr>
				<td class="windowtopbar" width=35%>Strava</td>
				<td class="windowtopbar" width=1><img src="images/transp.gif" height=16 width=1></td>
				<td class="windowtopbar" width=65% align=right>
				<img src="images/help.gif" width=15 height=15 border=0 title="Main window" alt="Main window">
				</td></tr>
			</table>
			</div>
			<div id="mainWinInner" class="innerwindow" style="position: relative; top: 0px; width: 100%; height: 100%; overflow:scroll;"> 
				<!-- <?= $_SESSION['strava_access_token'] ?> -->
				<br>
				<?php
				if (!$loggedInStrava) {
				?>
				<a href=# onClick="window.open('https://www.strava.com/oauth/authorize?client_id=29307&redirect_uri=http://geocarn-env.avzztenr5u.eu-west-3.elasticbeanstalk.com/strava.php&response_type=code&approval_prompt=auto&scope=view_private,write', '_blank', 'toolbar=no,scrollbars=yes,resizable=yes,top=100,left=150,width=600,height=600'); ">Log into Strava >></a>
				<?php
				} else {
				?>
				
				<p>
				<img src="<?= $objAthlete->{'profile'} ?>">
				<p>
				<?= $objAthlete->{'firstname'} ?>&nbsp;<?= $objAthlete->{'lastname'} ?>
				<p>
				<?= $objAthlete->{'username'} ?>
				<p>
				<a href="https://www.strava.com/athletes/<?= $objAthlete->{'id'} ?>">Strava profile >></a>
				<p>
				&nbsp;
				<p>
				<a href=# onClick="window.open('/stravaLogout.php', '_blank', 'toolbar=no,scrollbars=yes,resizable=yes,top=100,left=150,width=600,height=600'); ">Logout trava >></a>
				<?php
				}
				?>
			  </div>
			</div>
		</td>
	</tr>
</table>
</div>

<?php include 'bodyFooter.php' ?>

</body>
</html>
