// --
// -- Ajax functions
// -- 

var CACHE_TIMEOUT = 120000; // 2 min

var AJAX_CACHE_URL = 0;
var AJAX_CACHE_DATETIME = 1;
var AJAX_CACHE_VALUE = 2;

var ajaxCacheArrayLength = 0;
var ajaxCacheArray = new Array();
for (i = 0; i < 3; i++)
	ajaxCacheArray[i] = new Array();

function cacheCall(url, value) {
	//logDebug("cacheCall(" + url + ", value");
	cacheIndex = -1;
	for (i = 0; i < ajaxCacheArray.length; i++) {
		if (ajaxCacheArray[AJAX_CACHE_URL][i] == url) {
			cacheIndex = i;
			break;
		}
	}
	if (cacheIndex == -1) {
		cacheIndex = ajaxCacheArrayLength;
		ajaxCacheArrayLength++;
	}
	var currentDate = new Date();
	//logDebug("cacheIndex = " + cacheIndex);
	ajaxCacheArray[AJAX_CACHE_URL][cacheIndex] = url;
	ajaxCacheArray[AJAX_CACHE_DATETIME][cacheIndex] = currentDate.getTime();
	ajaxCacheArray[AJAX_CACHE_VALUE][cacheIndex] = value;
	//logDebug("cacheCall -> " + cacheIndex);
}

function getCache(url) {
	//logDebug("getCache(" + url);
	rtn = null;
	cacheIndex = -1;
	for (i = 0; i < ajaxCacheArray.length; i++) {
		if (ajaxCacheArray[AJAX_CACHE_URL][i] == url) {
			cacheIndex = i;
			break;
		}
	}
	var currentDate = new Date();
	if (cacheIndex == -1 || (currentDate.getTime() - ajaxCacheArray[AJAX_CACHE_DATETIME][cacheIndex] > CACHE_TIMEOUT)) {
		rtn = null;
		//logDebug("cacheIndex -> null");
	}
	else {
		rtn = ajaxCacheArray[AJAX_CACHE_VALUE][cacheIndex];		
		//logDebug("cacheIndex -> " + cacheIndex);		
	}
	
	return rtn;
}

function AJAXInteraction(url, body, callback, isXML) {
	var req = init();
	
    req.onreadystatechange = processRequest;

    function init() {
      if (window.XMLHttpRequest) {
          xmlHttpReq = new XMLHttpRequest();
          if (xmlHttpReq.overrideMimeType) {
				if (isXML) {}
				else {
					xmlHttpReq.overrideMimeType("text/html; charset=ISO-8859-1");
				}
			}
        return xmlHttpReq;
      } else if (window.ActiveXObject) {
        return new ActiveXObject("Microsoft.XMLHTTP");
      }
    }

    function processRequest () {
		//logDebug("processRequest() - req.readyState = " + req.readyState);
      if (req.readyState == 4) {
		//logDebug("req.status = " + req.status + " - " + req.statusText);
        if (req.status == 200 || req.status == 0) {
		  cacheCall(url, req.responseText);
		  //logDebug("callback = " + callback);
          if (callback) {
			if (isXML)
				callback(req.responseXML);		  						
			else 
				callback(req.responseText);		  						
		  }
        }
      }
    }

    this.doGet = function() {
      if (getCache(url) == null) {
	    if (url.indexOf('?') > -1) {
			url = url + "&";
		}
		else {
			url = url + "?";
		}
		//url = url + SESSION_NAME + "=" + SESSION_ID;
		//logDebug("url = " + url);
		req.open("GET", url, true);
		req.send(null);
	  }
      else {
		if (callback) {
			callback(getCache(url));				
		}
	  }
    }

    this.doPost = function() {
      req.open("POST", url, true);
      req.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	  req.setRequestHeader("Content-length", body.length);
      req.setRequestHeader("Connection", "close");
      req.send(body);
		
    }
	
	return this;
}

function erreurAjax(){
	if (erreurPerso != null){
		eval(erreurPerso);
	} else {
		document.getElementById(blocElement).innerHTML='<img class="erreurRss" src="ima/erreur.gif" alt="erreur" />Le serveur distant n\'est pas joignable.';
	}
	clearTimeout(timerAjax);
}

function removeXML(message) {
	message = message.replace("<xml>", "");
	message = message.replace("<XML>", "");
	message = message.replace("</xml>", "");
	message = message.replace("</XML>", "");
	return message;
}
