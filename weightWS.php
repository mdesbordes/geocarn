<?php include 'inc/connectDb.php' ?>
<?php include 'inc/logger.php' ?>
<?php
function getWeightList($db, $user) {
	logDebug('-> getWeightList(' . $user);
	global $ACTIVITY;
	
	$request = "";
	$request = "SELECT weighttable.ID id, weighttable.WEIGHT weight, weighttable.DATETIME datetime, usertable.HEIGHT height FROM WEIGHT weighttable, USER usertable";
	$request .= " where weighttable.USER_ID = '$user' AND usertable.USERNAME = '$user' ";
	$request .= " order by DATETIME DESC";
	logDebug($request);
	mysqli_set_charset($db, "SET NAMES UTF8");
	$result = mysqli_query($db, $request);
	
	$rows = array();
	while($r = mysqli_fetch_assoc($result)) {
		$rows['weights'][] = $r;
	}	

	return json_encode($rows);
}

function addWeight($db, $user, $weight, $date) {
	logDebug('-> addWeight(' . $weight);
	global $ACTIVITY;
	
	$weight = str_replace(",", ".", $weight);
	
	$request = "";
	$request = "INSERT INTO WEIGHT (ID, DATETIME, WEIGHT, USER_ID) VALUES (NULL, '$date', '$weight', '$user')";
	logDebug($request);
	mysqli_set_charset($db, "SET NAMES UTF8");
	$result = mysqli_query($db, $request);
	
	/*
	$rows = array();
	if ($result === FALSE) {
	}
	else {
		while($r = mysql_fetch_assoc($result)) {
			$rows['weights'][] = $r;
		}	
	}
	return json_encode($rows);
	*/
	return "{}";
}

function deleteWeight($db, $user, $rowId) {
	logDebug('-> deleteWeight(' . $rowId);
	global $ACTIVITY;
	
	$request = "";
	$request = "DELETE FROM WEIGHT where ID=$rowId and USER_ID='$user'";
	logDebug($request);
	mysqli_set_charset($db, "SET NAMES UTF8");
	$result = mysqli_query($db, $request);
	
	/*
	$rows = array();
	while($r = mysql_fetch_assoc($result)) {
		$rows['weights'][] = $r;
	}	

	return json_encode($rows);
	*/
	return "{}";
}


// --- Begin ---

logDebug("-> weightWS.php");

if (isset($_POST['action']))
	$action = $_POST['action'];
else 
	$action = null;
if (isset($_GET['action']))
	$actionGet = $_GET['action'];
else 
	$actionGet = null;
//$user = $_GET['user'];
//$userId = $_POST['user'];
if (isset($_POST['weight']))
	$weight = $_POST['weight'];
else 
	$weight = null;
if (isset($_POST['date']))
	$date = $_POST['date'];
else 
	$date = null;
if (isset($_GET['rowId']))
	$rowId = $_GET['rowId'];
else 
	$rowId = null;

$user = $_SESSION['user'];
$userId = $_SESSION['user'];
logDebug("user = $user");
logDebug("action = $action");

if ($action == "ADD")
	$weightArray = addWeight($db, $userId, $weight, $date);
else if ($actionGet == "DEL")
	$weightArray = deleteWeight($db, $user, $rowId);
else 
	$weightArray = getWeightList($db, $user);
		
logDebug($weightArray);

header('Content-type: text/xml;  application/json', true);
?><?= $weightArray ?><?php exit; ?>