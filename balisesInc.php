<?php include 'inc/common.php' ?>
<?php
require 'inc/parser.php'; 
//require 'inc/urlLoader.php'; 

ini_set('display_errors', TRUE);

function translateDirection($direction) {
	$str = "";
	for ($i = 0; $i < strlen($direction); $i++) {
		$c = substr($direction, $i, 1);
		if ($c == 'O')
			$str .= "W";
		else
			$str .= $c;			
	}
	return $str;
}

$id = $_GET['wind'];  
if ($id != null) {
	$content = '';
	//$url = 'http://localhost/test/http/bd_balise-1.xml'; 
	$url = 'http://www.balisemeteo.com/wap/bd_balise.php?idBalise=' . $id; 
	
	$content=getUrlContent($url, null, 600); // 10 min cache
	//$content = file_get_contents($url);
		if ($content != null) {
			$content=cleanString($content);
		
			$name = searchString($content, array ("<em>"), "</em>");
			$update = searchString($content, array ("</em><br/>"), "<br/>");
			$direction = searchString($content, array ("<u>Vent moyen</u><br/>", "Direction : "), " - ");
			$speed = searchString($content, array ("<u>Vent moyen</u><br/>", "Vitesse : "), " km/h<br/>");
			$temp = searchString($content, array ("Temp : "), " degre C<br/>");
			$speedMin = searchString($content, array ("<u>Vent min</u><br/>", "Vitesse : "), " km/h<br/>");
			$speedMax = searchString($content, array ("<u>Vent max</u><br/>", "Vitesse : "), " km/h<br/>");
			$transdirection = translateDirection($direction);
		?>
		<style type="text/css">
		td {font-family:Arial, Helvetica;font-size: 12px;}
		</style>
		<table border=0 width=100%>
		<tr><td colspan=3 title="Update: <?= $update ?>" alt="Update: <?= $update ?>"><b><?= $name ?></b></td></tr>
		<tr><td>Wind:</td><td><b><?= $speed ?> km/h</b><br>(<?= $speedMin ?> - <?= $speedMax ?> km/h)</td><td><b><?= $direction ?></b>&nbsp;&nbsp;<img src='/images/wind/<?= $transdirection ?>.gif' alt="<?= $direction ?>" /></td></tr>
		<tr><td>Temp:</td><td><b><?= $temp ?>&deg;C</b></td></tr>
		<tr><td align=left></td><td align=right valign=middle colspan=2><i>Update: <?= $update ?></i></td></tr>
		<?php
		}
		else {
		?>
		Error
		<?php
		}
		?>
<?php 
} else {
?>
No wind station defined!
<?php 
}
?>