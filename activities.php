<?php include 'header.php' ?>

<script>
lang = "fr";

function init() {
	$("#titleDiv").html("");
	$("#subTitleDiv").html("");
	
	$.getJSON("tracksWS.php?track=latest&limit=500" , function(data) {
		drawTable(data.tracks);
	});
	
}


function drawTable(data) {
	drawHeader();
    for (var i = 0; i < data.length; i++) {
        drawRow(i, data[i]);
    }
	$("#tracksTable td").addClass("tracksTableIndex");
	$("#tracksTable > tbody > tr:odd").addClass("rowOdd");
    $("#tracksTable > tbody > tr:not(.odd)").addClass("rowEven"); 
}

function drawRow(rowId, rowData) {
	var row = $("<tr />")
    $("#tracksTable").append(row); //this will append tr element to table... keep its reference for a while since we will add cels into it
    row.append($("<td><img src='/images/" + rowData.activityId + ".png'></td>"));
    row.append($("<td><a href='track.php?trackId=" + rowData.trackId + "'>" + shortDate(rowData.trackDate, 10) + "</a></td>"));
    row.append($("<td><a href='tour.php?tourId=" + rowData.tourId + "'>"  + rowData.tourName + "</a></td>"));
	if (rowData.trackName != "")
		trackName = rowData.trackName;
	else 
		trackName = shortDate(rowData.trackDate, 10);
	row.append($("<td><a href='track.php?trackId=" + rowData.trackId + "'>" + trackName + "</a></td>"));
	row.append($("<td>" + rowData.trackDuration + "</td>"));
	row.append($("<td>" + rowData.trackDistance + " km</td>"));
	row.append($("<td>" + rowData.trackAltGain + " m</td>"));
	
}

function drawHeader() {
	$("#tracksTable").html("");
	var header = $('<tr/>');	
	$("#tracksTable").append(header);
	header.append($("<td class='indextab'>activityId</td>"));
	header.append($("<td class='indextab'>trackDate</td>"));
	header.append($("<td class='indextab'>tourName</td>"));
	header.append($("<td class='indextab'>trackName</td>"));
	header.append($("<td class='indextab'>trackDuration</td>"));
	header.append($("<td class='indextab'>trackDistance</td>"));
	header.append($("<td class='indextab'>trackAltGain</td>"));
}

function showActivityStat(data) {
	var activityData = [];
	for (var i = 0; i < data.length; i++) {
        activityData.push([data[i].activityId, data[i].activitySum]);
		logDebug(activityData[i]);
    }
	logDebug(activityData);
	showActivityStatChart(activityData);
}

$(function showActivityStatChart(activityData) {
    $('#container').highcharts({
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: 0,
            plotShadow: false
        },
        title: {
            text: 'Activities',
            align: 'center',
            verticalAlign: 'middle',
            y: 50
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                dataLabels: {
                    enabled: true,
                    distance: -50,
                    style: {
                        fontWeight: 'bold',
                        color: 'white',
                        textShadow: '0px 1px 2px black'
                    }
                },
                startAngle: -90,
                endAngle: 90,
                center: ['50%', '75%']
            }
        },
        series: [{
            type: 'pie',
            name: 'Activities',
            innerSize: '50%',
            data: activityData
        }]
    });
});

</script>

<body onLoad="init();">

<?php include 'bodyHeader.php' ?>

<div id="maiwindow" class="maiwindow" " style="position: absolute; top: 45px; left: 0px; bottom: -10px; right: -5px; ">
<table width=100% height=100%>
	<tr height=100%>
		<td width=85% height=100% valign=top>
			<DIV id="mainWin" class="window"  style="width: 100%; height: 100%; z-index : 3; overflow:hidden;">
			<div id="" class="windowtopbar" style="position: relative; top: 0px; left: 0px; height=15px; width: 100%">
			<table width=100% cellpadding=0 cellspacing=0 border=0><tr>
				<td class="windowtopbar" width=35%>Main</td>
				<td class="windowtopbar" width=1><img src="images/transp.gif" height=16 width=1></td>
				<td class="windowtopbar" width=65% align=right>
				<img src="images/help.gif" width=15 height=15 border=0 title="Main window" alt="Main window">
				</td></tr>
			</table>
			</div>
			<div id="mainWinInner" class="innerwindow" style="position: relative; top: 20px; width: 100%; height: 100% overflow:scroll; overflow-y: scroll"> 
			<table width=100% id="tracksTable" class="tracksTableIndex">
			  <tr> 
				<td align=center><img src='images/ajaxLoader.gif'> <p>
				  <p>&nbsp;</td>
			  </tr>
			</table>
		  </div>
		</div>
		</td>
	</tr>
</table>

</div>

<?php include 'bodyFooter.php' ?>

</body>
</html>
