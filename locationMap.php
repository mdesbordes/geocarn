<?php include 'inc/logger.php' ?>
<?php

session_start();

$_SESSION['user'] = "marco";

?>

<html lang="en">
<head>
  <META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <title>geocarn</title>

</head>

<link rel="shortcut icon" href="images/favicon.ico">

<link rel=stylesheet type=text/css href=common.css>
	  
	  
<script src=jquery-2.1.1.js LANGUAGE="JavaScript"></script>
<script src=common.js LANGUAGE="JavaScript"></script>
<script src=log.js LANGUAGE="JavaScript"></script>
<script src=ajax.js LANGUAGE="JavaScript"></script>
<script src=wind.js LANGUAGE="JavaScript"></script>
<script src=weather.js LANGUAGE="JavaScript"></script>
<script src=snow.js LANGUAGE="JavaScript"></script>

<script src="http://code.highcharts.com/highcharts.js"></script>
<script src="http://code.highcharts.com/modules/exporting.js"></script>
<script type="text/javascript" src="http://api.ign.fr/geoportail/api/js/latest/Geoportal.js"></script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyARw_8RDJJPsqe-m_tD1n3Ie9tBpTx2kL0"></script>


<script>
lang = "fr";
mapType = "<?= $_GET['mapType']; ?>";
view = "<?= $_GET['view']; ?>";

var markerArray = [];
var pointArray =  [];

var jsonData = null;

function init() {
	$("#titleDiv").html("");
	$("#subTitleDiv").html("");
	
	//setTitle();
	
	if (view == "map")
		showMap();
	else
		showList();
}

function showList() {
	$.getJSON("locationWS.php?limit=500" , function(data) {
		drawTable(data.locations);
	});
	document.getElementById("listTabDiv").className ="toplinkboxSel";
	document.getElementById("mapTabDiv").className ="toplinkbox";
	document.getElementById("agendaTabDiv").className ="toplinkbox";
}

function showMap() {
	$.getJSON("locationWS.php?limit=200" , function(data) {
		jsonData = data;
		logDebug("-> " + data);
		if (mapType == "GEO")
			initGeoPortail();
		else
			initGoogleMaps(data.locations);
	});
	document.getElementById("listTabDiv").className ="toplinkbox";
	document.getElementById("mapTabDiv").className ="toplinkboxSel";
	document.getElementById("agendaTabDiv").className ="toplinkbox";
}

function setTitle() {
	title = "";
	for (i = 1; i < 12; i++)
		title = title + "<img src='/images/" + i + ".png' width=45 height=45>&nbsp;&nbsp;";
	$("#titleDiv").html(title);
}

function drawTable(data) {
	document.getElementById("viewerDiv").innerHTML="<table width=100% id='tracksTable' class='tracksTableIndex'></table>";
	drawHeader();
    for (var i = 0; i < data.length; i++) {
        drawRow(i, data[i]);
    }
	$("#tracksTable td").addClass("tracksTableIndex");
	$("#tracksTable > tbody > tr:odd").addClass("rowOdd");
    $("#tracksTable > tbody > tr:not(.odd)").addClass("rowEven"); 
}

function drawRow(rowId, rowData) {
	var row = $("<tr />")
    $("#tracksTable").append(row); //this will append tr element to table... keep its reference for a while since we will add cels into it
    row.append($("<td>" + rowData.LOCATION_UPDATE + "</td>"));
    row.append($("<td>"  + rowData.LATITUDE + "</td>"));
	row.append($("<td>"  + rowData.LONGITUDE + "</td>"));
	row.append($("<td>" + rowData.ACCURACY + "</td>"));
	row.append($("<td>" + rowData.SPEED + "</td>"));
	row.append($("<td>" + rowData.METHOD + "</td>"));
	
}

function drawHeader() {
	$("#tracksTable").html("");
	var header = $('<tr/>');	
	$("#tracksTable").append(header);
	header.append($("<td class='indextab'>datetime</td>"));
	header.append($("<td class='indextab'>latitude</td>"));
	header.append($("<td class='indextab'>longitude</td>"));
	header.append($("<td class='indextab'>accuracy</td>"));
	header.append($("<td class='indextab'>speed</td>"));
	header.append($("<td class='indextab'>method</td>"));
	
}

        <!--//--><![CDATA[//><!--
            	var iv= null;
				var viewer=null;
								
                function initGeoPortail() {
                
                        iv= Geoportal.load(
                                 // div's ID:
                                 'viewerDiv',
                                 // API's keys:
                                 ['rhfiamuq2cejl2h4xgcmwpge'],
                                 {// map's center :
                                                // longitude:
                                                lon:5.9532536,
                                                // latitude:
                                                lat:45.2723186
                                 },
                                 //zoom level 
                                 15,
                                 //options
                                 {
									layers:['GEOGRAPHICALGRIDSYSTEMS.MAPS','ORTHOIMAGERY.ORTHOPHOTOS','CADASTRALPARCELS.PARCELS'],
							
									layersOptions:{'GEOGRAPHICALGRIDSYSTEMS.MAPS':{visibility:true,opacity:1,  minZoomLevel:1,maxZoomLevel:18},
												'ORTHOIMAGERY.ORTHOPHOTOS':{visibility:false,opacity:1, minZoomLevel:1, maxZoomLevel:18},
												'CADASTRALPARCELS.PARCELS':{visibility:false,opacity:1, minZoomLevel:12,maxZoomLevel:18}},
									onView: function() {
										viewer=this.getViewer();					
										for (i = 0; i < trackIdArray.length; i++) {
											/* style de la trace */
											var styleTrace = new OpenLayers.StyleMap({
											"default": new OpenLayers.Style({
							 
											strokeColor: colorTrackArray[i],
											strokeOpacity: 0.8,
											strokeWidth:4
							
											}),
											"select": new OpenLayers.Style({
											strokeColor: '#FF0000',
											})
											});
											
											/* ajout du fichier gpx   */
											gpxLayer = viewer.getMap().addLayer(
											"GPX",
											"trace",
											"http://geocarn-marcdesbordes.rhcloud.com/gpx.php?tourId=" + tourId + "&trackId=" + trackIdArray[i],{
												visibility: true,
												opacity:0.8,
												styleMap: styleTrace,
												eventListeners:{
													'loadend':function(){
														if (this.maxExtent) {
															this.map.zoomToExtent(this.maxExtent);
															this.setVisibility(true);
															}
														}
													}
												}
											);
										}
									}
							}
                        );
						

						
                };
				
		function initGoogleMaps(data) {
			logDebug("-> initGoogleMaps(" + data);
			var mapOptions = {
			  center: { lat: 45.2723186, lng: 5.9532536},
			  zoom: 10,
			  mapTypeId: google.maps.MapTypeId.SATELLITE
			};
			var map = new google.maps.Map(document.getElementById('viewerDiv'),
				mapOptions);
			
			var bikeLayer = new google.maps.BicyclingLayer();
			  bikeLayer.setMap(map);
			
			logDebug("data = " + data);
			logDebug("data.length = " + data.length);
			
			bounds = displayMarkers(map, data);
			map.fitBounds(bounds);
			
			

			var flightPath = new google.maps.Polyline({
				path: pointArray,
				geodesic: true,
				strokeColor: '#FF0000',
				strokeOpacity: 1.0,
				strokeWeight: 2
			  });

			  flightPath.setMap(map);			
				
			}
			
function updateTourList(map, minLat, minLong, maxLat, maxLong) {
	logDebug("-> updateTourList(" + map + ", " + minLat + ", " + minLong + ", " + maxLat + ", " + maxLong);
	$.getJSON("tracksWS.php?track=map&minLat=" + minLat + "&minLong=" + minLong+ "&maxLat=" + maxLat + "&maxLong=" + maxLong + "&limit=50" , function(data) {
		jsonData = data;
		logDebug("-> " + data);
		for (i = 0; i < markerArray.length; i++)
			markerArray[i].setMap(null);
		markerArray = [];
		if (mapType != "GEO")
			displayMarkers(map, data.tracks);
	});
}

function displayMarkers(map, data) {
	logDebug("-> displayMarkers(" + map + ", " + data);
	var bounds = new google.maps.LatLngBounds ();
			
			for (var i = 0; i < data.length; i++) {
				logDebug(data[i]);
				lat = data[i].LATITUDE;
				lon = data[i].LONGITUDE;
				
				if (lat != 0 && lon != 0) {
					point = new google.maps.LatLng(lat, lon),
					pointArray[i] = point;
					
					var p = new google.maps.LatLng(lat,lon);
					bounds.extend(p);
						
					var image = {
						url: 'images/1.png',
						// This marker is 20 pixels wide by 32 pixels tall.
						size: new google.maps.Size(60, 60),
						// The origin for this image is 0,0.
						origin: new google.maps.Point(0,0),
						// The anchor for this image is the base of the flagpole at 0,32.
						anchor: new google.maps.Point(0, 10),
						scaledSize: new google.maps.Size(30, 30)
					  };


					/*
					var marker = new google.maps.Marker({
						  position: p,
						  map: map,
						  title: data[i].tourName + " / " + data[i].trackName,
						  icon: image
					  });
					*/
					
					marker = createMarker(p, map, data, image, i);
					markerArray[i] = marker;
						
						// fit bounds to track
						//map.fitBounds(bounds);
					  }
				  }
				 return bounds;
}	
                //--><!]]>

function createMarker(p, map, data, image, i) {
  var marker = new google.maps.Marker({
					  position: p,
					  map: map,
					  title: data[i].LOCATION_UPDATE + " / " + data[i].ACCURACY + " / " + data[i].METHOD,
				  });
	rowData = data[i];
	google.maps.event.addListener(marker, "click", function() {
		//alert(number + " - " + marker.getLatLng() + " - " + marker.getTitle());
		var populationOptions = {
		  strokeColor: '#FF0000',
		  strokeOpacity: 0.8,
		  strokeWeight: 2,
		  fillColor: '#FF0000',
		  fillOpacity: 0.35,
		  map: map,
		  center: marker.getPosition(),
		};
		radius = Math.sqrt(data[i]) * 1;
		populationOptions.setRadius(radius);
		// Add the circle for this city to the map.
		cityCircle = new google.maps.Circle(populationOptions);
	  });
  return marker;
}				
				
</script>

<body onLoad="init();">


<div id="tabDiv" class="title" style="height: 45px; position: relative; top: 0px; left:0px; width: 240px;">
	<div id="listTabDiv" class="toplinkbox" style="display: inline;top: 0px; left:0px; align: center;"><a href=# onClick="showList()">List</a></div>
	<div id="mapTabDiv" class="toplinkbox" style="display: inline;top: 0px; left:80px; align: center;"><a href=# onClick="showMap()">Map</a></div>
</div>
<div id="titleDiv" class="title" style="height: 45px; position: absolute; top: 52px; right: 0px; left: 280px;"></div>
<div id="titleDiv" class="toolbar" style="width: 100px; height: 45px; position: absolute; top: 52px; right: -5px;">
<a href=# onClick='initGeoPortail()'>[GEO]</a><br>
<a href=# onClick='initGoogleMaps()'>[GOOGLE]</a>
</div>

<div id="maiwindow" class="mainwindow" style="position: absolute; top: 100px; left: 0px; bottom: -20px; right: -10px; ">

			<DIV id="mainWin" class="window" style="position: absolute; left: 0px; top: 0px; bottom: -30px; right: -20px; z-index : 3; overflow:hidden;">
			<div id="" class="windowtopbar" style="position: relative; top: 0px; left: 0px; height=15px; width: 100%">
			<table width=100% cellpadding=0 cellspacing=0 border=0><tr>
				<td class="windowtopbar" width=35%>Main</td>
				<td class="windowtopbar" width=1><img src="images/transp.gif" height=16 width=1></td>
				<td class="windowtopbar" width=65% align=right>
				<img src="images/help.gif" width=15 height=15 border=0 title="Main window" alt="Main window">
				</td></tr>
			</table>
			</div>
			<div id="viewerDiv" class="innerwindow" style="position: relative; top: 20px; width: 100%; height: 100%; overflow:scroll;"> 
			<table width=100% height=100% id="tracksTable" class="tracksTableIndex">
			  <tr> 
				<td align=center><img src='images/ajaxLoader.gif'> <p>
				  <p>&nbsp;</td>
			  </tr>
			</table>
		  </div>
		</div>
		

</div>

<div id="bottombar" class="bottombar">
&nbsp;
</div>

<?php include 'bodyFooter.php' ?>

</body>
</html>
