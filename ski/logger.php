
<?php

$INFO =  "Info ";
$DEBUG = "Debug";
$ERROR = "Error";
$PERF = "Perf";

define('LOG_DIR', getenv('OPENSHIFT_DATA_DIR'));
$logDir=constant("LOG_DIR"); 

function initLog($type) {
	global $PERF;
	global $ERROR;

	$logDir=getenv('OPENSHIFT_DATA_DIR');
	$filename = $logDir;
	
	if ($type == $PERF)
		$filename .= "ski_perf";
	else if ($type == $ERROR)
		$filename .= "ski_error";
	else
		$filename .= "ski_log";
		
	$filename .= date("Ymd", mktime()) . ".log";
	$handle = fopen($filename, "a");
	return $handle;
}

function writelog($msg, $type) {
	$handle = initLog($type);
	$contents = date("H:i:s", mktime()) . " | " . $type . " | " . $_SERVER["SCRIPT_NAME"] . " | " . $msg . "\r\n";
	fwrite($handle, $contents);
	fclose($handle);
}

function logDebug($msg) {
	global $DEBUG;
	writelog($msg, $DEBUG);
}

function logInfo($msg) {
	global $INFO;
	writelog($msg, $INFO);
}

function logError($msg) {
	global $ERROR;
	writelog($msg, $ERROR);
}

function logPerf($msg, $time) {
	global $PERF;
	
	$time = round($time * 10000) / 10000;
	$msg = $msg . " : " . $time;
	writelog($msg, $PERF);
}

function microtime_float()
{
    list($usec, $sec) = explode(" ", microtime());
    return ((float)$usec + (float)$sec);
}


?>