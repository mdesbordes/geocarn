var applicationLogString = "";
var logDivId = "logDiv";
var CR_CHAR = "<br>\n";
var LOG_MAX_LENGTH = 50000;

function formatDateNumber(number) {
	if (number < 10)
		number = "0" + number;
	return number;
}

function formatDate(date) {
	return (1900 + date.getYear()) + "/" 
			+ formatDateNumber(1 + date.getMonth()) + "/" 
			+ formatDateNumber(date.getDate()) + " " 
			+ formatDateNumber(date.getHours()) + ":" 
			+ formatDateNumber(date.getMinutes()) + ":" 
			+ formatDateNumber(date.getSeconds());
}

function showLog() {
	document.getElementById(logDivId).innerHTML = getLog();
}

function logMessage(type, str) {
	// currentDate.toUTCString()
	if (applicationLogString.length > LOG_MAX_LENGTH)
		applicationLogString = applicationLogString.substring(0, LOG_MAX_LENGTH);
	applicationLogString = formatDate(new Date()) + " - " + type + " - " + str;
	if (document.getElementById(logDivId) != null)
		showLog();
	else {
		try {
            console.log(getLog());
            return true;
        } catch(e) {
		}
	}		
}

function logError(str) {
	logMessage("ERROR", str);
}

function logDebug(str) {
	logMessage("DEBUG", str);
}

function getLog() {
	return applicationLogString;
}

function initLog(divId) {
	logDivId = divId;
}