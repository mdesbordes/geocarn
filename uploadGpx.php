<?php include 'inc/logger.php' ?>
<?php

session_start();

$server = $_SERVER['SERVER_NAME'];
echo $server;
// /htdocs/gpx
if ($server == "geocarn.free.fr" || $server == "geocarn.freecluster.eu" || $server == "geocarn-env.avzztenr5u.eu-west-3.elasticbeanstalk.com")
	$uploaddir = $_SERVER['DOCUMENT_ROOT']."gpx/";
else if ($server == "geocarn.horizon-host.com")
	$uploaddir = "/home/u148545268/public_html/gpx/";
else
	$uploaddir = "/var/lib/openshift/543693d05973ca1e3c00024c/app-root/data/gpx/";

logDebug("uploaddir = " . $uploaddir);

ini_set("upload_tmp_dir", $uploaddir);

function uploadFile($userfile, $tmpuserfile, $elevationws, $username) {
	global $uploadfile;
	global $uploaddir;
	$_SESSION['uploadfile'] = "";
        $_SESSION['uploadfile'] = $tmpuserfile;
	//$albumId = $_POST['albumId'];   
	//$uploaddir = "/home/a3953673/public_html/gpx/";

	if(!file_exists($uploaddir)) {
			mkdir($uploaddir);
		}
		$currentTime = time();

		$error = "";

		if ($userfile != "") {
			$uploadfile = $uploaddir . $currentTime . '_' . $userfile;	
			logDebug("" . $tmpuserfile . " - " . $uploadfile);
			//if ($succes = move_uploaded_file($tmpuserfile, $uploadfile)) {   
			if ($succes = copy($tmpuserfile, $uploadfile)) {   
				if ($uploadfile != '') {
					logDebug($succes . " - " . $uploadfile);
					if ($succes)
						logDebug("-> TRUE");
					//saveInDb($uploadfile, $albumId, $desc, $desc_se, $userId, $db);
					//$_SESSION['uploadfile'] = $uploadfile;

					//parseGpx($uploadfile, $elevationws);
					//echo "<script>parent.document.loading.src = 'images/transp.gif';</script>";
				}
				$error = "";
			} else {
				$error = "Error!";
			}
		}
		
	$_SESSION['uploadfile'] = $uploadfile;
	logDebug("session = " . $_SESSION['uploadfile']);
		return $error;
}


logDebug( "ini_get = " . ini_get("upload_tmp_dir"));
logDebug( "server = " . $server);
logDebug( "uploaddir = " . $uploaddir);
logDebug( "DOCUMENT_ROOT = " . $_SERVER['DOCUMENT_ROOT']);

$uploadfile = "";


$username = "marc.desbordes";
logDebug( "uploadFile(" . $_FILES['file']['name'] . " " . $_FILES['file']['tmp_name']);
if (file_exists($_FILES['file']['tmp_name'])) {
    logDebug( "Le fichier ".$_FILES['file']['tmp_name']." existe");
} else {
    logDebug( "Le fichier ".$_FILES['file']['tmp_name']." n'existe pas");
}
if (is_readable($_FILES['file']['tmp_name'])) {
   logDebug( "Le fichier est lisible");
} else {
   logDebug( "Le fichier n\'est pas lisible");
}
logDebug( "error = " . $_FILES["file"]["error"]);
$error = uploadFile($_FILES['file']['name'], $_FILES['file']['tmp_name'], $_POST['elevationws'], $username);
logDebug( "Error = " . $error);

?>
<?php
header('Location: addTrack.php');
exit();
?>


