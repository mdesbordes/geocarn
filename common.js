//
// --- Contants ---
//
var TITLE = "geocarn";
var TITLE_SEPARATOR = " | ";
var ACTIVITY_NUM = 17;

//var colorTrackArray = new Array("#ff0000", "#0000ff", "#00ff00", "#000099", "#009900", "#009999", "#990000", "#990099");
var colorTrackArray = new Array("red", "blue", "fuchsia", "green", "lime", "maroon", "navy", "olive", "orange", "purple", "silver", "teal", "yellow", "aqua", "red", "blue", "fuchsia", "green", "lime", "maroon", "navy", "olive", "orange", "purple", "silver", "teal", "yellow", "aqua", "red", "blue", "fuchsia", "green", "lime", "maroon", "navy", "olive", "orange", "purple", "silver", "teal", "yellow", "aqua", "red", "blue", "fuchsia", "green", "lime", "maroon", "navy", "olive", "orange", "purple", "silver", "teal", "yellow", "aqua", "red", "blue", "fuchsia", "green", "lime", "maroon", "navy", "olive", "orange", "purple", "silver", "teal", "yellow", "aqua" );
var categoryArray = new Array("Indoor", "Skiing", "Ice Skating", "Hiking", "Sailing", "Cycling", "Nordic Skiing", "Mountain Biking", "Paddling", "Roller Skating", "Walking", "Running", "Trail Running", "Paragliding", "Climbing", "Motor" );

var mSecToKmH = 3.6;
var KMtoNM = 1.852;
var CO2_PER_KM = 0.25;
var EURO_PER_KM = 0.5;

//
// --- Navigator ---
// --- Navigator ---
//
IE5=NN4=NN6=false;
if(document.all) IE5=true;
else if(document.getElementById) NN6=true;
else if(document.layers) NN4=true;

//
// --- Fonctions ---
//
function getAbsX(elt) { return parseInt(elt.x) ? elt.x : getAbsPos(elt,"Left"); };
function getAbsY(elt) { return parseInt(elt.y) ? elt.y : getAbsPos(elt,"Top"); };
function getAbsPos(elt,which) {
  iPos = 0;
  while (elt != null) {
   iPos += elt["offset" + which];
   elt = elt.offsetParent;
  }
  return iPos;
};

function checkKey(event){ //e is event object passed from function invocation
	var chCode = ('charCode' in event) ? event.charCode : event.keyCode;
    console.log("The Unicode character code is: " + chCode);

	if(chCode == 13 || chCode == 0){ //if generated character code is equal to ascii 13 (if enter key)
		return true;
	}
	else{
		return false;
	}
}

function checkEnter(event, callback, div){ //e is event object passed from function invocation
	var chCode = ('charCode' in event) ? event.charCode : event.keyCode;
    console.log("The Unicode character code is: " + chCode);

	if(chCode == 13 || chCode == 0){ //if generated character code is equal to ascii 13 (if enter key)
		callback(); 
		return false
	}
	else{
		return true
	}
}

function movingMean(theArray, id) {
	console.log("-> movingMean(" + id);
	mean = 0;
	begin = new Number(id) -1;
	end = new Number(id) + 1;
	if (begin < 0)
		begin = 0;
	if (end > (theArray.length -1))
		end = theArray.length -1;
	count = 0;
	sum = 0;
	for (var index = begin; index <= end; index++) {
		sum += new Number(theArray[index]);
		count++;
	}
	mean = sum / count;
	return mean;
}

function shortDate(text, maxLength) {
	text = "" + text;
	if (typeof(text) != "undefined" && text != null) {
		if (text.length > maxLength)
			newtext = text.substr(0, maxLength);
		else
				newtext = text;
	}
	else {
			newtext = text;
	}
		return newtext;
}

function shortText(text, maxLength, classStr) {
	if (typeof(text) != "undefined" && text != null) {
		if (text.length > maxLength) {
			if (classStr != null)
				newtext = "<div class='" + classStr + "' style='display: inline;' title='" + text + "' alt='" + text + "'>" + text.substr(0, (maxLength-3)) + "...</div>";
			else
				newtext = text.substr(0, (maxLength-3)) + "...";
		}
		else
				newtext = text;
	}
	else {
			newtext = text;
	}
	return newtext;
}

function roundNumber(num, rounder) {
	result = num * rounder;
	result = Math.round(result);
	result = result / rounder;
	result = ''+result;
	if (result.indexOf('.',0) == -1)
		result = result + ".00";
	else if (result.length - result.indexOf('.',0) == 2)
		result = result + "0";
	return result;
}

function formatNumber(num) {
	if (num > 9)
		return num;
	else
		return "0" + num;
}

function dateToYmd(datestr) {
	var date = new Date(datestr);
	var str = date.getFullYear() + "/" + formatNumber(1 + new Number(date.getMonth())) + "/" + formatNumber(date.getDate());
	return str;
}	

function ymdToDay(str) {
	console.log("ymdToDay(" + str);
    var p = str.split('-'),
        s = 0, m = 1;
	var index = 0;
    while (p.length > 0) {
        pStr = p.pop();
		
		console.log(index + " - " + parseInt(pStr, 10));
		if (index == 0)
			s += parseInt(pStr, 10);
		else if (index == 1)
			s += parseInt(pStr, 10) * 30;
		else if (index == 2)
			s += parseInt(pStr, 10) * 365;
		index++;
    }

    return s;
}

function ymdToDate(str) {
	console.log("ymdToDate(" + str);
    var p = str.split('-'),
        s = 0, m = 1;
	var index = 0;
    while (p.length > 0) {
        pStr = p.pop();
		
		console.log(index + " - " + parseInt(pStr, 10));
		if (index == 0)
			day = parseInt(pStr, 10);
		else if (index == 1)
			month = parseInt(pStr, 10) - 1;
		else if (index == 2)
			year = parseInt(pStr, 10);
		index++;
    }
	date = new Date(year, month, day);
	console.log(date + " - " + year  + " - " + month  + " - " + day);
    return date;

}

function hmsToSecondsOnly(str) {
    var p = str.split(':'),
        s = 0, m = 1;

    while (p.length > 0) {
        s += m * parseInt(p.pop(), 10);
        m *= 60;
    }

    return s;
}

function secondsToHms(s) {
    var sec_num = parseInt(s, 10); // don't forget the second param
    var hours   = Math.floor(sec_num / 3600);
    var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
    var seconds = sec_num - (hours * 3600) - (minutes * 60);

    if (hours   < 10) {hours   = "0"+hours;}
    if (minutes < 10) {minutes = "0"+minutes;}
    if (seconds < 10) {seconds = "0"+seconds;}
    var time    = hours+':'+minutes+':'+seconds;
    return time;
}

function secondsToHours(s) {
    var sec_num = parseInt(s, 10); // don't forget the second param
    var hours   = Math.floor(sec_num / 3600);
    
    return hours;
}

function getWindIcon(windOrientation) {		
	windIcon = "";
	if (windOrientation >= 337.5)
		windIcon = "N";
	else if (windOrientation < 22.5)
		windIcon = "N";
	else if (windOrientation >= 22.5 && windOrientation < 67.5)
		windIcon = "NE";
	else if (windOrientation >= 67.5 && windOrientation < 112.5)
		windIcon = "E";
	else if (windOrientation >= 112.5 && windOrientation < 157.5)
		windIcon = "SE";
	else if (windOrientation >= 157.5 && windOrientation < 202.5)
		windIcon = "S";
	else if (windOrientation >= 202.5 && windOrientation < 247.5)
		windIcon = "SW";
	else if (windOrientation >= 247.5 && windOrientation < 292.5)
		windIcon = "W";
	else if (windOrientation >= 292.5 && windOrientation < 337.5)
		windIcon = "NW";
	return windIcon;
}

function getUrlParams() {
	var params = new Array();
	var query = window.location.hash;
	if (query.charAt(query.length-1) == '/')
		query = query.substring(0, (query.length-1));
	var pairs = query.split("/");
	for(var i = 1; i < pairs.length; i++) {
		params[i-1] = unescape(pairs[i]);
	}
	return params;
}

function changePage(title) {
	logDebug("-> changePage(" + title);
	document.title = TITLE + TITLE_SEPARATOR + title;
	
}

function getPixelSize(size) {
	return new Number(size.substr(0, size.length - 2))
}

function updateAds() {
	var currentTime = new Date();
    //googlead.location = "ads.html?s=" + currentTime.getTime(); 
}

function showLoader(msg) {
	document.getElementById("loaderDiv").style.visibility = "visible";
	document.getElementById("loaderMsgDiv").innerHTML = msg;
}

function hideLoader() {
	document.getElementById("loaderDiv").style.visibility = "hidden";
}

function showStatus(status, msg, divId, closeFunction) {
	div = document.getElementById(divId);
	statusDiv = document.getElementById("statusDiv");
	divWidth = getPixelSize(div.style.width);
	divHeight = getPixelSize(div.style.height);
	logDebug("divHeight = " + divHeight + " - divWidth = " + divWidth);
	divTop = getPixelSize(div.style.top);
	divLeft = getPixelSize(div.style.left);
	logDebug("divTop = " + divTop + " - divLeft = " + divLeft);
	statusDivWidth = getPixelSize(statusDiv.style.width);
	statusDivHeight = getPixelSize(statusDiv.style.height);
	logDebug("statusDivHeight = " + statusDivHeight + " - statusDivWidth = " + statusDivWidth);
	statusDivTop = Math.round((divHeight / 2) -  (statusDivHeight / 2)) + divTop + "px";
	statusDivLeft = Math.round((divWidth / 2) -  (statusDivWidth / 2)) + divLeft + "px";
	logDebug("statusDivTop = " + statusDivTop + " - statusDivLeft = " + statusDivLeft);
	statusDiv.style.top = statusDivTop;
	statusDiv.style.left = statusDivLeft;
	statusDiv.style.zIndex = 10;
	str = msg;	
	if (closeFunction == null)
		closeFunction = "";
	str += "<p><a href=# id='statusCloseButton' onClick='hideStatus();" + closeFunction + "' class='formbutton' onblur='hideStatus();" + closeFunction + "'>Close</a>";
	str = "<table height=100% width=100%><tr><td align=center valign=middle>" + str + "</td></tr></table>";
	statusDiv.innerHTML = str;
	if (status == STATUS_SUCCESS)
		statusDiv.className = "formSuccess";
	else
		statusDiv.className = "formError";
	document.getElementById("statusCloseButton").focus();
}

function hideStatus() {
	statusDiv = document.getElementById("statusDiv");
	statusDiv.style.zIndex = 0;
}

function getCheckedValue(radioObj) {
	if(!radioObj)
		return "";
	var radioLength = radioObj.length;
	if(radioLength == undefined)
		if(radioObj.checked)
			return radioObj.value;
		else
			return "";
	for(var i = 0; i < radioLength; i++) {
		if(radioObj[i].checked) {
			return radioObj[i].value;
		}
	}
	return "";
}

var STATUS_SUCCESS = 0;
var STATUS_ERROR = 1;
