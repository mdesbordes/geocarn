<?php include 'inc/logger.php' ?>
<?php
function convertChar($str, $old, $new) {
	$removeChar = array($old);
	return str_replace($removeChar, $new, $str);
}

function convertLatLon($latlon) {
	// CONVIERTO LONGITUD A FORMATO E6
	$pieces = explode(".", $latlon);
	
	// echo "<br>LON:".$lon." ENT:".$pieces[0].", DEC: ".$pieces[1];  
	
	// AJUSTO A 6 DECIMALES LA PARTE DECIMAL
	$decimales ="";
	$size= strlen($pieces[1]);
	if ($size >= 6) $decimales = substr($pieces[1], 0, 6); 
	else {$decimales = $pieces[1]; for ($i=$size;$i<6;$i++) $decimales .= "0"; }
	
		// LA PARTE ENTERA LA AJUSTO A DOS DIGITOS
		$entero = "";
		$signo = "";
		$size= strlen($pieces[0]);
		$i=0;
		if ($pieces[0][0]=='-') { $signo = "-";  $i = 1; } 
		for ($j=$i;$j< $size;$j++) $entero .= $pieces[0][$j];
		if (strlen ($entero) == 1) $entero = "0".$entero; 	                   
	$latlon = trim($signo.$entero.$decimales);
	return $latlon;
}

// Get cityID
if (isset($_GET['cityID']))
	$cityID = $_GET['cityID'];
else
	$cityID = null;
if (isset($_GET['lat']))
	$lat = $_GET['lat'];
else
	$lat = null;
if (isset($_GET['lon']))
	$lon = $_GET['lon'];
else
	$lon = null;
$lang = $_GET['lang'];

//,,,-19500000,146500000

if ($cityID != "" && $cityID != null) {
	$cityID = urlencode($cityID);
	//$fileUrl = "http://www.google.com/ig/api?weather=$cityID&hl=$lang";
	$fileUrl = "http://www.yr.no/place/$cityID/forecast.xml";
}
else {
	$lat = convertLatLon($lat);
	$lon = convertLatLon($lon);
	//$fileUrl = "http://www.google.com/ig/api?weather=,,,$lat,$lon&hl=$lang";
	$fileUrl = "http://www.yr.no/place/$cityID/forecast.xml";
}
//echo $fileUrl;
$xmldata = file_get_contents($fileUrl);
//$xmldata=getUrlContent($fileUrl, null, 600); 
logDebug($fileUrl);
$output = utf8_encode($xmldata);
header('Content-type: text/xml; charset=UTF-8', true);
?><?= $output ?><?php exit; ?>