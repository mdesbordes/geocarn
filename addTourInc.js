// -- addTourWs --
var pointIndex = 0;
var country = "";
latArray = new Array();
lonArray = new Array();
eleArray = new Array();
distArray = new Array();
pointArray = new Array();
timeArray = new Array();

var polyline, markers = new Array();

STOP_SPEED = 0.5; // under 0.5 km/h consider as stopped

function calcDist(lat1, lat2, lon1, lon2) {
	if (lat1 != lat2 || lon1 != lon2) {
		x = (Math.sin(lat1/57.2958) * Math.sin(lat2/57.2958)) + (Math.cos(lat1/57.2958) * Math.cos(lat2/57.2958) * Math.cos(lon2/57.2958 - lon1/57.2958));
		thisDist = 6378.7 * Math.atan(Math.sqrt(1-x*x)/x);
	}
	else {
		thisDist = 0;
	}
	return thisDist;
}

function calculateAverageSpeed() {
	logDebug("-> calculateAverageSpeed()");
	distance = this.document.tourform.distance.value;
	logDebug("distance = " + distance);
	timediff = this.document.tourform.timediff.value;
	logDebug("timediff = " + timediff);
	if (timediff != "") {
		timeTour = new Number(timediff.substr(0, 2)) * 3600 + new Number(timediff.substr(3, 2)) * 60 + new Number(timediff.substr(6, 2));
	}
	logDebug("timeTour = " + timeTour);
	avgSpeed = 3600 * distance / timeTour;
	logDebug("avgSpeed = " + avgSpeed);
	this.document.tourform.averagespeed.value = roundNumber(avgSpeed, 100);

}

function filterTourList(activity) {
	logDebug("-> filterTourList(" + activity + ") -> " + tourform.tour.options.length);
	filterTourFormText = new Array();
	filterTourFormValue = new Array();
	index = 0;
	for (i = 0; i < tourform.tour.options.length; i++) {
		logDebug(tourform.tour.options[i].value);
		if (tourform.tour.options[i].value.indexOf(activity + "|") == 0 || tourform.tour.options[i].value == "") {
			filterTourFormText[index] = tourform.tour.options[i].text;
			filterTourFormValue[index] = tourform.tour.options[i].value.substring(2, tourform.tour.options[i].value.length);
			logDebug(index + " -> " + filterTourFormText[index] + " - " + filterTourFormValue[index]);
			index++;
		}
	}
	formLength = tourform.tour.options.length;
	for (i = 0; i < formLength; i++) {
		tourform.tour.remove(0);
		logDebug("remove(" + i + ") -> " + tourform.tour.options.length);
	}
	for (i = 0; i < filterTourFormValue.length; i++) {
		optn = document.createElement("OPTION");
		optn.text = filterTourFormText[i];
		optn.value = filterTourFormValue[i];
		//logDebug("add(" + i + ") -> " + optn.text);
		tourform.tour.options.add(optn);
	}
}

function createTourResult(txtResponse) {
	logDebug("txtResponse = " + txtResponse);
	//hideLoader();

	//txtResponse = removeXML(txtResponse);
	logDebug("txtResponse = " + txtResponse);
	data = JSON.parse(txtResponse);
	logDebug("data = "  + data);

	tourId = data.track.tourId;
	trackId = data.track.trackId;

	logDebug("tourId = " + tourId);

	var url = "track.php?tourId=" + tourId + "&trackId=" + trackId;
	$(location).attr('href',url);
}

function createTour() {
	logDebug("-> createTour()");
	tourname = this.document.tourform.tourname.value;
	tourIndex = this.document.tourform.tour.selectedIndex;
	if (tourIndex > -1)
		tour = this.document.tourform.tour.options[tourIndex].value;
	else
		tour = "";
	activity = -1;
	for (i = 0; i < this.document.tourform.activity.length; i++) {
		selactivity = this.document.tourform.activity[i].checked;
		if (selactivity)
			activity = this.document.tourform.activity[i].value;
	}
	trackname = this.document.tourform.trackname.value;
	altdiff = this.document.tourform.altdiff.value;
	if (this.document.tourform.date != null) {
		datestr = this.document.tourform.date.value;
		date = datestr.replace("/", "-");
	}
	else
		date = "0000/00/00";
	if (this.document.tourform.averagespeed != null)
		averagespeed = this.document.tourform.averagespeed.value;
	else
		averagespeed = 0;
	if (this.document.tourform.altgain != null)
		altgain = this.document.tourform.altgain.value;
	else
		altgain = 0;
	if (this.document.tourform.altmin != null)
		altmin = this.document.tourform.altmin.value;
	else
		altmin = 0;
	if (this.document.tourform.altmax != null)
		altmax = this.document.tourform.altmax.value;
	else
		altmax = 0;
	altstart = this.document.tourform.altstart.value;

	if (this.document.tourform.timediff != null)
		timediff = this.document.tourform.timediff.value;
	else
		timediff = 0;
	if (this.document.tourform.timeup != null)
		timeup = this.document.tourform.timeup.value;
	else
		timeup = 0;
	if (this.document.tourform.averageupspeed != null)
		averageupspeed = this.document.tourform.averageupspeed.value;
	else
		averageupspeed = 0;

	distance = this.document.tourform.distance.value;

	country = this.document.tourform.country.value;
	
	if (this.document.tourform.planned.checked)
		planned = 1;
	else
		planned = 0;

	// TODO Get all values
	showLoader("Creating tour " + tourname + "...");

	str = "tourname=" + encodeURIComponent(tourname) + "&trackname=" + encodeURIComponent(trackname) + "&tour=" + tour + "&activity=" + activity + "&date=" + date + "&country=" + country + "&planned=" + planned + "&altdiff=" + altdiff + "&altgain=" + altgain + "&altmin=" + altmin + "&altmax=" + altmax + "&altstart=" + altstart + "&timediff=" + timediff + "&distance=" + distance + "&averagespeed=" + averagespeed + "&averageupspeed=" + averageupspeed + "&timeup=" + timeup + "&num=" + latArray.length + "&latlonarray=";
	for (i = 0; i < latArray.length; i++)
		//str += "&lat" + i + "=" + latArray[i] + "&lon" + i + "=" + lonArray[i] + "&ele" + i + "=" + eleArray[i] + "&time" + i + "=" + timeArray[i];
		str += latArray[i] + ";" + lonArray[i]  + ";" + eleArray[i]  + ";" + timeArray[i] + "|";
	url = "/createTourWs.php";

	var ai = AJAXInteraction(url, str, createTourResult);
	ai.doPost();
	}

function modifyCreateTour() {
	createTourStatusDiv = document.getElementById("createTourStatus");
	createTourStatusDiv.style.zIndex = 0;
}

function cancelCreateTour() {
	// TODO Remove track...
	closeDiv(WIN_ADDTOUR);
	closeDiv(WIN_TOUR_POINTS);

}

function addTourSelectActivity(activityId) {
	logDebug("-> addTourSelectActivity(" + activityId);
	document.getElementById("addTourGpx").style.zIndex = 10;
	filterTourList(activityId);
	addTourActivity = activityId;
}

function startUpload() {
	logDebug("-> startUpload()");
	showLoader("Loading .gpx file...");
	mode = MODE_ADDTOURFILE;
	//alert("Loading .gpx file...");
}

function uploadDone(fileId) {
	logDebug("-> uploadDone("+fileId);
	showLoader("Parsing .gpx file...");
	logDebug(fileId);
	var ai = AJAXInteraction("parseGpx.php?fileId=" + fileId, null, gpxParsingDone);
	ai.doGet();
	//alert("Parsing .gpx file...");
}

function removePoint(marker) {

    for (var i = 0; i < markers.length; i++) {

        if (markers[i] === marker) {

            markers[i].setMap(null);
            markers.splice(i, 1);

            polyline.getPath().removeAt(i);
        }
    }
}

function addPoint(latlng) {
	var image = {
			url: '/images/red.gif',
			// This marker is 20 pixels wide by 32 pixels high.
			size: new google.maps.Size(8, 8),
			// The origin for this image is (0, 0).
			origin: new google.maps.Point(0, 0),
			// The anchor for this image is the base of the flagpole at (0, 32).
			anchor: new google.maps.Point(4, 4)
		};
    var marker = new google.maps.Marker({
        position: latlng,
        map: map,
    		icon: image
    });

    markers.push(marker);

    polyline.getPath().setAt(markers.length - 1, latlng);

    google.maps.event.addListener(marker, 'click', function (event) {

        removePoint(marker);
    });
}

function tourDrawingDone() {
	logDebug("-> tourDrawingDone()");
	//showElevations();
	logDebug("pointIndex = " + pointIndex);
	if (latArray != null && latArray[0] != null) {
		var pointasstring=latArray[0]+","+lonArray[0];
		geocoder = new google.maps.Geocoder();
		latlng = new google.maps.LatLng(latArray[0], lonArray[0]);

		geocoder.geocode({'latLng': latlng}, function(results, status) {
		  if (status == google.maps.GeocoderStatus.OK) {
			if (results[1]) {
			  map.setZoom(12);
			  marker = new google.maps.Marker({
				  position: latlng,
				  map: map
			  });
			  logDebug(results[1].formatted_address);
			  for (i = 0; i < results[1].address_components.length; i++) {
				logDebug(results[1].address_components[i].short_name + " - " + results[1].address_components[i].long_name + " - " + results[1].address_components[i].types[0]);
				if (results[1].address_components[i].types[0] == "country")
					country = results[1].address_components[i].short_name;
			  }
			  logDebug("Country = " + country);

			  map.setCenter(latlng);

			  logDebug(this.document.tourform.country);
			  for(var i=0; i < this.document.tourform.country.length; i++)
			  {
				if(this.document.tourform.country[i].value === country) {
				  this.document.tourform.country.selectedIndex = i;
				  break;
				}
			  }
			  //infowindow.setContent(results[1].formatted_address);
			  //infowindow.open(map, marker);
			}
		  } else {
			alert("Geocoder failed due to: " + status);
		  }
		});

	}
	showAddTourFormElements();
}

function showAddTourFormElements() {
	//document.getElementById("addTourInfo").style.zIndex = 10;
	//document.getElementById(divArray[WIN_ADDTOUR][DIV_ID]).style.height = "280px";
	//document.getElementById("addTourGpx").style.zIndex = 0;
	//logDebug("addTourActivity = " + addTourActivity + " " + document.getElementById("addTourInfoElevation").style.zIndex);
	/*
	if (addTourActivity == 2 || addTourActivity == 4) {
		//document.getElementById("addTourInfoElevation").style.visibility = "hidden";
	}
	else {
		if (addTourActivity == 6) {
			//document.getElementById("addTourInfoElevationDiff").style.visibility = "hidden";
			//document.getElementById("addTourInfoElevationDiff").style.height = "0px";
		}
		else {
			//document.getElementById("addTourInfoElevationGain").style.visibility = "hidden";
			//document.getElementById("addTourInfoElevationGain").style.height = "0px";
		}
	}
	*/
}

function gpxParsingDone(txtResponse) {
	logDebug("-> gpxParsingDone("+txtResponse);
	hideLoader();
	//showAddTourFormElements();

	latArray = new Array();
	lonArray = new Array();
	timeArray = new Array();
	eleArray = new Array();
	distArray = new Array();

	//logDebug("txtResponse = " + txtResponse);
	//logDebug("Start evaluating JSON...");
	//data = txtResponse.evalJSON();
	data = JSON.parse(txtResponse);
	logDebug("data.track.length = " + data.track.length);
	for (i = 0; i < data.track.length; i++) {
		timeArray[i] = data.track[i].time;
		latArray[i] = data.track[i].lat;
		lonArray[i] = data.track[i].lon;
		if (i > 0)
			eleArray[i] = (new Number(data.track[i].ele) + new Number(eleArray[i-1])) / 2; // Average elevation between last 2 points
		else
			eleArray[i] = data.track[i].ele;
		if (i > 0)
			distArray[i] = calcDist(latArray[i-1], latArray[i], lonArray[i-1], lonArray[i])
		else
			distArray[i] = 0;
	}
	//showPoints(latArray, lonArray, timeArray, eleArray, distArray);
	showPoints();
	tourDrawingDone();
}

function stravaActivitySelected(data) {
	logDebug("-> stravaActivitySelected("+data);
	hideLoader();
	
	latArray = new Array();
	lonArray = new Array();
	timeArray = new Array();
	eleArray = new Array();
	distArray = new Array();

	//data = JSON.parse(txtResponse);
	logDebug("data.length = " + data.length);
	distance = 0;
	for (i = 0; i < data.length; i++) {
		timeArray[i] = data[i].time;
		latArray[i] = data[i].lat;
		lonArray[i] = data[i].lon;
		distArray[i] = roundNumber((data[i].dist/1000),100) - distance;
		distance = roundNumber((data[i].dist/1000),100);		
		eleArray[i] = data[i].ele;		
	}
	for (i = 0; i < data.length; i++) {
		eleArray[i] = movingMean(eleArray, i);
	}
	//showPoints(latArray, lonArray, timeArray, eleArray, distArray);
	showPoints();
	tourDrawingDone();
}

function setElevation(txtResponse) {
	logDebug("-> setElevation(" + txtResponse);
	txtResponse = removeXML(txtResponse);
	//alert(txtResponse);
	var response=txtResponse.split('|');
	id = new Number(response[0]);
	elevation = response[1];

	if (elevation < -1000 || elevation > 9000)
		elevation = "N/A";

	eleArray[id] = movingMean(eleArray, id);
	elevationDiv = document.getElementById('ele' + id);
	logDebug("'ele" + id + "'" + " - " + elevationDiv);
	if (elevationDiv != null) {
		elevationDiv.innerHTML = eleArray[id];
	}

	id++;
	eleArray[id] = elevation;
	elevationDiv = document.getElementById('ele' + id);
	if (elevationDiv != null) {
		elevationDiv.innerHTML = eleArray[id];
	}
}

function getElevation(pointIndex) {
	logDebug("-> getElevation(" + pointIndex + ")");
	elevationDiv = document.getElementById('ele' + pointIndex);
	if (elevationDiv != null) {
		elevationDiv.innerHTML = "<img src='images/ajaxLoader.gif' width=15 height=15>";
		logDebug("elevationWs.php?id=" + pointIndex + "&lat=" + latArray[pointIndex] + "&lon=" + lonArray[pointIndex]);
		//alert("elevationWs.php?id=" + pointIndex + "&lat=" + latArray[pointIndex] + "&lon=" + lonArray[pointIndex]);
		var ai = AJAXInteraction("elevationWs.php?id=" + pointIndex + "&lat=" + latArray[pointIndex] + "&lon=" + lonArray[pointIndex], null, setElevation);
		ai.doGet();
	}
	//alert("Parsing .gpx file...");
}

function getElevations(encodeString) {
	console.log("-> getElevations(" + encodeString + ")");

	$.getJSON( "elevationWs.php?locations=enc:" + encodeString)
	  .done(function( json ) {
			console.log( "JSON Data: " + json.results );
			for (i = 0; i < json.results.length; i++) {
				//latArray[i] = json.results[i].location.lat;
				//lonArray[i] = json.results[i].location.lng;
				eleArray[i] = json.results[i].elevation;
				console.log( "" + i + " - " + latArray[i] + " - " + lonArray[i] + " - " + eleArray[i] );
			}
			showPoints();
	  })
	  .fail(function( jqxhr, textStatus, error ) {
		var err = textStatus + ", " + error;
		console.log( "Request Failed: " + err );
		showPoints();
	});
}

function getAllElevations() {
	logDebug("-> getAllElevations()");
	getElevation(pointIndex);
	pointIndex = pointIndex + 2;
	if (pointIndex <= numPoints) {
		progressValue = Math.round(pointIndex * 100 / numPoints);
		progressDiv = document.getElementById('addTourElevationProgressValue');
		progressDiv.innerHTML = progressValue + "%";
		eval("document['progress-" + progressValue + "'].src = 'images/progress.gif'");
		setTimeout(getAllElevations, 500);
	}
	else {
		progressDiv = document.getElementById('addTourElevationProgress');
		progressDiv.innerHTML = "";
		showElevations();
	}
}


// Show elevations
//function showElevations(latArray, lonArray, eleArray) {
function showElevations() {
	logDebug("-> showElevations()");
	topIndex = eleArray.length - 1;
	elevationDiff = 0;
	durationStopped = 0;
	timeUp = 0;
	altGain = 0;
	maxElev = 0;
	minElev = 99999;
	previousElevation = eleArray[0];
	for (i = 0; i < eleArray.length; i++) {
		if (eleArray[i] != 0 && eleArray[i] != "N/A") {
			if (eleArray[i] == maxElev) {
				topIndex = i;
			}
			if (new Number(eleArray[i]) > previousElevation) {
				elevationDiff += new Number(eleArray[i] - previousElevation);
				//alert(elevationDiff + " += " + eleArray[i] + " - " + previousElevation);
			}
			if (maxElev < new Number(eleArray[i]))
				maxElev = new Number(eleArray[i]);
			if (minElev > new Number(eleArray[i]))
				minElev = new Number(eleArray[i]);

			// Going up
			if (i > 0 && eleArray[i] > previousElevation) {
				altGain += (eleArray[i] - previousElevation);
				elapsTime = (timeArray[i] - timeArray[i-1]);
				avgUpSpeed = 3600 * (eleArray[i] - previousElevation) / elapsTime;
				avgSpeed = 3600 * distArray[i] / elapsTime;
				if (avgSpeed < STOP_SPEED) {
					durationStopped += elapsTime;
				}
				else {
					timeUp += elapsTime;
				}
				//logDebug(eleArray[i] + " > " + previousElevation + " -> " + altGain + " - " + elapsTime + " - " + avgUpSpeed + " - " + timeUp);

			}

			previousElevation = new Number(eleArray[i]);
		}
	}

	logDebug("timeUp = " + timeUp);
	//altGain = maxElev - eleArray[0];
	averageUpSpeed = (altGain / timeUp) * 3600; // Up speed meter/hour

	date = new Date();
	date.setTime(timeUp * 1000);
	timeUpStr = formatNumber((date.getHours() - 1)) + ":" + formatNumber(date.getMinutes()) + ":" + formatNumber(date.getSeconds());

	if (timeArray.length > 0) {

	}
	else {
		timeUpStr = "00:00:00";
		averageUpSpeed = 0;
	}
	this.document.tourform.altdiff.value = roundNumber(elevationDiff, 1);
	this.document.tourform.altgain.value = roundNumber(altGain, 1);
	this.document.tourform.altmin.value = roundNumber(minElev, 1);
	this.document.tourform.altmax.value = roundNumber(maxElev, 1);
	this.document.tourform.altstart.value = roundNumber(eleArray[0], 1);
	this.document.tourform.averageupspeed.value = roundNumber(averageUpSpeed, 1);
	this.document.tourform.timeup.value = timeUpStr;

	//showPoint(latArray[topIndex], lonArray[topIndex], "http://maps.google.com/mapfiles/kml/pal5/icon13.png");
	//topDiv = document.getElementById('point' + topIndex);
	//topDiv.innerHTML = "<img src=http://maps.google.com/mapfiles/kml/pal5/icon13.png height=16>";
}

function drawpoint(point) {
	logDebug("-> drawpoint(" + point);
	listDiv = document.getElementById('pointlist');
	pointArray[pointIndex] = point;
	latArray[pointIndex] = point.lat();
	lonArray[pointIndex] = point.lng();
	//marker = new GMarker(point);
	//map.addOverlay(marker);
	eleArray[pointIndex] = "N/A";

	distance = 0;
	for (i = 0; i <= pointIndex; i++) {
		if (i > 0) {
			distArray[i] = calcDist(pointArray[i].lat(), pointArray[i-1].lat(), pointArray[i].lng(), pointArray[i-1].lng());
			distance += distArray[i];
		}
		else
			distArray[i] = 0;
	}
	if (pointIndex > 0) {
		var polyline = new google.maps.Polyline([  pointArray[pointIndex-1],  pointArray[pointIndex]], "#ff0000", 2);
		map.addOverlay(polyline);
	}

	i = pointIndex;

	str = "";
	for (i = pointIndex; i >= 0; i--) {
		if (i/2 == Math.round(i/2))
			className = "rowEven";
		else
			className = "rowOdd";
		str += "<tr class='" + className + "'><td align=center><div id='point" + i + "'></div></td><td align=right><a href=# onMouseOver='showPointer(" + latArray[i] + ", " + lonArray[i] + ");'>N/A</a></td><td align=right>" + roundNumber(distArray[i]*1000, 1) + "</td><td align=right>N/A</td><td align=right>N/A</td><td align=right id=ele"+i+">" + roundNumber(eleArray[i], 1) + "</td></tr>";

	}
	displaystr = POINTS_TABLE_HEADER + str + "</table>";
	listDiv.innerHTML = displaystr;

	getElevation(pointIndex);

	pointIndex++;

	this.document.tourform.distance.value = roundNumber(distance, 100);
}

function drawDone(path) {
	console.log("-> drawDone(" + path);
	distance = 0;
	latlonArray = path.getArray();
	pointArray  = path.getArray();
	console.log("latlonArray.length = " + latlonArray.length);
	for (i = 0; i < latlonArray.length; i++) {
		console.log(latlonArray[i].lat() + ", " + latlonArray[i].lng() + "\n");
		if (i > 0) {
			distArray[i] = calcDist(pointArray[i].lat(), pointArray[i-1].lat(), pointArray[i].lng(), pointArray[i-1].lng());
			distance += distArray[i];
		}
		else
			distArray[i] = 0;
		latArray[i] = latlonArray[i].lat();
		lonArray[i] = latlonArray[i].lng();
		//pointArray[i] = new google.maps.LatLng(latArray[i], lonArray[i]);
	}

	var flightPath = new google.maps.Polyline({
		path: pointArray,
		geodesic: true,
		strokeColor: '#FF0000',
		strokeOpacity: 1.0,
		strokeWeight: 2
	  });
	var encodePath = flightPath.getPath();
	var encodeString = google.maps.geometry.encoding.encodePath(encodePath);
	getElevations(encodeString);
}

//function showPoints(latArray, lonArray, timeArray, eleArray, distArray) {
function showPoints() {
	logDebug("-> showPoints()");
	//latArray = latArray;
	//lonArray = lonArray;
	str = "<tr class='rowEven'><td align=center><div></div></td><td align=right>Time</td><td align=right>Distance<br>(m)</td><td align=right>Total dist<br>(Km)</td><td align=right>Elapsed</td><td align=right>Speed<br>(Km/h)</td><td align=right>Elevation<br>(m)</td></tr>";

	distance = 0;
	durationStopped = 0;
	points = new Array();
	logDebug("latArray.length = " + latArray.length);

	for (i = 0; i < latArray.length; i++) {

		distance += new Number(distArray[i]);

		time = new Date();
		time.setTime(timeArray[i] * 1000);
		timeStr = formatNumber(time.getHours()) + ":" + formatNumber(time.getMinutes()) + ":" + formatNumber(time.getSeconds());
		if (i > 0) {
			if (timeArray.length > 0) {
				elapsTime = (timeArray[i] - timeArray[i-1]);
				avgSpeed = 3600 * distArray[i] / elapsTime;
				if (avgSpeed < STOP_SPEED)
					durationStopped += elapsTime;
				time.setTime(elapsTime * 1000);
				elapsTimeStr = formatNumber((time.getHours()-1)) + ":" + formatNumber(time.getMinutes()) + ":" + formatNumber(time.getSeconds());
			}
			else {
				timeStr = "00:00:00";
				avgSpeed = 0;
				elapsTimeStr = "00:00:00";
			}
		}
		else {
			timeStr = "00:00:00";
			avgSpeed = 0;
			elapsTimeStr = "00:00:00";
		}
		if (i/2 == Math.round(i/2))
			className = "rowEven";
		else
			className = "rowOdd";
		str += "<tr class='" + className + "'><td align=center><div id='point" + i + "'></div></td><td align=right>" + timeStr + "</td><td align=right>" + roundNumber(distArray[i]*1000, 1) + "</td><td align=right>" + roundNumber(distance, 100) + "</td><td align=right>" + elapsTimeStr + "</td><td align=right>" + roundNumber(avgSpeed, 100) + "</td><td align=right id=ele"+i+">" + roundNumber(eleArray[i], 1) + "</td></tr>";
		points[i] = new google.maps.LatLng(latArray[i], lonArray[i]);
		//map.addOverlay(marker);
	}
	logDebug("points = " + points);
	var flightPath = new google.maps.Polyline({
		path: points,
		geodesic: true,
		strokeColor: '#FF0000',
		strokeOpacity: 1.0,
		strokeWeight: 2
	  });
	flightPath.setMap(map);

	/*
	var path = flightPath.getPath();
	var encodeString = google.maps.geometry.encoding.encodePath(path);
	getElevations(encodeString);
	*/

	listDiv = document.getElementById('pointlist');
	str = "<table>" + str + "</table>";
	listDiv.innerHTML = str;

	if (timeArray.length > 0) {		
		date = new Date();
		date.setTime(timeArray[0] * 1000);
		dateStr = date.getFullYear() + "/" + formatNumber((Number(1) + Number(date.getMonth()))) + "/" + formatNumber(date.getDate());

		timeDiff = timeArray[timeArray.length-1] - timeArray[0] - durationStopped;
		date.setTime(timeDiff * 1000);
		timeDiffStr = formatNumber((date.getHours() - 1)) + ":" + formatNumber(date.getMinutes()) + ":" + formatNumber(date.getSeconds());

		averageSpeed = (distance / timeDiff) * 3600;		
	}
	else {
		dateStr = "";
		timeDiffStr = "00:00:00";
		averageSpeed = 0;
	}
	this.document.tourform.distance.value = roundNumber(distance, 100);
	if (!planned && (timeArray[0] > 0))
		this.document.tourform.date.value = dateStr;
	this.document.tourform.timediff.value = timeDiffStr;
	this.document.tourform.averagespeed.value = roundNumber(averageSpeed, 100);


	numPoints = latArray.length;
	pointIndex = 0;

	activity = -1;
	/*
	for (i = 0; i < this.document.activityform.activity.length; i++) {
		selactivity = this.document.activityform.activity[i].checked;
		if (selactivity)
			activity = this.document.activityform.activity[i].value;
	}
	*/
	showElevations();

	resizeMap();
}

function createProgressBar() {
	str = "Retrieving elevations...&nbsp;&nbsp;";
	str += "<div id='addTourElevationProgressValue' style='display:inline;'>0%</div><br>";
	str += "<img src='images/progress.gif' width=2 height=10 name='progress-0'>";
	for (i = 1; i <= 100; i++)
		str += "<img src='images/progress-off.gif' width=2 height=10 name='progress-" + i + "'>";
	return str;
}

function setCountry(response) {
	logDebug("-> setCountry(" + response);
	var texttodisplay = "";
	if (!response || response.Status.code != 200)
	{
		texttodisplay="No Country";
	}
	else
	{
		place = response.Placemark[0];
		if (place.address == null)
		{
			texttodisplay="No Country";
		}
		else
		{
			texttodisplay=place.AddressDetails.Country.CountryNameCode;
		}
	}
	selectCountry(texttodisplay);
}

function selectCountry(selectedCountryCode) {
	if (selectedCountryCode != "") {
		selectedIndex = -1;
		for (i = 0; i < this.document.tourform.country.length; i++) {
			if (this.document.tourform.country[i].value == selectedCountryCode) {
				selectedIndex = i;
				break;
			}
		}
		this.document.tourform.country.selectedIndex = selectedIndex;
	}
}
