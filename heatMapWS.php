<?php include 'inc/connectDb.php' ?>
<?php include 'inc/logger.php' ?>
<?php

function getTimeStat($db, $user) {
	logDebug('-> getTimeStat(' . $user);
	global $ACTIVITY;

	$request = "SELECT SEC_TO_TIME(SUM(TIME_TO_SEC(duration))) sumtime, round(SUM(TIME_TO_SEC(duration))/60) summin FROM TRACKER_TRACK join TRACKER_TOUR on TRACKER_TRACK.TOUR_ID=TRACKER_TOUR.ID where week(TRACKER_TRACK.datetime) = week(now()) and year(TRACKER_TRACK.datetime) = year(now()) and TRACKER_TOUR.ACTIVITY_ID != 15";
	if ($user != null && $user != "")
		$request .= " AND TRACKER_TOUR.USER_ID = '$user' ";
	logDebug($request);
	$result = mysqli_query($db, $request);

	$rows = array();
	while($r = mysqli_fetch_assoc($result)) {
		$rows['activities'][] = $r;
	}

	return json_encode($rows);
}

function getConcatTracks($db, $user, $activity, $format) {
	logDebug('-> getConcatTracks(' . $user);
	global $ACTIVITY;
	
	if ($activity != null && $activity != "" && $activity != -1)
		$activityReq = " and ACTIVITY_ID = '$activity' ";
	else
		$activityReq = "";
	
	$request = "SELECT TRACK as track FROM TRACKER_TRACK WHERE TOUR_ID IN (SELECT ID FROM TRACKER_TOUR WHERE USER_ID='$user' $activityReq) AND PLANNED_ONLY = 0";
	
	logDebug($request);
	$result = mysqli_query($db, $request);

	$rows = array();
	$resultStr = "";
	if ($format == "json")
		$locListTxt = "[\n";
	else
		$locListTxt = "";
	$locListJson = "";
	
	//logDebug(mysql_num_rows($resutl));
	
	$index = 0;
		
	while($r = mysqli_fetch_assoc($result)) {
		$trackStr = $r["track"];
		logDebug($trackStr);
		
		$locations = explode("|", $trackStr);
		
		$size = count($locations);
		for ($i = 0; $i < $size; $i++) {
			$loc = explode(";", $locations[$i]);
			if ($loc[0] != null && $loc[0] != "" && $loc[0] != -1 && $loc[1] != null && $loc[1] != "" && $loc[1] != -1) {
				if ($format == "json") {			
					
					if ($index != 0) {
						$locListTxt .= ",";						
					}
					else {						
					}
					$locTxt = "{ \"lat\" : " . $loc[0] . ", \"lon\" : " . $loc[1] . " }\n";		
					$locListTxt .= $locTxt;					
				}
				else
					$locListTxt .= $loc[0] . "," . $loc[1] . "\n";
				$index++;
			}
		}
		
		$rowStr = str_replace("|", "\n", $trackStr);
		$rows['track'][] = $r;
		$resultStr .= $rowStr;
	}
	if ($format == "json")
		$locListTxt .= "]";
	return $locListTxt;
}


$activity = $_GET['activity'];
$user = $_GET['user'];
$format = $_GET['format'];

$user = $_SESSION['user'];

$activityStat = getConcatTracks($db, $user, $activity, $format);

header('Content-type: application/json', true);
?><?= $activityStat ?><?php exit; ?>
