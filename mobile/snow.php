<?php include 'header.php' ?>

<script>
lang = "fr";
thenumDays = 0;

function init() {
	loadSnowWin("alpes-du-nord/les-7-laux");
}

</script>

<body onLoad="init();">

<?php include 'bodyHeader.php' ?>

<div id="maiwindow" class="maiwindow" style="position: relative; top: 5px; left: 0px; ">
<table width=100% height=130>
	<tr height=130>
		<td width=100% valign=top>
		
		<DIV id="snowWin" class="window" style="width: 100%; height: 160px; z-index : 3; overflow:hidden;">
		<div id="" class="windowtopbar" style="position: relative; top: 0px; left: 0px; height=15px; width: 100%">
		<table width=100% cellpadding=0 cellspacing=0><tr>
			<td class="windowtopbar" width=35%>Snow</td>
			<td class="windowtopbar" width=1><img src="/images/transp.gif" height=16 width=1></td>
			<td class="windowtopbar" width=65% align=right>
			<a href=# onClick=loadSnowWin('alpes-du-nord/alpe-dhuez') class="windowtopbar"><img src="/images/orange.gif" width=15 height=15 border=0 alt="Alpe d'Huez" title="Alpe d'Huez"></a>
			<a href=# onClick=loadSnowWin('alpes-du-nord/les-7-laux') class="windowtopbar"><img src="/images/green.gif" width=15 height=15 border=0 alt="Les 7 Laux" title="Les 7 Laux"></a>
			<a href=# onClick=loadSnowWin('alpes-du-nord/chamrousse') class="windowtopbar"><img src="/images/brown.gif" width=15 height=15 border=0 alt="Chamrousse" title="Chamrousse"></a>
			<a href=# onClick=loadSnowWin('alpes-du-nord/tignes') class="windowtopbar"><img src="/images/orange.gif" width=15 height=15 border=0 alt="Tignes" title="Tignes"></a>
			<img src="/images/help.gif" width=15 height=15 border=0 title="Snow info from Ski France" alt="Snow info from Ski France">
			</td></tr>
		</table>
		</div>
		  <div id="snowWinInner" class="innerwindow" style="position: relative; top: 10px;"> 
			<table width=100% height=100%>
			  <tr> 
				<td align=center><img src='/images/ajaxLoader.gif'> <p>Loading
				  <p>&nbsp;</td>
			  </tr>
			</table>
		  </div>
		</DIV>
		
		

</td></tr>
</table>

</div>

<div id="bottombar" class="bottombar">
&nbsp;
</div>

<?php include 'bodyFooter.php' ?>

</body>
</html>
