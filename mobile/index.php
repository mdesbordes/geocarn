<?php include 'header.php' ?>

<script>
lang = "fr";
thenumDays = 0;

function init() {
	$("#titleDiv").html("");
	$("#subTitleDiv").html("");
	
	getLatestTracks(10);	
}

function getLatestTracks(numTracks) {
	$.getJSON("/tracksWS.php?user=" + user + "&track=latest&planned=false&limit=" + numTracks  , function(data) {
		drawTable(data.tracks);
	});
}

function drawTable(data) {
	//$("#latestTracksTable").html("<tr /><td colspan=6><img src='/images/transp.gif' width=300 height=1></td>");
    $("#latestTracksTable").html("");
    for (var i = 0; i < data.length; i++) {
        drawRow(i, data[i]);
    }
	//$("#latestTracksTable td").addClass("tracksTableIndex");
	$("#latestTracksTable > tbody > tr:odd").addClass("rowOdd");
    $("#latestTracksTable > tbody > tr:not(.odd)").addClass("rowEven"); 
}

function drawRow(rowId, rowData) {
	var row = $("<tr />")
    $("#latestTracksTable").append(row); //this will append tr element to table... keep its reference for a while since we will add cels into it
    if (rowData.trackName != "")
		tourName = rowData.tourName + " / " + rowData.trackName;
	else 
		tourName = rowData.tourName;
	row.append($("<td width=30><img src='/images/" + rowData.activityId + ".png' width=30 height=30 alt='" + shortDate(rowData.trackDate, 10) + "' title='" + shortDate(rowData.trackDate, 10) + "'></td>"));
    row.append($("<td width=150><a href='track.php?trackId=" + rowData.trackId + "'>"  + shortText(tourName, 50, '') + "</a></td>"));
	row.append($("<td width=60>" + rowData.trackDuration + "</td>"));
	row.append($("<td width=60>" + rowData.trackDistance + " km</td>"));
	row.append($("<td width=60>" + rowData.trackAltGain + " m</td>"));
	
}

function drawHeader() {
	$("#latestTracksTable").html("");
	var header = $('<tr/>');	
	//header.append($("<td colspan=6><img src='/images/transp.gif' width=300 height=1></td>"));
	var header = $('');	
	$("#latestTracksTable").append(header);
	header.append($('<tr/>'));
	header.append($("<td class='indextab'>activityId</td>"));
	header.append($("<td class='indextab'>trackDate</td>"));
	header.append($("<td class='indextab'>tourName</td>"));
	header.append($("<td class='indextab'>trackDuration</td>"));
	header.append($("<td class='indextab'>trackDistance</td>"));
	header.append($("<td class='indextab'>trackAltGain</td>"));
}

</script>

<body onLoad="init();">

<?php include 'bodyHeader.php' ?>

<div id="maiwindow" class="maiwindow" style="position: relative; top: 5px; left: 0px; width: 100%; height: 100%; ">
<table width=100% height=100%>
	<tr height=100%>
		
		<td width=100% height=100% valign=top>
			<DIV id="mainWin" class="window" style="width: 100%; height: 100%; z-index : 3; overflow:hidden;">
			<div id="" class="windowtopbar" style="position: relative; top: 0px; left: 0px; height=15px; width: 100%">
			<table width=100% cellpadding=0 cellspacing=0 border=0><tr>
				<td class="windowtopbar" width=35%>Latest activities</td>
				<td class="windowtopbar" width=1><img src="/images/transp.gif" height=16 width=1></td>
				<td class="windowtopbar" width=65% align=right>
				<img src="/images/help.gif" width=15 height=15 border=0 title="Main window" alt="Main window">
				</td></tr>
			</table>
			</div>
			<div id="latestTracksWinInner" class="innerwindow" style="position: relative; top: 10px; width: 100%; height: 100%; overflow:auto; overflow-x: hidden"> 
				
				<table id="latestTracksTable" class="statTable" width=100%>
					<tr> 
					<td align=center><img src='/images/transp.gif' width=360 height=1></td>
				  </tr><tr> 
					<td align=center><img src='/images/ajaxLoader.gif'> <p>Loading
					  <p>&nbsp;</td>
				  </tr>
				</table>
		  </div>
		</div>
		</td>

	</tr>
</table>

</div>

<div id="bottombar" class="bottombar">
&nbsp;
</div>

<?php include '../bodyFooter.php' ?>

</body>
</html>
