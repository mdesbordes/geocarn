<?php
$pageName = basename($_SERVER["REQUEST_URI"], ".php");

$classArray = [];
for ($i = 0; $i <= 5; $i++)
	$classArray[$i] = "";

switch ($pageName) {
    case "":
       $classArray[0] = " class='active'";
       break;
    case "index":
       $classArray[0] = " class='active'";
       break;
    case "stat":
        $classArray[1] = " class='active'";
        break;
    case "season":
       $classArray[2] = " class='active'";
       break;
	case "green":
       $classArray[3] = " class='active'";
       break;
	case "conditions":
       $classArray[4] = " class='active'";
       break;
	case "addTrackMobile":
       $classArray[5] = " class='active'";
       break;
}

?>

 
<div id="sidr">
<!-- Your content -->
<ul>
<li <?= $classArray[0] ?>><a href="index.php">Latest activities</a></li>
<li <?= $classArray[1] ?>><a href="stat.php">Activity stats</a></li>
<li <?= $classArray[2] ?>><a href="season.php">Season stats</a></li>
<li <?= $classArray[3] ?>><a href="green.php">Eco-friendly</a></li>
<li <?= $classArray[4] ?>><a href="conditions.php">Conditions</a></li>
<li <?= $classArray[5] ?>><a href="addTrackMobile.php">Add track</a></li>
<li <?= $classArray[6] ?>><a href="/logout.php">Logout</a></li>
</ul>
</div>

<script>
$(document).ready(function() {
$('#simple-menu').sidr();
});

</script>


<div id="topbar" style="position: relative; top: 0px; left: 0px; ">
<a id="simple-menu" href="#sidr"><img src="images/mobile_menu.jpg"></a>
<?php
if (substr($pageName, 0, 5) == "track") {
?>

<?php
}
?>
<div id="topbarInner" style="position: absolute; top: 0px; right: 15px; " class="topbar"><?= $userSession ?></div>
</div> 