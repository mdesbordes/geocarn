<?php include 'header.php' ?>

<script>
lang = "fr";
thenumDays = 0;

function init() {
	loadWindWin(15);
	loadSnowWin("alpes-du-nord/les-7-laux");
	loadWeatherFromID("France/Rhône-Alpes/Les_Adrets");
}

</script>

<body onLoad="init();">

<?php include 'bodyHeader.php' ?>

<div id="maiwindow" class="maiwindow" style="position: relative; top: 5px; left: 0px; ">
<table width=100% height=130>
	<tr height=130>
		<td width=100% valign=top>

		<DIV id="windWin" class="window" style="width: 100%; height: 130px; z-index : 3; overflow:hidden;">
		<div id="" class="windowtopbar" style="position: relative; top: 0px; left: 0px; height=15px; width: 100%">
		<table width=100% cellpadding=0 cellspacing=0 border=0><tr>
			<td class="windowtopbar" width=15%>Wind</td>
			<td class="windowtopbar" width=1><img src="/images/transp.gif" height=16 width=1></td>
			<td class="windowtopbar" width=85% align=right valign=middle><form id="activityForm">
					<select id=windFormMySelectOption onChange="e = document.getElementById('windFormMySelectOption');loadWindWin(e.options[e.selectedIndex].value);">
						<option value="15">Moucherotte</option>
						<option value="61">St Hilaire</option>
						<option value="600">Attero St Hilaire</option>
						<option value="13">La Scia</option>						
						<option value="14">Villard Cote 2000</option>
						<option value="12">Collet d'Allevard</option>
					</select>						
				&nbsp;<img src="/images/help.gif" width=15 height=15 border=0 title="Main window" alt="Main window">
				</form></td>
			<!--a href=# onClick=loadWindWin(15) class="windowtopbar"><img src="images/orange.gif" width=15 height=15 border=0 alt="Moucherotte" title="Moucherotte"></a>
			<a href=# onClick=loadWindWin(61) class="windowtopbar"><img src="images/green.gif" width=15 height=15 border=0 alt="St Hilaire du Touvet" title="St Hilaire du Touvet"></a>
			<a href=# onClick=loadWindWin(13) class="windowtopbar"><img src="images/brown.gif" width=15 height=15 border=0 alt="La Scia" title="La Scia"></a>
			<img src="images/help.gif" width=15 height=15 border=0 title="Wind info from FFVL" alt="Wind info from FFVL">
			</td-->
			</tr>
		</table>
		</div>
		  <div id="windWinInner" class="innerwindow" style="position: relative; top: 10px; width: 100%; height: 100%"> 
			<table width=100% height=100%>
			  <tr> 
				<td align=center><img src='/images/ajaxLoader.gif'> <p></p>
				  <p></p>&nbsp;</td>
			  </tr>
			</table>
		  </div>
		</DIV>
	
		
	</td>
</tr>

<tr height=160>
		<td width=100% valign=top>
		
		<DIV id="snowWin" class="window" style="width: 100%; height: 160px; z-index : 3; overflow:hidden;">
		<div id="" class="windowtopbar" style="position: relative; top: 0px; left: 0px; height=15px; width: 100%">
		<table width=100% cellpadding=0 cellspacing=0><tr>
			<td class="windowtopbar" width=35%>Snow</td>
			<td class="windowtopbar" width=1><img src="images/transp.gif" height=16 width=1></td>
			<td class="windowtopbar" width=65% align=right><form id="skiStationForm">
					<select id=skiStationFormMySelectOption onChange="e = document.getElementById('skiStationFormMySelectOption');loadSnowWin(e.options[e.selectedIndex].value);">
						<option value="alpes-du-nord/les-7-laux">Les 7 Laux</option>
						<option value="alpes-du-nord/alpe-dhuez">Alpe d'Huez</option>
						<option value="alpes-du-nord/les-2-alpes">Les 2 Alpes</option>
						<option value="alpes-du-sud/la-grave-la-meije">La Grave</option>
						<option value="alpes-du-nord/chamrousse">Chamrousse</option>
						<option value="alpes-du-nord/villard-de-lans">Villard de Lans</option>
						<option value="alpes-du-nord/le-collet-dallevard">Collet d'Allevard</option>
						<option value="alpes-du-nord/tignes">Tignes</option>						
						<option value="alpes-du-nord/val-disere">Val d'Is&egrave;re</option>
					</select>	
			<img src="/images/help.gif" width=15 height=15 border=0 title="Snow info from Ski Info" alt="Snow info from Ski Info">
			</form></td></tr>
		</table>
		</div>
		  <div id="snowWinInner" class="innerwindow" style="position: relative; top: 10px;"> 
			<table width=100% height=100%>
			  <tr> 
				<td align=center><img src='/images/ajaxLoader.gif'> <p></p>Loading
				  <p></p>&nbsp;</td>
			  </tr>
			</table>
		  </div>
		</DIV>
		
	</td>
</tr>

<tr height=230>
		<td width=100% valign=top>
		<DIV id="weatherdiv" class="window" style="width: 100%; height: 220px; z-index : 3; overflow:hidden;">
		<div id="" class="windowtopbar" style="position: relative; top: 0px; left: 0px; height=15px; width: 100%">
		<table width=100% cellpadding=0 cellspacing=0><tr>
			<td class="windowtopbar" width=35%><div class="windowtopbar" id="weatherLocationDiv">Weather</div></td>
			<td class="windowtopbar" width=1><img src="/images/transp.gif" height=16 width=1></td>
			<td class="windowtopbar" width=65% align=right>
			<a href=# onClick=loadWeatherFromID("France/Rhône-Alpes/Les_Adrets") class="windowtopbar"><img src="/images/orange.gif" width=15 height=15 border=0 alt="Les Adrets" title="Les Adrets"></a>
			<a href=# onClick=loadWeatherFromID("France/Provence-Alpes-Côte_d’Azur/Hyères") class="windowtopbar"><img src="/images/green.gif" width=15 height=15 border=0 alt="Hyères" title="Hyères"></a>
			<a href=# onClick=loadWeatherFromID("Sweden/Stockholm/Stockholm") class="windowtopbar"><img src="/images/brown.gif" width=15 height=15 border=0 alt="Stockholm" title="Stockholm"></a>
			<img src="/images/help.gif" width=15 height=15 border=0 title="Weather info from YR.no" alt="Weather info from YR.no">	
		</td></tr>
		</table>
		</div>
		  <div id="weatherdivInner" class="innerwindow" style="position: relative; top: 10px;"> 
			<table width=100% height=100%>
			  <tr> 
				<td align=center><img src='/images/ajaxLoader.gif'> <p></p>Loading
				  <p></p>&nbsp;</td>
			  </tr>
			</table>
		  </div>
		</DIV>
				
	</td>
</tr>
</table>

</div>

<div id="bottombar" class="bottombar">
&nbsp;
</div>

</body>
</html>
