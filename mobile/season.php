<?php include 'header.php' ?>

<script>
lang = "fr";
thenumDays = 0;

function init() {
	$("#titleDiv").html("");
	$("#subTitleDiv").html("");
	var currentDate = new Date();
	getSeasonStat(currentDate.getFullYear(), 1+currentDate.getMonth());
}

function getSeasonStat(year, month) {
	logDebug("-> getSeasonStat("+year + "," + month);
	$.getJSON("/activityStatWS.php?user=" + user + "&action=season&year=" + year + "&month=" + month , function(data) {
		logDebug("activityStatWS -> "+data);
		document.getElementById("seasonStatTitle").innerHTML = "Season " + year;		
		drawSeasonTable(data.activities, year, month);
	});
}

function drawSeasonTable(data, year, month) {
	logDebug("-> drawSeasonTable("+data);
	
	//$("#seasonStatTable").html("<tr /><td colspan=6><img src='/images/transp.gif' width=300 height=1></td>");
	$("#seasonStatTable").html("");
    for (var i = 0; i < data.length; i++) {
		if (data[i].activityId >= 0)
			drawSeasonRow(i, data[i]);
    }
	var row = $("<tr />")
    $("#seasonStatTable").append(row);
	
	var currentDate = new Date();
	if (month >= 8)
		year = 1 + year;
	row.append($("<td colspan=4 height=20 valign=middle>&nbsp;* : For winter sports, the season is : " + (year-1) + " - " + year + "</td>"));
	//$("#latestTracksTable td").addClass("tracksTableIndex");
	$("#seasonStatTable > tbody > tr:odd").addClass("rowOdd");
    $("#seasonStatTable > tbody > tr:not(.odd)").addClass("rowEven"); 
}

function drawSeasonRow(rowId, rowData) {
	var row = $("<tr />")
    $("#seasonStatTable").append(row); //this will append tr element to table... keep its reference for a while since we will add cels into it
   
	row.append($("<td width=60 align=center><img src='/images/" + rowData.activityId + ".png' width=60 height=60></td>"));
	if (rowData.activityId == 0)
		row.append($("<td width=100 class='dataBig' colspan=2>" + secondsToHours(rowData.summin *60) + "<div class='unit'> h</div></td>"));
	else if (rowData.activityId == 1)
		row.append($("<td width=100 class='dataBig' colspan=2>" + rowData.activityAltDiff + "<div class='unit'> m</div></td>"));
	else if (rowData.activityId == 4)
		row.append($("<td width=200 class='dataBig' colspan=2>" + Math.round(rowData.activityDist / 1.852) + "<div class='unit'> nm</div></td>"));
	else if (rowData.activityId == 7 || rowData.activityId == 3) {
		row.append($("<td width=100 class='dataBig'>" + Math.round(rowData.activityDist) + "<div class='unit'> km</div></td>"));
		row.append($("<td width=100 class='data'>" + rowData.activityAltDiff + "<div class='unit'> m</div></td>"));
	}
	else
		row.append($("<td width=100 class='dataBig' colspan=2>" + Math.round(rowData.activityDist) + "<div class='unit'> km</div></td>"));
	
	row.append($("<td width=40 align=center>" + rowData.activitySum + "</td>"));
	
	
}
</script>

<body onLoad="init();">

<?php include 'bodyHeader.php' ?>

<div id="maiwindow" class="maiwindow" style="position: relative; top: 5px; left: 0px; height: 100%">
<table width=100% height=100%>
	<tr height=100%>
		
		<td width=100% height=100% valign=top>
			<DIV id="mainWin" class="window" style="width: 100%; height: 100%; z-index : 3; overflow:hidden;">
				<div id="" class="windowtopbar" style="position: relative; top: 0px; left: 0px; height=15px; width: 100%">
					<table width=100% cellpadding=0 cellspacing=0 border=0><tr>
						<td class="windowtopbar" width=50% id="seasonStatTitle" valign=middle>Season statistics</td>
						<td class="windowtopbar" width=1><img src="/images/transp.gif" height=16 width=1></td>
						<form id="seasonForm"><td class="windowtopbar" width=50% align=right valign=middle>
							<select id=seasonFormMySelectOption onChange="e = document.getElementById('seasonFormMySelectOption');getSeasonStat(e.options[e.selectedIndex].value, 1);">
								<script>
								var currentDate = new Date();
								optionStr = "";
								for (theYear = currentDate.getFullYear(); theYear >= 2005 ; theYear--) {
									optionStr = optionStr + "<option value=" + theYear + ">" + theYear + "</option>";
								}
								document.write(optionStr);
								</script>
							</select>						
						&nbsp;<img src="/images/help.gif" width=15 height=15 border=0 title="Main window" alt="Main window">
						</td></form></tr>
					</table>
				</div>
				<div id="seasonStatWinInner" class="innerwindow" style="position: relative; top: 10px; width: 100%; height: 100%; overflow: auto; overflow-x: hidden;"> 
					
					<table id="seasonStatTable" class="statTable" width=100%>
						<tr> 
						<td align=center><img src='/images/transp.gif' width=360 height=1></td>
					  </tr><tr> 
						<td align=center><img src='/images/ajaxLoader.gif'> <p>Loading
						  <p>&nbsp;</td>
					  </tr>
					</table>
			  </div>
			</div>
		</td>
</tr>
</table>

</div>

<div id="bottombar" class="bottombar">
&nbsp;
</div>

</body>
</html>
