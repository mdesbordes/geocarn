<?php include '../inc/logger.php' ?>
<?php
session_set_cookie_params(680400);
session_start(); 
	
logDebug("_SESSION = " . isset($_SESSION));
logDebug("_SESSION[user] = " . $_SESSION['user']);
	
$userSession = null;	
if(!isset($_SESSION) || !isset($_SESSION['user'])) 
{ 
    $userSession = null;
	header('Location: /mobile/login.php');
} 
else {
	$userSession = $_SESSION['user'];
}

// Create the function, so you can use it
function isMobile() {
    return preg_match("/(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i", $_SERVER["HTTP_USER_AGENT"]);
}
// If the user is on a mobile device, redirect them


?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html lang="en">
<head>
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no, minimal-ui">
  <meta name="mobile-web-app-capable" content="yes">
  <meta name="apple-mobile-web-app-capable" content="yes">
  
  <title>geocarn</title>

</head>

<link rel="shortcut icon" href="images/favicon.ico">

<link rel="stylesheet" type="text/css" href="/common.css">
<link rel="stylesheet" href="jquery.sidr.dark.css">	  

<style>
img {
max-width: 100%;
}
</style>
	  
<script src="/jquery-2.1.1.js" LANGUAGE="JavaScript"></script>
<script src="/common.js" LANGUAGE="JavaScript"></script>
<script src="/log.js" LANGUAGE="JavaScript"></script>
<script src="/ajax.js" LANGUAGE="JavaScript"></script>
<script src="/wind.js" LANGUAGE="JavaScript"></script>
<script src="/weather.js" LANGUAGE="JavaScript"></script>
<script src="/snow.js" LANGUAGE="JavaScript"></script>

<script src="jquery.sidr.min.js"></script>
<script src="jquery.touchwipe.1.1.1"></script>

<script type="text/javascript" src="https://api.ign.fr/geoportail/api/js/latest/Geoportal.js"></script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyARw_8RDJJPsqe-m_tD1n3Ie9tBpTx2kL0"></script>

<script>
sfHover = function() {
        var sfEls = document.getElementById("menu").getElementsByTagName("LI");
        for (var i=0; i<sfEls.length; i++) {
                sfEls[i].onmouseover=function() {
                        this.className+=" sfhover";
                }
                sfEls[i].onmouseout=function() {
                        this.className=this.className.replace(new RegExp(" sfhover\\b"), "");
                }
        }
}
if (window.attachEvent) window.attachEvent("onload", sfHover);

var MA_CLE= "p3155qxcnkn3hcfl6xm4j5k9";

var user = "<?= $userSession ?>";
</script>