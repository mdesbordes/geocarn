<?php include 'header.php' ?>
<?php
if (isset($_GET['activityId']))
	$activityId = $_GET['activityId'];
else
	$activityId = -1;
?>

<script>
lang = "fr";

activityId = <?= $activityId; ?>;
if (activityId == null || activityId == "")
	activityId = -1;
var user = "<?= $userSession; ?>";

var map;
var openskimap;
var trackIdArray = [];
var tourLat = 0;
var tourLon = 0;
var trackData;
var weekData;

var markerArray = [];

var currentDate = new Date();
var season;
var year = currentDate.getFullYear();
var month = 1 + currentDate.getMonth();
var isInitDone = false;

function init() {
	logDebug("-> init()");
	logDebug("year = " + year);
	
	logDebug("activityId = " + activityId);
	
	$.getJSON("/activityStatWS.php?user=" + user + "&action=season&green=1&year=" + year + "&month=" + month + "&activity=" + activityId , function(data) {
		activityData = data.activities;
		drawTable(activityData);
		isInitDone = true;
	});
}

function drawTable(activityData) {
	$("#summaryTable").html("");
	if (activityData != null && activityData.length > 0) {
		activitySum = 0;
		summin = 0;
		activityDist = 0;
		activityAltDiff = 0;

		for (var i = 0; i < activityData.length; i++) {
			rowData = activityData[i];
			activitySum += new Number(rowData.activitySum);
			summin += new Number(rowData.summin);
			activityDist += new Number(rowData.activityDist);
			activityAltDiff += new Number(rowData.activityAltDiff);
		}
		var c = [];
		timeSpent = secondsToHms(summin *60);
		co2 = Math.round(activityDist * CO2_PER_KM); // 200 g/CO2/km
		price = Math.round(activityDist * EURO_PER_KM); // 0.5€/km
		c.push("<tr height=100><td align=center valign=middle width=64><img src='/images/CO2.png' width=64></td><td>&nbsp;</td><td align=center class='dataHuge' valign=middle>" + activitySum + "<div class='bigunit'> Kg CO2</div></td></tr>");
		c.push("<tr height=100><td align=center valign=middle width=64><img src='/images/bicycle.png' width=64></td><td>&nbsp;</td><td align=center class='dataHuge' valign=middle>" + Math.round(activityDist) + "<div class='bigunit'> km</div></td></tr>");
		c.push("<tr height=100><td align=center valign=middle width=64><img src='/images/coin.png' width=64></td><td>&nbsp;</td><td align=center class='dataHuge' valign=middle>" + price + "<div class='bigunit'> €</div></td></tr>");
		
		$("#summaryTable").html(c.join(""));
	}
	else {
		$("#summaryTable").html("<tr><td class='title' align=center>No tracks for this category during the season " + season + "</td></tr>");
	}
}

</script>

<body onLoad="init();">

<?php include 'bodyHeader.php' ?>

<div id="maiwindow" class="maiwindow" style="position: relative; top: 5px; left: 0px; height: 60%">
<table width=100% height=100%>
	<tr height=100%>
		<td width=100% height=100%>
			<DIV id="summaryWin" class="window" style="width: 100%; height: 100%; z-index : 3; overflow:hidden;">
				<div id="" class="windowtopbargreen" style="position: relative; top: 0px; left: 0px; height=15px; width: 100%">
				<table width=100% height=100% cellpadding=0 cellspacing=0 border=0><tr>
					<td class="windowtopbargreen" width=35%>Summary</td>
					<td class="windowtopbargreen" width=1><img src="images/transp.gif" height=16 width=1></td>
					<form id=\"seasonForm\"><td align=right valign=middle><td class="windowtopbargreen" width=50% align=right valign=middle>
					<select id="seasonFormMySelectOption" onChange="e = document.getElementById('seasonFormMySelectOption'); year=e.options[e.selectedIndex].value;month=1; init();">
					<script>
						var currentDate = new Date();
						for (theYear = currentDate.getFullYear(); theYear >= 2005 ; theYear--) {
							if (theYear == year)
								selected = " selected";
							else
								selected = "";
							document.write("<option value=" + theYear + selected + ">" + theYear + "</option>");
						}
					</script>
					</select></td></form>
					</tr>
				</table>
				</div>
				<div id="mainWinInner" class="innerwindow" style="position: relative; top: 0px; width: 100%; height: 100%; overflow:hidden;">
					<table width=100% height=100%>
						<tr>
							<td width=100% align=center valign=middle>
								<table width=100% height=100% id="summaryTable" class="summaryTable">
								  <tr>
									<td align=center><img src='/images/ajaxLoader.gif'> <p>
									  <p>&nbsp;</td>
								  </tr>
								</table>
							</td>
						</tr>
					</table>
				  </div>
				</div>
			</div>
		</td>
	</tr>
</table>
</div>

</body>
</html>
