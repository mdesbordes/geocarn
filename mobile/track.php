<?php include 'header.php' ?>

<script>
lang = "fr";

var trackId = <?= $_GET['trackId']; ?>;
var tourId = -1;

function init() {
	$.getJSON("/tracksWS.php?track=<?= $_GET['trackId']; ?>" , function(data) {
		tourId = data.tracks[0].tourId;
		setTitle(data.tracks);
		drawTable(data.tracks);
		setImg(data.tracks);
		initGeoPortail();
	});
	
}

function setTitle(data) {
	title = "<table width=100%><tr><td><img src='/images/" + data[0].activityId + ".png' width=45 height=45> <a href=/tour.php?tourId=" + data[0].tourId + "></td><td>" + data[0].tourName + "</a>";
	//$("#titleDiv").html(title);
	
	//title = "<img src='/images/" + data[0].activityId + ".png'>";
	title = title + "<br>";
	if (data[0].trackName != "")
		title = title + "" + data[0].trackName + "";
	title = title + "&nbsp;" + shortDate(data[0].trackDate, 10) + "</td>";
	title = title + "<td><a href='/gpx.php?tourId=" + tourId + "&trackId=" + trackId + "'>[gpx]</a>";
	title = title + "<br><a href='/kml.php?tourId=" + tourId + "&trackId=" + trackId + "'>[kml]</a>";
	title = title + "</td>";
	title = title + "<td></td>";
	title = title + "</tr></table>";
	$("#titleDiv").html(title);
	
	document.getElementById("topbarInner").innerHTML = "<a href=# onClick='initGeoPortail()'>[GEO]</a><br><a href=# onClick='initGoogleMaps()'>[GOOGLE]</a>";
}

function setImg(data) {
	img = "<img src='/imgAltitude.php?tourId=" + tourId + "&trackId=" + trackId + "&rel=1&width=340&height=160'>";
	$("#speedWinInner").html(img);
}

function drawTable(data) {
	for (var i = 0; i < data.length; i++) {
        drawRow(i, data[i]);
    }
}

function drawRow(rowId, rowData) {
	var c = [];
	
	//var row = $("<tr />");
    //$("#tracksTable").append(row); //this will append tr element to table... keep its reference for a while since we will add cels into it
    c.push("<tr><td align=center colspan=2>Distance</td></tr>");
	c.push("<tr><td class='dataBig' colspan=2>" + rowData.trackDistance + " km</td></tr>");
	c.push("<tr><td align=center>Average speed</td><td align=center>Duration</td></tr>");
	c.push("<tr><td class='dataBig'>" + rowData.trackAverageSpeed + " km/h</td><td class='dataBig'>" + rowData.trackDuration + "</td></tr>");
	c.push("<tr><td align=center>Average up speed</td><td align=center>Altitude difference</td></tr>");
	c.push("<tr><td class='data'>" + rowData.trackAverageUpSpeed + " m/h</td><td class='data'>" + rowData.trackAltDiff + " m</td></tr>");
	c.push("<tr><td align=center>Altitude min</td><td align=center>Altitude max</td></tr>");
	c.push("<tr><td class='data'>" + rowData.trackAltMin + " m</td><td class='data'>" + rowData.trackAltMax + " m</td></tr>");
	$("#tracksTable").html(c.join(""));
}


         <!--//--><![CDATA[//><!--
            	var iv= null;
				var viewer=null;
								
                function initGeoPortail() {
                
                        iv= Geoportal.load(
                                 // div's ID:
                                 'viewerDiv',
                                 // API's keys:
                                 [MA_CLE],
                                 {// map's center :
                                                // longitude:
                                                lon:5.9532536,
                                                // latitude:
                                                lat:45.2723186
                                 },
                                 //zoom level 
                                 15,
                                 //options
                                 {
									layers:['GEOGRAPHICALGRIDSYSTEMS.MAPS','ORTHOIMAGERY.ORTHOPHOTOS','CADASTRALPARCELS.PARCELS'],
							
									layersOptions:{'GEOGRAPHICALGRIDSYSTEMS.MAPS':{visibility:true,opacity:1,  minZoomLevel:1,maxZoomLevel:18},
												'ORTHOIMAGERY.ORTHOPHOTOS':{visibility:false,opacity:1, minZoomLevel:1, maxZoomLevel:18},
												'CADASTRALPARCELS.PARCELS':{visibility:false,opacity:1, minZoomLevel:12,maxZoomLevel:18}},
									onView: function() {
										viewer=this.getViewer();										
										
										/* style de la trace */
										var styleTrace = new OpenLayers.StyleMap({
										"default": new OpenLayers.Style({
						 
										strokeColor: '#ff0000',
										strokeOpacity: 0.8,
										strokeWidth:5
						
										}),
										"select": new OpenLayers.Style({
										strokeColor: '#FF0000',
										})
										});
				   
										/* ajout du fichier gpx   */
										gpxLayer = viewer.getMap().addLayer(
										"GPX",
										"trace",
										"https://geocarn.freecluster.eu/gpx.php?tourId=" + tourId + "&trackId=" + trackId,{
											visibility: true,
											opacity:0.8,
											styleMap: styleTrace,
											eventListeners:{
												'loadend':function(){
													if (this.maxExtent) {
														this.map.zoomToExtent(this.maxExtent);
														this.setVisibility(true);
														}
													}
												}
											}
										);
									}
							}
                        );
												
                };
				
		function initGoogleMaps() {
			var mapOptions = {
			  center: { lat: 45.2723186, lng: 5.9532536},
			  zoom: 8,
			  mapTypeId: google.maps.MapTypeId.TERRAIN
			};
			var map = new google.maps.Map(document.getElementById('viewerDiv'),
				mapOptions);
			
			var bikeLayer = new google.maps.BicyclingLayer();
			  bikeLayer.setMap(map);
			
			$.ajax({
				  type: "GET",
				  url: "https://geocarn.freecluster.eu/gpx.php?tourId=" + tourId + "&trackId=" + trackId,
				  dataType: "xml",
				  success: function(xml) {
					var points = [];
					var bounds = new google.maps.LatLngBounds ();
					$(xml).find("trkpt").each(function() {
					  var lat = $(this).attr("lat");
					  var lon = $(this).attr("lon");
					  var p = new google.maps.LatLng(lat, lon);
					  points.push(p);
					  bounds.extend(p);
					});

					var poly = new google.maps.Polyline({
					  // use your own style here
					  path: points,
					  strokeColor: colorTrackArray[i],
					  strokeOpacity: .8,
					  strokeWeight: 4,
					});
					logDebug(poly);
					
					poly.setMap(map);
					
					// fit bounds to track
					map.fitBounds(bounds);
				  }
				});
		}


	
                //--><!]]>

</script>

<body onLoad="init();" scrolling="no">

<?php include 'bodyHeader.php' ?>

<div id="maiwindow" class="maiwindow" style="position: relative; top: 5px; left: 0px; ">
<table width=100%>
	<tr height=480>
		<td width=100% height=480>
			<DIV id="mainWin" class="window" style="width: 100%; height: 100%; z-index : 3; overflow:hidden;">
			<div id="" class="windowtopbar" style="position: relative; top: 0px; left: 0px; height=15px; width: 100%">
			<table width=100% cellpadding=0 cellspacing=0 border=0><tr>
				<td class="windowtopbar" width=35%>Main</td>
				<td class="windowtopbar" width=1><img src="/images/transp.gif" height=16 width=1></td>
				<td class="windowtopbar" width=65% align=right>
				<img src="/images/help.gif" width=15 height=15 border=0 title="Main window" alt="Main window">
				</td></tr>
			</table>
			</div>
			
			<div id="titleDiv" class="titleMobile" style="position: absolute; top: 25px; left: 5px; ">
				
			</div>
				
			<div id="mainWinInner" class="innerwindow" style="position: relative; top: 50px; width: 100%; height: 100% overflow:scroll;"> 
				<div id="tableDiv" style="width: 100%, height: 440px">							
					<table width=100% id="tracksTable" class="window">
					  <tr> 
						<td align=center><img src='/images/ajaxLoader.gif'> <p>
						  <p>&nbsp;</td>
					  </tr>
					</table>
				</div>	
				
				<div id="speedWinInner" style="width: 100%, height: 280px"></div>
			  </div>
			</div>			
		</td>
	</tr>
	<tr height=600>
		<td width=100% height=500>
			<DIV id="mainWin" class="window" style="width: 100%; height: 100%; z-index : 3; overflow:hidden;">
			<div id="" class="windowtopbar" style="position: relative; top: 0px; left: 0px; height=15px; width: 100%">
			<table width=100% cellpadding=0 cellspacing=0 border=0><tr>
				<td class="windowtopbar" width=35%>Main</td>
				<td class="windowtopbar" width=1><img src="/images/transp.gif" height=16 width=1></td>
				<td class="windowtopbar" width=65% align=right>
				<img src="/images/help.gif" width=15 height=15 border=0 title="Main window" alt="Main window">
				</td></tr>
			</table>
			</div>
			<div id="viewerDiv" class="innerwindow" style="position: relative; top: 20px; width: 100%; height: 100% overflow:scroll;"> 
				
			  </div>
			</div>
		</td>
	</tr>
		
</table>
</div>

</body>
</html>
