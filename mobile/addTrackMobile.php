<?php include 'header.php' ?>
<?php
if (isset($_GET['fileId']))
	$fileId = $_GET['fileId'];
else
	$fileId = "";
if (isset($_GET['uploadfile']))
	$uploadfile = $_GET['uploadfile'];
else
	$uploadfile = "";

?>

<script src="/addTourInc.js" LANGUAGE="JavaScript"></script>

<?php
$action = "manual";

$currentDate = $date = date('Y/m/d', time());
?>

<script>
var map = null;
var planned = false;
</script>


<script>
lang = "fr";

var tourId = -1;
var action = "<?= $action; ?>";
var fileId = "<?= $fileId; ?>";
var fileName = "<?= $uploadfile; ?>";
var user = "<?= $_SESSION['user'] ?>";
var map = null;
var mapSize = "NORMAL";

function init() {
	console.log("-> init( ) ");
	planned = false;
	
	console.log("<- init()");
}

function chooseActivityCategory(activityId) {
	logDebug("-> chooseActivityCategory(" + activityId);
	$.getJSON("/toursWS.php?activity=" + activityId + "&user=" + user + "&limit=500" , function(data) {
		logDebug("data = " + data.length);
		var $select = $('#tour');
		$select.empty();
		var tours = data.tours;
		$select.append("<option value=''>...</option>");
		for (i = 0; i < tours.length; i++)
		{
			$select.append('<option value=' + tours[i].tourId + '>' + tours[i].tourName + '</option>');
		};
	});
	document.getElementById("formValues").style.visibility="visible";
}

function chooseTour(theform) {
	theform.tourname.disabled=true;

	// TODO Load latest track from tour
}

function loadTourData() {
	logDebug("-> loadTourData(");
	i = tourform.tour.selectedIndex;
	logDebug(i + " - " + tourform.tour.options[i].value);

	tourId = tourform.tour.options[i].value;
	$.getJSON("/toursWS.php?tour=" + tourId, function(data) {
		logDebug("data = " + data.length);
		var tracks = data.tracks;
		logDebug("tracks = " + tracks.length);
		if (tracks.length > 0) {
			logDebug("data[0] = " + tracks[0].trackDate + " - " +  tracks[0].tourName);
			tourform.timediff.value = tracks[0].trackDuration;
			tourform.distance.value = tracks[0].trackDistance;
		}
	});

}

</script>

<body onLoad="init();" scrolling="no">

<?php include 'bodyHeader.php' ?>


<div id="maiwindow" class="maiwindow" style="position: relative; top: 5px; left: 0px; right: 0px; bottom: 0px;">
<table width=100% height=100%>
	<tr height=100%>
		<td height=100%>
			<DIV id="mainWin" class="window" style="width: 100%; height: 100%; z-index : 3; overflow:hidden;">
				<div id="" class="windowtopbar" style="position: relative; top: 0px; left: 0px; height=15px; width: 100%">
					<table width=100% cellpadding=0 cellspacing=0 border=0><tr>
						<td class="windowtopbar" width=35%>Track Details</td>
						<td class="windowtopbar" width=1><img src="/images/transp.gif" height=16 width=1></td>
						<td class="windowtopbar" width=65% align=right>
						<img src="/images/help.gif" width=15 height=15 border=0 title="Main window" alt="Main window">
						</td></tr>
					</table>
				</div>

				<div id="formDiv" class="innerwindow" style="position: relative; top: 0px; width: 100%; height: 100% overflow:scroll;">
				
					<table width=100%>
						<form name=tourform onSubmit="createTour();return false;">
						<tr>
							<td width=100% valign=top class=box>
								<table class="addTrackTable" width=100%>
									<tr>
										<td align=center>Choose a sport category :</td>
									</tr>
									<tr>
										<td valign=middle align=center>
											<input type=radio name=activity value="0" onClick="chooseActivityCategory(0)"><img src="/images/0.png" width=30 height=30 border=0>&nbsp;&nbsp;
											<input type=radio name=activity value="1" onClick="chooseActivityCategory(1)"><img src="/images/1.png" width=30 height=30 border=0>&nbsp;&nbsp;
											<input type=radio name=activity value="2" onClick="chooseActivityCategory(2)"><img src="/images/2.png" width=30 height=30 border=0>&nbsp;&nbsp;
											<input type=radio name=activity value="3" onClick="chooseActivityCategory(3)"><img src="/images/3.png" width=30 height=30 border=0>
											<br>											
											<input type=radio name=activity value="4" onClick="chooseActivityCategory(4)"><img src="/images/4.png" width=30 height=30 border=0>&nbsp;&nbsp;
											<input type=radio name=activity value="5" onClick="chooseActivityCategory(5)"><img src="/images/5.png" width=30 height=30 border=0>&nbsp;&nbsp;
											<input type=radio name=activity value="6" onClick="chooseActivityCategory(6)"><img src="/images/6.png" width=30 height=30 border=0>&nbsp;&nbsp;
											<input type=radio name=activity value="7" onClick="chooseActivityCategory(7)"><img src="/images/7.png" width=30 height=30 border=0>
											<br>
											<input type=radio name=activity value="8" onClick="chooseActivityCategory(8)"><img src="/images/8.png" =30 height=30 border=0>&nbsp;&nbsp;
											<input type=radio name=activity value="9" onClick="chooseActivityCategory(9)"><img src="/images/9.png" width=30 height=30 border=0>&nbsp;&nbsp;
											<input type=radio name=activity value="10" onClick="chooseActivityCategory(10)"><img src="/images/10.png" width=30 height=30 border=0>&nbsp;&nbsp;
											<input type=radio name=activity value="11" onClick="chooseActivityCategory(11)"><img src="/images/11.png" width=30 height=30 border=0>
											<br>
											<input type=radio name=activity value="12" onClick="chooseActivityCategory(12)"><img src="/images/12.png" width=30 height=30 border=0>&nbsp;&nbsp;
											<input type=radio name=activity value="13" onClick="chooseActivityCategory(13)"><img src="/images/13.png" width=30 height=30 border=0>&nbsp;&nbsp;
											<input type=radio name=activity value="14" onClick="chooseActivityCategory(14)"><img src="/images/14.png" width=30 height=30 border=0>&nbsp;&nbsp;
											<input type=radio name=activity value="15" onClick="chooseActivityCategory(15)"><img src="/images/15.png" width=30 height=30 border=0>&nbsp;&nbsp;
										</td>
									</tr>
								</table>

								<div id="formValues" style="visibility: hidden;">
								
									<table width=100% class="addTrackTable">
										<tr>
											<td>Choose a tour :</td>
										</tr>
										<tr>
											<td>
											<select id="tour" name="tour" onChange="chooseTour(this.form);">
											</select>
											<br>
											<a href=# onClick="loadTourData();">Import</a>
											</td>
										</tr>
										
										<tr>
											<td>Track name :</td>
										</tr>
										<tr>
											<td><input name="trackname" size="20" type="text" /></td>
										</tr>

										<tr>
											<td>Date: </td>
										</tr>
										<tr>
											<td><input type=text name=date id="date" size=10 value="<?= $currentDate ?>">		</td>
										</tr>																		
									</table>

										<p>

									<table width=100%>
										<tr valign=top>
											<td>
												<table class="subAddTrackTable" width=100%>
													<tr><td>Duration: </td><td><input type=text name=timediff size=6 value="00:00:00">				</td></tr>
													<tr><td>Distance: </td><td><input type=text name=distance size=6 value="0.00"> km	</td></tr>
													<tr><td></td><td>&nbsp;</td></tr>
													<tr><td>Average: </td><td></td></tr>
													<tr><td>Speed: </td><td><input type=text name=averagespeed size=6> km/h</td></tr>
													<tr><td>Up speed: </td><td><input type=text name=averageupspeed size=6> m/h</td></tr>
													<tr><td></td><td><a href=# onClick="calculateAverageSpeed();return false;" class="formbutton">Calculate avg</a></td></tr>
													<tr><td><img src="/images/transp.gif" width=1 height=15></td></tr>
												</table>
											</td>											
										</tr>
										<tr>
											<td height=15><img src="/images/transp.gif" width=50 height=15></td>
										</tr>
									</table>

									<p>
									<table width=100%>
										<tr>
											<td height=15><img src="/images/transp.gif" width=50 height=15></td>
										</tr>
										<tr>
											<td width=100% height=25 align=center>
											<a href=# onClick="createTour();return false;" class="formbuttonbig">Create track</a>
											</td>
										</tr>
									</table>
									<!--input type=submit onClick="document.getElementById('loaderWin').style.visibility='visible'; return true;"-->

								</div>

							</td>
						</tr>
						
						<input name="tourname" type="hidden" />
						<input type="hidden" name=planned size=6>
						<input type="hidden" name=country size=6>
						<input type="hidden" name=altgain size=6>
						<input type="hidden" name=altdiff size=6>
						<input type="hidden" name=altmax size=6>
						<input type="hidden" name=altstart size=6>
						<input type="hidden" name=timeup size=6 value="00:00:00">					
						</form>
					</table>
				</div>
			</div>			
		</td>

	</tr>
</table>
</div>

<div id="loaderDiv" class="innerwindow" style="position: relative; top: 20px; width: 100px; height: 100px overflow:scroll;"> </div>
<div id="loaderMsgDiv" class="innerwindow" style="position: relative; top: 20px; width: 100px; height: 100px overflow:scroll;"> </div>
<div id="statusDiv" class="innerwindow" style="position: relative; top: 20px; width: 100px; height: 100px overflow:scroll;"> </div>
<div id="statusCloseButton" class="innerwindow" style="position: relative; top: 20px; width: 100px; height: 100px overflow:scroll;"> </div>


</body>
</html>
