<?php

session_start();

$userSession = null;

?>

<html lang="en">
<head>
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no, minimal-ui">
  <meta name="mobile-web-app-capable" content="yes">
  <meta name="apple-mobile-web-app-capable" content="yes">
  
  <title>geocarn</title>

</head>

<link rel="shortcut icon" href="images/favicon.ico">

<link rel="stylesheet" type="text/css" href="/common.css">
<link rel="stylesheet" href="jquery.sidr.dark.css">	  

<script src="/jquery-2.1.1.js" LANGUAGE="JavaScript"></script>
<script src="/common.js" LANGUAGE="JavaScript"></script>
<script src="/log.js" LANGUAGE="JavaScript"></script>
<script src="/ajax.js" LANGUAGE="JavaScript"></script>
<script src="/wind.js" LANGUAGE="JavaScript"></script>
<script src="/weather.js" LANGUAGE="JavaScript"></script>
<script src="/snow.js" LANGUAGE="JavaScript"></script>

<script src="jquery.sidr.min.js"></script>
<script src="jquery.touchwipe.1.1.1"></script>

<style>
img {
max-width: 100%;
}
</style>
	  

<script>
lang = "fr";
thenumDays = 0;

/*
function doLogin(txtResponse) {
	//hideLoader();
	logDebug("-> doLogin(...) - " + msgDivId);	
	document.getElementById(msgDivId).innerHTML = "Login...";
	txtResponse = removeXML(txtResponse);
	var result = txtResponse.split('|');
	var code = result[0];
	msg = result[1];
	detail = result[2];
	str = "<b>" + msg + "</b><p>" + detail;		
	if (code != 0) {
		closeFunction = null;
		status = STATUS_ERROR;
	}
	else {
		closeFunction = null;
		status = STATUS_SUCCESS;
	}
	//showStatus(status, str, "loginWin", closeFunction);
	
	if (status == STATUS_SUCCESS) {
		document.getElementById(msgDivId).innerHTML = "<font color=green><b>Login successful. Redirecting to home page...</b></font>";
		window.location = "/";
	}
	else {
		document.getElementById(msgDivId).innerHTML = "<font color=red><b>Login error : " + detail + "</b></font>";
	}
}
*/

function doLogin(username, password, themsgDivId) {
	logDebug("-> login()");	
	//showLoader("Login...");
	msgDivId = themsgDivId;
	logDebug("msgDivId = " + msgDivId);	
	document.getElementById(msgDivId).innerHTML = "<font color=blue><b>Login in progress...</b></font>";
	//var ai = AJAXInteraction("/loginWs.php?username=" + username + "&password=" + password, null, doLogin);
	//ai.doGet();
	
	$.post("/loginWs.php",
        {
          username: username,
          password: password
        },
        function(txtResponse,status){
            //hideLoader();
			logDebug("-> doLogin(...) - " + msgDivId);	
			if (msgDivId != null && msgDivId.length > 0)
				document.getElementById(msgDivId).innerHTML = "Login...";
			txtResponse = removeXML(txtResponse);
			var result = txtResponse.split('|');
			var code = result[0];
			msg = result[1];
			detail = result[2];
			str = "<b>" + msg + "</b><p>" + detail;		
			if (code != 0) {
				closeFunction = null;
				status = STATUS_ERROR;
			}
			else {
				closeFunction = null;
				status = STATUS_SUCCESS;
			}
			//showStatus(status, str, "loginWin", closeFunction);
			
			if (status == STATUS_SUCCESS) {
				document.getElementById(msgDivId).innerHTML = "<font color=green><b>Login successful. Redirecting to home page...</b></font>";
				window.location = "/mobile/";
			}
			else {
				document.getElementById(msgDivId).innerHTML = "<font color=red><b>Login error : " + detail + "</b></font>";
			}
        });
}


</script>

<body>

<?php include 'bodyHeader.php' ?>

<DIV id="mainWin" class="window" style="width: 100%; height: 200px; z-index : 3; overflow:hidden;">
	<div id="" class="windowtopbar">
		<table width=100% cellpadding=0 cellspacing=0 border=0><tr>
			<td class="windowtopbar" width=50% id="">Login</td>
			<td class="windowtopbar" width=1><img src="images/transp.gif" height=16 width=1></td>
			<td class="windowtopbar" width=50% align=right valign=middle>
									
			</td>
			<td valign=middle>
			<img src="/images/help.gif" width=15 height=15 border=0 title="Main window" alt="Main window">
			</td>
		</table>
	</div>
	<div id="mainWinInner" class="innerwindow" style="position: relative; top: 10px; width: 100%; height: 350px; overflow: auto; overflow-x: hidden;"> 
		<table border=0 width=100%>
		  <form name="login" onSubmit="doLogin(document.forms['login'].username.value, document.forms['login'].password.value, 'loginMsgMain'); return false;">
			<tr> 
			  <td>Username : </td>
			  <td><input type="text" name="username" size=15 value=""></td>					  
			</tr>
			<tr> 
			  <td>Password : </td>
			  <td><input type="password" name="password"  onKeyPress="if (checkKey(event)) {doLogin(document.forms['login'].username.value, document.forms['login'].password.value, 'loginMsgMain'); }" size=15 value=""></td>					  
			</tr>
			<tr>
				<td></td>
				<td><input type="submit" name="login" value="Login" onClick="doLogin(document.forms['login'].username.value, document.forms['login'].password.value, 'loginMsgMain'); return false;"/></td>
			</tr>
			<tr> 
			  <td colspan=2><img src="images/transp.gif" height=10 width=2></td>
			</tr>
			<tr> 
			  <td colspan=2 id="loginMsgMain"></td>
			</tr>
			<tr> 
			  <td colspan=2><img src="images/transp.gif" height=1 width=2></td>
			</tr>
			<!--tr> 
			  <td colspan=2>You don't have an account? <a href=# onClick="registerUser()">Create one</a></td>
			</tr-->										
		  </form>
		</table>		
  </div>
</DIV>


</body>
</html>