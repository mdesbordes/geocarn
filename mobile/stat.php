<?php include 'header.php' ?>

<script>
lang = "fr";
thenumDays = 0;

function init() {
	$("#titleDiv").html("");
	$("#subTitleDiv").html("");
	getActivityStat(30);
	
}

function getActivityStat(numDays) {
	thenumDays = numDays;
	$.getJSON("/activityStatWS.php?user=" + user + "&days=" + numDays , function(data) {
		showActivityStat(data.activities);
	});
}

function showActivityStat(data) {
	logDebug("-> showActivityStat(" + data);
	var activityData = [];
	//$("#activityStatTable").html("<tr /><td colspan=6><img src='/images/transp.gif' width=300 height=1></td>");
	$("#activityStatTable").html("");
	var row = $("<tr />");
	$("#activityStatTable").append(row);
	document.getElementById("latestActivitiesTitle").innerHTML = "Activities last " + thenumDays + " days";
	logDebug("data.length = " + data.length);
	maxSum = 0;
	for (var i = 0; i < data.length; i++) {
		logDebug(data[i].activitySum + " > " + maxSum);
		if (new Number(data[i].activitySum) > maxSum) {
			
			maxSum = data[i].activitySum;
			logDebug("-> " + maxSum);
		}
	}
	ratio = 200 / maxSum;
	logDebug("maxSum = " + maxSum);
	logDebug("ratio = " + ratio);
	for (var i = 0; i < data.length; i++) {
		var row = $("<tr align=left />");
		$("#activityStatTable").append(row);
		row.append($("<td width=200 ><img src='/images/" + data[i].activityId + ".png' width=" +  Math.round(data[i].activitySum * ratio) + " height=60 border=0></td><td class='data'>" + data[i].activitySum + "</td>"));
		
    }
	//$("#activityStatTable td").addClass("tracksTableIndex");
	
}

</script>

<body onLoad="init();">

<?php include 'bodyHeader.php' ?>


<div id="maiwindow" class="maiwindow" style="position: relative; top: 5px; left: 0px; ">
<table width=100% height=100%>
	<tr height=100%>
		<td width=100% height=300 valign=top>
			<DIV id="mainWin" class="window" style="width: 100%; height: 100%; z-index : 3; overflow:hidden;">
			<div id="" class="windowtopbar" style="position: relative; top: 0px; left: 0px; height=15px; width: 100%">
			<table width=100% cellpadding=0 cellspacing=0 border=0><tr>
				<td class="windowtopbar" width=50% id="latestActivitiesTitle">Latest activities</td>
				<td class="windowtopbar" width=1><img src="images/transp.gif" height=16 width=1></td>
				<form id="activityForm"><td class="windowtopbar" width=50% align=right valign=middle>
					<select id=activityFormMySelectOption onChange="e = document.getElementById('activityFormMySelectOption');getActivityStat(e.options[e.selectedIndex].value);">
						<option value="7">7 jours</option>
						<option value="30" selected>30 jours</option>
						<option value="180">6 mois</option>
						<option value="365">1 an</option>
						<option value="730">2 ans</option>
						<option value="1825">5 ans</option>
					</select>						
				&nbsp;<img src="/images/help.gif" width=15 height=15 border=0 title="Main window" alt="Main window">
				</td></form></tr>
			</table>
			</div>
			<div id="mainWinInner" class="innerwindow" style="position: relative; top: 10px; width: 100%; height: 100%; overflow:auto; overflow-x: hidden"> 
				<table id="activityStatTable" class="statTable" width=100%>
					<tr> 
					<td align=center><img src='/images/transp.gif' width=300 height=1></td>
				  </tr><tr> 
					<td align=center><img src='/images/ajaxLoader.gif'> <p>Loading
					  <p>&nbsp;</td>
				  </tr>
				</table>
		  </div>
		</div>
		</td>
		
		</tr>
</table>

</div>

<div id="bottombar" class="bottombar">
&nbsp;
</div>

</body>
</html>
