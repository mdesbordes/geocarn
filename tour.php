<?php include 'header.php' ?>
<?php include 'inc/country.php' ?>

<script>
lang = "fr";
tourId = "<?= $_GET['tourId']; ?>";

var map;
var openskimap;
var trackIdArray = [];
var tourLat = 0;
var tourLon = 0;
var tourData;
var mapSize = "NORMAL";

google.load('visualization', '1', {packages: ['corechart', 'line']});

window.onresize = function(event) {
    resizeDiv();
};

function init() {
	resizeDiv();

	$.getJSON("toursWS.php?tour=<?= $_GET['tourId']; ?>" , function(data) {
		tourData = data.tracks;
		setTitle(data.tracks);
		drawTable(data.tracks);
		drawGraph("Average Speed");
		if (data.tracks[0].latitude != "") {
			tourLat = data.tracks[0].latitude;
			tourLon = data.tracks[0].longitude;
		}
		if (data.tracks[0].activityId > 0) {
			initMap();
			initTrackMap();
		}
		else {
			// viewerDiv
			document.getElementById("map").innerHTML = "No Map available for this sport category";
		}
	});
}

function resizeDiv() {
	var width = window.innerWidth
		|| document.documentElement.clientWidth
		|| document.body.clientWidth;

	var height = window.innerHeight
		|| document.documentElement.clientHeight
		|| document.body.clientHeight;
	heightShift = 320;
	document.getElementById("maiwindow").style.height = (height - heightShift) + "px";
	document.getElementById("mapWin").style.height = (height - heightShift) + "px";
	document.getElementById("tableWin").style.height = (height - heightShift) + "px";
	document.getElementById("graphWin").style.height =  "200px";
}

function maxMinMap() {
	var width = window.innerWidth
		|| document.documentElement.clientWidth
		|| document.body.clientWidth;
	width = width - 40;

	var height = window.innerHeight
		|| document.documentElement.clientHeight
		|| document.body.clientHeight;

	if (mapSize == "NORMAL") {
		document.getElementById("mapWin").style.width = (width - 40) + "px";
		document.getElementById("map").style.width = "100%";
		//document.getElementById("mapWin").style.height = (height - 80) + "px";
		//document.getElementById("map").style.height = (height - 80) + "px";
		document.getElementById("tableWin").style.width = "20px";
		google.maps.event.trigger(map, 'resize');
		mapSize = "MAX";
	}
	else {
		document.getElementById("mapWin").style.width =  (width - 15 - 602) + "px";
		document.getElementById("map").style.width = "100%";
		//document.getElementById("mapWin").style.height = (height - 340) + "px";
		//document.getElementById("map").style.height = (height - 340) + "px";
		document.getElementById("tableWin").style.width = "617px";
		google.maps.event.trigger(map, 'resize');
		mapSize = "NORMAL";
	}
}

function delTrack(trackId) {
	var result = confirm("Delete the track #" + trackId + "?");
	if (result==true) {
		$.getJSON("tracksWS.php?action=DEL&track=" + trackId , function(data) {
			logDebug(data);
			init();
		});
	}
}

function showField(span) {
	span.style.backgroundColor = "#6a8fbd";
	span.style.borderColor = "#6a8fbd";
}

function hideField(span) {
	span.style.backgroundColor = "#FFFFFF";
	span.style.borderColor = "#FFFFFF";
}

function updateField(fieldName, fieldValue) {
	var newValue = prompt("Enter new value for " + fieldName + " : ", fieldValue);
	if (newValue != null && newValue.length > 0)
		updateFieldWSCall(fieldName, newValue);
}

function updateFieldWSCall(fieldName, fieldValue) {
	$("#tracksTable").html("<tr><td align=center><img src='images/ajaxLoader.gif'> <p><p>&nbsp;</td></tr>");
	$.getJSON(encodeURI("toursWS.php?action=UPDATE&tour=" + tourId + "&fieldname=" + fieldName + "&fieldvalue=" + fieldValue + "&user=" + user) , function(data) {
		logDebug("tourWS -> "+data);
		init();
	});
}


function setTitle(data) {
	title = "<table width=100% border=0><tr valign=middle width=100%>";
	title += "<td width=50><a href='activity.php?activityId=" + data[0].activityId + "'><img src='/images/" + data[0].activityId + ".png' width=45 height=45 border=0></a>";
	title += "<a href=# onMouseOut=\"hideField(this);\" onMouseOver=\"showField(this);\"  ondblclick=\"updateField('ACTIVITY_ID', '" + data[0].activityId + "');\"><img src='/images/transp.gif' width=15 height=45 border=0></td>";
	title += "<td width=15>&nbsp;</td>";
	title += "<td class='title' valign=middle width=40%><a href=# onMouseOut=\"hideField(this);\" onMouseOver=\"showField(this);\"  ondblclick=\"updateField('NAME', '" + data[0].tourName + "');\">" + data[0].tourName + "</a></td>";
	title += "<td valign=middle align=right width=40%><a href=# onMouseOut=\"hideField(this);\" onMouseOver=\"showField(this);\"  ondblclick=\"updateField('COUNTRY', '" + data[0].country + "');\"><img src='images/flags/32/" + data[0].country.toLowerCase() + ".png' border=0></a></td>";
	title = title + "</td></tr></table>";
	$("#titleDiv").html(title);
	/*
	title = "<img src='/images/" + data[0].activityId + ".png' width=45 height=45>&nbsp;&nbsp;&nbsp;" + data[0].tourName;
	title = title + "<div class='toolbar' style='position: absolute; right: 0px; display: inline;'>";
	title = title + " &nbsp;&nbsp; <a href=# onClick='initGeoPortail()'>[GEO]</a>";
	title = title + " &nbsp;&nbsp; <a href=# onClick='initGoogleMaps()'>[GOOGLE]</a>";
	title = title + "</div>";
	$("#titleDiv").html(title);
	*/
	$("#subTitleDiv").html("");
}

function drawTable(data) {
	var c = [];
	c.push("<tr class='tracksTableHeader' valign=top><td width=60>Date</td><td width=150>Title</td><td width=60>Duration</td><td width=50>Av. speed<br>km/h</td><td width=50>Av. up speed<br>m/h</td><td width=50>Distance<br>km</td><td width=50>Alt. gain<br>m</td><td width=40></td><td width=20></td><td width=24></td><td width=20></td></tr>");
	c.push("<tr><td width=60></td><td width=150></td><td></td><td></td><td></td><td></td><td></td><td width=45><img src='/images/transp.gif' width=45 height=1></td><td></td><td></td><td></td></tr>");

	totalDistance = 0;
	totalAltDiff = 0;
	totalDuration = "";
	
	recordUpId = -1;
	if (data[0].activityId == 1 || data[0].activityId == 3 || data[0].activityId == 5 || data[0].activityId == 7 || data[0].activityId == 12 || data[0].activityId == 16)
		recordUpId = getPersonalRecord(data, "averageup");
	recordId = getPersonalRecord(data, "average");

	for (var i = 0; i < data.length; i++) {
		rowData = data[i];
		trackIdArray[i] = rowData.trackId;
		trackPointsLength = rowData.trackPoints;
		if (rowData.activityId > 0)
			tourLink = "<a href=track.php?tourId=" + rowData.tourId + "&trackId=" + rowData.trackId + ">";
		else
			tourLink = "";

		//totalDuration += parseFloat(rowData.trackDuration);
		trackAverageSpeed = Math.round(rowData.trackAverageSpeed * 100) / 100;
		if (trackAverageSpeed == 0) trackAverageSpeed = "";
		trackAverageUpSpeed = rowData.trackAverageUpSpeed;
		if (trackAverageUpSpeed == 0) trackAverageUpSpeed = "";
		trackDistance = rowData.trackDistance;
		totalDistance += parseFloat(trackDistance);
		if (trackDistance == 0) trackDistance = "";
		trackAltDiff = rowData.trackAltDiff;
		totalAltDiff += parseFloat(trackAltDiff);
		if (trackAltDiff == 0) trackAltDiff = "";
		if (rowData.planned == 0)
				trackDate = shortDate(rowData.trackDate, 10);
		else {
			trackDate = "Planned";
		}
		totalDistance = Math.round(totalDistance);
		c.push("<tr><td>" + tourLink + "<font color='" + colorTrackArray[i] + "'>" + trackDate + "</a></font></td><td with=100>" + tourLink + "" + rowData.trackName + "</a></td><td align=center>" + rowData.trackDuration + "</td><td align=center>" + trackAverageSpeed + "</td><td align=center>" + trackAverageUpSpeed + "</td><td align=center>" + trackDistance + "</td><td align=center>" + trackAltDiff + "</td>");
		c.push("<td align=center>");
		if (trackPointsLength > 30) {
			c.push("<a href='/gpx.php?tourId=" + rowData.tourId + "&trackId=" + rowData.trackId + "'><img src='/images/gpx.gif' width=20 border=0></a>");
			c.push("&nbsp;<a href='/kml.php?tourId=" + rowData.tourId + "&trackId=" + rowData.trackId + "'><img src='/images/googleearth.gif' width=20 border=0></a>");
		}
		c.push("</td>");
		c.push("<td>");
		if (recordId == i)
			c.push("<img src='/images/gold-medal.png' width=20 border=0>");
		if (recordUpId == i)
			c.push("<img src='/images/gold-medal-up.png' width=20 border=0>");
		c.push("</td>");
		c.push("<td>");
		if (rowData.transport == 1)
			c.push("<img src='/images/ecocar.png' width=24 border=0>");
		c.push("</td>");
		c.push("<td align=center><a href=# onClick='delTrack(" + rowData.trackId + ");return false;'><img src='/images/bin.png' width=18 height=18 border=0></a></td></tr>");
  }

	c.push("<tr><td width=60></td><td width=150></td><td width=60></td><td width=60></td><td width=60></td><td width=60></td><td width=60></td><td width=45><img src='/images/transp.gif' width=45 height=1></td><td width=20></td><td width=20></td><td></td></tr>");
	c.push("<tr class='rowEven' valign=top><td width=60><b>Total</b></td><td width=150></td><td width=60 align=center>" + totalDuration + "</td><td width=60></td><td width=60></td><td width=60 align=center><b>" + totalDistance + "</b></td><td width=60 align=center><b>" + totalAltDiff + "</b></td><td width=40></td><td width=20> </td><td width=24> </td><td width=20> </td></tr>");

	$("#tracksTable").html(c.join(""));
	//$("#tracksTable td").addClass("tracksTable");
	$("#tracksTable > tbody > tr:odd").addClass("rowOdd");
    $("#tracksTable > tbody > tr:not(.odd)").addClass("rowEven");
}

function getPersonalRecord(data, type) {
	logDebug("-> getPersonalRecord(..., " + type);
	record = 0;
	recordId = -1;
	for (var i = 0; i < data.length; i++) {
		rowData = data[i];
		trackIdArray[i] = rowData.trackId;
		trackPointsLength = rowData.trackPoints;
		
		trackDuration = rowData.trackDuration;
		//logDebug("trackAverageUpSpeed = " + rowData.trackAverageUpSpeed);
		//logDebug("trackAverageSpeed = " + rowData.trackAverageSpeed);
		if (type == "averageup")
			theSpeed = new Number(rowData.trackAverageUpSpeed);
		else
			theSpeed = new Number(rowData.trackAverageSpeed);
		//logDebug("theSpeed = " + theSpeed + " > record = " + record);
		if (theSpeed > record) {
			//logDebug("theSpeed > record !");
			record = theSpeed;
			recordId = i;
		}
		//logDebug("-> theSpeed = " + theSpeed + " > record = " + record);
	}
	return recordId;
}

function drawGraph(type) {
	logDebug("drawGraph(" + type);

	var gdata = new google.visualization.DataTable();
    gdata.addColumn('number', '');
    gdata.addColumn('number', type);
	dataIndex = 0;
	for (var i = (tourData.length - 1); i >= 0; i--) {
		rowData = tourData[i];
		trackIdArray[i] = rowData.trackId;

		trackAverageSpeed = Math.round(rowData.trackAverageSpeed * 100) / 100;
		if (trackAverageSpeed == 0) trackAverageSpeed = "";
		trackAverageUpSpeed = rowData.trackAverageUpSpeed;
		if (trackAverageUpSpeed == 0) trackAverageUpSpeed = "";
		trackDistance = rowData.trackDistance;
		if (trackDistance == 0) trackDistance = "";
		trackAltDiff = rowData.trackAltDiff;
		if (trackAltDiff == 0) trackAltDiff = "";
		trackDuration = rowData.trackDuration;

		if (type == "Average Speed")
			value = trackAverageSpeed;
		else if (type == "Duration")
			value = hmsToSecondsOnly(trackDuration) / 60;
		else if (type == "Distance")
			value = trackDistance;
		else if (type == "Altitude gain")
			value = trackAltDiff;
		else if (type == "Average up speed")
			value = trackAverageUpSpeed;

		console.log(i + ", " + trackAverageSpeed);
		if (value != "" && !isNaN(value)) {
			//alert();

			//console.log("-> " + dataIndex + ", " + value + ", " + new Number(value));
			gdata.addRow([dataIndex, parseFloat(value)]);
			dataIndex++;
		}
    }

      var options = {
        hAxis: {
          title: ''
        },
        vAxis: {
          title: type
        },
		curveType: 'function',
        backgroundColor: '#FFFFFF'
      };

      var chart = new google.visualization.LineChart(document.getElementById('graphWinInner'));
      chart.draw(gdata, options);
}


         <!--//--><![CDATA[//><!--
var iv= null;
var viewer=null;

// Definition url des services Geoportail
function geoportailLayer(name, key, layer, options)
{ var l= new google.maps.ImageMapType
  ({ getTileUrl: function (coord, zoom)
      {  return "https://wxs.ign.fr/" + key + "/geoportail/wmts?LAYER=" + layer
          + "&EXCEPTIONS=text/xml"
          + "&FORMAT="+(options.format?options.format:"image/jpeg")
          + "&SERVICE=WMTS&VERSION=1.0.0&REQUEST=GetTile"
          + "&STYLE="+(options.style?options.style:"normal")+"&TILEMATRIXSET=PM"
          + "&TILEMATRIX=" + zoom
          + "&TILECOL=" + coord.x + "&TILEROW=" + coord.y;
      },
    tileSize: new google.maps.Size(256,256),
    name: name,
    minZoom: (options.minZoom ? options.minZoom:0),
    maxZoom: (options.maxZoom ? options.maxZoom:18)
  });
  l.attribution = ' &copy; <a href="https://www.ign.fr/">IGN-France</a>';
  return l;
}
// Ajout de l'attribution Geoportail a la carte
function geoportailSetAttribution (map, attributionDiv)
{ if (map.mapTypes.get(map.getMapTypeId()) && map.mapTypes.get(map.getMapTypeId()).attribution)
  {  attributionDiv.style.display = 'block';
    attributionDiv.innerHTML = map.mapTypes.get(map.getMapTypeId()).name
      +map.mapTypes.get(map.getMapTypeId()).attribution;
  }
  else attributionDiv.style.display = 'none';
}
var map;
// Initialisation de la carte
function initMap()
{ // La carte Google
  map = new google.maps.Map( document.getElementById('map'),
  {  mapTypeId: google.maps.MapTypeId.TERRAIN,
    streetViewControl: false,
    mapTypeControlOptions: { mapTypeIds: ['carte', google.maps.MapTypeId.TERRAIN, google.maps.MapTypeId.SATELLITE, 'OSM', 'OTM', 'OCM', "Outdoors", 'Nautical SWE'], style:google.maps.MapTypeControlStyle.DROPDOWN_MENU },
    center: new google.maps.LatLng(tourLat, tourLon),
    zoom: 12
  });

  /** Definition des couches  */
  // Carte IGN
  map.mapTypes.set('carte', geoportailLayer("IGN", MA_CLE, "GEOGRAPHICALGRIDSYSTEMS.MAPS", { maxZoom:18 }));
  // Ajouter un control pour l'attribution
  var attributionDiv = document.createElement('div');
  attributionDiv.className = "attribution";
  geoportailSetAttribution(map, attributionDiv);
  map.controls[google.maps.ControlPosition.BOTTOM_RIGHT].push(attributionDiv);
  // Afficher / masquer le copyright en fonction de la couche
  google.maps.event.addListener(map, 'maptypeid_changed',
    function()
    {  geoportailSetAttribution(this, attributionDiv);
    });


  //Define OSM map type pointing at the OpenStreetMap tile server
	map.mapTypes.set("OSM", new google.maps.ImageMapType({
		getTileUrl: function(coord, zoom) {
			return "https://tile.openstreetmap.org/" + zoom + "/" + coord.x + "/" + coord.y + ".png";
		},
		tileSize: new google.maps.Size(256, 256),
		name: "OSM",
		maxZoom: 18
	}));

  //Define OTM map type pointing at the OpenStreetMap tile server
	map.mapTypes.set("OTM", new google.maps.ImageMapType({
		getTileUrl: function(coord, zoom) {
			return "https://a.tile.opentopomap.org/" + zoom + "/" + coord.x + "/" + coord.y + ".png";
		},
		tileSize: new google.maps.Size(256, 256),
		name: "OTM",
		maxZoom: 18
	}));

	//Define OCM map type pointing at the Open Cycle Map tile server
	map.mapTypes.set("OCM", new google.maps.ImageMapType({
		getTileUrl: function(coord, zoom) {
			return "http://tile.thunderforest.com/cycle/" + zoom + "/" + coord.x + "/" + coord.y + ".png?apikey=" + OCM_KEY;
		},
		tileSize: new google.maps.Size(256, 256),
		name: "OpenCycleMap",
		maxZoom: 18
	}));

  //Define Outdoors map type pointing at the OpenStreetMap tile server
	map.mapTypes.set("Outdoors", new google.maps.ImageMapType({
		getTileUrl: function(coord, zoom) {
			return "http://tile.thunderforest.com/outdoors/" + zoom + "/" + coord.x + "/" + coord.y + ".png?apikey=" + OCM_KEY;
		},
		tileSize: new google.maps.Size(256, 256),
		name: "Outdoors",
		maxZoom: 18
	}));
	
	// Eniro sjökort
	map.mapTypes.set("Nautical SWE", new google.maps.ImageMapType({
		getTileUrl: function(coord, zoom) {
			return "https://map.eniro.com/geowebcache/service/tms1.0.0/nautical/"+zoom+"/"+ coord.x +"/"+((1 << zoom) - 1 - coord.y) + ".png";
			//return "https://map.eniro.com/geowebcache/service/gmaps?layers=nautical&zoom=" + zoom + "&x=" + coord.x + "&y=" + coord.y + "&format=image/png";
		},
		tileSize: new google.maps.Size(256, 256),
		name: "Nautical SWE",
		maxZoom: 18
	}));


	//Define OSM map type pointing at the OpenSnowMap tile server
	openskimap = new google.maps.ImageMapType({
			getTileUrl: function(coord, zoom) {
			return "https://www.opensnowmap.org/opensnowmap-overlay/" + zoom + "/" + coord.x + "/" + coord.y + ".png";
		},
		tileSize: new google.maps.Size(256, 256),
		name: "OpenSkiMap",
		maxZoom: 18
		});

}
//google.maps.event.addDomListener(window, 'load', initMap);

function initTrackMap() {
	logDebug("-> initTrackMap()");
	var bikeLayer = new google.maps.BicyclingLayer();
	  bikeLayer.setMap(map);

	map.setCenter(new google.maps.LatLng(tourLat, tourLon));

	var bounds = new google.maps.LatLngBounds ();
	var trackIndex = 0;
	var arrayLen = trackIdArray.length;
	logDebug(arrayLen + " - " + trackIdArray);

	for (var i = 0; i < trackIdArray.length; i++) {
		//var ctaLayer = new google.maps.KmlLayer("http://geocarn-marcdesbordes.rhcloud.com/gpx.php?tourId=" + tourId + "&trackId=" + trackIdArray[i] + "&id=" + i);
		//ctaLayer.setMap(map);
		$.ajax({
		  type: "GET",
		  url: "/gpx.php?tourId=" + tourId + "&trackId=" + trackIdArray[i] + "&id=" + i,
		  dataType: "xml",
		  success: function(xml) {
			logDebug("keywords = " + $(xml).find("metadata").find("keywords").text());  
			var index = $(xml).find("metadata").find("keywords").text();
			  
			var points = [];
			var poly = [];

			$(xml).find("trkpt").each(function() {
			  var lat = $(this).attr("lat");
			  var lon = $(this).attr("lon");
			  var p = new google.maps.LatLng(lat, lon);
			  points.push(p);
			  bounds.extend(p);
			});

			colorStr = colorTrackArray[index];
			poly[trackIndex] = new google.maps.Polyline({
			  // use your own style here
			  path: points,
			  strokeColor: colorStr,
			  strokeOpacity: .8,
			  strokeWeight: 4,
			});

			poly[trackIndex].setMap(map);
			// fit bounds to track
			map.fitBounds(bounds);
			trackIndex++;
		  }
		});
	}
}

function initGeoPortail() {

		iv= Geoportal.load(
				 // div's ID:
				 'viewerDiv',
				 // API's keys:
				 ['rhfiamuq2cejl2h4xgcmwpge'],
				 {// map's center :
								// longitude:
								lon:45,
								// latitude:
								lat:6
				 },
				 //zoom level
				 15,
				 //options
				 {
					layers:['GEOGRAPHICALGRIDSYSTEMS.MAPS','ORTHOIMAGERY.ORTHOPHOTOS','CADASTRALPARCELS.PARCELS'],

					layersOptions:{'GEOGRAPHICALGRIDSYSTEMS.MAPS':{visibility:true,opacity:1,  minZoomLevel:1,maxZoomLevel:18},
								'ORTHOIMAGERY.ORTHOPHOTOS':{visibility:false,opacity:1, minZoomLevel:1, maxZoomLevel:18},
								'CADASTRALPARCELS.PARCELS':{visibility:false,opacity:1, minZoomLevel:12,maxZoomLevel:18}},
					onView: function() {
						viewer=this.getViewer();
						for (i = 0; i < trackIdArray.length; i++) {
							/* style de la trace */
							var styleTrace = new OpenLayers.StyleMap({
							"default": new OpenLayers.Style({

							strokeColor: colorTrackArray[i],
							strokeOpacity: 0.8,
							strokeWidth:4

							}),
							"select": new OpenLayers.Style({
							strokeColor: '#FF0000',
							})
							});

							/* ajout du fichier gpx   */
							gpxLayer = viewer.getMap().addLayer(
							"GPX",
							"trace",
							"/gpx.php?tourId=" + tourId + "&trackId=" + trackIdArray[i],{
								visibility: true,
								opacity:0.8,
								styleMap: styleTrace,
								eventListeners:{
									'loadend':function(){
										if (this.maxExtent) {
											this.map.zoomToExtent(this.maxExtent);
											this.setVisibility(true);
											}
										}
									}
								}
							);
						}
					}
			}
		);



};

function initGoogleMaps() {
	var mapOptions = {
	  center: { lat: 45, lng: 6},
	  zoom: 8,
	  mapTypeId: google.maps.MapTypeId.TERRAIN
	};
	var map = new google.maps.Map(document.getElementById('viewerDiv'),
		mapOptions);

	var bikeLayer = new google.maps.BicyclingLayer();
	  bikeLayer.setMap(map);

	logDebug("trackIdArray.length = " + trackIdArray.length);
	var bounds = new google.maps.LatLngBounds ();
	var trackIndex = 0;
	for (var i = 0; i < trackIdArray.length; i++) {
		//var ctaLayer = new google.maps.KmlLayer("http://geocarn-marcdesbordes.rhcloud.com/gpx.php?tourId=" + tourId + "&trackId=" + trackIdArray[i]);
		//ctaLayer.setMap(map);
		$.ajax({
		  type: "GET",
		  url: "/gpx.php?tourId=" + tourId + "&trackId=" + trackIdArray[i],
		  dataType: "xml",
		  success: function(xml) {
			var points = [];
			var poly = [];

			$(xml).find("trkpt").each(function() {
			  var lat = $(this).attr("lat");
			  var lon = $(this).attr("lon");
			  var p = new google.maps.LatLng(lat, lon);
			  points.push(p);
			  bounds.extend(p);
			});


				poly[trackIndex] = new google.maps.Polyline({
				  // use your own style here
				  path: points,
				  strokeColor: colorTrackArray[trackIndex],
				  strokeOpacity: .8,
				  strokeWeight: 4,
				});

			poly[trackIndex].setMap(map);
			// fit bounds to track
			map.fitBounds(bounds);
			trackIndex++;
		  }
		});
	}

}


function handleClickShowSlopes(checkBoxValue) {
	logDebug("-> handleClickShowSlopes(" + checkBoxValue);
	if (checkBoxValue) {
		logDebug("-> true - " + openskimap);
		map.overlayMapTypes.push(null); // create empty overlay entry
        map.overlayMapTypes.setAt("1",openskimap);
		//map.overlayMapTypes.insertAt(1, openskimap);
	}
	else {
		logDebug("-> false");
		map.overlayMapTypes.clear();
	}
}
                //--><!]]>

</script>

<body onLoad="init();">

<?php include 'bodyHeader.php' ?>

<div id="titleDiv" class="title" style="width: 100%, height: 45px; position: relative; top: 45px;"></div>
<div id="titleDivToolBar" class="toolbar" style="width: 100px; height: 45px; position: absolute; top: 45px; right: 0px;"></div>

<div id="maiwindow" class="maiwindow" style="position: relative; top: 45px; left: 0px; height: 60%">
<table width=100% height=100%>
	<tr height=70%>
		<td width=50% height=100%>
			<DIV id="mapWin" class="window" style="width: 100%; height: 100%; z-index : 3; overflow:hidden;">
				<div id="" class="windowtopbar" style="position: relative; top: 0px; left: 0px; height=15px; width: 100%">
				<table width=100% height=100% cellpadding=0 cellspacing=0 border=0><tr>
					<td class="windowtopbar" width=35%>Map</td>
					<td class="windowtopbar" width=1><img src="images/transp.gif" height=16 width=1></td>
					<form id="showSlopes"><td class="windowtopbar" width=65% align=right valign=middle>
					<input type=checkbox name="showSlopes" onClick="handleClickShowSlopes(this.checked);"> Show pistes
					<a href=# onClick="maxMinMap();"><img src="images/max-min-win.png" width=15 height=15 border=0 title="Main window" alt="Main window"></a>
					</td></form></tr>
				</table>
				</div>
				<div id="map" class="innerwindow" style="position: relative; top: 0px; width: 100%; height: 100%; overflow:hidden;">

				</div>
			</div>
		</td>

		<td width=5><img src="images/transp.gif" height=5 width=1></td>

		<td width=50% height=100%>
		<DIV id="tableWin" class="window" style="width: 100%; height: 100%; z-index : 3; overflow:hidden;">
			<div id="" class="windowtopbar" style="position: relative; top: 0px; left: 0px; height=15px; width: 100%">
			<table width=100% cellpadding=0 cellspacing=0 border=0><tr>
				<td class="windowtopbar" width=35%>Tracks</td>
				<td class="windowtopbar" width=1><img src="images/transp.gif" height=16 width=1></td>
				<td class="windowtopbar" width=65% align=right>
				<img src="images/help.gif" width=15 height=15 border=0 title="Main window" alt="Main window">
				</td></tr>
			</table>
			</div>
			<div id="mainWinInner" class="innerwindow" style="position: relative; top: 0px; width: 100%; height: 100%; overflow:scroll;">
				<table width=100% id="tracksTable" class="tracksTable">
				  <tr>
					<td align=center><img src='images/ajaxLoader.gif'> <p>
					  <p>&nbsp;</td>
				  </tr>
				</table>
			  </div>
			</div>
		</td>

	</tr>

	<tr height=30%>
		<td width=100% height=200 colspan=3>

			<DIV id="graphWin" class="window" style="width: 100%; height: 100%; z-index : 3; overflow:hidden;">
				<div id="" class="windowtopbar" style="position: relative; top: 0px; left: 0px; height=15px; width: 100%">
				<table width=100% cellpadding=0 cellspacing=0 border=0><tr>
					<td class="windowtopbar" width=35%>Graph</td>
					<td class="windowtopbar" width=1><img src="images/transp.gif" height=16 width=1></td>
					<form id="typeForm"><td class="windowtopbar" width=65% align=right valign=middle>
						<select id=typeFormMySelectOption onChange="e = document.getElementById('typeFormMySelectOption');drawGraph(e.options[e.selectedIndex].value);">
							<option value="Average Speed">Average Speed</option>
							<option value="Duration">Duration</option>
							<option value="Distance">Distance</option>
							<option value="Altitude gain">Altitude gain</option>
							<option value="Average up speed">Average up speed</option>
						</select>
					</td></form>
					<td class="windowtopbar" align=right width=15>
					<img src="images/help.gif" width=15 height=15 border=0 title="Main window" alt="Main window">
					</td></tr>
				</table>
				</div>
				<div id="graphWinInner" class="innerwindow" style="position: relative; top: 0px; width: 100%; height: 180; overflow:hidden;">
					<table width=100% class="tracksTable">
					  <tr>
						<td align=center><img src='images/ajaxLoader.gif'> <p>
						  <p>&nbsp;</td>
					  </tr>
					</table>
				</div>
			</div>

		</td>
	</tr>

</table>
</div>

<?php include 'bodyFooter.php' ?>

</body>
</html>
