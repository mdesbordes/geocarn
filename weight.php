<?php include 'header.php' ?>

<?php
$currentDate = $date = date('Y-m-d', time());
?>

<script>
lang = "fr";
user = "<?= $_SESSION['user'] ?>";

userWeight = 0;
userWeightDate = 0;
userHeight = 0;
userImc = 0;

window.onresize = function(event) {
    resizeDiv();
};

google.load('visualization', '1', {packages: ['corechart', 'line']});

function init() {
	resizeDiv();
	
	$("#titleDiv").html("");
	$("#subTitleDiv").html("");
	
	$.getJSON("weightWS.php?user=" + user , function(data) {
		drawTable(data.weights);
		drawGraph(data.weights);
	});
	
}

function resizeDiv() {
	var width = window.innerWidth
		|| document.documentElement.clientWidth
		|| document.body.clientWidth;

	var height = window.innerHeight
		|| document.documentElement.clientHeight
		|| document.body.clientHeight;
	heightShift = 100;
	heightTable = 300;
	document.getElementById("maiwindow").style.height = (height - heightShift) + "px";
	document.getElementById("mapWin").style.height = heightTable + "px";
	document.getElementById("tableWin").style.height = heightTable + "px";
	document.getElementById("graphWin").style.height =  (height - heightShift - heightTable) + "px";
}

function delWeight(rowId, weight) {
	var result = confirm("Delete the weight : " + weight + " Kg ?");
	if (result==true) {
		$.getJSON("weightWS.php?action=DEL&rowId=" + rowId + "&user=" + user , function(data) {
			logDebug(data);
			init();
		});
	}
}

function drawTable(data) {
	userHeight = data[0].height;
    
	drawHeader();
	drawInput();
	drawFooter();
	for (var i = 0; i < data.length; i++) {		
        drawRow(i, data[i]);
    }
	$("#weightListTable td").addClass("tracksTableIndex");
	$("#weightListTable > tbody > tr:odd").addClass("rowOdd");
    $("#weightListTable > tbody > tr:not(.odd)").addClass("rowEven"); 
}

function drawRow(rowId, rowData) {
	imc = Math.round(rowData.weight * 100 / (rowData.height * rowData.height)) / 100.0;
	if (imc <= 18.5)
		imccolor = "#66CCFF";
	if (imc > 18.5 && imc <= 25)
		imccolor = "#00CC00";
	else if (imc > 25 && imc <= 30)
		imccolor = "#FFFF00";
	else if (imc > 30 && imc <= 35)
		imccolor = "#FF9900";
	else if (imc > 35 && imc <= 40)
		imccolor = "#FF0000";
	else if (imc > 40)
		imccolor = "#990000";

	var row = $("<tr />")
    $("#weightListTable").append(row); //this will append tr element to table... keep its reference for a while since we will add cels into it
    row.append($("<td align=center>" + rowData.datetime.substr(0, 10) + "</td>"));
	row.append($("<td align=center>" + rowData.weight + " Kg</td>"));
	row.append($("<td align=center bgcolor=" + imccolor + ">" + imc + "</td>"));
	row.append($("<td align=center width=20><a href=# onClick='delWeight(" + rowData.id + ", " + rowData.weight + ");return false;'><img src='/images/bin.png' width=18 height=18></a></td>"));
	
	if (rowId == 0) {
		userWeight = rowData.weight;
		userWeightDate = rowData.datetime;
		userImc = imc;	
		var c = [];
		c.push("<tr /><td align=center><font size=+8>" + userWeight + "</font> Kg</td>");
		c.push("<tr /><td align=center>" + userWeightDate.substr(0, 10) + "</td>");
		c.push("<tr /><td align=center>&nbsp;</td>");
		c.push("<tr /><td align=center bgcolor=" + imccolor + "><font size=+2>IMC = " + imc + "</font></td>");
		c.push("<tr /><td align=center>&nbsp;</td>");
		$("#weightTable").html(c.join(""));
	}
}

function drawHeader() {
	$("#weightListTable").html("");
	var header = $('<tr/>');	
	$("#weightListTable").append(header);
	header.append($("<td class='indextab' align=center>Date</td>"));
	header.append($("<td class='indextab' align=center>Weight</td>"));	
	header.append($("<td class='indextab' align=center>IMC</td>"));
	header.append($("<td class='indextab' align=center></td>"));
}

function drawFooter() {
	$("#imcTable").html("");
	var header = $('<tr/>');	
	$("#imcTable").append(header);
	header.append($("<td align=center width=10%>&nbsp;</td>"));
	header.append($("<td align=center width=16%>" + Math.round(18.5 * 100 * (userHeight * userHeight)) / 100.0 + " Kg</td>"));
	header.append($("<td align=center width=16%>" + Math.round(25 * 100 * (userHeight * userHeight)) / 100.0 + " Kg</td>"));
	header.append($("<td align=center width=16%>" + Math.round(30 * 100 * (userHeight * userHeight)) / 100.0 + " Kg</td>"));
	header.append($("<td align=center width=16%>" + Math.round(35 * 100 * (userHeight * userHeight)) / 100.0 + " Kg</td>"));
	header.append($("<td align=center width=16%>" + Math.round(40 * 100 * (userHeight * userHeight)) / 100.0 + " Kg</td>"));
	header.append($("<td align=center width=10%>&nbsp;</td>"));
}

function drawInput() {
	var input = $('<tr/>');	
	$("#weightListTable").append(input);
	input.append($("<td align=center><input type=text name=\"weightdate\" size=10 value=\"<?= $currentDate ?>\"></td>"));
	input.append($("<td align=center><input type=text name=\"weight\" size=5> Kg</td>"));	
	input.append($("<td align=center><a href=# onClick=\"addWeight(document.forms['addWeightForm'].weightdate.value, document.forms['addWeightForm'].weight.value)\" class=\"formbutton\">&nbsp;&nbsp;&nbsp;&nbsp;Add&nbsp;&nbsp;&nbsp;&nbsp;</a>"));
}

function addWeightResult(txtResponse) {
	logDebug("-> addWeightResult(" + txtResponse);
	//hideLoader();
	
	//txtResponse = removeXML(txtResponse);
	logDebug("txtResponse = " + txtResponse);
	data = JSON.parse(txtResponse);
	logDebug("data = "  + data);
	
	init();
}

function addWeight(date, weight) {
	logDebug("-> addWeight(" + date + " - " + weight);
	str = "action=ADD&date=" + date + "&weight=" + weight + "&user=" + user; 
	url = "weightWS.php";
	
	$("#weightListTable").html("<table width=100% height=100%><tr><td height=100% valign=middle align=center><img src='images/ajaxLoader.gif'> <p><p> Adding weight for user : <br>" + weight + " Kg...</td></tr></table>");
	var ai = AJAXInteraction(url, str, addWeightResult);
	ai.doPost();
}

function drawGraph(tourData) {
	logDebug("drawGraph(" + tourData);

	var gdata = new google.visualization.DataTable();
    gdata.addColumn('date', '');
    gdata.addColumn('number', 'weight');
	gdata.addColumn('number', 'IMC 25');
	//gdata.addColumn('number', 'IMC 18.5');
	dataIndex = 0;

	lineIMC25 = Math.round(25 * 100 * (userHeight * userHeight)) / 100.0;
	lineIMC185 = Math.round(18.5 * 100 * (userHeight * userHeight)) / 100.0;

	for (var i = (tourData.length - 1); i >= 0; i--) {
		rowData = tourData[i];
		value = rowData.weight;
		datetime = rowData.datetime;
		
		console.log(i + ", " + value + ", " + datetime + ", " + Math.round(ymdToDay(datetime)));
		if (value != "" && !isNaN(value)) {
			//alert();
	
			gdata.addRow([new Date(shortDate(datetime, 10)), parseFloat(value), lineIMC25]);
			console.log("-> " + new Date(shortDate(datetime, 10)) + ", " + value);
			dataIndex++;
		}
    }

      var options = {
        hAxis: {
          title: '',
		  format: 'd/MM/yyyy'
        },
        vAxis: {
          title: 'Weight'
        },
		colors: ['#e2431e', '#00CC00', '#66CCFF'],
		series: {
            0: { lineWidth: 3 },
            1: { lineWidth: 1 },
            2: { lineWidth: 1 }
		},
		curveType: 'function',
        backgroundColor: '#FFFFFF'
      };

      var chart = new google.visualization.LineChart(document.getElementById('graphWinInner'));
      chart.draw(gdata, options);
}


</script>

<body onLoad="init();">

<?php include 'bodyHeader.php' ?>

<div id="titleDiv" class="title" style="width: 100%, height: 45px; position: relative; top: 45px;"></div>
<div id="titleDivToolBar" class="toolbar" style="width: 100px; height: 45px; position: absolute; top: 45px; right: 0px;"></div>

<div id="maiwindow" class="maiwindow" style="position: relative; top: 45px; left: 0px; height: 60%">
<table width=100% height=100%>
	<tr height=320>
		<td width=50%>
			<DIV id="mapWin" class="window" style="width: 100%; height: 320px; z-index : 3; overflow:hidden;">
				<div id="" class="windowtopbar" style="position: relative; top: 0px; left: 0px; height=15px; width: 100%">
				<table width=100% height=100% cellpadding=0 cellspacing=0 border=0><tr>
					<td class="windowtopbar" width=35%>Poids</td>
					<td class="windowtopbar" width=1><img src="images/transp.gif" height=16 width=1></td>
					<td class="windowtopbar" width=65% align=right>
					<a href=# onClick="document.getElementById('maiwindow').style.bottom='0px';document.getElementById('maiwindow').style.height='600px';"><img src="images/help.gif" width=15 height=15 border=0 title="Main window" alt="Main window"></a>
					</td></tr>
				</table>
				</div>
				<div id="weight" class="innerwindow" style="position: relative; top: 0px; width: 100%; height: 305px; overflow:hidden;"> 
					<table width=100% id="weightTable" class="tracksTableIndex">
					  <tr> 
						<td align=center><img src='images/ajaxLoader.gif'> <p>
						  <p>&nbsp;</td>
					  </tr>
					</table>
					<table width=100% class="tracksTableIndex">
					  <tr> 
						<td align=center width=10%>&nbsp;</td>
						<td align=center width=16%>18,5</td>
						<td align=center width=16%>25</td>
						<td align=center width=16%>30</td>
						<td align=center width=16%>35</td>
						<td align=center width=16%>40</td>
						<td align=center width=10%>&nbsp;</td>
					  </tr>
					</table>
					<table width=100% class="tracksTableIndex">
					  <tr> 
						<td align=center bgcolor="#66CCFF" width=15><img src='images/transp.gif' height=25 width=15></td>
						<td align=center bgcolor="#00CC00" width=15><img src='images/transp.gif' height=25 width=15></td>
						<td align=center bgcolor="#FFFF00" width=15><img src='images/transp.gif' height=25 width=15></td>
						<td align=center bgcolor="#FF9900" width=15><img src='images/transp.gif' height=25 width=15></td>
						<td align=center bgcolor="#FF0000" width=15><img src='images/transp.gif' height=25 width=15></td>
						<td align=center bgcolor="#990000" width=15><img src='images/transp.gif' height=25 width=15></td>
					  </tr>
					</table>
					<table width=100% id="imcTable" class="tracksTableIndex">
					  
					</table>
				</div>	
			</div>
		</td>
	
		<td width=5><img src="images/transp.gif" height=5 width=1></td>
	
		<td width=50%> 
		<DIV id="tableWin" class="window" style="width: 100%; height: 320px; z-index : 3; overflow:hidden;">
			<div id="" class="windowtopbar" style="position: relative; top: 0px; left: 0px; height=15px; width: 100%">
			<table width=100% cellpadding=0 cellspacing=0 border=0><tr>
				<td class="windowtopbar" width=35%>Tableau de poids</td>
				<td class="windowtopbar" width=1><img src="images/transp.gif" height=16 width=1></td>
				<td class="windowtopbar" width=65% align=right>
				<img src="images/help.gif" width=15 height=15 border=0 title="Main window" alt="Main window">
				</td></tr>
			</table>
			</div>
			<div id="mainWinInner" class="innerwindow" style="position: relative; top: 0px; width: 100%; height: 305px; overflow:scroll;"> 
				<form name="addWeightForm">
					<table width=100% id="weightListTable" class="tracksTableIndex">
					  <tr> 
						<td align=center><img src='images/ajaxLoader.gif'> <p>
						  <p>&nbsp;</td>
					  </tr>
					</table>
				</form>	
			  </div>
			</div>
		</td>
	</tr>
	
	<tr height=280>
		<td colspan=3 width=100%> 
		<DIV id="graphWin" class="window" style="width: 100%; height: 280px; z-index : 3; overflow:hidden;">
			<div id="" class="windowtopbar" style="position: relative; top: 0px; left: 0px; height=15px; width: 100%">
			<table width=100% cellpadding=0 cellspacing=0 border=0><tr>
				<td class="windowtopbar" width=35%>Courbe de poids</td>
				<td class="windowtopbar" width=1><img src="images/transp.gif" height=16 width=1></td>
				<td class="windowtopbar" width=65% align=right>
				<img src="images/help.gif" width=15 height=15 border=0 title="Main window" alt="Main window">
				</td></tr>
			</table>
			</div>
			<div id="graphWinInner" class="innerwindow" style="position: relative; top: 0px; width: 100%; height: 100%; overflow:hidden;"> 
					<table width=100% class="tracksTable">
					  <tr> 
						<td align=center><img src='images/ajaxLoader.gif'> <p>
						  <p>&nbsp;</td>
					  </tr>
					</table>
				</div>
			</div>
		</td>
	</tr>
</table>
</div>



<?php include 'bodyFooter.php' ?>

</body>
</html>
