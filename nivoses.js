nivoseIdArray = new Array();
nivoseNameArray = new Array();
mountainNameArray = new Array();
nivoseLatArray = new Array();
nivoseLonArray = new Array();
 
nivoseIdArray[i] = "SOUMCS";
nivoseNameArray[i] = "Aspe-Ossau (Soum Couy)";
mountainNameArray[i] = "ASPEOS";
nivoseLatArray[i] = "42.96";
nivoseLonArray[i] = "-0.72";
i++;

nivoseIdArray[i] = "DUPIN";
nivoseNameArray[i] = "Aigleton";
mountainNameArray[i] = "BELLEDONNE";
nivoseLatArray[i] = "45.23";
nivoseLonArray[i] = "6.04";
i++;

nivoseIdArray[i] = "STHIL";
nivoseNameArray[i] = "Saint Hilaire";
mountainNameArray[i] = "CHARTREUSE";
nivoseLatArray[i] = "45.31";
nivoseLonArray[i] = "5.86";
i++;

nivoseIdArray[i] = "COLPO";
nivoseNameArray[i] = "Col de Porte";
mountainNameArray[i] = "CHARTREUSE";
nivoseLatArray[i] = "45.295042";
nivoseLonArray[i] = "5.765517";
i++;

nivoseIdArray[i] = "LEGUA";
nivoseNameArray[i] = "Le Gua";
mountainNameArray[i] = "VERCORS";
nivoseLatArray[i] = "45.01";
nivoseLonArray[i] = "5.59";
i++;

nivoseIdArray[i] = "BELLE";
nivoseNameArray[i] = "Bellec�te";
mountainNameArray[i] = "VANOISE";
nivoseLatArray[i] = "45.49";
nivoseLonArray[i] = "6.77";
i++;

nivoseIdArray[i] = "RESTE";
nivoseNameArray[i] = "Restefond";
mountainNameArray[i] = "UBAYE";
nivoseLatArray[i] = "44.34";
nivoseLonArray[i] = "6.8";
i++;

