<?php include 'inc/logger.php' ?>
<?php

// Create the function, so you can use it
function isMobile() {
    return preg_match("/(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i", $_SERVER["HTTP_USER_AGENT"]);
}
// If the user is on a mobile device, redirect them
$redirectUrl = "http://" . $_SERVER['SERVER_NAME'] . "/mobile/";
if(isMobile())
    header("Location: $redirectUrl");

if(!isset($_SESSION)) 
{ 
    session_set_cookie_params(680400);
	session_start(); 
	$userSession = null;
} 
else {
	$userSession = $_SESSION['user'];
}
?>


<html lang="en">
<head>
  <META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <title>geocarn</title>

</head>

<link rel="shortcut icon" href="images/favicon.ico">

<link rel=stylesheet type=text/css href=common.css>
<link rel=stylesheet type=text/css href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel=stylesheet type=text/css href=jquery-ui.structure.css>
	  
	  
<script src=jquery-2.1.1.js LANGUAGE="JavaScript"></script>
<script src=jquery-ui.js LANGUAGE="JavaScript"></script>
<script src=common.js LANGUAGE="JavaScript"></script>
<script src=log.js LANGUAGE="JavaScript"></script>
<script src=ajax.js LANGUAGE="JavaScript"></script>
<script src=wind.js LANGUAGE="JavaScript"></script>
<script src=weather.js LANGUAGE="JavaScript"></script>
<script src=snow.js LANGUAGE="JavaScript"></script>
<script src=pass.js LANGUAGE="JavaScript"></script>

<!--script src="http://code.highcharts.com/highcharts.js"></script -->
<!-- script src="http://code.highcharts.com/modules/exporting.js"></script -->
<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<script type="text/javascript" src="https://api.ign.fr/geoportail/api/js/latest/Geoportal.js"></script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyARw_8RDJJPsqe-m_tD1n3Ie9tBpTx2kL0&libraries=drawing,visualization"></script>

<script>

logDebug("window.location.protocol = " + window.location.protocol);	

/*
if (window.location.protocol != "https:") {
	var url = window.location.href
	var arrUrl = url.split("/");
	logDebug("arrUrl = " + arrUrl);	
	logDebug("arrUrl = " + arrUrl.length);	
	var newUrl = "https://" + arrUrl[2] + "/" + arrUrl[3];
	logDebug("newUrl = " + newUrl);	
	this.location = newUrl;
}
*/

var MA_CLE= "p3155qxcnkn3hcfl6xm4j5k9";
var OCM_KEY = "a5a60b1040a445ceb9b45f9e2af0e73c";
var user = "<?= $userSession ?>";

var msgDivId = null;

sfHover = function() {
        var sfEls = document.getElementById("menu").getElementsByTagName("LI");
        for (var i=0; i<sfEls.length; i++) {
                sfEls[i].onmouseover=function() {
                        this.className+=" sfhover";
                }
                sfEls[i].onmouseout=function() {
                        this.className=this.className.replace(new RegExp(" sfhover\\b"), "");
                }
        }
}
if (window.attachEvent) window.attachEvent("onload", sfHover);

/*
function doLogin(txtResponse) {
	//hideLoader();
	logDebug("-> doLogin(...) - " + msgDivId);	
	if (msgDivId != null && msgDivId.length > 0)
		document.getElementById(msgDivId).innerHTML = "Login...";
	txtResponse = removeXML(txtResponse);
	var result = txtResponse.split('|');
	var code = result[0];
	msg = result[1];
	detail = result[2];
	str = "<b>" + msg + "</b><p>" + detail;		
	if (code != 0) {
		closeFunction = null;
		status = STATUS_ERROR;
	}
	else {
		closeFunction = null;
		status = STATUS_SUCCESS;
	}
	//showStatus(status, str, "loginWin", closeFunction);
	
	if (status == STATUS_SUCCESS) {
		document.getElementById(msgDivId).innerHTML = "<font color=green><b>Login successful. Redirecting to home page...</b></font>";
		window.location = "/";
	}
	else {
		document.getElementById(msgDivId).innerHTML = "<font color=red><b>Login error : " + detail + "</b></font>";
	}
}
*/

/*
function login(username, password, themsgDivId) {
	logDebug("-> login()");	
	//showLoader("Login...");
	if (msgDivId != null && msgDivId.length > 0)
		msgDivId = themsgDivId;
	else
		msgDivId = "loginMsgMain";
	logDebug("msgDivId = " + msgDivId);	
	var ai = AJAXInteraction("https://" + location.hostname + "/loginWs.php?username=" + username + "&password=" + password, null, doLogin);
	ai.doGet();
	
}
*/

function doLogin(username, password, themsgDivId) {
	logDebug("-> login()");	
	//showLoader("Login...");
	if (msgDivId != null && msgDivId.length > 0)
		msgDivId = themsgDivId;
	else
		msgDivId = "loginMsgMain";
	logDebug("msgDivId = " + msgDivId);	
	
	$.post("/loginWs.php",
        {
          username: username,
          password: password
        },
        function(txtResponse,status){
            //hideLoader();
			logDebug("-> doLogin(...) - " + msgDivId);	
			if (msgDivId != null && msgDivId.length > 0)
				document.getElementById(msgDivId).innerHTML = "Login...";
			txtResponse = removeXML(txtResponse);
			var result = txtResponse.split('|');
			var code = result[0];
			msg = result[1];
			detail = result[2];
			str = "<b>" + msg + "</b><p>" + detail;		
			if (code != 0) {
				closeFunction = null;
				status = STATUS_ERROR;
			}
			else {
				closeFunction = null;
				status = STATUS_SUCCESS;
			}
			//showStatus(status, str, "loginWin", closeFunction);
			
			if (status == STATUS_SUCCESS) {
				document.getElementById(msgDivId).innerHTML = "<font color=green><b>Login successful. Redirecting to home page...</b></font>";
				window.location = "/";
			}
			else {
				document.getElementById(msgDivId).innerHTML = "<font color=red><b>Login error : " + detail + "</b></font>";
			}
        });
}

function submitUploadGpx() {
	logDebug("-> submitUploadGpx()");
	document.getElementById('uploadTrackWin').innerHTML = "<table width=100% height=100%><tr><td align=center valign=middle><img src='images/ajaxLoader.gif' width=32 height=32 name=loading></td></tr><tr><td align=center class='data'>Uploading file...</td></tr></table>";
	logDebug("<- submitUploadGpx()");
}
</script>