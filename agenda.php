<?php include 'header.php' ?>

<script src='/moment.min.js'></script>
<link href='/fullcalendar.min.css' rel='stylesheet' />
<script src='/fullcalendar.min.js'></script>


<script>

	$(document).ready(function() {

		$('#calendar').fullCalendar({
			header: {
				left: 'prev,next today',
				center: 'title',
				right: 'month,basicWeek,basicDay'
			},
			weekMode: 'variable',
			firstDay: 0,
			editable: true,
			eventLimit: false, // allow "more" link when too many events
			events: [],

			dayClick: function(date, jsEvent, view) {

				console.log('Clicked on: ' + date.format());

				console.log('Coordinates: ' + jsEvent.pageX + ',' + jsEvent.pageY);

				console.log('Current view: ' + view.name);

				// change the day's background color just for fun
				$(this).css('background-color', 'red');

			}
		});

		$('#calendar').fullCalendar( 'today' );

		$.getJSON("tracksWS.php?user=" + user + "&track=latest&planned=false&limit=500" , function(data) {
			addEventsToAgenda(data.tracks);
		});




	});

	function addEventsToAgenda(data) {
		logDebug("-> addEventsToAgenda(" + data);
		var events=new Array();
		for (var i = 0; i < data.length; i++) {
			var rowData = data[i];
			event = new Object();
			if (rowData.trackName == "")
				trackName = rowData.trackDate.substring(0,10);
			else
				trackName = rowData.trackName;
			event.title = rowData.tourName + "\n" + trackName;
			event.description = rowData.trackName;

			startDate = new Date(rowData.trackDate.substring(0,4), rowData.trackDate.substring(5,7) - 1, rowData.trackDate.substring(8,10), 0, 0, 0, 0);
			event.start = rowData.trackDate.substring(0,10);    // its a date string

			logDebug(startDate + " - " + event.start + " - " + event.title);

			if (rowData.activityId == 0)
				event.color = "#e63a80";
			else if (rowData.activityId == 1)
				event.color = "#12abb5";
			else if (rowData.activityId == 2)
				event.color = "#ac5aeb";
			else if (rowData.activityId == 3)
				event.color = "#f86300";
			else if (rowData.activityId == 4)
				event.color = "#179abc";
			else if (rowData.activityId == 5)
				event.color = "#0088ec";
			else if (rowData.activityId == 6)
				event.color = "#00c1fa";
			else if (rowData.activityId == 7)
				event.color = "#7fbb10";
			else if (rowData.activityId == 8)
				event.color = "#00b4da";
			else if (rowData.activityId == 9)
				event.color = "#ad5beb";
			else if (rowData.activityId == 10)
				event.color = "#ff941c";
			else
				event.color = "gray";
			event.allDay = true;

			events.push(event);

		}

		$('#calendar').fullCalendar('addEventSource',events);

	}

	function init() {

	}

</script>
<style>

	body {
		margin: 40px 10px;
		padding: 0;
		font-family: "Lucida Grande",Helvetica,Arial,Verdana,sans-serif;
		font-size: 14px;
	}

	#calendar {
		max-width: 900px;
		margin: 0 auto;
	}

</style>
</head>

<body onLoad="init();">

	<?php include 'bodyHeader.php' ?>

	<div id='calendar' style="position: absolute; top: 55px; left: 0px; bottom: -20px; right: -10px; ">
	</div>

	<?php include 'bodyFooter.php' ?>

</body>
</html>
