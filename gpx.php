<?php include 'inc/connectDb.php' ?>
<?php include 'inc/logger.php' ?>
<?php

function normalizeString($str)
{
    $str = strip_tags($str); 
    $str = preg_replace('/[\r\n\t ]+/', ' ', $str);
    $str = preg_replace('/[\"\*\/\:\<\>\?\'\|]+/', ' ', $str);
    $str = strtolower($str);
    $str = html_entity_decode( $str, ENT_QUOTES, "utf-8" );
    $str = htmlentities($str, ENT_QUOTES, "utf-8");
    $str = preg_replace("/(&)([a-z])([a-z]+;)/i", '$2', $str);
    $str = str_replace(' ', '-', $str);
    $str = rawurlencode($str);
    $str = str_replace('%', '-', $str);
    return $str;
}

// --- Begin ---
if (isset($_GET['id']))
	$id = $_GET['id'];
else
	$id = 0;
$trackId = $_GET['trackId'];
$tourId = $_GET['tourId'];

$request = "select NAME from TRACKER_TOUR where ID = $tourId" ;
$result = mysqli_query($db, $request);
while ($row = mysqli_fetch_assoc($result)) {	
	//echo $row["URL"]." - ".$row["LATITUDE"]." - ".$row["LONGITUDE"]."<br>";
	$tourname = $row["NAME"];
}

$request = "select * from TRACKER_TRACK where ID = $trackId AND TOUR_ID = $tourId" ;
$result = mysqli_query($db, $request);
while ($row = mysqli_fetch_assoc($result)) {	
	//echo $row["URL"]." - ".$row["LATITUDE"]." - ".$row["LONGITUDE"]."<br>";
	$trackname = $row["NAME"];
	$datetime = $row["DATETIME"];
	$description = $row["DESCRIPTION"];
	$track = $row["TRACK"];
	// 45.1094;5.8767;1751;1247497|45.1095;5.8769;1751
}
//$track = "45.1094;5.8767;1247|45.1095;5.8769;1751|";

$trackExplode = explode('|', $track);
for ($i = 0; $i < (count($trackExplode)-1); $i++) {
	$point = $trackExplode[$i];
	$points[$i] = explode(';', $point);	
}

logDebug("tourname = " . $tourname);
logDebug("trackname = " . $trackname);

$name = $tourname . " | " . $trackname;
$name = utf8_encode($name);
if (is_numeric($points[0][3]))
	$filename = normalizeString($tourname) . "-" . date("Ymj", $points[0][3]);
else
	$filename = normalizeString($tourname);

logDebug("filename = " . $filename);

header('Content-type: text/xml; charset=UTF-8', true);
header("Content-disposition: attachment; filename=".$filename.".gpx");
header("Pragma: no-cache");
header("Cache-Control: no-cache, must-revalidate");
echo '<?xml version="1.0" encoding="UTF-8" standalone="no" ?>';
?>
<gpx version="1.1"
creator="geocarn - https://geocarn.freecluster.eu/"
xmlns="http://www.topografix.com/GPX/1/1"
xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
xsi:schemaLocation="http://www.topografix.com/GPX/1/1 http://www.topografix.com/GPX/1/1/gpx.xsd"> 

<metadata>
    <link href="<?= "https://" . $_SERVER['SERVER_NAME'] . "/track.php?tourId=". $tourId . "&amp;trackId=" . $trackId ?>">
      <text>geocarn</text>
    </link>
    <time><?= date(DATE_RFC3339, $points[0][3]) ?></time>
	<keywords><?= $id ?></keywords>
</metadata>
<trk>
	<name><?php echo $name ?></name>
	<trkseg>
<?php
for ($i = 0; $i < count($points); $i++) {
	$lat = $points[$i][0];
	$lon = $points[$i][1];
	$ele = $points[$i][2];
	$diff = $points[$i][3];
	if ($i == 0) {
		$startDateTime = $diff;
		$dateTime = $diff;
	}
	else {
		$dateTime = $startDateTime + $diff;
	}
?>
			<trkpt lat="<?= $lat ?>" lon="<?= $lon ?>">
				<?php 
				if ($ele != "N/A" && $ele > 0) {
				?>
				<ele><?= $ele ?></ele>
				<?php 
					}
				?>
				<time><?= date(DATE_RFC3339, $dateTime) ?></time>
			</trkpt>            
<?php 
}
?>
	</trkseg>
</trk>
</gpx>

<?php
exit;
?>