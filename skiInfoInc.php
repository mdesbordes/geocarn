<?php include 'inc/common.php' ?>
<?php
require 'inc/parser.php'; 
//require 'inc/urlLoader.php'; 
 
ini_set('display_errors','On');

// Contant definition
 
$REPORT['en'] = "<h1";
$REPORT['fr'] = "<span class=";
$REPORT['sv'] = "<h1";

$REPORT2['en'] = "Report";
$REPORT2['fr'] = "resort_name";
$REPORT2['sv'] = "rapport";

$CONDITIONS['en'] = "Snow conditions";
$CONDITIONS['fr'] = "Enneigement</h3>";
$CONDITIONS['sv'] = "Sn�f�rh�llanden";

$UPDATE['en'] = "Last";
$UPDATE['fr'] = "<span class=\"lastUpdated\">";
$UPDATE['sv'] = "Senast";

$UPDATE2['en'] = "updated: ";
$UPDATE2['fr'] = " jour:</span> <strong>";
$UPDATE2['sv'] = "uppdaterad: den ";

$LAST_SNOW['en'] = "<th>Last snowfall</th>";
$LAST_SNOW['fr'] = "<th>Dern. chute</th>";
$LAST_SNOW['sv'] = "<th>Senaste sn�fall</th>";

$TOP['en'] = "Middle";
$TOP['fr'] = "station summit";
$TOP['sv'] = "Mellan";

$MIDDLE['en'] = "Middle";
$MIDDLE['fr'] = "Milieu";
$MIDDLE['sv'] = "Mellan";

$BASE['en'] = "Base";
$BASE['fr'] = "station base";
$BASE['sv'] = "Dal";

$WEATHER['en'] = "<h2>Weather conditions";
$WEATHER['fr'] = "weather summit";
$WEATHER['sv'] = "<h2>V�derlek";

$TEMP['en'] = "<th>Temperature";
$TEMP['fr'] = "<th>Temp�rature";
$TEMP['sv'] = "<th>Temperatur";

$AVALANCHE['en'] = "<td class=\"bold\">Avalanche risk:</td> ";
$AVALANCHE['fr'] = "<td class=\"bold\">Risque d'avalanche:</td> ";
$AVALANCHE['sv'] = "<td class=\"bold\">Lavinfara:</td> ";

$AVALANCHE_NUM['en'] = " -";
$AVALANCHE_NUM['fr'] = "/";
$AVALANCHE_NUM['sv'] = " -";

$LAST_SNOW_TXT['en'] = "Last snowfall: ";
$LAST_SNOW_TXT['fr'] = "Derni�re chute: ";
$LAST_SNOW_TXT['sv'] = "Senaste sn�fall: ";

$snow = null;
$lang = null;
if (isset($_GET['snow']))
	$snow = $_GET['snow']; 
if (isset($_GET['lang']))
	$lang = $_GET['lang']; 

if ($snow != null) {
	/*
	$url = "http://www.skiinfo.fr/enneigement-bulletin-neige/";
	$url .= $id;
	$url .= "-" . $lang . ".jhtml";
	*/
	$url = "http://www.skiinfo.fr/";
	//http://www.skiinfo.fr/alpes-du-nord/alpe-dhuez/bulletin-neige.html
	$url .= $snow; // ex: alpes-du-nord/les-sept-laux
	$url .= "/bulletin-neige.html";
	
	$content=getUrlContent($url, null, 600); // 10 min cache	
	//$content = file_get_contents($url);
	
	if ($content != '') {
	$name = searchString($content, array ($REPORT[$lang], $REPORT2[$lang], ">"), "</span>");
	//echo "name = '" . $name . "'<br>";
	
	//echo "status = '" . $status . "'<br>";
	
	$update = searchString($content, array ($UPDATE[$lang]), "</span>");
	//echo "update = '" . $update . "'<br>";
		
	$snowTop = searchString($content, array ($CONDITIONS[$lang], "elevation upper", "bluePill", ">"), "</div>");	
	$eleTop = searchString($content, array ($TOP[$lang], "</strong> | "), "</div>");
	$stateTop= searchString($content, array ($CONDITIONS[$lang], "elevation upper", "bluePill", "surface", "<strong", ">"), "</strong>");
	$tempTop = searchString($content, array ("snow_conditions", "station summit", "temp below", ">"), "</div>");
	if (strlen($stateTop) > 20)
		$stateTopShort = substr($stateTop, 0, 17) . "...";
	else
		$stateTopShort = $stateTop;
	
	$snowMiddle = searchString($content, array ($CONDITIONS[$lang], "elevation middle", "bluePill", ">"), "</div>");	
	$eleMiddle = "";
	$stateMiddle = searchString($content, array ($CONDITIONS[$lang], "elevation middle", "bluePill", "surface", "<strong", ">"), "</strong>");
	$tempMiddle = "";
	if (strlen($stateMiddle) > 20)
		$stateMiddleShort = substr($stateMiddle, 0, 17) . "...";
	else
		$stateMiddleShort = $stateMiddle;
	
	$snowBottom = searchString($content, array ($CONDITIONS[$lang], "elevation lower", "bluePill", ">"), "</div>");	
	$eleBottom = searchString($content, array ($BASE[$lang], "</strong> | "), "</div>");
	$stateBottom = searchString($content, array ($CONDITIONS[$lang], "elevation lower", "bluePill", "surface", "<strong", ">"), "</strong>");
	$tempBottom = searchString($content, array ("snow_conditions", "station base", "temp above", ">"), "</div>");
	if (strlen($stateBottom) > 20)
		$stateBottomShort = substr($stateBottom, 0, 17) . "...";
	else
		$stateBottomShort = $stateBottom;
				       
	$avalancheNum = searchString($content, array ("avalanche_risk_level", ">"), " <span>");
	//$avalancheNum = searchString($avalanche, array (), $AVALANCHE_NUM[$lang]);
	
	if ($avalancheNum == '1' || $avalancheNum == '2')
		$avalancheIcon = '1-2';
	else if ($avalancheNum == '3' || $avalancheNum == '4')
		$avalancheIcon = '3-4';
	else  if ($avalancheNum == '5')
		$avalancheIcon = '5';
	else {
		$avalancheIcon = 'transp';
		$avalancheNum = 0;
	}
	
	if ($avalancheNum != null && $avalancheNum != '')	
		$avalancheStr = "<table cellpadding=0 cellspacing=0 border=0><tr><td valign=middle><b><font size=$avalancheNum>$avalancheNum</font></b></td><td valign=middle><img src='/images/snow/$avalancheIcon.png' title='$avalancheNum' alt='$avalancheNum' width=54 height=37></td><td valign=middle><a href='http://www.meteofrance.com/previsions-meteo-montagne/bulletin-avalanches/isere/avdept38'><img src='/images/snow/R$avalancheNum.png' title='$avalancheNum' alt='$avalancheNum' height=37 border=0></a></td></tr></table>";
	else
		$avalancheStr = "";
	
	?>
	
<!-- ------------------------------------------------------------------------->	
	
	<table width=100% border=0>
	<tr>
		<td colspan=2 alt='' title='<?php echo  ' Update: ' . $update . " " . $lang ?>'><a href='<?= $url ?>' target=_new><b><?php echo shortTextFormat($name, 15, "b") ?></b></a></td>
		<td valign=top align=right colspan=2><?php echo $avalancheStr ?></td>
	</tr>
	<tr align=right>
		<td><?php echo  $eleTop ?></td>
		<td align=right><b><?php echo $snowTop ?></b></td>
		<td align=right><?php echo $tempTop ?></td>
		<td align=left alt='<?php echo  $stateTop ?>' title='<?php echo  $stateTop ?>'><?php echo $stateTopShort ?></td>
	</tr>
	<?php
		if ($snowMiddle != '')  {
	?>
	<!--tr align=right><td><?php echo  $eleMiddle ?></td><td align=right><b><?php echo $snowMiddle ?></b></td><td align=right><?php echo $tempMiddle ?></td><td align=left alt='<?php echo  $stateMiddle ?>' title='<?php echo  $stateMiddle ?>'><?php echo $stateMiddleShort ?></td></tr-->
	<?php
		}
	?>
	<tr align=right><td><?php echo $eleBottom ?></td><td align=right><b><?php echo $snowBottom ?></b></td><td align=right><?php echo $tempBottom ?></td><td align=left alt='<?php echo  $stateBottom ?>' title='<?php echo  $stateBottom ?>'><?php echo $stateBottomShort ?></td></tr>
	<tr><td width=20><img src='/images/transp.gif' width=30 height=1></td><td width=30><img src='/images/transp.gif' width=30 height=1></td><td width=30><img src='/images/transp.gif' width=30 height=1></td><td width=50><img src='/images/transp.gif' width=50 height=1></td></tr>
	<tr><td colspan=4 align=right><i><?php echo  ' Update: ' . $update ?></i></td></tr>
	</table>
	
<?php 
	}
	else {
		echo "Unable to reach site: " . $url;
	}
} else {
?>
No ski resort defined!
<?php 
}
exit;
?>
<!-- ------------------------------------------------------------------------->	
