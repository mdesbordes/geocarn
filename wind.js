function initWindWin() {
	logDebug("-> initWindWin()");
	wind = "15";
	logDebug("wind = " + wind );
	var ai = AJAXInteraction("/balisesInc.php?wind=" + wind, null, setWind);
	ai.doGet();
}

function setWind(txtResponse) {
	logDebug("-> setWind(...)");
	document.getElementById("windWinInner").innerHTML = txtResponse;
}

function loadWindWin(wind) {
	//loaderDiv(WIN_WIND);
	document.getElementById("windWinInner").innerHTML = "<table width=100% height=100%><tr><td align=center><img src='/images/ajaxLoader.gif'><p>Loading<p>&nbsp;</td></tr></table>";
	logDebug("-> loadWindWin(" + wind);
	var ai = AJAXInteraction("/balisesInc.php?wind=" + wind, null, setWind);
	ai.doGet();
}
