<?php include 'inc/logger.php' ?>
<?php
function convertChar($str, $old, $new) {
	$removeChar = array($old);
	return str_replace($removeChar, $new, $str);
}

function convertLatLon($latlon) {
	// CONVIERTO LONGITUD A FORMATO E6
	$pieces = explode(".", $latlon);
	
	// echo "<br>LON:".$lon." ENT:".$pieces[0].", DEC: ".$pieces[1];  
	
	// AJUSTO A 6 DECIMALES LA PARTE DECIMAL
	$decimales ="";
	$size= strlen($pieces[1]);
	if ($size >= 6) $decimales = substr($pieces[1], 0, 6); 
	else {$decimales = $pieces[1]; for ($i=$size;$i<6;$i++) $decimales .= "0"; }
	
		// LA PARTE ENTERA LA AJUSTO A DOS DIGITOS
		$entero = "";
		$signo = "";
		$size= strlen($pieces[0]);
		$i=0;
		if ($pieces[0][0]=='-') { $signo = "-";  $i = 1; } 
		for ($j=$i;$j< $size;$j++) $entero .= $pieces[0][$j];
		if (strlen ($entero) == 1) $entero = "0".$entero; 	                   
	$latlon = trim($signo.$entero.$decimales);
	return $latlon;
}

// Get cityID
$lat = $_GET['lat'];
$lon = $_GET['lon'];
$timestamp = $_GET['timestamp'];
//,,,-19500000,146500000

$fileUrl = "http://api.openweathermap.org/data/2.5/history/city?lat=$lat&lon=$lon&type=hour&start=$timestamp&cnt=1";

//echo $fileUrl;
$xmldata = file_get_contents($fileUrl);
//$xmldata=getUrlContent($fileUrl, null, 600); 
logDebug($fileUrl);
$output = utf8_encode($xmldata);
header('Content-type: text/json; charset=UTF-8', true);
?><?= $output ?><?php exit; ?>