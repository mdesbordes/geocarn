<?php include 'header.php' ?>

<link rel="stylesheet" href="https://js.arcgis.com/4.9/esri/css/main.css">
<script src="https://js.arcgis.com/4.9/"></script>
<script>


var view = null;
require([
	"esri/Map",
    "esri/views/SceneView",
	"esri/layers/GraphicsLayer",
    "esri/Graphic"
    ], function(
      Map,
      SceneView, GraphicsLayer, Graphic
    ) {

  var map = new Map({
    basemap: "hybrid", // topo
    ground: "world-elevation"
  });
  
  view = new SceneView({
    container: "viewDiv",     // Reference to the scene div created in step 5
    map: map,                 // Reference to the map object created before the scene
    scale: 25000      // Sets the initial scale to 1:50,000,000
    //center: [45.2666498,5.944648]  // Sets the center point of view with lon/lat
  });
  
  
  $.getJSON("tracksWS.php?track=<?= $_GET['trackId']; ?>" , function(data) {
		tourId = data.tracks[0].tourId;
		tourLat = data.tracks[0].latitude;
		tourLon = data.tracks[0].longitude;
		categoryId = data.tracks[0].activityId;
		timestamp = new Number(data.tracks[0].trackTimestamp) + 43200;
		trackPointsLength = data.tracks[0].trackPoints;
		
		initTrackMap(categoryId);
		setTitle(data.tracks);
	});
	
	
	function initTrackMap(categoryId) {
		
		if (trackPointsLength > 0) {
			$.ajax({
			  type: "GET",
			  url: "/gpx.php?tourId=" + tourId + "&trackId=" + trackId,
			  dataType: "xml",
			  success: function(xml) {
				var path = [];
				var points = [];
				var index = 0;
				$(xml).find("trkpt").each(function() {
				  var lat = $(this).attr("lat");
				  var lon = $(this).attr("lon");
				  var ele = $(this).find("ele").text();
				  var point = [
					  lon,
					  lat
					];
				  logDebug("point = " + point);
				  path[index] = point;
				  
				  if (index == 0) {
					  initView(lat, lon);
					  showIcon(lat, lon, categoryId);
				  }
				  //bounds.extend(p);
				  index++;
				});
				logDebug("index = " + index);
				logDebug("path = " + path);
				polyline = {
				  type: "polyline", // autocasts as new Polyline()
				  hasZ: false,
				  hasM: true,
				  spatialReference: { wkid: 4326 },
				  paths: [
					path
				  ]
				};
				logDebug("polyline = " + polyline);
				
				lineSymbol = {
				  type: "simple-line", // autocasts as SimpleLineSymbol()
				  color: [226, 119, 40],
				  width: 4
				};

				var graphicsLayer = new GraphicsLayer();
				map.add(graphicsLayer);
				
				// Add the geometry and symbol to a new graphic
				var polylineGraphic = new Graphic({
				  geometry: polyline,
				  symbol: lineSymbol
				});

				graphicsLayer.add(polylineGraphic);			
				
			  }
			});
		}
	}   
	
	function initView(startLat, startLon) {
		view.goTo({
		  position: {
			x: startLon,
			y: startLat - 0.085,
			z: 4000
		  },
		  heading: 0,
		  tilt: 75
		}, {
		  speedFactor: 0.8,
		  easing: "linear"
		});				
	}
	
	function showIcon(startLat, startLon, categoryId) {
		view.graphics.add({
		  symbol: {
			 type: "picture-marker",
			 url: "http://geocarn-env.avzztenr5u.eu-west-3.elasticbeanstalk.com/images/" + categoryId + ".png",
			 width: "14px",
			 height: "14px"
			  },
		  geometry: {
			type: "point",
			 longitude: startLon,
			 latitude: startLat   
		  }
		});
	}
});

// http://geocarn-env.avzztenr5u.eu-west-3.elasticbeanstalk.com/kml.php?tourId=424&trackId=1465

lang = "fr";

var normalmap;
var bigmap;
var map;

var photoNum = 0;
var openskimap;
var trackId = <?= $_GET['trackId']; ?>;
var categoryId = -1;
var tourId = -1;
var tourLat;
var tourLon;
var timestamp;
var trackPointsLength = -1;
var mapSize = "NORMAL";

var trackNum = 0;

function init() {
	
}

function resizeDiv() {
	logDebug("-> resizeDiv()");
	var width = window.innerWidth
		|| document.documentElement.clientWidth
		|| document.body.clientWidth;

	var height = window.innerHeight
		|| document.documentElement.clientHeight
		|| document.body.clientHeight;

	logDebug("photoNum = "+photoNum);
	heightShift = 120;
	document.getElementById("mainwindow").style.height = (height - heightShift) + "px";
	document.getElementById("mainWinMap").style.height = (height - heightShift) + "px";
}

function setTitle(data) {
	title = "<table height=45><tr valign=middle>";
  title += "<td><a href='activity.php?activityId=" + data[0].activityId + "'><img src='/images/" + data[0].activityId + ".png' width=45 height=45 border=0></a></td>";
	title += "<td>&nbsp;</td>";
	title += "<td class='title' valign=middle><a href=/tour.php?tourId=" + data[0].tourId + ">" + data[0].tourName + "</a></td>";
	//$("#titleDiv").html(title);

	//title = "<img src='/images/" + data[0].activityId + ".png'>";
	if (data[0].trackName != "")
		title = title + "<td class='subtitle' valign=middle onMouseOut=\"hideField(this);\" onMouseOver=\"showField(this);\" ondblclick=\"updateField('NAME', '" + data[0].trackName + "');\">&nbsp;&nbsp;/&nbsp;&nbsp;" + data[0].trackName + "</td>";
	else
		title = title + "<td class='subtitle' valign=middle onMouseOut=\"hideField(this);\" onMouseOver=\"showField(this);\" ondblclick=\"updateField('NAME', '');\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";
	title = title + "</td></tr></table>";
	$("#titleDiv").html(title);

	titleToolbar = "<table height=45>";
	titleToolbar = titleToolbar + "<tr valign=middle>";
	if (data[0].transport == 1)
		titleToolbar = titleToolbar + "<td rowspan=2 valign=middle onMouseOut=\"hideField(this);\" onMouseOver=\"showField(this);\" ondblclick=\"updateField('TRANSPORT', '1');\"><img src=/images/ecocar.png width=24></td>";
	else 
		titleToolbar = titleToolbar + "<td rowspan=2 valign=middle onMouseOut=\"hideField(this);\" onMouseOver=\"showField(this);\" ondblclick=\"updateField('TRANSPORT', '0');\"><img src=/images/transp.gif width=24></td>";
	titleToolbar = titleToolbar + "<td class='titleDate' align=center onMouseOut=\"hideField(this);\" onMouseOver=\"showField(this);\" ondblclick=\"updateField('DATETIME', '" + data[0].trackDate + "');\">" + shortDate(data[0].trackDate, 10) + "</td>";
	titleToolbar = titleToolbar + "</tr>";
	if (trackPointsLength > 0) {
		titleToolbar = titleToolbar + "<tr valign=middle>";
		titleToolbar = titleToolbar + "<td align=center><a href='/gpx.php?tourId=" + tourId + "&trackId=" + trackId + "'><img src='/images/gpx.gif' width=20 border=0></a>";
		titleToolbar = titleToolbar + "<a href='/kml.php?tourId=" + tourId + "&trackId=" + trackId + "'><img src='/images/googleearth.gif' width=20 border=0></a></td>";
		titleToolbar = titleToolbar + "</td>";
		titleToolbar = titleToolbar + "</tr>";
	}
	titleToolbar = titleToolbar + "</table>";
	$("#titleDivToolBar").html(titleToolbar);
}

function initMap() {

	
}	




</script>

<body onLoad="init();" onResize="resizeDiv();">

<?php include 'bodyHeader.php' ?>

<div id="titleDiv" class="title" style="height: 45px; position: relative; left: 0px; right: 140px; top: 45px;"></div>
<div id="titleDivToolBar" class="toolbar" style="width: 140px; height: 45px; position: absolute; top: 45px; right: 0px;"></div>

<div id="mainwindow" class="maiwindow" style="position: relative; top: 45px; left: 0px; height: 400px">
<table width=100% height=100% border=0 id="mainTable">
	<tr height=100% id="trTop">
		<td width=100% height=100% valign=top>
			<DIV id="mainWinMap" class="window" style="width: 100%; height: 100%; z-index : 3; overflow:hidden;">
				<div id="" class="windowtopbar" style="position: relative; top: 0px; left: 0px; height=15px; width: 100%">
				<table width=100% cellpadding=0 cellspacing=0 border=0><tr>
					<td class="windowtopbar" width=35%>Map</td>
					<td class="windowtopbar" width=1 valign=middle><img src="/images/transp.gif" height=16 width=1></td>
					<form id="showSlopes"><td class="windowtopbar" width=65% align=right valign=middle>
					&nbsp;&nbsp;
					<a href="track.php?trackId=<?= $_GET['trackId']; ?>">2D Map</a>
					&nbsp;&nbsp;
					<a href=# onClick="maxMinMap();"><img src="images/max-min-win.png" width=15 height=15 border=0 title="Main window" alt="Main window"></a>
					</td></form></tr>
				</table>
				</div>
				<div id="viewDiv" class="innerwindow" style="position: relative; top: 0px; width: 100%; height: 100% overflow:scroll;">

				</div>
			</div>
		</td>		
	</tr>
	
</table>
</div>

<?php include 'bodyFooter.php' ?>

<!--div id="bigMap" style="height: 100%; width: 100%; position: absolute; left: 0px; top: 0px; z-index: -1;"></div-->

</body>

<script>
resizeDiv();
</script>

</html>
