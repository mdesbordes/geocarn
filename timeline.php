<?php include 'header.php' ?>

<script>
lang = "fr";

var markerArray = [];

var previousYear = -1;
var previousMonth = -1;

var jsonData = null;

function init() {
	$("#titleDiv").html("");
	$("#subTitleDiv").html("");
	
	setTitle();
	
	showList();
}

function showList() {
	$.getJSON("tracksWS.php?track=latest&limit=500" , function(data) {
		drawTable(data.tracks);
	});
}


function setTitle() {
	title = "";
	for (i = 1; i < 12; i++)
		title = title + "<img src='/images/" + i + ".png' width=45 height=45>&nbsp;&nbsp;";
	$("#titleDiv").html(title);
}

function drawTable(data) {
	document.getElementById("viewerDiv").innerHTML="<table width=100% id='tracksTable' class='tracksTableIndex'></table>";
	drawHeader();
    for (var i = 0; i < data.length; i++) {
        drawRow(i, data[i]);
    }
	$("#tracksTable td").addClass("tracksTableIndex");
	
}

function drawRow(rowId, rowData) {
	logDebug("rowData.trackDate.substring(0, 4) = '" + rowData.trackDate.substring(0, 4) + "' - '" + rowData.trackDate.substring(5, 7) + "'");
	if (rowData.trackDate.substring(0, 4) != previousYear) {
		previousYear = rowData.trackDate.substring(0, 4);
		var row = $("<tr />");
		$("#tracksTable").append(row);
		row.append($("<td colspan=10 align=center>" + rowData.trackDate.substring(0, 4) + "</td>"));
		
		
	}
	if (rowData.trackDate.substring(5, 7) != previousMonth) {
		previousMonth = rowData.trackDate.substring(5, 7);
		var row = $("<tr />");
		$("#tracksTable").append(row);
		row.append($("<td colspan=10 align=center>" + rowData.trackDate.substring(5, 7) + "</td></tr>"));
	}

	row = $("<tr />");
	$("#tracksTable").append(row);
    
	row.append($("<td><img src='/images/" + rowData.activityId + ".png'></td>"));
    row.append($("<td><a href='track.php?trackId=" + rowData.trackId + "'>" + shortDate(rowData.trackDate, 10) + "</a></td>"));
    row.append($("<td><a href='tour.php?tourId=" + rowData.tourId + "'>"  + rowData.tourName + "</a></td>"));
	if (rowData.trackName != "")
		trackName = rowData.trackName;
	else 
		trackName = shortDate(rowData.trackDate, 10);
	row.append($("<td><a href='track.php?trackId=" + rowData.trackId + "'>" + trackName + "</a></td>"));
	row.append($("<td>" + rowData.trackDuration + "</td>"));
	row.append($("<td>" + rowData.trackDistance + "</td>"));
	row.append($("<td>" + rowData.trackAltGain + "</td>"));
	
}

function drawHeader() {
	$("#tracksTable").html("");
	var header = $('<tr/>');	
	$("#tracksTable").append(header);
	header.append($("<td class='indextab'>activityId</td>"));
	header.append($("<td class='indextab'>trackDate</td>"));
	header.append($("<td class='indextab'>tourName</td>"));
	header.append($("<td class='indextab'>trackName</td>"));
	header.append($("<td class='indextab'>trackDuration</td>"));
	header.append($("<td class='indextab'>trackDistance</td>"));
	header.append($("<td class='indextab'>trackAltGain</td>"));
}

				
</script>

<body onLoad="init();">

<?php include 'bodyHeader.php' ?>

<div id="tabDiv" class="title" style="height: 45px; position: relative; top: 45px; left:0px; width: 240px;">
	
</div>
<div id="titleDiv" class="title" style="height: 45px; position: absolute; top: 52px; right: 0px; left: 280px;"></div>
<div id="titleDiv" class="toolbar" style="width: 100px; height: 45px; position: absolute; top: 52px; right: -5px;">

</div>

<div id="maiwindow" class="mainwindow" style="position: absolute; top: 100px; left: 0px; bottom: -20px; right: -10px; ">

			<DIV id="mainWin" class="window" style="position: absolute; left: 0px; top: 0px; bottom: -30px; right: -20px; z-index : 3; overflow:hidden;">
			<div id="" class="windowtopbar" style="position: relative; top: 0px; left: 0px; height=15px; width: 100%">
			<table width=100% cellpadding=0 cellspacing=0 border=0><tr>
				<td class="windowtopbar" width=35%>Main</td>
				<td class="windowtopbar" width=1><img src="images/transp.gif" height=16 width=1></td>
				<td class="windowtopbar" width=65% align=right>
				<img src="images/help.gif" width=15 height=15 border=0 title="Main window" alt="Main window">
				</td></tr>
			</table>
			</div>
			<div id="viewerDiv" class="innerwindow" style="position: relative; top: 20px; width: 100%; height: 100%; overflow:scroll;"> 
			<table width=100% height=100% id="tracksTable" class="tracksTableIndex">
			  <tr> 
				<td align=center><img src='images/ajaxLoader.gif'> <p>
				  <p>&nbsp;</td>
			  </tr>
			</table>
		  </div>
		</div>
		

</div>

<div id="bottombar" class="bottombar">
&nbsp;
</div>

<?php include 'bodyFooter.php' ?>

</body>
</html>
