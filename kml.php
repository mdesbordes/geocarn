<?php 
include 'inc/common.php';

$trackId = $_GET['trackId'];
$tourId = $_GET['tourId'];

function distance($lat1, $lon1, $lat2, $lon2, $unit) { 

  $theta = $lon1 - $lon2; 
  $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta)); 
  $dist = acos($dist); 
  $dist = rad2deg($dist); 
  $miles = $dist * 60 * 1.1515;
  $unit = strtoupper($unit);

  if ($unit == "K") {
    return ($miles * 1.609344); 
  } else if ($unit == "N") {
      return ($miles * 0.8684);
    } else {
        return $miles;
      }
}

function bearing($lat1, $lon1, $lat2, $lon2) {
return (rad2deg(atan2(sin(deg2rad($lon2) - deg2rad($lon1)) * cos(deg2rad($lat2)), cos(deg2rad($lat1)) * sin(deg2rad($lat2)) - sin(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($lon2) - deg2rad($lon1)))) + 360) % 360;
}

$sql = "select * from TRACKER_TOUR where ID = $tourId" ;
$result = mysql_query($sql, $db);
while ($row = mysql_fetch_array($result)) {	
	//echo $row["URL"]." - ".$row["LATITUDE"]." - ".$row["LONGITUDE"]."<br>";
	$tourname = $row["NAME"];
        $latOrg = $row["LATITUDE"];
        $lonOrg = $row["LONGITUDE"];
        $activity = $row["ACTIVITY_ID"];
        $activityName = $ACTIVITY[$activity];
		$user = $row["USER_ID"];
}

if ($trackId == null || $trackId == "")
	$sql = "select * from TRACKER_TRACK where TOUR_ID = $tourId ORDER BY DATETIME ASC" ;
else
	$sql = "select * from TRACKER_TRACK where ID = $trackId AND TOUR_ID = $tourId" ;
$result = mysql_query($sql, $db);
$index = 0;
while ($row = mysql_fetch_array($result)) {	
	//echo $row["URL"]." - ".$row["LATITUDE"]." - ".$row["LONGITUDE"]."<br>";
	$tracknames[$index] = $row["NAME"];
	$trackname = $row["NAME"];
	$datetime = $row["DATETIME"];
	$description = $row["DESCRIPTION"];
	$track = $row["TRACK"];
	// 45.1094;5.8767;1751;1247497|45.1095;5.8769;1751
	$trackExplode = explode('|', $track);
	for ($i = 0; $i < (count($trackExplode)-1); $i++) {
		$point = $trackExplode[$i];
		$points[$index][$i] = explode(';', $point);	
	}
        $index++;
}
$numLine = $index;

// Photos
if ($trackId == null || $trackId == "")
	$sql = "select * from TRACKER_IMAGE where TOUR_ID = $tourId ORDER BY TRACK_ID DESC" ;
else
	$sql = "select * from TRACKER_IMAGE where TOUR_ID = $tourId AND TRACK_ID = $trackId ORDER BY TRACK_ID DESC" ;
$result = mysql_query($sql, $db);

$urlArray = array();
$urlThumbArray = array();
$latArray = array();
$lonArray = array();
$widthArray = array();
$heightArray = array();
$index = 0;
if ($result != null) {
	while ($row = mysql_fetch_array($result)) {	
		//echo $row["URL"]." - ".$row["LATITUDE"]." - ".$row["LONGITUDE"]."<br>";
		$url = $row["URL"];
		$urlExplode = explode('/', $url);
		$urlBase = "";
		for ($i = 0; $i < (count($urlExplode)-1); $i++) {
			$urlBase .= $urlExplode[$i] . "/";
		}
		$imgName = $urlExplode[count($urlExplode)-1];
		//echo $urlBase . " - " . $urlExplode[count($urlExplode)-1] ."<br>";
		$urlArray[$index] = $urlBase . "s800/" . $imgName;
		$urlThumbArray[$index] = $urlBase . "s64-c/" . $imgName;
		
		$widthArray[$index] = $row["WIDTH"];
		$heightArray[$index] = $row["HEIGHT"];
		
		$latArray[$index] = $row["LATITUDE"];
		$lonArray[$index] = $row["LONGITUDE"];
		$index++;
	}
}

if ($trackId == null || $trackId == "")
	$name = $tourname;
else
	$name = $tourname . " | " . $trackname;

//header('Content-type: application/vnd.google-earth.kml+xml; charset=UTF-8', true);
//header("Content-disposition: attachment; filename=".urlencode($tourname).".kml");
//header("Pragma: no-cache");
//header("Cache-Control: no-cache, must-revalidate");

$kml = array('<?xml version="1.0" encoding="UTF-8"?>');
$kml[] = '<kml xmlns="http://www.opengis.net/kml/2.2">';
$kml[] = ' <Document>';

$kml[] = ' <name>' . $name . '</name>';
$kml[] = ' <description>';
$kml[] = ' <![CDATA[';
$kml[] = $description;
$kml[] = ' <p>';
$kml[] = ' See ' . $name . ' in <a href="' . $DOMAIN . '/track.php?tourId='. $tourId . '&trackId=' . $trackId . '">geocarn</a>';
$kml[] = ' ]]>';
$kml[] = ' </description>';
$kml[] = ' <Style id="start">';
$kml[] = '  <IconStyle>';
$kml[] = '     <scale>0.8</scale>';
$kml[] = '     <Icon>';
$kml[] = '        <href>' . $DOMAIN . '/images/' . $activity . '.png</href>';
$kml[] = '     </Icon>';
$kml[] = '  </IconStyle>';
$kml[] = ' </Style>';
$kml[] = ' ';
for ($index = 0; $index <  $numLine; $index++) {
	$kml[] = ' <Style id="line' . $index . '">';
	$kml[] = '   <LineStyle>';
	$kml[] = '     <color>FF' . substr($TRACK_COLOR[$index], 1) .'</color>';
	$kml[] = '     <width>2</width>';
	$kml[] = '   </LineStyle>';
	$kml[] = ' </Style>';
}
for ($index = 0; $index < count($urlArray); $index++) {
	$kml[] = ' <Style id="icon' . $index . '">';
	$kml[] = '   <scale>0.8</scale>';
	$kml[] = '   <Icon>';
	$kml[] = '     <href>' . $urlThumbArray[$index] .'</href>';
	$kml[] = '   </Icon>';
	$kml[] = ' </IconStyle>';
}

if ($numLine == 1) {
	$latExtreme = 0;
	$lonExtreme = 0;
	$maxDistance = 0;
	for ($i = 0; $i < count($points[0]); $i++) {
		$lat = $points[0][$i][0];
		$lon = $points[0][$i][1];
		$dist = distance($lat, $lon, $latOrg, $lonOrg, 'K');
		if ($dist > $maxDistance) {
			$maxDistance = $dist;
			$latExtreme = $lat;
			$lonExtreme = $lon;
		}
	}
	$bearing = bearing($latOrg, $lonOrg, $latExtreme, $lonExtreme);

    $kml[] = ' <Camera>';
    $kml[] = '   <longitude>' . $lonOrg . '</longitude>';
    $kml[] = '   <latitude>' . $latOrg . '</latitude>  ';
    $kml[] = '   <altitude>1000</altitude>  ';
    //$kml[] = '   <range>3500</range>   ';
    $kml[] = '   <tilt>60</tilt> ';
    $kml[] = '   <heading>' . $bearing . '</heading> ';
	$kml[] = '   <roll>0</roll>';
      
	if ($activity != 6) {
		$kml[] = '   <altitudeMode>relativeToGround</altitudeMode>';
	}
	else 
	{
		//$kml[] = '   <extrude>1</extrude>';
		//$kml[] = '   <tessellate>1</tessellate>';
		$kml[] = '   <altitudeMode>absolute</altitudeMode>';
	}
    $kml[] = ' </Camera> ';
}

$kml[] = ' <Folder> ';
$kml[] = '   <name>Tracks</name> ';
$kml[] = '   <visibility>0</visibility> ';

for ($index = 0; $index < $numLine; $index++) {
    $lat = $points[$index][0][0];
    $lon = $points[$index][0][1];
    $ele = $points[$index][0][2];
    if ($ele == 'N/A')
    	$ele = 0;
        if ($lat >= -90 && $lat <= 90) {
		$coordinates = " " . $lon . "," . $lat . "," . $ele . "\n";
	}
	if ($trackId == null || $trackId == "")
		$trackname = $tracknames[$index];

	$kml[] = ' <Placemark> ';
	$kml[] = '   <name>' . $trackname . '</name> ';
	$kml[] = '   <styleUrl>#start</styleUrl> ';
	$kml[] = '     <Point> ';
	$kml[] = '       <coordinates>' . $coordinates . '</coordinates> ';
	$kml[] = '     </Point> ';
	$kml[] = ' </Placemark> ';

	$kml[] = ' <Placemark> ';
	$kml[] = '   <name>' . $trackname . '</name> ';
	$kml[] = '   <description>' . $trackname . '</description> ';
	$kml[] = '   <styleUrl>#line' . $index . '</styleUrl> ';
	$kml[] = '   <LineString> ';
	$kml[] = '     <tessellate>0</tessellate> ';
	$kml[] = '       <coordinates> ';

	$coordinates = "";
	for ($i = 0; $i < count($points[$index]); $i++) {
		$lat = $points[$index][$i][0];
		$lon = $points[$index][$i][1];
		$ele = $points[$index][$i][2];
		if ($ele == 'N/A')
			$ele = 0;
		if ($lat >= -90 && $lat <= 90) {
	//		$coordinates .= " " . $lon . "," . $lat . ",0 \n";
			$coordinates .= " " . $lon . "," . $lat . "," . $ele . "\n";
		}
	}
	$kml[] = '       ' . $coordinates;

	$kml[] = '     </coordinates> ';
	$kml[] = '   </LineString> ';
	$kml[] = ' </Placemark> ';
}
  
$kml[] = ' </Folder> ';

$kml[] = ' <Folder> ';
$kml[] = '   <name>Photos</name> ';
$kml[] = '   <visibility>0</visibility> ';

for ($index = 0; $index < count($urlArray); $index++) {
	if ($widthArray[$index] > $heightArray[$index])
		$width = 320;
	else
		$width = 210;
		
	$kml[] = ' 	<Placemark> ';
	$kml[] = '       <name></name> ';
	$kml[] = '       <styleUrl>#icon' . $index . '</styleUrl> ';
	$kml[] = '       <description> ';
			  
	$kml[] = '         <![CDATA[ ';
	$kml[] = '         <table>';
	$kml[] = '         <tr width=100%>';
	$kml[] = '         	          <td><a href="' . $urlArray[$index] . '"><img src="' . $urlArray[$index] . '" width=$width border=0></a></td>';
	$kml[] = '         </tr>';
	$kml[] = '         <tr width=100%>';
	$kml[] = '         	          <td align=right><img src="' . $DOMAIN . '/images/geocarn-transp.png" width=71 height="22"></td>';
	$kml[] = '         </tr>';
	$kml[] = '         </table>';
	$kml[] = '         ]]>';
	$kml[] = '        </description>';
	$kml[] = '        <Point>';
	$kml[] = '          <coordinates>' . $lonArray[$index] . ',' . $latArray[$index] . '</coordinates>';
	$kml[] = '        </Point>';
	$kml[] = '        </Placemark>';
}

$kml[] = '</Folder>';
$kml[] = '</Document>';
$kml[] = '</kml>';
$kmlOutput = join("\n", $kml);
header('Content-type: application/vnd.google-earth.kml+xml');
header("Content-disposition: attachment; filename=".urlencode($tourname).".kml");
header("Pragma: no-cache");
header("Cache-Control: no-cache, must-revalidate");
echo $kmlOutput;exit;?>