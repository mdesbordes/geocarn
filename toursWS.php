<?php include 'inc/connectDb.php' ?>
<?php include 'inc/logger.php' ?>
<?php

function getLatestTours($db, $limit) {
	logDebug('-> getLatestTourstTour(' . $limit . ' -> Not yet implemented');
	return "";
}

function getTour($db, $tourId) {
	logDebug('-> getTour(' . $tourId);
	global $ACTIVITY;

	$request = "";
	$request = "SELECT TRACKER_TOUR.ACTIVITY_ID activityId, TRACKER_TOUR.COUNTRY country, TRACKER_TOUR.LATITUDE latitude, TRACKER_TOUR.LONGITUDE longitude, TRACKER_TRACK.DATETIME trackDate, TRACKER_TRACK.ID trackId, TRACKER_TRACK.TOUR_ID tourId, TRACKER_TOUR.NAME tourName, TRACKER_TRACK.NAME trackName, TRACKER_TRACK.DURATION trackDuration, TRACKER_TRACK.DISTANCE trackDistance, TRACKER_TRACK.AVERAGE_SPEED trackAverageSpeed, TRACKER_TRACK.AVERAGE_UP_SPEED trackAverageUpSpeed, TRACKER_TRACK.ALT_GAIN trackAltGain, TRACKER_TRACK.ALT_DIFF trackAltDiff, TRACKER_TRACK.ALT_MAX trackAltMax, TRACKER_TRACK.ALT_MIN trackAltMin, TRACKER_TRACK.ALT_START trackAltStart, TRACKER_TRACK.PLANNED_ONLY planned, TRACKER_TRACK.TRANSPORT transport, OCTET_LENGTH(TRIM(TRACKER_TRACK.TRACK)) trackPoints FROM TRACKER_TRACK join TRACKER_TOUR on TRACKER_TRACK.TOUR_ID=TRACKER_TOUR.ID AND TRACKER_TOUR.ID = " . $tourId . " ORDER BY TRACKER_TRACK.DATETIME DESC";
	logDebug($request);
	mysqli_set_charset($db, "SET NAMES UTF8");
	$result = mysqli_query($db, $request);

	$rows = array();
	while($r = mysqli_fetch_assoc($result)) {
		$rows['tracks'][] = $r;
	}

	return json_encode($rows);
}

function getToursForCategory($db, $limit, $categoryId, $user) {
	logDebug('-> getToursForCategory(' . $user);
	global $ACTIVITY;

	$request = "";
	$request = "SELECT TRACKER_TOUR.ID tourId, TRACKER_TOUR.NAME tourName FROM TRACKER_TOUR where TRACKER_TOUR.ACTIVITY_ID = " . $categoryId . " AND TRACKER_TOUR.USER_ID = '" . $user . "' order by TRACKER_TOUR.NAME ASC LIMIT " . $limit;;
	logDebug($request);
	mysqli_set_charset($db, "SET NAMES UTF8");
	$result = mysqli_query($db, $request);

	$rows = array();
	while($r = mysqli_fetch_assoc($result)) {
		$rows['tours'][] = $r;
	}

	return json_encode($rows);
}

function updateTour($db, $tourId, $fieldName, $fieldValue, $user) {
	logDebug('-> updateTour(' . $tourId .", ". $fieldName .", ". $fieldValue .", ". $user);
	logDebug('USERNAME = ' . $_SESSION['user']);

	$fieldValue = mysqli_real_escape_string($db, $fieldValue);

	if ($tourId > 0 && $_SESSION['user'] == $user) {
		$request = "UPDATE TRACKER_TOUR SET $fieldName = '$fieldValue' WHERE ID = " . $tourId;

		logDebug($request);
		mysqli_set_charset($db, "SET NAMES UTF8");
		$result = mysqli_query($db, $request);
		
		$rows = array();
		/*
		while($r = mysql_fetch_assoc($result)) {
			$rows['tours'][] = $r;
		}
		*/
		return json_encode($rows);
	} else {
		return 0;
	}
}

function deleteTour($db, $tourId, $user) {
	logDebug('-> deleteTour(' . $tourId .", ". $user);
	global $ACTIVITY;

	$request = "";
	$request = "DELETE from TRACKER_TOUR where ID=" . $tourId . " AND USER_ID= '$user'";
	logDebug($request);
	mysqli_set_charset($db, "SET NAMES UTF8");
	$result = mysqli_query($db, $request);

	$rows = array();
	return json_encode($rows);
}

// --- Begin ---

logDebug("-> toursWS.php");

if (isset($_GET['user']))
	$user = $_GET['user'];
else
	$user = null;
if (isset($_GET['action']))
	$action = $_GET['action'];
else 
	$action = null;
if (isset($_GET['tour']))
	$tourReq = $_GET['tour'];
else
	$tourReq = null;
if (isset($_GET['planned']))
	$planned = $_GET['planned'];
else
	$planned = null;
if (isset($_GET['activity']))
	$categoryId = $_GET['activity'];
else
	$categoryId = null;
if (isset($_GET['limit']))
	$limit = $_GET['limit'];
else
	$limit = 50;
if (isset($_GET['fieldname']) && isset($_GET['fieldvalue'])) {
	$fieldName = $_GET['fieldname'];
	$fieldValue = $_GET['fieldvalue'];
}

logDebug("tourReq = $tourReq");

$user = $_SESSION['user'];

logDebug("user = $user");

if ($action == "DEL")
	$toursArray = deleteTour($db, $tourReq, $user);
else if ($action == "UPDATE")
	$toursArray = updateTour($db, $tourReq, $fieldName, $fieldValue, $user);
else if ($tourReq == "latest")
	$toursArray = getLatestTours($db, 50);
else if ($categoryId != null && $categoryId != "" && $categoryId >= 0)
	$toursArray = getToursForCategory($db, $limit, $categoryId, $user);
else
	$toursArray = getTour($db, $tourReq);
logDebug($toursArray);

header('Content-type: text/xml;  application/json', true);
?><?= $toursArray ?><?php exit; ?>