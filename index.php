<?php include 'header.php' ?>

<script>
lang = "fr";
thenumDays = 0;
var currentSeasonStatData = null;
var PROGRESS_BAR_WIDTH = 480;
var MAX_PROGRESS_BAR_WIDTH = 600;

function init() {
	$("#titleDiv").html("");
	$("#subTitleDiv").html("");
	
	imageNum = Math.round(Math.random() * 9);
	imageUrl = "/images/photos/index" + imageNum + ".jpg";
	logDebug("imageUrl = " + imageUrl);
	document.getElementById("mainIndexTable").style.backgroundImage="url('" + imageUrl + "')";

	getLatestTracks(14);
	getActivityStat(30);
	var currentDate = new Date();
	getSeasonStat(currentDate.getFullYear(), 1+currentDate.getMonth());

}

function initExt() {
	loadWindWin(15);
	loadWeatherFromID("France/Rhône-Alpes/Les_Adrets");
	loadSnowWin("alpes-du-nord/les-7-laux");
	loadPassWin();
}


function getTimeStat() {
	$.getJSON("activityStatWS.php?user=" + user + "&action=time" , function(data) {
		showTimeStat(data.activities);
	});
}

function getActivityStat(numDays) {
	logDebug("-> getActivityStat("+numDays);
	
	$("#activityStatTable").html("<tr /><td colspan=6><td align=center><img src='images/ajaxLoader.gif'> <p>Loading<p>&nbsp;</td></td>");
	
	if (numDays > 0)
		thenumDays = numDays;
	$.getJSON("activityStatWS.php?user=" + user + "&days=" + thenumDays , function(data) {
		showActivityStat(data.activities);
	});
}

function getLatestTracks(numTracks) {
	logDebug("-> getLatestTracks("+numTracks);
	
	$("#latestTracksTable").html("<tr /><td colspan=6><td align=center><img src='images/ajaxLoader.gif'> <p>Loading<p>&nbsp;</td></td>");
	
	$.getJSON("tracksWS.php?user=" + user + "&track=latest&planned=false&limit=" + numTracks , function(data) {
		drawTable(data.tracks);
	});
}

function getSeasonStat(year, month) {
	logDebug("-> getSeasonStat("+year + "," + month);
	
	$("#seasonStatTable").html("<tr /><td colspan=6><td align=center><img src='images/ajaxLoader.gif'> <p>Loading<p>&nbsp;</td></td>");
	
	$.getJSON("activityStatWS.php?user=" + user + "&action=season&year=" + year + "&month=" + month , function(data) {
		logDebug("activityStatWS -> "+data);
		document.getElementById("seasonStatTitle").innerHTML = "Season " + year;
		currentSeasonStatData = data.activities;
		drawSeasonTable(data.activities, year, month);
		
		initExt();
	});
}

function drawSeasonTable(activitydata, year, month) {
	logDebug("-> drawSeasonTable("+activitydata);
	var currentDate = new Date();
	//if (currentDate.getFullYear == year) {

	//}

	$("#seasonStatTable").html("");
	//activitydata.sort(function(a,b) { return parseInt(b.activitySum) - parseInt(a.activitySum) } );
	activitydata.sort(function(a,b) { return parseInt(b.summin) - parseInt(a.summin) } );

    for (var i = 0; i < activitydata.length; i++) {
		if (activitydata[i].activityId >= 0)
			drawSeasonRow(i, activitydata[i]);
    }
	var row = $("<tr />")
    $("#seasonStatTable").append(row);

	var currentDate = new Date();
	if (month >= 8)
		year = 1 + year;
	row.append($("<td colspan=4 height=20 valign=middle>&nbsp;* : For winter sports, the season is : " + (year-1) + " - " + year + "</td>"));
	//$("#latestTracksTable td").addClass("tracksTableIndex");
	$("#seasonStatTable > tbody > tr:odd").addClass("rowOdd");
    $("#seasonStatTable > tbody > tr:not(.odd)").addClass("rowEven");

	getTimeStat();
}

function drawSeasonRow(rowId, rowData) {
	var row = $("<tr />")
    $("#seasonStatTable").append(row); //this will append tr element to table... keep its reference for a while since we will add cels into it

	row.append($("<td width=60 align=center><a href='activity.php?activityId=" + rowData.activityId + "'><img src='images/" + rowData.activityId + ".png' width=60 height=60></a></td>"));
	if (rowData.activityId == 0)
		row.append($("<td width=100 class='dataBig' colspan=2>" + secondsToHours(rowData.summin *60) + "<div class='unit'> h</div></td>"));
	else if (rowData.activityId == 1)
		row.append($("<td width=100 class='dataBig' colspan=2>" + rowData.activityAltDiff + "<div class='unit'> m</div></td>"));
	else if (rowData.activityId == 4)
		row.append($("<td width=200 class='dataBig' colspan=2>" + Math.round(rowData.activityDist / 1.852) + "<div class='unit'> nm</div></td>"));
	else if (rowData.activityId == 7 || rowData.activityId == 3) {
		row.append($("<td width=100 class='dataBig'>" + Math.round(rowData.activityDist) + "<div class='unit'> km</div></td>"));
		row.append($("<td width=100 class='data'>" + rowData.activityAltDiff + "<div class='unit'> m</div></td>"));
	}
	else
		row.append($("<td width=100 class='dataBig' colspan=2>" + Math.round(rowData.activityDist) + "<div class='unit'> km</div></td>"));

	timeSpent = secondsToHms(rowData.summin *60);
	/*
	if (timeSpent < 60)
		timeSpent = timeSpent + " min";
	else
		timeSpent = Math.round(timeSpent / 60) + " h";
		*/
	row.append($("<td width=60 align=center>" + rowData.activitySum + "<br>" + timeSpent + "</td>"));


}

function drawTable(data) {
	$("#latestTracksTable").html("");
    for (var i = 0; i < data.length; i++) {
        drawRow(i, data[i]);
    }
	//$("#latestTracksTable td").addClass("tracksTableIndex");
	$("#latestTracksTable > tbody > tr:odd").addClass("rowOdd");
    $("#latestTracksTable > tbody > tr:not(.odd)").addClass("rowEven");
}

function drawRow(rowId, rowData) {
	var row = $("<tr />")
    $("#latestTracksTable").append(row); //this will append tr element to table... keep its reference for a while since we will add cels into it
    if (rowData.trackName != "")
		tourName = rowData.tourName + " / " + rowData.trackName;
	else
		tourName = rowData.tourName;
	row.append($("<td width=30 align=center><img src='images/" + rowData.activityId + ".png' width=30 height=30 alt='" + shortDate(rowData.trackDate, 10) + "' title='" + shortDate(rowData.trackDate, 10) + "'></td>"));
    if (rowData.activityId > 0)
		row.append($("<td width=150><a href='track.php?trackId=" + rowData.trackId + "'>"  + shortText(tourName, 50, '') + "</a></td>"));
	else
		row.append($("<td width=150>"  + shortText(tourName, 50, '') + "</td>"));

	if (rowData.activityId == 1)
		row.append($("<td width=80 align=center><b>" + rowData.trackAltGain + "</b> m" + "<br>" + rowData.trackDuration + "</td>"));
	else if (rowData.activityId == 4)
		row.append($("<td width=80 align=center><b>" + Math.round(rowData.trackDistance / 1.852) + "</b> nm" + "<br>" + rowData.trackDuration + "</td>"));
	else if (rowData.activityId == 7 || rowData.activityId == 3) {
		row.append($("<td width=80 align=center><b>" + Math.round(rowData.trackDistance) + "</b> km&nbsp;&nbsp;&nbsp;<b>" + rowData.trackAltGain + "</b> m" + "<br>" + rowData.trackDuration + "</td>"));
	}
	else
		row.append($("<td width=80 align=center><b>" + Math.round(rowData.trackDistance) + "</b> km" + "<br>" + rowData.trackDuration + "</td>"));

	//row.append($("<td width=80 align=center><b>" + rowData.trackDistance + "</b> km&nbsp;&nbsp;&nbsp;<b>" + rowData.trackAltGain + "</b> m" + "<br>" + rowData.trackDuration + "</td>"));
	//row.append($("<td width=60 align=right>" + rowData.trackDistance + " <div class='smallunit'>km&nbsp;</div></td>"));
	//row.append($("<td width=60 align=right>" + rowData.trackAltGain + " <div class='smallunit'>m&nbsp;</div></td>"));
	timeDiff = Math.round((new Date().getTime() - new Date(rowData.trackDate.substring(0,10)).getTime()) / (1000 * 3600 * 24));
	row.append($("<td width=30 align=center>" + timeDiff + " j</td>"));
}

function drawHeader() {
	$("#latestTracksTable").html("");
	var header = $('<tr/>');
	$("#latestTracksTable").append(header);
	header.append($('<tr/>'));
	header.append($("<td class='indextab'>activityId</td>"));
	header.append($("<td class='indextab'>trackDate</td>"));
	header.append($("<td class='indextab'>tourName</td>"));
	header.append($("<td class='indextab'>trackDuration</td>"));
	header.append($("<td class='indextab'>trackDistance</td>"));
	header.append($("<td class='indextab'>trackAltGain</td>"));
}

function showTimeStat(data) {
	logDebug("-> showTimeStat(" + data);
	var GOAL = 150;
	var activityData = [];
	var sumtime = data[0].sumtime;
	console.log("sumtime = " + sumtime);
	console.log("sumtime = " + (sumtime == "null"));
	console.log("sumtime = " + (sumtime == "NULL"));
	console.log("sumtime = " + (sumtime == null));
	if (sumtime == "null" || sumtime == "NULL" || sumtime == null)
		sumtime = 0;
	var summin = data[0].summin;
	if (summin == "null" || summin == "NULL" || !(summin > 0))
		summin = 0;
	if (summin > GOAL) {
		color = "green";
	}
	else {
		color = "blue";
	}
	progress = "progress-" + color;

	progressWidth = Math.round(summin * PROGRESS_BAR_WIDTH) / GOAL;
	if (progressWidth > MAX_PROGRESS_BAR_WIDTH)
		progressWidth = MAX_PROGRESS_BAR_WIDTH;

	$("#sumTimeTable").html("");
	$("#sumTimeTable").append("<tr /><td align=center>Weekly time</td><td class='dataMedium' align=right><font color='" + color + "'>" + Math.round(summin * 100 / GOAL)  + "%</font></td><td width=" + PROGRESS_BAR_WIDTH + " height=25><img src='/images/" + progress + ".png' width=" + progressWidth  + " height=25></td><td class='dataMedium' align=center>" + sumtime + "</td><td class='dataMedium' align=right><font color='" + color + "'>" + summin + "<div class='unit'> min</div></font></td><td class='dataMedium' align=right>(" + GOAL + "<div class='unit'> min</div>)</td><td></td>");

	data = currentSeasonStatData;
	for (var i = 0; i < currentSeasonStatData.length; i++) {
		if (currentSeasonStatData[i].activityId > 0)
			drawActivityGoalRow(i, currentSeasonStatData[i]);
    }
	//$("#sumTimeTable").append("<tr /><td width=35><img src='/images/transp.gif' width=35 height=1></td><td width=35><img src='/images/transp.gif' width=35 height=2></td><td width=" + PROGRESS_BAR_WIDTH + " height=1><img src='/images/transp.gif' width= height=1 width=" + PROGRESS_BAR_WIDTH + "></td><td></td><td width=120><img src='/images/transp.gif' width=120 height=1></td><td width=120><img src='/images/transp.gif' width=120 height=1></td>");
	$("#sumTimeTable > tbody > tr:odd").addClass("rowOdd");
    $("#sumTimeTable > tbody > tr:not(.odd)").addClass("rowEven");
}

function drawActivityGoalRow(rowId, rowData) {
	logDebug("-> drawActivityGoalRow(" + rowId + ", " + rowData);
	GOAL = -1;
	unit = "";
	total = -1;
	if (rowData.activityId == 1) {
		GOAL = 10000;
		unit = "m";
		total = rowData.activityAltDiff;
	}
		//row.append($("<td width=100 class='dataBig' colspan=2>" + rowData.activityAltDiff + "<div class='unit'> m</div></td>"));
	else if (rowData.activityId == 3)  {
		GOAL = 8000;
		unit = "m";
		total = rowData.activityAltDiff;
	}
	else if (rowData.activityId == 5)  {
		GOAL = 1000;
		unit = "km";
		total = rowData.activityDist;
	}
	else if (rowData.activityId == 7)  {
		unit = "km";
		GOAL = 500;
		total = rowData.activityDist;
	}

	if (total > GOAL) {
		color = "green";
	}
	else {
		color = "blue";
	}
	progress = "progress-" + color;

		//row.append($("<td width=200 class='dataBig' colspan=2>" + Math.round(rowData.activityDist / 1.852) + "<div class='unit'> nm</div></td>"));
	if (GOAL > -1) {
		var row = $("<tr />")
		$("#sumTimeTable").append(row); //this will append tr element to table... keep its reference for a while since we will add cels into it
		rowWidth = Math.round(total * PROGRESS_BAR_WIDTH) / GOAL;
		if (rowWidth > MAX_PROGRESS_BAR_WIDTH)
			rowWidth = MAX_PROGRESS_BAR_WIDTH;
		row.append($("<td align=center><a href='activity.php?activityId=" + rowData.activityId + "'><img src='images/" + rowData.activityId + ".png' height=25 width=25></a></td><td class='dataMedium' width=25 align=right><font color='" + color + "'>" + Math.round(total * 100 / GOAL)  + "%</font></td><td  height=25 colspan=2><img src='/images/" + progress + ".png' width=" + rowWidth  + " height=25></td><td class='dataMedium' align=right><font color='" + color + "'>" + Math.round(total) + "<div class='unit'> " + unit + "</div></font></td><td class='dataMedium' align=right>(" + GOAL + "<div class='unit'> " + unit + "</div>)</td>"));
	}
}

function showActivityStat(data) {
	logDebug("-> showActivityStat(" + data);
	var activityData = [];
	$("#activityStatTable").html("");
	$("#activityStatTable").append(row);
	durationStr = thenumDays + " days";
	if (thenumDays > 30)
		durationStr = thenumDays / 30 + " months";
	if (thenumDays >= 365)
		durationStr = thenumDays / 365 + " years";
	document.getElementById("latestActivitiesTitle").innerHTML = "Activities last " + durationStr;
	logDebug("data.length = " + data.length);
	maxSum = 0;
	for (var i = 0; i < data.length; i++) {
		logDebug(data[i].activitySum + " > " + maxSum);
		if (new Number(data[i].activitySum) > maxSum) {

			maxSum = data[i].activitySum;
			logDebug("-> " + maxSum);
		}
	}
	ratio = 280 / maxSum;
	logDebug("maxSum = " + maxSum);
	logDebug("ratio = " + ratio);
	for (var i = 0; i < data.length; i++) {
		var row = $("<tr align=left />");
		$("#activityStatTable").append(row);
		row.append($("<td width=200 ><img src='images/" + data[i].activityId + ".png' width=" +  Math.round(data[i].activitySum * ratio) + " height=30 border=0></td><td class='data' align=center>" + data[i].activitySum + "</td>"));

    }
	//$("#activityStatTable td").addClass("tracksTableIndex");
	$("#activityStatTable > tbody > tr:odd").addClass("rowOdd");
    $("#activityStatTable > tbody > tr:not(.odd)").addClass("rowEven");
}

$(function showActivityStatChart(activityData) {
    $('#container').highcharts({
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: 0,
            plotShadow: false
        },
        title: {
            text: 'Activities',
            align: 'center',
            verticalAlign: 'middle',
            y: 50
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                dataLabels: {
                    enabled: true,
                    distance: -50,
                    style: {
                        fontWeight: 'bold',
                        color: 'white',
                        textShadow: '0px 1px 2px black'
                    }
                },
                startAngle: -90,
                endAngle: 90,
                center: ['50%', '75%']
            }
        },
        series: [{
            type: 'pie',
            name: 'Activities',
            innerSize: '50%',
            data: activityData
        }]
    });
});

</script>

<body onLoad="init();">

<?php include 'bodyHeader.php' ?>

<div id="maiwindow" class="maiwindow" style="position: absolute; top: 45px; left: 0px;  ">

<!--table width=1190 height=100%-->

<table width=100% height=100% id="mainIndexTable" class=mainIndexTable border=0>
<?php
if ($userSession != null && $userSession != "") {
?>
	<tr height=1 width=100% align=center valign=middle valign=top>
		<td colspan=6><img src=/images/transp.gif width=1 height=1></td>
	</tr>
	<tr height=505 width=100% align=center valign=middle valign=top>

	<td width=26% height=400 valign=top>
			<DIV id="mainWin" class="window" style="width: 100%; height: 505px; z-index : 3; overflow:hidden;">
				<div id="" class="windowtopbar">
					<table width=100% cellpadding=0 cellspacing=0 border=0><tr>
						<td class="windowtopbar" width=50% id="seasonStatTitle" valign=middle>Season statistics</td>
						<form id="seasonForm"><td class="windowtopbar" width=50% align=right valign=middle>
							<select id=seasonFormMySelectOption onChange="e = document.getElementById('seasonFormMySelectOption');getSeasonStat(e.options[e.selectedIndex].value, 1);">
								<script>
								var currentDate = new Date();
								optionStr = "";
								for (theYear = currentDate.getFullYear(); theYear >= 2005 ; theYear--) {
									optionStr = optionStr + "<option value=" + theYear + ">" + theYear + "</option>";
								}
								document.write(optionStr);
								</script>
							</select>
						</td></form>
						<td valign=middle>
						<a href=# onClick="e = document.getElementById('seasonFormMySelectOption');getSeasonStat(e.options[e.selectedIndex].value, 1);return false;"><img src="images/reload.gif" width=15 height=15 border=0 title="Main window" alt="Main window"></a>
						</td>
						</tr>
					</table>
				</div>
				<div id="seasonStatWinInner" class="innerwindow" style="position: relative; top: 0px; width: 100%; height: 100%; overflow: auto; overflow-x: hidden;">
					<table id="seasonStatTable" class="statTable" width=100% border=0>
						<tr>
						<td align=center><img src='images/ajaxLoader.gif'> <p>Loading
						  <p>&nbsp;</td>
					  </tr>
					  <tr>
						<td align=center><img src='images/transp.gif' width=300 height=1></td>
					  </tr>
					</table>
			  </div>
			</div>
		</td>

		<td width=1%><img src=/images/transp.gif width=1 height=300></td>

		<td width=26% height=300 valign=top>
			<DIV id="mainWin" class="window" style="width: 100%; height: 505px; z-index : 3; overflow:hidden;">
			<div id="" class="windowtopbar">
			<table width=100% cellpadding=0 cellspacing=0 border=0><tr>
				<td class="windowtopbar" width=35%>Latest activities</td>
				<td class="windowtopbar" width=1><img src="images/transp.gif" height=16 width=1></td>
				<td class="windowtopbar" width=65% align=right>
				<a href=# onClick="getLatestTracks(14);return false;"><img src="images/reload.gif" width=15 height=15 border=0 title="Main window" alt="Main window"></a>
				</td></tr>
			</table>
			</div>
			<div id="latestTracksWinInner" class="innerwindow" style="position: relative; top: 0px; width: 100%; height: 100%; overflow:auto; overflow-x: hidden">

				<table id="latestTracksTable" class="statTable" width=100%>
					<tr>
					<td align=center><img src='images/ajaxLoader.gif'> <p>Loading
					  <p>&nbsp;</td>
				  </tr>
				  <tr>
					<td align=center><img src='images/transp.gif' width=300 height=1></td>
				  </tr>
				</table>
		  </div>
		</div>
		</td>

		<td width=1%><img src=/images/transp.gif width=1 height=300></td>

		<td width=26% height=300 valign=top>
			<DIV id="mainWin" class="window" style="width: 100%; height: 505px; z-index : 3; overflow:hidden;">
			<div id="" class="windowtopbar">
			<table width=100% cellpadding=0 cellspacing=0 border=0><tr>
				<td class="windowtopbar" width=50% id="latestActivitiesTitle">Latest activities</td>
				<td class="windowtopbar" width=1><img src="images/transp.gif" height=16 width=1></td>
				<form id="activityForm"><td class="windowtopbar" width=50% align=right valign=middle>
					<select id=activityFormMySelectOption onChange="e = document.getElementById('activityFormMySelectOption');getActivityStat(e.options[e.selectedIndex].value);">
						<option value="7">7 jours</option>
						<option value="30" selected>30 jours</option>
						<option value="60">2 mois</option>
						<option value="180">6 mois</option>
						<option value="365">1 an</option>
						<option value="730">2 ans</option>
						<option value="1825">5 ans</option>
						<option value="3650">10 ans</option>
					</select>
				</td></form>
				<td valign=middle>
				<a href=# onClick="e = document.getElementById('activityFormMySelectOption');getActivityStat(e.options[e.selectedIndex].value);return false;"><img src="images/reload.gif" width=15 height=15 border=0 title="Main window" alt="Main window"></a>
				</td>
			</table>
			</div>
			<div id="mainWinInner" class="innerwindow" style="position: relative; top: 0px; width: 100%; height: 100%; overflow: auto; overflow-x: hidden;">
				<table id="activityStatTable" class="statTable" width=100%>
					<tr>
					<td align=center><img src='images/ajaxLoader.gif'> <p>Loading
					  <p>&nbsp;</td>
				  </tr>
				  <tr>
					<td align=center><img src='images/transp.gif' width=300 height=1></td>
				  </tr>
				</table>
		  </div>
		</div>
		</td>

		<td width=1%><img src=/images/transp.gif width=1 height=400></td>

		<td width=20% valign=top rowspan=3>

		<DIV id="windWin" class="window" style="width: 240px; height: 130px; z-index : 3; overflow:hidden;">
		<div id="" class="windowtopbar" style="position: relative; top: 0px; left: 0px; height=15px; width: 100%">
		<table width=100% cellpadding=0 cellspacing=0 border=0><tr>
			<td class="windowtopbar" width=15%>Wind</td>
			<td class="windowtopbar" width=1><img src="images/transp.gif" height=16 width=1></td>
			<form id="activityForm"><td class="windowtopbar" width=85% align=right valign=middle>
					<select id=windFormMySelectOption onChange="e = document.getElementById('windFormMySelectOption');loadWindWin(e.options[e.selectedIndex].value);">
						<option value="15">Moucherotte</option>
						<option value="61">St Hilaire</option>
						<option value="600">Attero St Hilaire</option>
						<option value="13">La Scia</option>
						<option value="14">Villard Cote 2000</option>
						<option value="12">Collet d'Allevard</option>
					</select>
				</td></form>
				<td valign=middle>
				<img src="images/reload.gif" width=15 height=15 border=0 title="Main window" alt="Main window">
				</td>
			<!--a href=# onClick=loadWindWin(15) class="windowtopbar"><img src="images/orange.gif" width=15 height=15 border=0 alt="Moucherotte" title="Moucherotte"></a>
			<a href=# onClick=loadWindWin(61) class="windowtopbar"><img src="images/green.gif" width=15 height=15 border=0 alt="St Hilaire du Touvet" title="St Hilaire du Touvet"></a>
			<a href=# onClick=loadWindWin(13) class="windowtopbar"><img src="images/brown.gif" width=15 height=15 border=0 alt="La Scia" title="La Scia"></a>
			<img src="images/help.gif" width=15 height=15 border=0 title="Wind info from FFVL" alt="Wind info from FFVL">
			</td-->
			</tr>
		</table>
		</div>
		  <div id="windWinInner" class="innerwindow" style="position: relative; top: 5px; width: 100%; height: 100%">
			<table width=100% height=100%>
			  <tr>
				<td align=center><img src='images/ajaxLoader.gif'> <p>
				  <p>&nbsp;</td>
			  </tr>
			</table>
		  </div>
		</DIV>

		<img src="images/transp.gif" height=10 width=1>

		<DIV id="weatherdiv" class="window" style="width: 240px; height: 220px; z-index : 3; overflow:hidden;">
		<div id="" class="windowtopbar" style="position: relative; top: 0px; left: 0px; height=15px; width: 100%">
		<table width=100% cellpadding=0 cellspacing=0><tr>
			<td class="windowtopbar" width=35%><div class="windowtopbar" id="weatherLocationDiv">Weather</div></td>
			<td class="windowtopbar" width=1><img src="images/transp.gif" height=16 width=1></td>
			<td class="windowtopbar" width=65% align=right>
			<a href=# onClick=loadWeatherFromID("France/Rhône-Alpes/Les_Adrets") class="windowtopbar"><img src="images/orange.gif" width=15 height=15 border=0 alt="Les Adrets" title="Les Adrets"></a>
			<a href=# onClick=loadWeatherFromID("France/Provence-Alpes-Côte_d’Azur/Hyères") class="windowtopbar"><img src="images/green.gif" width=15 height=15 border=0 alt="Hyères" title="Hyères"></a>
			<a href=# onClick=loadWeatherFromID("Sweden/Stockholm/Stockholm") class="windowtopbar"><img src="images/brown.gif" width=15 height=15 border=0 alt="Stockholm" title="Stockholm"></a>
			<img src="images/help.gif" width=15 height=15 border=0 title="Weather info from YR.no" alt="Weather info from YR.no">
		</td></tr>
		</table>
		</div>
		  <div id="weatherdivInner" class="innerwindow" style="position: relative; top: 5px;">
			<table width=100% height=100%>
			  <tr>
				<td align=center><img src='images/ajaxLoader.gif'> <p>Loading
				  <p>&nbsp;</td>
			  </tr>
			</table>
		  </div>
		</DIV>

		<img src="images/transp.gif" height=10 width=1>

		<DIV id="snowWin" class="window" style="width: 240px; height: 160px; z-index : 3; overflow:hidden;">
		<div id="" class="windowtopbar" style="position: relative; top: 0px; left: 0px; height=15px; width: 100%">
		<table width=100% cellpadding=0 cellspacing=0><tr>
			<td class="windowtopbar" width=35%>Snow</td>
			<td class="windowtopbar" width=1><img src="images/transp.gif" height=16 width=1></td>
			<td class="windowtopbar" width=65% align=right>
			<form id="skiStationForm"><select id=skiStationFormMySelectOption onChange="e = document.getElementById('skiStationFormMySelectOption');loadSnowWin(e.options[e.selectedIndex].value);">
						<option value="alpes-du-nord/les-7-laux">Les 7 Laux</option>
						<option value="alpes-du-nord/alpe-dhuez">Alpe d'Huez</option>
						<option value="alpes-du-nord/les-2-alpes">Les 2 Alpes</option>
						<option value="alpes-du-sud/la-grave-la-meije">La Grave</option>
						<option value="alpes-du-nord/chamrousse">Chamrousse</option>
						<option value="alpes-du-nord/villard-de-lans">Villard de Lans</option>
						<option value="alpes-du-nord/le-collet-dallevard">Collet d'Allevard</option>
						<option value="alpes-du-nord/tignes">Tignes</option>
						<option value="alpes-du-nord/val-disere">Val d'Is&egrave;re</option>
					</select>
			<img src="images/help.gif" width=15 height=15 border=0 title="Snow info from Ski Info" alt="Snow info from Ski Info">
			</td></form></tr>
		</table>
		</div>
		  <div id="snowWinInner" class="innerwindow" style="position: relative; top: 5px;">
			<table width=100% height=100%>
			  <tr>
				<td align=center><img src='images/ajaxLoader.gif'> <p>Loading
				  <p>&nbsp;</td>
			  </tr>
			</table>
		  </div>
		</DIV>

		<img src="images/transp.gif" height=10 width=1>

		<DIV id="passWin" class="window" style="width: 240px; height: 180px; z-index : 3; overflow:hidden;">
		<div id="" class="windowtopbar" style="position: relative; top: 0px; left: 0px; height=15px; width: 100%">
		<table width=100% cellpadding=0 cellspacing=0><tr>
			<td class="windowtopbar" width=65%>Ouverture Cols en Is&egrave;re</td>
			<td class="windowtopbar" width=1><img src="images/transp.gif" height=16 width=1></td>
			<td class="windowtopbar" width=35% align=right>
			<a href=# onClick="loadPassWin();"><img src="images/reload.gif" width=15 height=15 border=0 title="Snow info from Ski Info" alt="Snow info from Ski Info"></a>
			</td></form></tr>
		</table>
		</div>
		  <div id="passWinInner" class="innerwindow" style="position: relative; top: 5px; height: 160px; overflow:scroll;">
			<table width=100% height=160 id="passTable">
			  <tr>
				<td align=center><img src='images/ajaxLoader.gif'> <p>Loading
				  <p>&nbsp;</td>
			  </tr>
			</table>
		  </div>
		</DIV>
	</td>
</tr>

<tr height=1 align=center valign=top>
	<td><img src="images/transp.gif" height=1 width=1></td>
</tr>

<tr height=205 width=100% align=center valign=top>
	<td width=80% height=205 valign=top colspan=5>
			<DIV id="mainWin" class="window" style="width: 100%; height: 205px; z-index : 3; overflow:hidden;">
			<div id="" class="windowtopbar">
			<table width=100% cellpadding=0 cellspacing=0 border=0><tr>
				<td class="windowtopbar" width=35%>Activity goals</td>
				<td class="windowtopbar" width=1><img src="images/transp.gif" height=16 width=1></td>
				<td class="windowtopbar" width=65% align=right>
				<img src="images/help.gif" width=15 height=15 border=0 title="Main window" alt="Main window">
				</td></tr>
			</table>
			</div>
			<div id="sumTimeWinInner" class="innerwindow" style="position: relative; top: 00px; width: 100%; height: 100%; overflow:hidden; overflow-x: hidden">
				<table id="sumTimeTable" width=100%>
					<tr>
						<td align=center><img src='images/ajaxLoader.gif'> <p>Loading
						<p>&nbsp;</td>
					</tr>
					<tr>
						<td align=center><img src='images/transp.gif' width=320 height=1></td>
					</tr>
				</table>
		  </div>
		</div>
		</td>
	</td>
</tr>
<tr height=20% align=center valign=top>
	<td>&nbsp;</td>
</tr>
<?php
}
else
{
?>
<tr height=100% width=100% align=center valign=middle>
		<td width=440>&nbsp;</td>
		<td width=320 height=300 valign=top>
			<p>
			<img src=/images/transp.gif height=50 width=1>
			<p>
			<DIV id="mainWin" class="window" style="width: 100%; height: 200px; z-index : 3; overflow:hidden;">
			<div id="" class="windowtopbar">
			<table width=100% cellpadding=0 cellspacing=0 border=0><tr>
				<td class="windowtopbar" width=50% id="">Login</td>
				<td class="windowtopbar" width=1><img src="images/transp.gif" height=16 width=1></td>
				<td class="windowtopbar" width=50% align=right valign=middle>

				</td>
				<td valign=middle>
				<img src="images/help.gif" width=15 height=15 border=0 title="Main window" alt="Main window">
				</td>
			</table>
			</div>
			<div id="mainWinInner" class="innerwindow" style="position: relative; top: 10px; width: 100%; height: 350px; overflow: auto; overflow-x: hidden;">
				<table border=0 width=100%>
				  <form method=post name="login" onSubmit="doLogin(document.forms['login'].username.value, document.forms['login'].password.value, 'loginMsgMain'); return false;">
					<tr>
					  <td>Username : </td>
					  <td><input type="text" name="username" size=15 value="" autocomplete="on"></td>
					</tr>
					<tr>
					  <td>Password : </td>
					  <td><input type="password" name="password" onKeyPress="if (checkKey(event)) {doLogin(document.forms['login'].username.value, document.forms['login'].password.value, 'loginMsgMain'); }" size=15 value="" autocomplete="on"></td>
					</tr>
					<tr>
						<td></td>
						<td>
						<input type="submit" name="login" value="Login" onClick="login(document.forms['login'].username.value, document.forms['login'].password.value, 'loginMsgMain'); return false;"/>
						<!--a href=# onClick="login(document.forms['login'].username.value, document.forms['login'].password.value, 'loginMsgMain')" class="formbutton">Login</a-->&nbsp;&nbsp;&nbsp;&nbsp;</td>
					</tr>
					<tr>
					  <td colspan=2><img src="images/transp.gif" height=10 width=2></td>
					</tr>
					<tr>
					  <td colspan=2 id="loginMsgMain"></td>
					</tr>
					<tr>
					  <td colspan=2><img src="images/transp.gif" height=1 width=2></td>
					</tr>
					<tr>
					  <td colspan=2>You don't have an account? <a href=# onClick="registerUser()">Create one</a></td>
					</tr>
				  </form>
				</table>
		  </div>
		</div>
	</td>
	<td width=440>&nbsp;</td>
</tr>
<?php
}
?>


</table>

</div>

<div id="bottombar" class="bottombar">
&nbsp;
</div>

<?php include 'bodyFooter.php' ?>
<iframe id="loginframe" name="loginframe" width=0 height=0 border=0></iframe>
</body>
</html>
