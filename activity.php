<?php include 'header.php' ?>
<?php include 'inc/country.php' ?>
<?php
if (isset($_GET['activityId']))
	$activityId = $_GET['activityId'];
else
	$activityId = -1;
?>

<script>
lang = "fr";

activityId = <?= $activityId; ?>;
if (activityId == null || activityId == "")
	activityId = -1;
var user = "<?= $userSession; ?>";

var map;
var openskimap;
var trackIdArray = [];
var tourLat = 0;
var tourLon = 0;
var trackData;
var weekData;

var markerArray = [];

var currentDate = new Date();
var season;
var year = currentDate.getFullYear();
var month = 1 + currentDate.getMonth();
var isInitDone = false;

var MA_CLE= "rhfiamuq2cejl2h4xgcmwpge";

google.load('visualization', '1', {packages: ['corechart', 'line']});
//google.setOnLoadCallback(init());

window.onresize = function() {
	logDebug("onresize");
    resizeDiv();
};

function init() {
	logDebug("-> init()");
	logDebug("year = " + year);
	resizeDiv();

	logDebug("activityId = " + activityId);
	typeFormSel = document.getElementById('typeFormMySelectOption');
	typeFormSel.options[0].selected;
	timeTypeFormSel = document.getElementById('timeTypeFormMySelectOption');
	timeTypeFormSel.options[0].selected;


	$.getJSON("activityStatWS.php?user=" + user + "&action=season&year=" + year + "&month=" + month + "&activity=" + activityId , function(data) {
		activityData = data.activities;
		setTitle(activityData);

		drawTable(activityData);
		isInitDone = true;
	});

	$.getJSON("tracksWS.php?track=season&year=" + year + "&month=" + month + "&limit=500&user=" + user + "&activity=" + activityId , function(data) {
		trackData = data.tracks;

		drawTrackTable(trackData);
		drawGraph("Duration", "Day");
		initMap(trackData);
	});

	$.getJSON("activityStatWS.php?user=" + user + "&year=" + year + "&action=week&activity=" + activityId , function(data) {
		weekData = data.activities;
	});

}

function resizeDiv() {
	logDebug("-> resizeDiv()");
	var width = window.innerWidth
		|| document.documentElement.clientWidth
		|| document.body.clientWidth;

	var height = window.innerHeight
		|| document.documentElement.clientHeight
		|| document.body.clientHeight;

	logDebug("width = " + width + " - height = " + height);
	document.getElementById("maiwindow").style.height = (height - 340) + "px";
	//document.getElementById("maiwindow").style.width = width - 5 + "px";
	document.getElementById("mapWin").style.height = (height - 340) + "px";
	document.getElementById("tableWin").style.height = (height - 420) + "px";
	document.getElementById("summaryWin").style.height = "80px";
	document.getElementById("graphWin").style.height =  "200px";
}

function setTitle(data) {
	logDebug("-> setTitle(" + data);
	var currentDate = new Date();
	//if (month >= 8)
	//	theYear = 1 + year;
	//else
		theYear = year;
	season = theYear;
	if (activityId == 1 || activityId == 2 || activityId == 6)
		season = (theYear-1) + " - " + theYear;

	if (activityId == -1)
		activityIcon = "all";
	else
		activityIcon = activityId;

	if (activityId == -1)
		seasonStr = "Year " + season;
	else
		seasonStr = "Season " + season;

	title = "<table><tr valign=middle>";
	title += "<td width=50><img src='/images/" + activityIcon + ".png' width=45 height=45></td>";
	title += "<td width=260 class='title' valign=middle>" + seasonStr + "</td>";

	title = title + "<form id=\"seasonForm\"><td align=right valign=middle>";
	title = title + "<select id=seasonFormMySelectOption onChange=\"e = document.getElementById('seasonFormMySelectOption');year=e.options[e.selectedIndex].value;month=1;init();\">";
	var currentDate = new Date();
	for (theYear = currentDate.getFullYear(); theYear >= 2005 ; theYear--) {
		if (theYear == year)
			selected = " selected";
		else
			selected = "";
		title = title + "<option value=" + theYear + selected + ">" + theYear + "</option>";
	}
	title = title + "</select></td></form>";

	title += "<td width=15>&nbsp;</td>";
	title = title + "<td>";
	title = title + "<a href=# onClick='handleClickCategory(-1); return false;'><img src='/images/all.png' width=45 height=45></a>&nbsp;&nbsp;";
	for (i = 0; i <= ACTIVITY_NUM; i++)
		title = title + "<a href=# onClick='handleClickCategory(" + i + "); return false;'><img src='/images/" + i + ".png' width=45 height=45></a>&nbsp;&nbsp;";
	title = title + "</td>";

	title = title + "</tr></table>";
	$("#titleDiv").html(title);
	/*
	title = "<img src='/images/" + data[0].activityId + ".png' width=45 height=45>&nbsp;&nbsp;&nbsp;" + data[0].tourName;
	title = title + "<div class='toolbar' style='position: absolute; right: 0px; display: inline;'>";
	title = title + " &nbsp;&nbsp; <a href=# onClick='initGeoPortail()'>[GEO]</a>";
	title = title + " &nbsp;&nbsp; <a href=# onClick='initGoogleMaps()'>[GOOGLE]</a>";
	title = title + "</div>";
	$("#titleDiv").html(title);
	*/
	$("#subTitleDiv").html("");
}

function drawTable(activityData) {
	$("#summaryTable").html("");
	if (activityData != null && activityData.length > 0) {
		activitySum = 0;
		summin = 0;
		activityDist = 0;
		activityAltDiff = 0;

		for (var i = 0; i < activityData.length; i++) {
			rowData = activityData[i];
			activitySum += new Number(rowData.activitySum);
			summin += new Number(rowData.summin);
			activityDist += new Number(rowData.activityDist);
			activityAltDiff += new Number(rowData.activityAltDiff);
		}
		var c = [];
		timeSpent = secondsToHms(summin *60);
		c.push("<tr height=60><td align=center class='dataBig' valign=middle>" + activitySum + "<div class='unit'> tracks</div></td>");
		c.push("<td align=center class='dataBig' valign=middle>" + timeSpent + "</td>");
		c.push("<td align=center class='dataBig' valign=middle>" + Math.round(activityDist) + "<div class='unit'> km</div></td>");
		if (activityId != 2 && activityId != 4 && activityId != 8)
			c.push("<td align=center class='dataBig' valign=middle>" + activityAltDiff + "<div class='unit'> m</div></td>");
		c.push("</tr>");

		$("#summaryTable").html(c.join(""));
	}
	else {
		$("#summaryTable").html("<tr><td class='title' align=center>No tracks for this category during the season " + season + "</td></tr>");
	}
}

function drawRow(rowId, rowData) {
	var c = [];
	timeSpent = secondsToHms(rowData.summin *60);
	/*
	if (timeSpent < 60)
		timeSpent = timeSpent + " min";
	else
		timeSpent = Math.round(timeSpent / 60) + " h";
		*/
	//var row = $("<tr />");
    //$("#tracksTable").append(row); //this will append tr element to table... keep its reference for a while since we will add cels into it
    c.push("<tr height=60><td align=center class='dataBig' valign=middle>" + rowData.activitySum + "<div class='unit'> tracks</div></td>");
	c.push("<td align=center class='dataBig' valign=middle>" + timeSpent + "</td>");
	c.push("<td align=center class='dataBig' valign=middle>" + Math.round(rowData.activityDist) + "<div class='unit'> km</div></td>");
	if (activityId != 2 && activityId != 4 && activityId != 8)
		c.push("<td align=center class='dataBig' valign=middle>" + rowData.activityAltDiff + "<div class='unit'> m</div></td>");
	c.push("</tr>");

	$("#summaryTable").html(c.join(""));
}

function drawTrackTable(data) {
	logDebug("drawTrackTable(" + data);
	$("#tracksTable").html("");
	if (data != null && data.length > 0) {
		var row = $("<tr width=100% />")
		$("#tracksTable").append(row);
		row.append($("<td width=20><img src=images/transp.gif width=20 height=1></td><td width=85><img src=images/transp.gif width=85 height=1></td><td width=60%></td><td width=25><img src=images/transp.gif width=25 height=1></td><td width=65><img src=images/transp.gif width=65 height=1></td><td width=50><img src=images/transp.gif width=50 height=1></td><td width=50><img src=images/transp.gif width=50 height=1></td>"));
		for (var i = 0; i < data.length; i++) {
			drawTrackRow(i, data[i]);
		}

		$("#tracksTable td").addClass("tracksTableIndex");
		$("#tracksTable > tbody > tr:odd").addClass("rowOdd");
		$("#tracksTable > tbody > tr:not(.odd)").addClass("rowEven");
	}
}

function drawTrackRow(rowId, rowData) {
	logDebug("drawTrackRow(" + rowId);
	var row = $("<tr />")
    $("#tracksTable").append(row); //this will append tr element to table... keep its reference for a while since we will add cels into it
    row.append($("<td align=center><a href='activity.php?activityId=" + rowData.activityId + "'><img src='/images/" + rowData.activityId + ".png' width=20 height=20></a></td>"));

	if (rowData.activityId > 0)
		row.append($("<td align=center><a href='track.php?trackId=" + rowData.trackId + "'>" + shortDate(rowData.trackDate, 10) + "</a></td>"));
	else
		row.append($("<td align=center>" + shortDate(rowData.trackDate, 10) + "</td>"));

	if (rowData.trackName != "")
		trackName = rowData.trackName;
	else
		trackName = "";
	if (rowData.activityId > 0) {
		if (trackName != "")
			row.append($("<td><a href='tour.php?tourId=" + rowData.tourId + "'>"  + rowData.tourName + "</a> / <a href='track.php?trackId=" + rowData.trackId + "'>" + trackName + "</a></td>"));
		else
			row.append($("<td><a href='tour.php?tourId=" + rowData.tourId + "'>"  + rowData.tourName + "</a></td>"));
	}
	else {
		if (trackName != "")
			row.append($("<td>" + rowData.tourName + " / " + trackName + "</td>"));
		else
			row.append($("<td>" + rowData.tourName + "</td>"));
	}

	row.append($("<td width=85 align=center><img src='/images/flags/24/" + (rowData.country).toLowerCase() + ".png'></td>"));

	row.append($("<td align=center>" + rowData.trackDuration + "</td>"));

	if (rowData.activityId > 0)
		row.append($("<td align=center>" + rowData.trackDistance + "</td>"));
	else
		row.append($("<td></td>"));

	if (activityId != 2 && activityId != 4 && activityId != 8)
		row.append($("<td align=center>" + rowData.trackAltGain + "</td>"));
	else
		row.append($("<td></td>"));
}

function handleClickCategory(categoryId) {
	activityId = categoryId;
	init();
}

function drawGraph(type, timeType) {
	logDebug("-> drawGraph(" + type + ", " + timeType);

	$("#graphWinInner").html("");

	var gdata = new google.visualization.DataTable();

	if (timeType == "Day" || timeType == "Total") {
		graphData = trackData;
		gdata.addColumn('date', 'Date');
	}
	else {
		graphData = weekData;
		gdata.addColumn('number', 'Week');
	}
	gdata.addColumn('number', type);
	gdata.addColumn({type: 'string', role: 'tooltip', 'p': {'html': true}});

	if (graphData != null && graphData.length > 0) {

		dataIndex = 0;
		totalValue = 0;
		for (var i = (graphData.length - 1); i >= 0; i--) {
			rowData = graphData[i];
			if (timeType == "Day" || timeType == "Total") {
				trackIdArray[i] = rowData.trackId;

				datetime = rowData.trackDate;

				trackAverageSpeed = Math.round(rowData.trackAverageSpeed * 100) / 100;
				if (trackAverageSpeed == 0) trackAverageSpeed = "";
				trackAverageUpSpeed = rowData.trackAverageUpSpeed;
				if (trackAverageUpSpeed == 0) trackAverageUpSpeed = "";
				trackDistance = rowData.trackDistance;
				if (trackDistance == 0) trackDistance = "";
				trackAltDiff = rowData.trackAltGain;
				if (trackAltDiff == 0) trackAltDiff = "";
				trackDuration = rowData.trackDuration;
				trackDuration = Math.round(hmsToSecondsOnly(trackDuration) / 36) / 100;

				name = rowData.tourName;
				if (rowData.trackName != null && rowData.trackName != "")
					name = name + " / " + rowData.trackName;

				formatStr = 'dd/MM';
				theDate = new Date(shortDate(datetime, 10));
			} else {
				trackDuration = Math.round((rowData.summin / 60) * 100) / 100;
				trackDistance = Math.round(rowData.sumDistance * 100) / 100;
				trackAltDiff = rowData.sumAltGain;

				name = "";

				formatStr = '#';
				theDate = parseFloat(rowData.week);
			}

			if (type == "Duration") {
				value = trackDuration;
				valueToolTip = secondsToHms(trackDuration * 3600);
				unit = "";
				legend = "Duration (h)";
			}
			else if (type == "Distance") {
				value = trackDistance;
				valueToolTip = value;
				unit = "km";
				legend = "Distance (" + unit + ")";
			}
			else if (type == "Altitude gain") {
				value = trackAltDiff;
				valueToolTip = value;
				unit = "m";
				legend = "Altitude gain (" + unit + ")";
			}

			if (value != "" && !isNaN(value)) {

				totalValue += parseFloat(value);
				//alert();
				if (name != "")
					tooltip = $.datepicker.formatDate( "dd-mm-yy",new Date(theDate)) + "<br>" + name + "<br><b>" + valueToolTip + "</b> " + unit;
				else
					tooltip = "Week " + theDate + "<br><b>" + valueToolTip + "</b> " + unit;

				if (timeType == "Total")
					value = totalValue;

				console.log("-> " + dataIndex + ", " + theDate + ", " + parseFloat(value));
				gdata.addRow([theDate, parseFloat(value), tooltip]);
				dataIndex++;
			}
		}

		var options = {
			bar: {groupWidth: 10},
			hAxis: {
			  title: '',
			  format: formatStr
			},
			vAxis: {
			  minValue: 0,
			  title: legend
			},
			backgroundColor: '#FFFFFF',
			tooltip: { isHtml: true },
			animation:{
				duration: 1000,
				easing: 'out',
			}
		};

		if (timeType == "Day" || timeType == "Week")
			chart = new google.visualization.ColumnChart(document.getElementById('graphWinInner'));
		else
			chart = new google.visualization.LineChart(document.getElementById('graphWinInner'));
		chart.draw(gdata, options);
	  }
}


         <!--//--><![CDATA[//><!--
var iv= null;
var viewer=null;

// Definition url des services Geoportail
function geoportailLayer(name, key, layer, options)
{ var l= new google.maps.ImageMapType
  ({ getTileUrl: function (coord, zoom)
      {  return "http://wxs.ign.fr/" + key + "/geoportail/wmts?LAYER=" + layer
          + "&EXCEPTIONS=text/xml"
          + "&FORMAT="+(options.format?options.format:"image/jpeg")
          + "&SERVICE=WMTS&VERSION=1.0.0&REQUEST=GetTile"
          + "&STYLE="+(options.style?options.style:"normal")+"&TILEMATRIXSET=PM"
          + "&TILEMATRIX=" + zoom
          + "&TILECOL=" + coord.x + "&TILEROW=" + coord.y;
      },
    tileSize: new google.maps.Size(256,256),
    name: name,
    minZoom: (options.minZoom ? options.minZoom:0),
    maxZoom: (options.maxZoom ? options.maxZoom:18)
  });
  l.attribution = ' &copy; <a href="http://www.ign.fr/">IGN-France</a>';
  return l;
}
// Ajout de l'attribution Geoportail a la carte
function geoportailSetAttribution (map, attributionDiv)
{
	logDebug("-> geoportailSetAttribution(" + map + ", " + attributionDiv);
	if (map.mapTypes.get(map.getMapTypeId()) && map.mapTypes.get(map.getMapTypeId()).attribution)
  {  attributionDiv.style.display = 'block';
    attributionDiv.innerHTML = map.mapTypes.get(map.getMapTypeId()).name
      +map.mapTypes.get(map.getMapTypeId()).attribution;
  }
  else attributionDiv.style.display = 'none';
}

var map;
// Initialisation de la carte
function initMap(data)
{ // La carte Google
  map = new google.maps.Map( document.getElementById('map'),
  {  mapTypeId: google.maps.MapTypeId.TERRAIN,
    streetViewControl: false,
    mapTypeControlOptions: { mapTypeIds: ['carte', google.maps.MapTypeId.TERRAIN, google.maps.MapTypeId.SATELLITE, 'OSM', 'OCM', 'Nautical SWE'], style:google.maps.MapTypeControlStyle.DROPDOWN_MENU },
    center: new google.maps.LatLng(tourLat, tourLon),
    zoom: 12
  });

  /** Definition des couches  */
  // Carte IGN
  map.mapTypes.set('carte', geoportailLayer("IGN", MA_CLE, "GEOGRAPHICALGRIDSYSTEMS.MAPS", { maxZoom:18 }));
  // Ajouter un control pour l'attribution
  var attributionDiv = document.createElement('div');
  attributionDiv.className = "attribution";
  geoportailSetAttribution(map, attributionDiv);
  map.controls[google.maps.ControlPosition.BOTTOM_RIGHT].push(attributionDiv);
  // Afficher / masquer le copyright en fonction de la couche
  google.maps.event.addListener(map, 'maptypeid_changed',
    function()
    {  geoportailSetAttribution(this, attributionDiv);
    });


  //Define OSM map type pointing at the OpenStreetMap tile server
	map.mapTypes.set("OSM", new google.maps.ImageMapType({
		getTileUrl: function(coord, zoom) {
			return "http://tile.openstreetmap.org/" + zoom + "/" + coord.x + "/" + coord.y + ".png";
		},
		tileSize: new google.maps.Size(256, 256),
		name: "OSM",
		maxZoom: 18
	}));

	//Define OCM map type pointing at the Open Cycle Map tile server
	map.mapTypes.set("OCM", new google.maps.ImageMapType({
		getTileUrl: function(coord, zoom) {
			return "http://tile.thunderforest.com/cycle/" + zoom + "/" + coord.x + "/" + coord.y + ".png?apikey=" + OCM_KEY;
		},
		tileSize: new google.maps.Size(256, 256),
		name: "OpenCycleMap",
		maxZoom: 18
	}));

	map.mapTypes.set("Nautical SWE", new google.maps.ImageMapType({
			getTileUrl: function(coord, zoom) {
				return "http://map.eniro.com/geowebcache/service/tms1.0.0/nautical/"+zoom+"/"+ coord.x +"/"+((1 << zoom) - 1 - coord.y) + ".png";
				//return "http://map.eniro.com/geowebcache/service/gmaps?layers=nautical&zoom=" + zoom + "&x=" + coord.x + "&y=" + coord.y + "&format=image/png";
			},
			tileSize: new google.maps.Size(256, 256),
			name: "Nautical SWE",
			maxZoom: 18
		}));

	//Define OSM map type pointing at the OpenSnowMap tile server
	openskimap = new google.maps.ImageMapType({
			getTileUrl: function(coord, zoom) {
			return "http://www.opensnowmap.org/opensnowmap-overlay/" + zoom + "/" + coord.x + "/" + coord.y + ".png";
		},
		tileSize: new google.maps.Size(256, 256),
		name: "OpenSkiMap",
		maxZoom: 18
		});

	if (data != null) {
		bounds = displayMarkers(map, data);
		map.fitBounds(bounds);
		if (map.getZoom() > 14)
			map.setZoom(14);
	}
	else {
		map.setZoom(4);
		map.setCenter({lat: 45, lng: 6});
	}

	if (activityId == 4)
		map.setMapTypeId("Nautical SWE");
	else if (activityId == 7)
		map.setMapTypeId("OCM");
}
//google.maps.event.addDomListener(window, 'load', initMap);

function displayMarkers(map, data) {
	logDebug("-> displayMarkers(" + map + ", " + data);
	var bounds = new google.maps.LatLngBounds ();

	for (var i = 0; i < data.length; i++) {
		if (data[i].activityId > 0) {
			tourId = data[i].tourId;
			trackId =  data[i].trackId;
			if (data[i].startlatitude != "" && data[i].startlatitude !=  0)
				lat = data[i].startlatitude;
			else
				lat = data[i].latitude;
			if (data[i].startlongitude != "" && data[i].startlongitude !=  0)
				lon = data[i].startlongitude;
			else
				lon = data[i].longitude;

			var p = new google.maps.LatLng(lat,lon);
			bounds.extend(p);

			var image = {
				url: 'images/' + data[i].activityId + '.png',
				// This marker is 20 pixels wide by 32 pixels tall.
				size: new google.maps.Size(60, 60),
				// The origin for this image is 0,0.
				origin: new google.maps.Point(0,0),
				// The anchor for this image is the base of the flagpole at 0,32.
				anchor: new google.maps.Point(0, 10),
				scaledSize: new google.maps.Size(30, 30)
			  };


			/*
			var marker = new google.maps.Marker({
				  position: p,
				  map: map,
				  title: data[i].tourName + " / " + data[i].trackName,
				  icon: image
			  });
			*/

			marker = createMarker(p, map, data, image, i, tourId, trackId);
			markerArray[i] = marker;

			// fit bounds to track
			//map.fitBounds(bounds);
		}
		else {
			markerArray[i] = null;
		}
	  }
	 return bounds;
}
                //--><!]]>

function createMarker(p, map, data, image, i, tourId, trackId) {
  var marker = new google.maps.Marker({
					  position: p,
					  map: map,
					  title: data[i].tourName + " / " + data[i].trackName + " / " + data[i].user,
					  icon: image
				  });
	google.maps.event.addListener(marker, "click", function() {
		//alert(number + " - " + marker.getLatLng() + " - " + marker.getTitle());
		window.location.href = "/track.php?tourId=" + tourId + "&trackId=" + trackId;
	  });
  return marker;
}

function handleClickShowSlopes(checkBoxValue) {
	logDebug("-> handleClickShowSlopes(" + checkBoxValue);
	if (checkBoxValue) {
		logDebug("-> true - " + openskimap);
		map.overlayMapTypes.push(null); // create empty overlay entry
        map.overlayMapTypes.setAt("1",openskimap);
		//map.overlayMapTypes.insertAt(1, openskimap);
	}
	else {
		logDebug("-> false");
		map.overlayMapTypes.clear();
	}
}
                //--><!]]>

</script>

<body onLoad="init();">

<?php include 'bodyHeader.php' ?>

<div id="titleDiv" class="title" style="width: 100%, height: 45px; position: relative; top: 45px;"></div>
<div id="titleDivToolBar" class="toolbar" style="width: 100px; height: 45px; position: absolute; top: 45px; right: 0px;"></div>

<div id="maiwindow" class="maiwindow" style="position: relative; top: 45px; left: 0px; height: 60%">
<table width=100% height=100%>
	<tr height=70%>
		<td width=50% height=100%>
			<DIV id="mapWin" class="window" style="width: 100%; height: 100%; z-index : 3; overflow:hidden;">
				<div id="" class="windowtopbar" style="position: relative; top: 0px; left: 0px; height=15px; width: 100%">
				<table width=100% height=100% cellpadding=0 cellspacing=0 border=0><tr>
					<td class="windowtopbar" width=35%>Map</td>
					<td class="windowtopbar" width=1><img src="images/transp.gif" height=16 width=1></td>
					<form id="showSlopes"><td class="windowtopbar" width=65% align=right valign=middle>
					<input type=checkbox name="showSlopes" onClick="handleClickShowSlopes(this.checked);"> Show ski pistes
					<a href=# onClick="maxMinMap();"><img src="images/help.gif" width=15 height=15 border=0 title="Main window" alt="Main window"></a>
					</td></form></tr>
				</table>
				</div>
				<div id="map" class="innerwindow" style="position: relative; top: 0px; width: 100%; height: 100%; overflow:hidden;">

				</div>
			</div>
		</td>

		<td width=5><img src="images/transp.gif" height=5 width=1></td>

		<td width=50% height=100%>
			<DIV id="summaryWin" class="window" style="width: 100%; height: 20%; z-index : 3; overflow:hidden;">
			<div id="" class="windowtopbar" style="position: relative; top: 0px; left: 0px; height=15px; width: 100%">
			<table width=100% cellpadding=0 cellspacing=0 border=0><tr>
				<td class="windowtopbar" width=35%>Summary</td>
				<td class="windowtopbar" width=1><img src="images/transp.gif" height=16 width=1></td>
				<td class="windowtopbar" width=65% align=right>
				<img src="images/help.gif" width=15 height=15 border=0 title="Main window" alt="Main window">
				</td></tr>
			</table>
			</div>
			<div id="mainWinInner" class="innerwindow" style="position: relative; top: 0px; width: 100%; height: 100%; overflow:hidden;">
				<table width=100% height=60 id="summaryTable" class="summaryTable">
				  <tr>
					<td align=center><img src='images/ajaxLoader.gif'> <p>
					  <p>&nbsp;</td>
				  </tr>
				</table>
			  </div>
			</div>

			<DIV id="tableWin" class="window" style="width: 100%; height: 80%; z-index : 3; overflow:hidden;">
			<div id="" class="windowtopbar" style="position: relative; top: 0px; left: 0px; height=15px; width: 100%">
			<table width=100% cellpadding=0 cellspacing=0 border=0><tr>
				<td class="windowtopbar" width=35%>Tracks</td>
				<td class="windowtopbar" width=1><img src="images/transp.gif" height=16 width=1></td>
				<td class="windowtopbar" width=65% align=right>
				<img src="images/help.gif" width=15 height=15 border=0 title="Main window" alt="Main window">
				</td></tr>
			</table>
			</div>
			<div id="mainWinInner" class="innerwindow" style="position: relative; top: 0px; width: 100%; height: 100%; overflow:scroll;">
				<table width=100% id="tracksTable" class="tracksTable">
				  <tr>
					<td align=center><img src='images/ajaxLoader.gif'> <p>
					  <p>&nbsp;</td>
				  </tr>
				</table>
			  </div>
			</div>
		</td>

	</tr>

	<tr height=30%>
		<td width=100% height=200 colspan=3>

			<DIV id="graphWin" class="window" style="width: 100%; height: 100%; z-index : 3; overflow:hidden;">
				<div id="" class="windowtopbar" style="position: relative; top: 0px; left: 0px; height=15px; width: 100%">
				<table width=100% cellpadding=0 cellspacing=0 border=0><tr>
					<td class="windowtopbar" width=35%>Graph</td>
					<td class="windowtopbar" width=1><img src="images/transp.gif" height=16 width=1></td>
					<form id="typeForm"><td class="windowtopbar" width=65% align=right valign=middle>
						<select id=typeFormMySelectOption onChange="typeFormSel = document.getElementById('typeFormMySelectOption'); timeTypeFormSel = document.getElementById('timeTypeFormMySelectOption'); drawGraph(typeFormSel.options[typeFormSel.selectedIndex].value , timeTypeFormSel.options[timeTypeFormSel.selectedIndex].value);">
							<option value="Duration" selected>Duration</option>
							<option value="Distance">Distance</option>
							<option value="Altitude gain">Altitude gain</option>
						</select>
						<select id=timeTypeFormMySelectOption onChange="typeFormSel = document.getElementById('typeFormMySelectOption'); timeTypeFormSel = document.getElementById('timeTypeFormMySelectOption'); drawGraph(typeFormSel.options[typeFormSel.selectedIndex].value , timeTypeFormSel.options[timeTypeFormSel.selectedIndex].value);">
							<option value="Day" selected>by day</option>
							<option value="Week">by week</option>
							<option value="Total">total</option>
						</select>
					</td></form>
					<td class="windowtopbar" align=right width=15>
					<img src="images/help.gif" width=15 height=15 border=0 title="Main window" alt="Main window">
					</td></tr>
				</table>
				</div>
				<div id="graphWinInner" class="innerwindow" style="position: relative; top: 0px; width: 100%; height: 180; overflow:hidden;">
					<table width=100% class="tracksTable">
					  <tr>
						<td align=center><img src='images/ajaxLoader.gif'> <p>
						  <p>&nbsp;</td>
					  </tr>
					</table>
				</div>
			</div>

		</td>
	</tr>

</table>
</div>

<?php include 'bodyFooter.php' ?>

</body>
</html>
