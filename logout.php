<?php
// Initialize the session.
// If you are using session_name("something"), don't forget it now!
session_start();

// Unset all of the session variables.
$_SESSION = array();

// If it's desired to kill the session, also delete the session cookie.
// Note: This will destroy the session, and not just the session data!
if (isset($_COOKIE[session_name()])) {
    setcookie(session_name(), '', time()-42000, '/');
}

// Finally, destroy the session.
session_destroy();

$url = "/";

?>

<html>
	<head>
		<title></title>
		<link rel=stylesheet type=text/css href=home.css>
		<script>window.location='<?= $url ?>'</script>
	</head>

	<body bgcolor="white">

	<center>
	<p>&nbsp;<p><span class=newstitle>Chargement en cours...</span>
	</center>
	
	</body>
</html>