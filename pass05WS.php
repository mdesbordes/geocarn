<?php include 'inc/common.php' ?><?php
require 'inc/parser.php'; 
//require 'inc/urlLoader.php'; 

ini_set('display_errors', TRUE);

$names = array ("Col de Vars", 
				"Col de l'Izoard", 
				"Col de l'Echelle cot", 
				"Col Agnel", 
				"Col du Granon", 
				"Col de Carab", "Col de la Haute Beaume", "Col du Festre", "Col de Moissi");
$content = '';
//$url = 'http://localhost/test/http/bd_balise-1.xml'; 
$url = 'https://www.inforoute05.fr/wir3/cartes/colFr.html'; 

$content=getUrlContent($url, null, 600); // 10 min cache

$jsonStr = "[";

//$content = file_get_contents($url);
if ($content != null) {
	$content=cleanString($content);
	logDebug(count($names));
	for ($i = 0; $i < count($names); $i++) {		
		logDebug($i . " - " . $names[$i]);
		$name = searchSimpleString($content, $names[$i], "</td><td>");		
		$state = searchString($content, array ($names[$i], "</td><td>"), ">");
		$openPos = strpos($state, "green");
		$closePos = strpos($state, "ifr");
		if ($openPos > -1)
			$status = "open";
		else if ($closePos > -1)
			$status = "close";
		else
			$status = "unknown";
		logDebug($name . " - " . $state . " - " . $status);
		if ($i > 0)
			$jsonStr .= ",\n";
		$jsonStr .= "{\"name\" : \"$name\", \"status\" : \"$status\"}";
	}
	
}
else {
}

$jsonStr .= "]";

$output = utf8_encode($jsonStr);
header('Content-type: text/json; charset=UTF-8', true);
?><?= $output ?><?php exit; ?>
