<?php include 'inc/common.php' ?>
<?php

function createTourAndTrack($db, $activity, $tourname, $tourId, $trackname, $country, $planned, $date, $track, $latInit, $lonInit, $altdiff, $altgain, $altmin, $altmax, $altstart, $timediff, $timeup, $distance, $averagespeed, $averageupspeed) {
	logDebug("createTourAndTrack(" . $activity . ", " . $tourname . ", " . $tourId . ", " . $trackname . ", " . $country. ", " . $date . ", " . $altdiff . ", " . $planned);
	
	$rtn = array();
	$result = 1;

	$user = $_SESSION['user'];

	if ($tourId > 0) {
		$request1 = "UPDATE TRACKER_TOUR SET DATETIME = '$date' WHERE USER_ID = '$user' AND ID = '$tourId';";
		logDebug($request1);
		$result = mysqli_query($db, $request1);
		logDebug($result );
	}
	else {
		logDebug($tourname);
		$tourname = mysqli_real_escape_string($db,$tourname);
		logDebug($tourname);
		mysqli_set_charset($db, "SET NAMES UTF8");
		$request1 = "INSERT INTO TRACKER_TOUR (NAME, DATETIME, USER_ID, DESCRIPTION, COUNTRY, ACTIVITY_ID, LATITUDE, LONGITUDE) VALUES ('$tourname', '$date', '$user', 'Description', '$country', '$activity', '$latInit', '$lonInit');";
		logDebug($request1);
		$result = mysqli_query($db, $request1);
		logDebug($result );
		$tourId = mysqli_insert_id($db);
	}

	if ($result == 1) {
		//$request = "SELECT ID FROM TRACKER_TOUR WHERE USER_ID='$user' AND NAME=''";
		//$result = mysql_query($request,$db);
		// $altdiff, $altgain, $altmin, $altmax, $altstart, $timediff, $distance
		logDebug($trackname);
		$trackname = mysqli_real_escape_string($db,$trackname);
		logDebug($trackname);
		mysqli_set_charset($db, "SET NAMES UTF8");
		$request = "insert into TRACKER_TRACK (TOUR_ID, DATETIME, NAME, TRACK, DURATION, DURATION_UP, DISTANCE, ALT_DIFF, ALT_GAIN, ALT_MIN, ALT_MAX, ALT_START, AVERAGE_SPEED, AVERAGE_UP_SPEED, START_LATITUDE, START_LONGITUDE, PLANNED_ONLY) values ($tourId, '$date', '$trackname', '$track', '$timediff', '$timeup', '$distance', '$altdiff', '$altgain', '$altmin', '$altmax', '$altstart', '$averagespeed', '$averageupspeed', '$latInit', '$lonInit', '$planned')";
		logDebug($request);
		$result = mysqli_query($db, $request);
		logDebug($result);
		if ($result != 1) {
			$rtn['error'] = "Creating track: " . $date . " " . $trackname . "[" . $tourId . "]";
		}
		else {
			$trackId= mysqli_insert_id($db);
			$rtn['error'] = "";
			$rtn['user'] = $user;
			$rtn['tourId'] = $tourId;
			$rtn['trackId'] = $trackId;
		}
	}
	else {
		$rtn['error'] = "Creating tour: " . $tourname;
	}
	return $rtn;
}


$num = $_POST['num'];
//$tourname = urldecode($_POST['tourname']);
//$trackname = urldecode($_POST['trackname']);
$tourname = $_POST['tourname'];
$trackname = $_POST['trackname'];
$tour = $_POST['tour'];
$activity = $_POST['activity'];
$country = $_POST['country'];
$planned = $_POST['planned'];
$date = $_POST['date'];
$altdiff = $_POST['altdiff'];
$altgain = $_POST['altgain'];
$altmin = $_POST['altmin'];
$altmax = $_POST['altmax'];
$altstart = $_POST['altstart'];
$timediff = $_POST['timediff'];
$timeup = $_POST['timeup'];
$distance = $_POST['distance'];
$averagespeed = $_POST['averagespeed'];
$averageupspeed = $_POST['averageupspeed'];
$latlonarray = $_POST['latlonarray'];

$track = $latlonarray;
$trackstr = "";
/*
$minTime = $_POST["time0"];
for ($i = 0; $i < $num; $i++) {
	$lat = $_POST["lat" . $i];
	$lat = round($lat, 4);
	$lon = $_POST["lon" . $i];
	$lon = round($lon, 4);
	if ($i == 0) {
		$latInit = $lat;
		$lonInit = $lon;
	}
	$ele = round($_POST["ele" . $i]);
	$timePost = $_POST["time" . $i];
	if ($i > 0)
		$time = $timePost - $minTime;
	else
		$time = $minTime;
	$track .= $lat . ";" . $lon  . ";" . $ele  . ";" . $time . "|";
}
*/

$trackExplode = explode('|', $track);
for ($i = 0; $i < (count($trackExplode)-1); $i++) {
	$point = $trackExplode[$i];
	$points[$i] = explode(';', $point);
	$lat = $points[$i][0];
	$lat = round($lat, 4);
	$lon = $points[$i][1];
	$lon = round($lon, 4);
	$ele = round($points[$i][2]);
	$time = $points[$i][3];
	if ($i == 0) {
		$latInit = $lat;
		$lonInit = $lon;
	}
	$timePost = $time;
	if ($i > 0)
		$time = $timePost - $minTime;
	else {
		$minTime = $timePost;
		$time = $timePost;
	}
	$trackstr .= $lat . ";" . $lon  . ";" . $ele  . ";" . $time . "|";
}



$success = 0;
$msg = "Track created succesfully";
$detail = "";

/*
logDebug("planned = " . $planned);
if ($planned == "true") {
	$planned = 1;
}
else {
	$planned = 0;
}
*/
logDebug("planned = " . $planned);

$rtn = createTourAndTrack($db, $activity, $tourname, $tour, $trackname, $country, $planned, $date, $trackstr, $latInit, $lonInit, $altdiff, $altgain, $altmin, $altmax, $altstart, $timediff, $timeup, $distance, $averagespeed, $averageupspeed);
if ($rtn['error'] == "") {
	$success = 0;
	$msg = "Track created succesfully";
	$detail = "";
	$url = "/user/" . $rtn['user'] . "/tour/" . $rtn['tourId'] . "/track/" . $rtn['trackId'] . "/";
}
else {
	$success = 1;
	$msg = "Error creating track";
	$detail = $rtn['error'];
	$url = "";
}
header('Content-type: text/xml;  application/json', true);?>
{
	"track":
	{
		"tourId": "<?=$rtn['tourId']?>",
		"trackId": "<?=$rtn['trackId']?>"
	},
	"error":
	{
		"success": "<?=$success?>",
		"msg": "<?=$msg?>",
		"detail": "<?=$rtn['error']?>"
	}
}<?php exit; ?>
