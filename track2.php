<?php include 'header.php' ?>

<script>
lang = "fr";

var trackId = <?= $_GET['trackId']; ?>;
var tourId = -1;
var tourLat;
var tourLon;
var trackPointsLength = -1;

var MA_CLE= "rhfiamuq2cejl2h4xgcmwpge";

function init() {
	$.getJSON("tracksWS.php?track=<?= $_GET['trackId']; ?>" , function(data) {
		tourId = data.tracks[0].tourId;
		tourLat = data.tracks[0].latitude;
		tourLon = data.tracks[0].longitude;
		trackPointsLength = data.tracks[0].trackPoints;
		setTitle(data.tracks);
		drawTable(data.tracks);
		setImg(data.tracks);
		//initGeoPortail();
		initMap();
		initTrackMap();
		document.getElementById("map").style.zIndex = 10;
		document.getElementById("map").style.width = 800px;
	});
	
}

function setTitle(data) {
	title = "<img src='/images/" + data[0].activityId + ".png' width=45 height=45> <a href=/tour.php?tourId=" + data[0].tourId + ">" + data[0].tourName + "</a>";
	//$("#titleDiv").html(title);
	
	//title = "<img src='/images/" + data[0].activityId + ".png'>";
	title = title + "<div class='subtitle' style='position: absolute; right: 100px; display: inline;'>";
	if (data[0].trackName != "")
		title = title + " &nbsp;&nbsp; <div class='titleTrack'>" + data[0].trackName + "</div>";
	title = title + " &nbsp;&nbsp; <div class='titleDate'>" + shortDate(data[0].trackDate, 10) + "</div>";
	title = title + " &nbsp;&nbsp; <a href='/gpx.php?tourId=" + tourId + "&trackId=" + trackId + "'><img src='/images/gpx.gif' width=20 border=0></a>";
	title = title + " &nbsp;&nbsp; <a href='/kml.php?tourId=" + tourId + "&trackId=" + trackId + "'><img src='/images/googleearth.gif' width=20 border=0></a>";
	title = title + "</div>";
	$("#titleDiv").html(title);
}

function setImg(data) {
	img = "<img src='imgAltitude.php?tourId=" + tourId + "&trackId=" + trackId + "&rel=1'>";
	$("#speedWinInner").html(img);
}

function drawTable(data) {
	for (var i = 0; i < data.length; i++) {
        drawRow(i, data[i]);
    }
}

function drawRow(rowId, rowData) {
	var c = [];
	
	//var row = $("<tr />");
    //$("#tracksTable").append(row); //this will append tr element to table... keep its reference for a while since we will add cels into it
    c.push("<tr><td align=center colspan=2>Distance</td></tr>");
	c.push("<tr><td align=center class='dataBig' colspan=2>" + rowData.trackDistance + " km</td></tr>");
	c.push("<tr><td align=center>Average speed</td><td align=center>Duration</td></tr>");
	c.push("<tr><td align=center class='dataBig'>" + rowData.trackAverageSpeed + " km/h</td><td align=center class='dataBig'>" + rowData.trackDuration + "</td></tr>");
	c.push("<tr><td align=center>Average up speed</td><td align=center>Altitude difference</td></tr>");
	c.push("<tr><td align=center class='data'>" + rowData.trackAverageUpSpeed + " m/h</td><td align=center class='data'>" + rowData.trackAltDiff + " m</td></tr>");
	c.push("<tr><td align=center>Altitude min</td><td align=center>Altitude max</td></tr>");
	c.push("<tr><td align=center class='data'>" + rowData.trackAltMin + " m</td><td align=center class='data'>" + rowData.trackAltMax + " m</td></tr>");
	$("#tracksTable").html(c.join(""));
}

var iv= null;
var viewer=null;

// Definition url des services Geoportail
function geoportailLayer(name, key, layer, options)
{ var l= new google.maps.ImageMapType
  ({ getTileUrl: function (coord, zoom)
      {  return "http://wxs.ign.fr/" + key + "/geoportail/wmts?LAYER=" + layer
          + "&EXCEPTIONS=text/xml"
          + "&FORMAT="+(options.format?options.format:"image/jpeg")
          + "&SERVICE=WMTS&VERSION=1.0.0&REQUEST=GetTile"
          + "&STYLE="+(options.style?options.style:"normal")+"&TILEMATRIXSET=PM"
          + "&TILEMATRIX=" + zoom
          + "&TILECOL=" + coord.x + "&TILEROW=" + coord.y;
      },
    tileSize: new google.maps.Size(256,256),
    name: name,
    minZoom: (options.minZoom ? options.minZoom:0),
    maxZoom: (options.maxZoom ? options.maxZoom:18)
  });
  l.attribution = ' &copy; <a href="http://www.ign.fr/">IGN-France</a>';
  return l;
}
// Ajout de l'attribution Geoportail a la carte
function geoportailSetAttribution (map, attributionDiv)
{ if (map.mapTypes.get(map.getMapTypeId()).attribution)
  {  attributionDiv.style.display = 'block';
    attributionDiv.innerHTML = map.mapTypes.get(map.getMapTypeId()).name
      +map.mapTypes.get(map.getMapTypeId()).attribution;
  }
  else attributionDiv.style.display = 'none';            
}
var map;
// Initialisation de la carte
function initMap()
{ // La carte Google
  map = new google.maps.Map( document.getElementById('map'),
  {  mapTypeId: 'carte',
    streetViewControl: false,
    mapTypeControlOptions: { mapTypeIds: ['carte', google.maps.MapTypeId.TERRAIN, google.maps.MapTypeId.SATELLITE] },
    center: new google.maps.LatLng(tourLat, tourLon),
    zoom: 12
  });
 
  /** Definition des couches  */
  // Carte IGN
  map.mapTypes.set('carte', geoportailLayer("Carte IGN", MA_CLE, "GEOGRAPHICALGRIDSYSTEMS.MAPS", { maxZoom:18 }));
  // Ajouter un control pour l'attribution
  var attributionDiv = document.createElement('div');
  attributionDiv.className = "attribution";
  geoportailSetAttribution(map, attributionDiv);
  map.controls[google.maps.ControlPosition.BOTTOM_RIGHT].push(attributionDiv);
  // Afficher / masquer le copyright en fonction de la couche
  google.maps.event.addListener(map, 'maptypeid_changed',
    function()
    {  geoportailSetAttribution(this, attributionDiv);
    });
  
}
//google.maps.event.addDomListener(window, 'load', initMap);  
		
function initTrackMap() {
var bikeLayer = new google.maps.BicyclingLayer();
	  bikeLayer.setMap(map);
	
	map.setCenter(new google.maps.LatLng(tourLat, tourLon));
	
	if (trackPointsLength > 0) {
		$.ajax({
		  type: "GET",
		  url: "http://geocarn-marcdesbordes.rhcloud.com/gpx.php?tourId=" + tourId + "&trackId=" + trackId,
		  dataType: "xml",
		  success: function(xml) {
			var points = [];
			var bounds = new google.maps.LatLngBounds ();
			$(xml).find("trkpt").each(function() {
			  var lat = $(this).attr("lat");
			  var lon = $(this).attr("lon");
			  var p = new google.maps.LatLng(lat, lon);
			  points.push(p);
			  bounds.extend(p);
			});

			var poly = new google.maps.Polyline({
			  // use your own style here
			  path: points,
			  strokeColor: colorTrackArray[i],
			  strokeOpacity: .8,
			  strokeWeight: 4,
			});
			logDebug(poly);
			
			poly.setMap(map);
			
			// fit bounds to track
			map.fitBounds(bounds);
		  }
		});
	}
}
		
</script>

<body onLoad="init();">

<?php include 'bodyHeader.php' ?>



<div id="titleDiv" class="title" style="height: 45px; position: relative; left: 0px; right: 100px; top: 45px;"></div>
<div id="titleDiv" class="toolbar" style="width: 100px; height: 45px; position: absolute; top: 45px; right: 0px;">
<a href=# onClick='initGeoPortail()'>[GEO]</a>
<a href=# onClick='initGoogleMaps()'>[GOOGLE]</a>
</div>

<div id="maiwindow" class="maiwindow" style="position: relative; top: 45px; left: 0px; ">
<table width=100% height=80%>
	<tr height=100%>
		<td width=65% height=100%>
			<DIV id="mainWin" class="window" style="width: 100%; height: 100%; z-index : 3; overflow:hidden;">
			<div id="" class="windowtopbar" style="position: relative; top: 0px; left: 0px; height=15px; width: 100%">
			<table width=100% cellpadding=0 cellspacing=0 border=0><tr>
				<td class="windowtopbar" width=35%>Main</td>
				<td class="windowtopbar" width=1><img src="images/transp.gif" height=16 width=1></td>
				<td class="windowtopbar" width=65% align=right>
				<img src="images/help.gif" width=15 height=15 border=0 title="Main window" alt="Main window">
				</td></tr>
			</table>
			</div>
			<div id="map" class="innerwindow" style="position: relative; top: 20px; width: 100%; height: 100% overflow:scroll;"> 
				
			  </div>
			</div>
		</td>
		
		<td width=5><img src="images/transp.gif" height=680 width=1></td>
		
		<td width=35% height=100%> 
			<DIV id="mainWin" class="window" style="width: 100%; height: 100%; z-index : 3; overflow:hidden;">
			<div id="" class="windowtopbar" style="position: relative; top: 0px; left: 0px; height=15px; width: 100%">
			<table width=100% cellpadding=0 cellspacing=0 border=0><tr>
				<td class="windowtopbar" width=35%>Main</td>
				<td class="windowtopbar" width=1><img src="images/transp.gif" height=16 width=1></td>
				<td class="windowtopbar" width=65% align=right>
				<img src="images/help.gif" width=15 height=15 border=0 title="Main window" alt="Main window">
				</td></tr>
			</table>
			</div>
			<div id="mainWinInner" class="innerwindow" style="position: relative; top: 20px; width: 100%; height: 100% overflow:scroll;"> 
				<div id="subTitleDiv" style="width: 100%, height: 50px"></div>
				
				<table width=100% id="tracksTable" class="window">
				  <tr> 
					<td align=center><img src='images/ajaxLoader.gif'> <p>
					  <p>&nbsp;</td>
				  </tr>
				</table>
				
				<div id="speedWinInner" style="width: 100%, height: 250px"></div>
			  </div>
			</div>
			
			
		</td>

	</tr>
</table>
</div>

<?php include 'bodyFooter.php' ?>

</body>
</html>
