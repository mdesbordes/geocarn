<?php include 'inc/connectDb.php' ?>
<?php include 'inc/logger.php' ?>
<?php
function getLatestTracks($db, $user, $categoryId, $limit, $planned) {
	logDebug('-> getLatestTracks(' . $user .", " . $categoryId .", " . $limit .", " . $planned);
	global $ACTIVITY;

	$request = "";
	$request = "SELECT TRACKER_TOUR.ACTIVITY_ID activityId, TRACKER_TOUR.COUNTRY country, TRACKER_TOUR.USER_ID user, TRACKER_TRACK.DATETIME trackDate, TRACKER_TRACK.ID trackId, TRACKER_TRACK.TOUR_ID tourId, TRACKER_TOUR.NAME tourName, TRACKER_TOUR.LATITUDE latitude, TRACKER_TOUR.LONGITUDE longitude, TRACKER_TRACK.START_LATITUDE startlatitude, TRACKER_TRACK.START_LONGITUDE startlongitude, TRACKER_TRACK.NAME trackName, TRACKER_TRACK.DURATION trackDuration, TRACKER_TRACK.DISTANCE trackDistance, TRACKER_TRACK.ALT_GAIN trackAltGain, CHAR_LENGTH(TRACKER_TRACK.TRACK) trackLength FROM TRACKER_TRACK join TRACKER_TOUR on  TRACKER_TRACK.TOUR_ID=TRACKER_TOUR.ID ";
	if ($user != null && $user != "") {
		$request .= " where TRACKER_TOUR.USER_ID = '$user'";
		if ($categoryId != null && $categoryId > -1)
			$request .= " AND TRACKER_TOUR.ACTIVITY_ID = '$categoryId'";
	}
	else if ($categoryId != null && $categoryId > -1)
		$request .= " where TRACKER_TOUR.ACTIVITY_ID = '$categoryId'";
	$request .= " AND TRACKER_TRACK.PLANNED_ONLY = $planned";
	$request .= " order by TRACKER_TRACK.DATETIME DESC LIMIT $limit";
	logDebug($request);
	mysqli_set_charset($db, "SET NAMES UTF8");
	//$result = mysql_query($request,$db);
	$result = mysqli_query($db, $request);
	
	$rows = array();
	if (isset($result)) {
		logDebug("->result ok");
		while($r = mysqli_fetch_assoc($result)) {
			logDebug("- " . $r['tourId'] . " - " . $r['trackId'] . " - " . $r['trackName']);
			$rows['tracks'][] = $r;
		}
	}
	else {
		logDebug("->result ko");
		logError("Error: " . mysqli_error($db));
	}
	return json_encode($rows);
}

function getSeasonTracksRequest($year, $month, $categoryId, $type, $user, $green) {
	logDebug('-> getSeasonTracksRequest(' . $year .", " . $month .", " . $categoryId .", " . $type .", " . $user .", " . $green);
	if ($type == "SUMMER") {
		$startDate = $year . "-01-01";
		$endDate = $year . "-12-31";
		$condition = " (TRACKER_TOUR.ACTIVITY_ID != 1 AND TRACKER_TOUR.ACTIVITY_ID != 2 AND TRACKER_TOUR.ACTIVITY_ID != 6) ";
	}
	else {
		if ($month < 8)
			$year = $year -1;
		$startDate = $year . "-08-01";
		$endDate = ($year+1) . "-07-31";
		$condition = " (TRACKER_TOUR.ACTIVITY_ID = 1 OR TRACKER_TOUR.ACTIVITY_ID = 2 OR TRACKER_TOUR.ACTIVITY_ID = 6) ";
	}

		/*
	$request = "SELECT TRACKER_TOUR.ACTIVITY_ID activityId, SUM(1) activitySum, SUM(DISTANCE) activityDist, SUM(ALT_DIFF) activityAltDiff, round(SUM(TIME_TO_SEC(duration))/60) summin FROM TRACKER_TRACK join TRACKER_TOUR on TRACKER_TRACK.TOUR_ID=TRACKER_TOUR.ID where TRACKER_TRACK.DATETIME >= '$startDate' and TRACKER_TRACK.DATETIME <= '$endDate'";
	$request .= " AND $condition";
	if ($user != null && $user != "")
		$request .= " AND TRACKER_TOUR.USER_ID = '$user'";
	if ($activity > 0)
		$request .= " AND TRACKER_TOUR.ACTIVITY_ID = $activity";
	$request .= " group by TRACKER_TOUR.ACTIVITY_ID ";
	$request .= " order by summin desc";
	*/

	$request = "SELECT TRACKER_TOUR.ACTIVITY_ID activityId, TRACKER_TOUR.COUNTRY country, TRACKER_TOUR.USER_ID user, TRACKER_TRACK.DATETIME trackDate, TRACKER_TRACK.ID trackId, TRACKER_TRACK.TOUR_ID tourId, TRACKER_TOUR.NAME tourName, TRACKER_TOUR.LATITUDE latitude, TRACKER_TOUR.LONGITUDE longitude, TRACKER_TRACK.START_LATITUDE startlatitude, TRACKER_TRACK.START_LONGITUDE startlongitude, TRACKER_TRACK.NAME trackName, TRACKER_TRACK.DURATION trackDuration, TRACKER_TRACK.DISTANCE trackDistance, TRACKER_TRACK.ALT_GAIN trackAltGain FROM TRACKER_TRACK join TRACKER_TOUR on  TRACKER_TRACK.TOUR_ID=TRACKER_TOUR.ID ";
	// TODO Security Issue
	if ($user != null && $user != "")
		$request .= " where TRACKER_TOUR.USER_ID = '$user'";
	if ($categoryId > -1)
		$request .= " AND TRACKER_TOUR.ACTIVITY_ID = '$categoryId'";
	$request .= " AND TRACKER_TRACK.DATETIME >= '$startDate' and TRACKER_TRACK.DATETIME <= '$endDate'";
	$request .= " AND $condition";
	$request .= " AND TRACKER_TRACK.PLANNED_ONLY = 0 ";	
	if ($green == 1)
		$request .= " AND TRANSPORT = 1";
	$request .= " order by TRACKER_TRACK.DATETIME DESC";

	logDebug($request);
	return $request;
}

function getSeasonTracks($db, $user, $categoryId, $limit, $year, $month, $green) {
	logDebug('-> getSeasonTracks(' . $user);
	global $ACTIVITY;

	mysqli_set_charset($db, "SET NAMES UTF8");

	$request = getSeasonTracksRequest($year, $month, $categoryId, "SUMMER", $user, $green);
	$resultSummer = mysqli_query($db, $request);

	$request = getSeasonTracksRequest($year, $month, $categoryId, "WINTER", $user, $green);
	$resultWinter = mysqli_query($db, $request);

	$rows = array();

	while($r = mysqli_fetch_assoc($resultWinter)) {
		$rows['tracks'][] = $r;		
	}
	while($r = mysqli_fetch_assoc($resultSummer)) {
		$rows['tracks'][] = $r;
	}

	return json_encode($rows);
}

function getAllSeasonTracks($db, $user, $limit, $year, $month, $green) {
	logDebug('-> getAllSeasonTracks(' . $user);
	global $ACTIVITY;

	$startDate = $year . "-01-01";
	$endDate = $year . "-12-31";

	$request = "SELECT TRACKER_TOUR.ACTIVITY_ID activityId, TRACKER_TOUR.COUNTRY country, TRACKER_TOUR.USER_ID user, TRACKER_TRACK.DATETIME trackDate, TRACKER_TRACK.ID trackId, TRACKER_TRACK.TOUR_ID tourId, TRACKER_TOUR.NAME tourName, TRACKER_TOUR.LATITUDE latitude, TRACKER_TOUR.LONGITUDE longitude, TRACKER_TRACK.START_LATITUDE startlatitude, TRACKER_TRACK.START_LONGITUDE startlongitude, TRACKER_TRACK.NAME trackName, TRACKER_TRACK.DURATION trackDuration, TRACKER_TRACK.DISTANCE trackDistance, TRACKER_TRACK.ALT_GAIN trackAltGain FROM TRACKER_TRACK join TRACKER_TOUR on  TRACKER_TRACK.TOUR_ID=TRACKER_TOUR.ID ";
	$request .= " where TRACKER_TOUR.USER_ID = '$user'";
	$request .= " AND TRACKER_TRACK.DATETIME >= '$startDate' and TRACKER_TRACK.DATETIME <= '$endDate'";
	if ($green == 1)
		$request .= " AND TRANSPORT = 1";
	$request .= " order by TRACKER_TRACK.DATETIME DESC";

	logDebug($request);

	mysqli_set_charset($db, "SET NAMES UTF8");
	$result = mysqli_query($db, $request);

	$rows = array();
	if (isset($result)) {
		while($r = mysqli_fetch_assoc($result)) {
			$rows['tracks'][] = $r;
		}
	}

	return json_encode($rows);
}

function getLatestTracksInMap($db, $user, $categoryId, $limit, $minLat, $minLong, $maxLat, $maxLong, $planned) {
	logDebug('-> getLatestTracksInMap(' . $user);
	global $ACTIVITY;

	$request = "";
	$request = "SELECT TRACKER_TOUR.ACTIVITY_ID activityId, TRACKER_TOUR.COUNTRY country, TRACKER_TOUR.USER_ID user, TRACKER_TRACK.DATETIME trackDate, TRACKER_TRACK.ID trackId, TRACKER_TRACK.TOUR_ID tourId, TRACKER_TOUR.NAME tourName, TRACKER_TOUR.LATITUDE latitude, TRACKER_TOUR.LONGITUDE longitude, TRACKER_TRACK.START_LATITUDE startlatitude, TRACKER_TRACK.START_LONGITUDE startlongitude, TRACKER_TRACK.NAME trackName, TRACKER_TRACK.DURATION trackDuration, TRACKER_TRACK.DISTANCE trackDistance, TRACKER_TRACK.ALT_GAIN trackAltGain FROM TRACKER_TRACK join TRACKER_TOUR on  TRACKER_TRACK.TOUR_ID=TRACKER_TOUR.ID";
	$request .= " WHERE TRACKER_TRACK.START_LATITUDE > $minLat AND TRACKER_TRACK.START_LATITUDE < $maxLat AND TRACKER_TRACK.START_LONGITUDE > $minLong AND TRACKER_TRACK.START_LONGITUDE < $maxLong";
	if ($user != null && $user != "") {
		$request .= " AND TRACKER_TOUR.USER_ID = '$user'";
		if ($categoryId != null && $categoryId > 0)
			$request .= " AND TRACKER_TOUR.ACTIVITY_ID = '$categoryId'";
	}
	else if ($categoryId != null && $categoryId > 0)
		$request .= " where TRACKER_TOUR.ACTIVITY_ID = '$categoryId'";
	$request .= " AND TRACKER_TRACK.PLANNED_ONLY = $planned";
	$request .= " order by TRACKER_TRACK.DATETIME DESC LIMIT $limit";
	logDebug($request);
	mysqli_set_charset($db, "SET NAMES UTF8");
	$result = mysqli_query($db, $request);

	$rows = array();
	if (isset($result)) {
		while($r = mysqli_fetch_assoc($result)) {
			$rows['tracks'][] = $r;
		}
	}

	return json_encode($rows);
}

function deleteTrack($db, $trackId, $user) {
	logDebug('-> deleteTrack(' . $trackId .", ". $user);
	global $ACTIVITY;

	$request = "";
	$request = "DELETE from TRACKER_TRACK where ID=" . $trackId;
	logDebug($request);
	mysqli_set_charset($db, "SET NAMES UTF8");
	$result = mysqli_query($db, $request);

	$rows = array();
	return json_encode($rows);
}

function updateTrack($db, $trackId, $fieldName, $fieldValue, $user) {
	logDebug('-> updateTrack(' . $trackId .", ". $fieldName .", ". $fieldValue .", ". $user);
	logDebug('USERNAME = ' . $_SESSION['user']);

	$fieldValue = mysqli_real_escape_string($db, $fieldValue);

	if ($trackId > 0 && $_SESSION['user'] == $user) {
		$request = "UPDATE TRACKER_TRACK SET $fieldName = '$fieldValue' WHERE ID = " . $trackId;

		logDebug($request);
		mysqli_set_charset($db, "SET NAMES UTF8");
		$result = mysqli_query($db, $request);

		$rows = array();
		
		return json_encode($rows);
	} else {
		return 0;
	}
}

function getPhotoForTrack($db, $trackId) {
	logDebug('-> getPhotoForTrack(' . $trackId);
	global $ACTIVITY;

	$request = "";
	$request = "SELECT TRACKER_IMAGE.ID id, TRACKER_IMAGE.USER_ID user, TRACKER_IMAGE.LATITUDE latitude, TRACKER_IMAGE.LONGITUDE longitude, TRACKER_IMAGE.DATETIME imageDate, TRACKER_IMAGE.URL url, TRACKER_IMAGE.WIDTH width, TRACKER_IMAGE.HEIGHT height FROM TRACKER_IMAGE WHERE TRACKER_IMAGE.TRACK_ID = " . $trackId;
	logDebug($request);
	mysqli_set_charset($db, "SET NAMES UTF8");
	$result = mysqli_query($db, $request);

	$rows = array();
	if (isset($result)) {
		while($r = mysqli_fetch_assoc($result)) {
			$rows['photos'][] = $r;
		}
	}
	
	return json_encode($rows);
}

function getTrack($db, $trackId) {
	logDebug('-> getTrack(' . $trackId);
	global $ACTIVITY;

	$request = "SELECT TRACKER_TOUR.ACTIVITY_ID activityId, TRACKER_TOUR.COUNTRY country, TRACKER_TOUR.USER_ID user, TRACKER_TOUR.LATITUDE latitude, TRACKER_TOUR.LONGITUDE longitude, TRACKER_TRACK.DATETIME trackDate, UNIX_TIMESTAMP(TRACKER_TRACK.DATETIME) trackTimestamp, TRACKER_TRACK.ID trackId, TRACKER_TRACK.TOUR_ID tourId, TRACKER_TOUR.NAME tourName, TRACKER_TRACK.NAME trackName, TRACKER_TRACK.DURATION trackDuration, TRACKER_TRACK.DURATION_UP trackDurationUp, TRACKER_TRACK.DISTANCE trackDistance, TRACKER_TRACK.AVERAGE_SPEED trackAverageSpeed, TRACKER_TRACK.AVERAGE_UP_SPEED trackAverageUpSpeed, TRACKER_TRACK.ALT_GAIN trackAltGain, TRACKER_TRACK.ALT_DIFF trackAltDiff, TRACKER_TRACK.ALT_MAX trackAltMax, TRACKER_TRACK.ALT_MIN trackAltMin, TRACKER_TRACK.ALT_START trackAltStart, TRACKER_TRACK.DESCRIPTION description, TRACKER_TRACK.PLANNED_ONLY planned, TRACKER_TRACK.TRANSPORT transport, OCTET_LENGTH(TRIM(TRACKER_TRACK.TRACK)) trackPoints FROM TRACKER_TRACK join TRACKER_TOUR on TRACKER_TRACK.TOUR_ID=TRACKER_TOUR.ID AND TRACKER_TRACK.ID = " . $trackId;
	logDebug($request);
	mysqli_set_charset($db, "SET NAMES UTF8");
	//$result = mysql_query($request,$db);
	$result = mysqli_query($db, $request);
	
	$rows = array();
	if (isset($result)) {
		while($r = mysqli_fetch_assoc($result)) {
			$rows['tracks'][] = $r;
		}
	}

	return json_encode($rows);
}

// --- Begin ---
logDebug("-> tracksWS.php");

if (isset($_GET['user']))
	$user = $_GET['user'];
else
	$user = null;
if (isset($_GET['action']))
	$action = $_GET['action'];
else 
	$action = null;
if (isset($_GET['track']))
	$trackReq = $_GET['track'];
else
	$trackReq = null;
if (isset($_GET['planned']))
	$planned = $_GET['planned'];
else
	$planned = null;
if (isset($_GET['activity']))
	$categoryId = $_GET['activity'];
else
	$categoryId = null;
if (isset($_GET['limit']))
	$limitReq = $_GET['limit'];
else
	$limitReq = 50;
if (isset($_GET['minLat']) && isset($_GET['minLong']) && isset($_GET['maxLat']) && isset($_GET['maxLong'])) {
	$minLat = $_GET['minLat'];
	$minLong = $_GET['minLong'];
	$maxLat = $_GET['maxLat'];
	$maxLong = $_GET['maxLong'];
}
if (isset($_GET['year']) && isset($_GET['month'])) {
	$year = $_GET['year'];
	$month = $_GET['month'];
}
if (isset($_GET['fieldname']) && isset($_GET['fieldvalue'])) {
	$fieldName = $_GET['fieldname'];
	$fieldValue = $_GET['fieldvalue'];
}
logDebug("_SESSION['user'] = " . $_SESSION['user']);
logDebug("action = $action");
logDebug("user = $user");
logDebug("categoryId = $categoryId");
logDebug("limitReq = $limitReq");
if ($limitReq > 0)
	$limit=$limitReq;
else
	$limit=50;
logDebug("limit = $limit");
if (isset($_GET['green']))
	$green = $_GET['green'];
else
	$green = 0;

$user = $_SESSION['user'];

if ($action == "DEL")
	$tracksArray = deleteTrack($db, $trackReq, $user);
if ($action == "UPDATE")
	$tracksArray = updateTrack($db, $trackReq, $fieldName, $fieldValue, $user);
else if ($trackReq == "latest")
	$tracksArray = getLatestTracks($db, $user, $categoryId, $limit, $planned);
else if ($trackReq == "map")
	$tracksArray = getLatestTracksInMap($db, $user, $categoryId, $limit, $minLat, $minLong, $maxLat, $maxLong, $planned);
else if ($trackReq == "season" && $categoryId > -1)
	$tracksArray = getSeasonTracks($db, $user, $categoryId, $limit, $year, $month, $green);
else if ($trackReq == "season")
	$tracksArray = getAllSeasonTracks($db, $user, $limit, $year, $month, $green);
else if ($action == "photo")
	$tracksArray = getPhotoForTrack($db, $trackReq);
else
	$tracksArray = getTrack($db, $trackReq);

	//getLatestTracksInMap($db, $limit, $minLat, $minLong, $maxLat, $maxLong)
logDebug($tracksArray);

header('Content-type: text/xml;  application/json', true);
?><?= $tracksArray ?><?php exit; ?>