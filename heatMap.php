<?php include 'header.php' ?>
<?php include 'inc/country.php' ?>

<script>
activityId = "<?= $_GET['activityId']; ?>";
</script>

<script>
function getGradientArray(gradientId) {
	if (gradientId == 0) {
		gradient = [
			'rgba(0, 255, 255, 0)',
			'rgba(0, 255, 255, 1)',
			'rgba(0, 191, 255, 1)',
			'rgba(0, 127, 255, 1)',
			'rgba(0, 63, 255, 1)',
			'rgba(0, 0, 255, 1)',
			'rgba(0, 0, 223, 1)',
			'rgba(0, 0, 191, 1)',
			'rgba(0, 0, 159, 1)',
			'rgba(0, 0, 127, 1)',
			'rgba(63, 0, 91, 1)',
			'rgba(127, 0, 63, 1)',
			'rgba(191, 0, 31, 1)',
			'rgba(255, 0, 0, 1)'
		  ];
	}
	else if (gradientId == 1) {
		gradient = [
			'rgba(255,247,243, 0)',
			'rgba(253,224,221, 1)',
			'rgba(252,197,192, 1)',
			'rgba(250,159,181, 1)',
			'rgba(247,104,161, 1)',
			'rgba(221,52,151, 1)',
			'rgba(174,1,126, 1)',
			'rgba(122,1,119, 1)'
		  ];
	}
	else {
		gradient = [
			'rgba(255,255,204, 0)',
			'rgba(255,237,160, 1)',
			'rgba(254,217,118, 1)',
			'rgba(254,178,76, 1)',
			'rgba(253,141,60, 1)',
			'rgba(252,78,42, 1)',
			'rgba(227,26,28, 1)',
			'rgba(177,0,38, 1)'
		  ];
	}
	return gradient;
}

var map, heatmap;

var pointArray =  [];

var radius = 20;
var opacity = 0.8;
var gradientId = 0;

function init() {
	setTitle();
	
	initMap();
	
	//changeOpacity();
	//changeRadius();
	//changeGradient();
	
}

function setTitle() {
	title = "<a href=# onClick='handleClickCategory(-1); return false;'><img src='/images/all.png' width=45 height=45></a>&nbsp;&nbsp;";
	for (i = 0; i <= 15; i++)
		title = title + "<a href=# onClick='handleClickCategory(" + i + "); return false;'><img src='/images/" + i + ".png' width=45 height=45></a>&nbsp;&nbsp;";	
	$("#titleDiv").html(title);
}

function handleClickCategory(categoryId) {
	activityId = categoryId;
	pointArray =  [];
	heatmap.setMap(null);
	getPoints();
}

function initMap() {
  map = new google.maps.Map(document.getElementById('map'), {
    zoom: 9,
    center: {lat: 45.2, lng: 5.80},
    mapTypeId: google.maps.MapTypeId.TERRAIN
  });
  
  getPoints();
  
}

function toggleHeatmap() {
  heatmap.setMap(heatmap.getMap() ? null : map);
}

function changeRadius() {
  radius += 5;
  if (radius > 30)
	  radius = 5;
  heatmap.set('radius', radius);
  $("#radiusDiv").html(radius);
}

function changeOpacity() {
  opacity += 0.2;
  if (opacity > 1)
	  opacity = 0.2;
  heatmap.set('opacity', opacity);
  $("#opacityDiv").html(Math.round(10*opacity) / 10);
}

function changeGradient() {
	gradientId++;
	if (gradientId > 2)
		gradientId = 0;
	
  heatmap.set('gradient', getGradientArray(gradientId));
  $("#gradientDiv").html(gradientId);
}

function getPoints() {
	console.log("-> getPoints(");
	console.log("/heatMapWS.php?user=" + user + "&activity=" + activityId + "&format=json");
	$("#loaderImg").attr("src","/images/ajaxLoader.gif");
 
	$.getJSON("/heatMapWS.php?user=" + user + "&activity=" + activityId + "&format=json" , function(data) {
		console.log("data = " + data);
		console.log("data.length = " + data.length);
		
		for (var i = 0; i < data.length; i++) {
        	//console.log("Lift = " + data[i]);
			pointArray.push(new google.maps.LatLng(data[i].lat, data[i].lon));
    	}
		console.log("PointArray = " + pointArray.length);
		console.log("PointArray = " + pointArray + " - " + pointArray.length);		
		
		heatmap = new google.maps.visualization.HeatmapLayer({
			data: pointArray,
			map: map, 
			radius: 20,
			opacity: 0.9,
			gradient: getGradientArray(gradientId)
		  });
		heatmap.set('dissipating', true);
		
		console.log("heatmap = " + heatmap.data);
		console.log("heatmap = " + heatmap.data.length);
		
		$("#loaderImg").attr("src","/images/transp.gif");
	});
	return pointArray;
}

</script>

<body onLoad="init();">


<?php include 'bodyHeader.php' ?>

<div id="tabDiv" class="title" style="height: 45px; position: relative; top: 45px; left:0px; width: 240px;">
	
	<table width=100% cellpadding=0 cellspacing=0 border=0>
		<tr>
		<td align=center><button onclick="changeRadius()">Radius</button><br><div id="radiusDiv">20</div></td><td></td>
		<td width=1><img src="images/transp.gif" height=16 width=1></td>
		<td align=center><button onclick="changeGradient()">Gradient</button><div id="gradientDiv">0</div></td>
		<td width=1><img src="images/transp.gif" height=16 width=1></td>
		<td align=center><button onclick="changeOpacity()">Opacity</button><div id="opacityDiv">0.8</div></td>
		</tr>
	</table>
	
</div>
<div id="titleDiv" class="title" style="height: 45px; position: absolute; top: 52px; right: 0px; left: 280px;"></div>
<div id="toolDiv" class="toolbar" style="width: 285px; height: 45px; position: absolute; top: 52px; right: 0px;">
	<table>
		<tr>
			<td width=32 rowspan=2><img src="/images/transp.gif" width=32 height=32 id="loaderImg"></td>
			<td>&nbsp;</td>
			<?php if ($_SESSION['user'] != null && $_SESSION['user'] != "") { ?><td>My tracks only</td><?php } ?>
			<td>&nbsp;&nbsp;&nbsp;</td>
			<td><a href=# onClick="getLocation()">Center Map</a></td>
		</tr>
		<tr>
			<td></td>
			<?php if ($_SESSION['user'] != null && $_SESSION['user'] != "") { ?><form id="showTracks"><td align=center><input type=checkbox name="mytracks" checked onClick="handleClickMyTracks(this.checked);"></td></form><?php } ?>
			<td></td>
			<td></td>
		</tr>
	</table>
</div>

<div id="maiwindow" class="mainwindow" style="position: absolute; top: 100px; left: 0px; bottom: 5px; right: 5px; ">

		<DIV id="mainWin" class="window" style="position: absolute; left: 0px; top: 0px; bottom: 5px; right: 5px; z-index : 3; overflow:hidden;">
		<div id="" class="windowtopbar" style="position: relative; top: 0px; left: 0px; height=15px; width: 100%">
		<table width=100% cellpadding=0 cellspacing=0 border=0><tr>
			<td class="windowtopbar" width=35%>Main</td>
			<td class="windowtopbar" width=1><img src="images/transp.gif" height=16 width=1></td>
			<td class="windowtopbar" width=65% align=right>
			<img src="images/help.gif" width=15 height=15 border=0 title="Main window" alt="Main window">
			</td></tr>
		</table>
		</div>
		<div id="viewerDiv" class="innerwindow" style="position: relative; top: 0px; left: 0px; bottom: 5px; right: 5px; overflow:scroll;">
		<table width=100% height=100% id="tracksTable" class="tracksTableIndex">
		  <tr>
			<td align=center>
			<div id="map" style="position: relative; top: 0px; width: 100%; height: 100%; overflow:hidden;"></div>
			</td>
		  </tr>
		</table>
	  </div>
	</div>


</div>
    
       

<?php include 'bodyFooter.php' ?>

</body>
</html>