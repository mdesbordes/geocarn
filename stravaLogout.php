<?php include 'header.php' ?>
<?php

function postRequest($url, $data) {
	logDebug("-> postRequest(".$url);

	// use key 'http' even if you send the request to https://...
	$options = array(
		'http' => array(
			'header'  => "Content-type: application/json\r\nAuthorization: Bearer " . $_SESSION['strava_access_token'] . "\r\n",
			'method'  => 'POST',
			'content' => ''
		)
	);
	$context  = stream_context_create($options);
	$result = file_get_contents($url, false, $context);
	return $result;
}

function getRequest($url) {
	logDebug("-> getRequest(".$url);
	$result = file_get_contents($url);
	return $result;
}

if (isset($_GET['code']))
	$code = $_GET['code'];
else
	$code = null;
if (isset($_GET['scope']))
	$scope = $_GET['scope'];
else
	$scope = null;
if (isset($_GET['error']))
	$error = $_GET['error'];
else
	$error = null;

logDebug("code = ".$code);
logDebug("scope = ".$scope);
logDebug("error = ".$error);

$url = "https://www.strava.com/oauth/deauthorize";
$data = array('test' => "");
$result = postRequest($url, $data);
	
if ($result === FALSE) { 
	logError("Error!");
	logDebug("result = ".$result);
}
else {
	logDebug("result = ".$result);
	
	$_SESSION['strava_access_token'] = null;
	$_SESSION['strava_athlete_id'] = null;	
}
?>

<body onLoad="init();">

<div id="maiwindow" class="maiwindow" style="position: relative; top: 5px; left: 0px; height: 100%">
<table width=100% height=100%>
	<tr height=100%>
		<td width=100%>
			<DIV id="mapWin" class="window" style="width: 100%; height: 100%; z-index : 3; overflow:hidden;">
				<div id="" class="windowtopbar" style="position: relative; top: 0px; left: 0px; height=15px; width: 100%">
				<table width=100% height=100% cellpadding=0 cellspacing=0 border=0><tr>
					<td class="windowtopbar" width=35%>Strava</td>
					<td class="windowtopbar" width=1><img src="images/transp.gif" height=16 width=1></td>
					<td class="windowtopbar" width=65% align=right>
					<a href=# onClick="document.getElementById('maiwindow').style.bottom='0px';document.getElementById('maiwindow').style.height='600px';"><img src="images/help.gif" width=15 height=15 border=0 title="Main window" alt="Main window"></a>
					</td></tr>
				</table>
				</div>
				<div id="weight" class="innerwindow" style="position: relative; top: 0px; width: 100%; height: 100%; overflow:hidden;"> 
					<?= $error ?>
					<!-- <?= $_SESSION['strava_access_token'] ?> -->
					<p>
					Logged out!
					<p>
					<a href=# onClick="window.opener.location.reload(false);window.close();" class="formbutton">Back to geocarn</a>

				</div>	
			</div>
		</td>
	</tr>
</table>


</div>

</body>
</html>
