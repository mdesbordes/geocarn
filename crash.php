<?php include 'inc/logger.php' ?>
<?php

session_start();

$_SESSION['user'] = "marco";

?>

<html lang="en">
<head>
  <META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <title>geocarn</title>

</head>

<link rel="shortcut icon" href="images/favicon.ico">

<link rel=stylesheet type=text/css href=common.css>
	  
	  
<script src=jquery-2.1.1.js LANGUAGE="JavaScript"></script>
<script src=common.js LANGUAGE="JavaScript"></script>
<script src=log.js LANGUAGE="JavaScript"></script>
<script src=ajax.js LANGUAGE="JavaScript"></script>
<script src=wind.js LANGUAGE="JavaScript"></script>
<script src=weather.js LANGUAGE="JavaScript"></script>
<script src=snow.js LANGUAGE="JavaScript"></script>

<script>
lang = "fr";

var markerArray = [];
var pointArray =  [];

var jsonData = null;

function init() {
	$("#titleDiv").html("");
	$("#subTitleDiv").html("");
	
	//setTitle();
	
	showList();
}

function showList() {
	$.getJSON("crashWS.php?limit=500" , function(data) {
		drawTable(data.locations);
	});
	document.getElementById("listTabDiv").className ="toplinkboxSel";
	document.getElementById("mapTabDiv").className ="toplinkbox";
	document.getElementById("agendaTabDiv").className ="toplinkbox";
}

function drawTable(data) {
	document.getElementById("viewerDiv").innerHTML="<table width=100% id='tracksTable' class='tracksTableIndex'></table>";
	drawHeader();
    for (var i = 0; i < data.length; i++) {
        drawRow(i, data[i]);
    }
	$("#tracksTable td").addClass("tracksTableIndex");
	$("#tracksTable > tbody > tr:odd").addClass("rowOdd");
    $("#tracksTable > tbody > tr:not(.odd)").addClass("rowEven"); 
}

function drawRow(rowId, rowData) {
	var row = $("<tr />")
    $("#tracksTable").append(row); //this will append tr element to table... keep its reference for a while since we will add cels into it
    row.append($("<td valign=top width=100>" + rowData.CRASH_DATE + "</td>"));
    row.append($("<td valign=top width=20>"  + rowData.ANDROID_VERSION + "</td>"));
	row.append($("<td valign=top width=20>"  + rowData.APP_VERSION_NAME + "</td>"));
	row.append($("<td valign=top width=20>" + rowData.BRAND + "</td>"));
	row.append($("<td valign=top width=20>" + rowData.PHONE_MODEL + "</td>"));
	row.append($("<td valign=top>" + rowData.STACK_TRACE + "</td>"));
	row.append($("<td valign=top>" + rowData.LOGCAT + "</td>"));
// CRASH_DATE, ANDROID_VERSION, APP_VERSION_NAME, BRAND, PHONE_MODEL, STACK_TRACE, LOGCAT 	
}

function drawHeader() {
	$("#tracksTable").html("");
	var header = $('<tr/>');	
	$("#tracksTable").append(header);
	header.append($("<td class='indextab'>CRASH_DATE</td>"));
	header.append($("<td class='indextab'>ANDROID</td>"));
	header.append($("<td class='indextab'>APP</td>"));
	header.append($("<td class='indextab'>BRAND</td>"));
	header.append($("<td class='indextab'>MODEL</td>"));
	header.append($("<td class='indextab'>STACK_TRACE</td>"));
	header.append($("<td class='indextab'>LOGCAT</td>"));
}

       
</script>

<body onLoad="init();">

<div id="maiwindow" class="mainwindow" style="position: absolute; top: 10px; left: 0px; bottom: -20px; right: -10px; ">

			<DIV id="mainWin" class="window" style="position: absolute; left: 0px; top: 0px; bottom: -30px; right: -20px; z-index : 3; overflow:hidden;">
			<div id="" class="windowtopbar" style="position: relative; top: 0px; left: 0px; height=15px; width: 100%">
			<table width=100% cellpadding=0 cellspacing=0 border=0><tr>
				<td class="windowtopbar" width=35%>Main</td>
				<td class="windowtopbar" width=1><img src="images/transp.gif" height=16 width=1></td>
				<td class="windowtopbar" width=65% align=right>
				<img src="images/help.gif" width=15 height=15 border=0 title="Main window" alt="Main window">
				</td></tr>
			</table>
			</div>
			<div id="viewerDiv" class="innerwindow" style="position: relative; top: 20px; width: 100%; height: 100%; overflow:scroll;"> 
			<table width=100% height=100% id="tracksTable" class="tracksTableIndex">
			  <tr> 
				<td align=center><img src='images/ajaxLoader.gif'> <p>
				  <p>&nbsp;</td>
			  </tr>
			</table>
		  </div>
		</div>
		

</div>

<div id="bottombar" class="bottombar">
&nbsp;
</div>

<?php include 'bodyFooter.php' ?>

</body>
</html>
