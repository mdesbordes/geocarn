<?php include 'inc/connectDb.php' ?>
<?php include 'inc/logger.php' ?>
<?php
function searchTracks($db, $user, $search, $categoryId) {
	logDebug('-> getLatestTracks(' . $user .", " . $search .", " . $categoryId);
	global $ACTIVITY;

	$request = "";
	$request = "SELECT TRACKER_TOUR.ACTIVITY_ID activityId, TRACKER_TOUR.COUNTRY country, TRACKER_TOUR.USER_ID user, TRACKER_TRACK.DATETIME trackDate, TRACKER_TRACK.ID trackId, TRACKER_TRACK.TOUR_ID tourId, TRACKER_TOUR.NAME tourName, TRACKER_TOUR.LATITUDE latitude, TRACKER_TOUR.LONGITUDE longitude, TRACKER_TRACK.START_LATITUDE startlatitude, TRACKER_TRACK.START_LONGITUDE startlongitude, TRACKER_TRACK.NAME trackName, TRACKER_TRACK.DURATION trackDuration, TRACKER_TRACK.DISTANCE trackDistance, TRACKER_TRACK.ALT_GAIN trackAltGain, CHAR_LENGTH(TRACKER_TRACK.TRACK) trackLength FROM TRACKER_TRACK join TRACKER_TOUR on  TRACKER_TRACK.TOUR_ID=TRACKER_TOUR.ID ";
	$request .= "WHERE (lower(TRACKER_TOUR.NAME) LIKE '%$search%') OR (lower(TRACKER_TRACK.NAME) LIKE '%$search%')";
	if ($user != null && $user != "") {
		$request .= " AND TRACKER_TOUR.USER_ID = '$user'";
		if ($categoryId != null && $categoryId > -1)
			$request .= " AND TRACKER_TOUR.ACTIVITY_ID = '$categoryId'";
	}
	else if ($categoryId != null && $categoryId > -1)
		$request .= " where TRACKER_TOUR.ACTIVITY_ID = '$categoryId'";
	$request .= " order by TRACKER_TRACK.DATETIME DESC LIMIT 30";
	logDebug($request);
	mysqli_set_charset($db, "SET NAMES UTF8");
	//$result = mysql_query($request,$db);
	$result = mysqli_query($db, $request);
	
	$rows = array();
	if (isset($result)) {
		logDebug("->result ok");
		while($r = mysqli_fetch_assoc($result)) {
			logDebug("- " . $r['tourId'] . " - " . $r['trackId'] . " - " . $r['trackName']);
			$rows['tracks'][] = $r;
		}
	}
	else {
		logDebug("->result ko");
		logError("Error: " . mysqli_error($db));
	}
	return json_encode($rows);
}

// --- Begin ---
logDebug("-> searchWS.php");

$user = $_SESSION['user'];

if (isset($_GET['search']))
	$search = strtolower($_GET['search']);
else
	$search = null;
$categoryId = null;

$tracksArray = searchTracks($db, $user, $search, $categoryId);

logDebug($tracksArray);

header('Content-type: text/xml;  application/json', true);
?><?= $tracksArray ?><?php exit; ?>