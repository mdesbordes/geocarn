<?php
	
function getLocationFromIpWS($ipaddress) {
	
	$location = "";
	
	$fileUrl = "http://api.hostip.info/?ip=$ipaddress&position=true";
	
	$retry = 0;
	error_reporting(0);
	$xmldata = FALSE;
	while ($xmldata == FALSE && $retry < 5) {
		$xmldata = file_get_contents($fileUrl);
		$retry++;
	}
	if ($xmldata != FALSE) {
		$parser = new XMLParser($xmldata);
		$parser->Parse();
		$parserDoc = $parser->document;
		
		$cityName = "";
		$countryName = "";
		$countryCode = null;
		$coordinates = "";
		
		if ($parser != null && $parser->document && $parser->document->tagChildren != null) {
			foreach($parser->document->tagChildren as $child) {
				$tag = $child->tagData;
				$tagNameStr = $child->tagName;
				//echo "<br>0 -&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;- $tagNameStr = $tag";
				
				if ($tagNameStr == 'gml:featuremember') {
					
					foreach($child->tagChildren as $subchild) {
						if(is_object($subchild)) {
							$suboutName = $subchild->tagName;
							$subout = $subchild->tagData;
							//echo "<br>1 -&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;- $suboutName = $subout";
							
							if ($suboutName == "hostip") {
								foreach($subchild->tagChildren as $subchild2) {
									$suboutName = $subchild2->tagName;
									$subout = $subchild2->tagData;
									//echo "<br>2 -&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;- $suboutName = $subout";
									if ($suboutName == "gml:name") {
										$cityName = $subout;
									}
									else if ($suboutName == "countryname") {
										$countryName = $subout;
									}							
									else if ($suboutName == "countryabbrev") {
										$countryCode = $subout;
									}
									else if ($suboutName == "iplocation") {
										
										foreach($subchild2->tagChildren as $subchild3) {
											$suboutName = $subchild3->tagName;
											$subout = $subchild3->tagData;
											
											//echo "<br>3 -&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;- $suboutName = $subout";
											if ($suboutName == 'gml:pointproperty') {
												foreach($subchild3->tagChildren as $subchild4) {
													$suboutName = $subchild4->tagName;
													$subout = $subchild4->tagData;
													//echo "<br>4 -&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;- $suboutName = $subout";
													if ($suboutName == 'gml:point') {
														foreach($subchild4->tagChildren as $subchild5) {
															$suboutName = $subchild5->tagName;
															$subout = $subchild5->tagData;
															//echo "<br>5 -&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;- $suboutName = $subout";
															if ($suboutName == 'gml:coordinates') {
																$coordinates = $subout;
															}
														}
													}
												}
											}
										}	
										
									}
								}	
							
							}
							
						}
					}
				}
			}
		}
		
		//echo "<p>" . $cityName . "<br>" . $countryCode . "<br>" . $countryName . "<br>" . $coordinates;
		
		if ($cityName == '(Unknown city)')
			$cityName = "*";
		return $cityName . "|" . $countryCode . "|" . $countryName;
	}
	else {
		return "";
	}
}
	
// NOT USED	
function getLocationFromIp($ipaddress) {
	
	$location = "";
	
	$fileUrl = "http://www.db.ripe.net/whois?form_type=simple&full_query_string=&searchtext=$ipaddress&do_search=Search";
	
	$txtdata = file_get_contents($fileUrl);
	
	//echo $txtdata;
	
	$pieces = explode("\n", $txtdata);
	
	//echo count($pieces);
	for ($i = 0; $i < count($pieces); $i++) {
		$pos = strpos($pieces[$i], "address");
		if ($pos === false) {
			//echo " ";
		}
		else {
			$location .= substr($pieces[$i], 8);
			//echo "-> ";
		}
		//echo "" . $pos . " - " . ($pos == true) . " - " . ($pos == false) . " - " . ($pos == "") . " - " . ($pos == "0") . " - " . ($pos == 0) . "<br>";
		
	}
	
	return $location;
}

$action = "";
if (isset($_GET['action']))
	$action = $_GET['action'];
$ip = $_SERVER['REMOTE_ADDR'];
if ($action != "")
	$location = getLocationFromIpWS($ip);

//echo $location;
?>